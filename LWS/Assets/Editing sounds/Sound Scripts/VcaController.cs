using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VcaController : MonoBehaviour
{
    private FMOD.Studio.VCA Vca;
    public string VcaName;

    private Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        Vca = FMODUnity.RuntimeManager.GetVCA("vca:/" + VcaName);
        slider = GetComponent<Slider>();
    }
    
    public void SetVolume(float volume)
    {
        Vca.setVolume(volume);
    }
}
