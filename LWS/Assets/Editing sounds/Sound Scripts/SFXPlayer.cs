using FMODUnity;
using FMOD.Studio;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
    private EventInstance holdSFX;
    private PLAYBACK_STATE pb;

    [EventRef] public string sfx;
    public KeyCode key;
    public bool startOnAwake;
    public bool On_Off;
    public int numOfInstances = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (startOnAwake)
            playEvent(sfx);

        if (On_Off)
        {
            holdSFX = RuntimeManager.CreateInstance(sfx);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key) && !startOnAwake && !On_Off)
            playEvent(sfx);

        if (Input.GetKeyDown(key) && !startOnAwake && On_Off)
            playHeldEvent();
    }

    void playEvent(string sfx)
    {
        for (int i = 0; i < numOfInstances; i++)
            RuntimeManager.PlayOneShotAttached(sfx, gameObject);
    }

    void playHeldEvent()
    {
        holdSFX.getPlaybackState(out pb);
        if (pb != PLAYBACK_STATE.PLAYING)
            holdSFX.start();
        else
            holdSFX.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}