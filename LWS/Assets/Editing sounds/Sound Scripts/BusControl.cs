using FMODUnity;
using FMOD.Studio;
using UnityEngine;

public class BusControl : MonoBehaviour
{
    Bus bus;
    public string busPath;
    [Range(0f, 10f)] public float volume = 7f;

    // Start is called before the first frame update
    void Start()
    {
        bus = RuntimeManager.GetBus("bus:/Master/" + busPath);
    }

    // Update is called once per frame
    void Update()
    {
        bus.setVolume(volume / 10f);
    }
}