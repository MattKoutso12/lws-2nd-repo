using UnityEngine;
using System.Collections;


public class PauseGame : MonoBehaviour
{
    public Transform canvas;





    // Update is called once per frame
    void Update()
    {
             
    }
    public void Pause()
    {
            if (canvas.gameObject.activeInHierarchy == false)
            {
                canvas.gameObject.SetActive(true);
            }
            else
            {
                canvas.gameObject.SetActive(false);
            }
        
    }
}
