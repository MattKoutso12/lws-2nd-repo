using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    
    private GameObject objectToSpawn; // what spawns
    public Transform parent; // where ^^ spawns

    private void Awake()
    {
        //finds spirit orb template
        objectToSpawn = transform.Find("Spirit Orb Template").gameObject;

        //places copy of objectToSpawn (rn just spirit orb template), at this objects position and rotation, under the parent object
        Instantiate(objectToSpawn, transform.position, transform.rotation, parent);
    }

    public void SpawnObject()
    {
        int spawnScene = PersistantManager.Instance.hireScene;
        if(spawnScene >= 1 && spawnScene <= 5)
        {
            if((PersistantManager.Instance.crews[spawnScene].Count - PersistantManager.Instance.hiredCrews[spawnScene].Count) > 0 && GameObject.Find("Spirit Orb Template(Clone)") == null)
            {
                Instantiate(objectToSpawn, transform.position, transform.rotation, parent);
            }
            else
            {
                Debug.Log("All potential familiars spawned.");
            }
            //places copy of objectToSpawn (rn just spirit orb template), at this objects position and rotation, under the parent object, if there are still familiars that haven't been hired
        }
    }
}
