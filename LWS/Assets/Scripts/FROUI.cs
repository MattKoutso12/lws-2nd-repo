using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FROUI : MonoBehaviour, Savable
{

    public static FROUI Instance;


    /// <summary>
    /// The maximum amount of orders available to the player at the start of a day
    /// </summary>
    const int MAX_ROUI_ORDERS = 5;

    /// <summary>
    /// The minimum amount of orders available to the player at the start of the day
    /// </summary>
    const int MIN_ROUI_ORDERS = 3;

    public bool onFOUI = false; // if true, we're in the fulfillment screen.

    bool checkOpenedFROUI = false;
    bool checkAcceptedOrder = false;

    public Task claudTest;


    [SerializeField]
    public bool toggled_on = false;

    [SerializeField]
    public List<Task> tasks;

    [SerializeField]
    public List<OrderWrapper> orders_taken = new List<OrderWrapper>(); // the orders we have accepted

    [SerializeField]
    public GameObject order_card_prefab, fulfill_card_prefab;

    [SerializeField]
    public Transform order_card_list, fulfill_card_list;

    [SerializeField]
    public GameObject ROUI, FOUI;

    public List<OrderCard> available_orders_today = new List<OrderCard>(); // the orders in the ROUI

    [SerializeField]
    public UnityEvent OnOrderAccepted;

    public delegate void OrderEvent(object sender, System.EventArgs e);
    public static event OrderEvent OnAcceptOrders;

    private HashSet<Task> foundTasks;

    private void Awake()
    {
        foundTasks = new HashSet<Task>();
        SaveManager.OnPreSave += AddToSavables;
        toggled_on = transform.GetChild(0).gameObject.activeInHierarchy;
        if (Instance == null) Instance = this;
        if(order_card_prefab == null)
        {
            Debug.LogError("NO ORDER CARD PREFAB ATTACHED TO FROUI", this.gameObject);
        }

        if(order_card_list == null)
        {
            Debug.LogError("NO PLACE TO PUT ORDER CARDS WAS DESIGNATED!", this.gameObject);
        }
    }

    private void Start()
    {
        DialougeCommands.OnTutoStarted += Instance_OnTutoStarted;
        PersistantManager.OnDayChanged += Instance_OnDayChanged;
        PersistantManager.Instance.OnShopLevelUp += Instance_OnShopLevelUp;
    }

    private void Instance_OnShopLevelUp(object sender, System.EventArgs e)
    {
        bool hasNewOrder = false;
        foreach(Task order in tasks)
        {
            if(order.give_on_shop_Level == PersistantManager.Instance.shopLevel && !foundTasks.Contains(order))
            {
                hasNewOrder = true;
                foundTasks.Add(order);
            }
        }
        if (hasNewOrder)
        {
            Debug.Log("Dyu: Added new order letter");
            LetterStack.Instance.letterStack.Add("New Orders");
        }
    }

    private void Instance_OnTutoStarted(object sender, System.EventArgs e)
    {
        //Destroy orders
        foreach (OrderCard card in order_card_list.GetComponentsInChildren<OrderCard>())
        {
            available_orders_today.Remove(card);
            Destroy(card.gameObject);
            continue;
        }
        //Rengenerate orders
        FetchDailyAvailableOrders();
    }

    /// <summary>
    /// Every new day, we decrement the remaining days for order we've taken and generate
    /// new orders for the player to take.
    /// </summary>
    private void Instance_OnDayChanged(object sender, System.EventArgs e)
    {
        
        FetchDailyAvailableOrders(); // generate the day's orders
    }

    private void OnDestroy()
    {
        DialougeCommands.OnTutoStarted -= Instance_OnTutoStarted;
        PersistantManager.OnDayChanged -= Instance_OnDayChanged;
        SaveManager.OnPreSave -= AddToSavables;
    }

    /// <summary>
    /// For now, it works like this: orders can carry over from the previous day (we can make them expire in the future).
    /// If we have less than <see cref="MIN_ROUI_ORDERS"/> orders, add enough orders to reach that number (e.g. if we have 1 order the player didn't accept, add 2 more orders the next day).
    /// If we have greater than or equal to <see cref="MIN_ROUI_ORDERS"/>, we still will offer the player a new order today, but only up to <see cref="MAX_ROUI_ORDERS"/>.
    /// 
    /// This gives a better feeling of orders coming in from an outside source, and allows us to create the extra element of orders that are available for more or less time.
    /// 
    /// In the future, I think we can play around with this as an extra dimension of gameplay. Emergency orders, for instance, could only be available for one day- so the player has to really
    /// think about whether or not they want to take it.
    /// 
    /// This system is also going to help a lot with hunting, since now the player can wait and see if their hunting party returns with a necessary ingredient before they take an order.
    /// </summary>
    public void FetchDailyAvailableOrders()
    {
        int orders_still_available = 0;
        foreach (OrderCard card in order_card_list.GetComponentsInChildren<OrderCard>())
        {
            if (card.ripped)
            {
                available_orders_today.Remove(card);
                Destroy(card.gameObject);
                continue;
            }
            

            orders_still_available++; // count up all the orders today and get rid of the ripped ones we accepted yesterday
        }
        if (TutorialRunner.Instance.tutorialActive) //adding this to guarentee the claudia's test order is the only order while the tutorial is active
        {
            if (orders_still_available < 1)
            {
                for (int i = 0; i < 1 - orders_still_available; i++)
                {
                    GenerateNewOrder();
                }
            }
            else if (orders_still_available > 1)
            {
                GenerateNewOrder();
            }
        }
        else
        {
            if (orders_still_available < MIN_ROUI_ORDERS)
            {
                for (int i = 0; i < MIN_ROUI_ORDERS - orders_still_available; i++)
                {
                    GenerateNewOrder();
                }
            }
            else if (orders_still_available < MAX_ROUI_ORDERS)
            {
                GenerateNewOrder(); // just add one if we're over 3 available.
            }
        }
        RemoveClosedOrders();
    }

    private void GenerateNewOrder()
    {
        List<Task> availableTasks = new List<Task>();
        if (TutorialRunner.Instance.tutorialActive) //Guarenteeing Claudia's Test is the only task that gets made
        {
            Debug.Log("Des: Making clauds orders");
            availableTasks.Add(claudTest);
        }
        else
        {
            Debug.Log("Des: Making orders");
            foreach (Task t in tasks)
            {
                if (!TutorialRunner.Instance.tutorialActive && t.give_on_shop_Level <= PersistantManager.Instance.shopLevel)
                {
                    availableTasks.Add(t);
                }
            }
        }
        if (availableTasks.Count != 0)
        {
            int rand = Random.Range(0, availableTasks.Count);
            GameObject new_order = Instantiate(order_card_prefab, order_card_list);
            OrderCard card = new_order.GetComponent<OrderCard>();            
            card.Init(availableTasks[rand]); // give it a random task
            available_orders_today.Add(card);
        }
    }

    void RemoveClosedOrders()
    {
        foreach(FulfillCard card in fulfill_card_list.GetComponentsInChildren<FulfillCard>())
        {
            if(card.order.daysRemaining <= 0)
            {
                orders_taken.Remove(card.order);
                Destroy(card.gameObject, 1f);
            }
        }
    }

    public void SetFOUIVisible(bool visible)
    {
        // enable one, disable the other.
        onFOUI = visible;
        if(onFOUI)
        {
            ROUI.SetActive(false);
            FOUI.SetActive(true);
        }
        else
        {
            ROUI.SetActive(true);
            FOUI.SetActive(false);
        }
    }

    public void Toggle()
    {
        if(!checkOpenedFROUI && TutorialRunner.Instance.tutorialActive)
        {
            checkOpenedFROUI = true;
            TutorialRunner.Instance.OrderCheck("openOrders");
        }

        toggled_on = !toggled_on;
        this.transform.GetChild(0).gameObject.SetActive(toggled_on);

        if (toggled_on)
        {

            Player.Instance.SetMoveLock(true);

            
        }
        else
        {
            Player.Instance.SetMoveLock(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void AcceptOrder(OrderWrapper order)
    {
        orders_taken.Add(order); // add the order
        GameObject new_order = Instantiate(fulfill_card_prefab, fulfill_card_list);
        FulfillCard card = new_order.GetComponent<FulfillCard>();
        card.Init(order); // give it the accepted task
        OnOrderAccepted?.Invoke();

        if(!checkAcceptedOrder && TutorialRunner.Instance.tutorialActive)
        {
            checkAcceptedOrder = true;
            TutorialRunner.Instance.OrderCheck("selectOrders");
        }
    }

    public void AcceptCheckedOrders()
    {
        foreach(OrderCard card in available_orders_today)
        {
            if(card.isSelected && !card.ripped)
            {
                AcceptOrder(card.order);
                card.TakeOrder();
            }
        }

    }

    #region Savable Implementations
    public void Save(PlayerData data)
    {
        data.availableOrders = new List<OrderWrapper>();
        data.taken_orders = new List<OrderWrapper>();
        // save available orders
        foreach(OrderCard card in available_orders_today)
        {
            data.availableOrders.Add(card.order); // save these orders!
        }

        // save taken orders
        foreach(OrderWrapper order in orders_taken)
        {
            data.taken_orders.Add(order); // easy peasy
        }
    }

    public void LoadFrom(PlayerData data)
    {
        // load available orders
        foreach (OrderWrapper order in data.availableOrders)
        {
            GameObject new_order = Instantiate(order_card_prefab, order_card_list);
            OrderCard card = new_order.GetComponent<OrderCard>();
            card.Init(order); // should work
            available_orders_today.Add(card);
        }

        // load taken orders
        foreach(OrderWrapper order in data.taken_orders)
        {
            GameObject joy_division = Instantiate(fulfill_card_prefab, fulfill_card_list); // get it? joy division? see, because, uh, y'know, like, new order? like, y'know, uhhh, it's funny, right? where's everybody going?
            FulfillCard card = joy_division.GetComponent<FulfillCard>();
            card.Init(order); // initialize our fulfill card with the right order.
            orders_taken.Add(order);
        }
    }

    public void AddToSavables(SaveManager manager)
    {
        manager.saveables.Add(this);
    }

    public void RemoveFromSavables()
    {
        SaveManager.Instance.saveables.Remove(this);
    }
    #endregion
}
