using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


/// <summary>
/// Like an item slot, but only displays a preview of an item. Used for hunting rewards.
/// </summary>

[RequireComponent(typeof(Image))]
public class ItemBadge : MonoBehaviour
{
    // TODO: Make this tooltippable
    protected Image display;
    protected TextMeshProUGUI amount_text;

    public bool showYieldInstead;

    private void Awake()
    {
        display = GetComponent<Image>();
        amount_text = GetComponentInChildren<TextMeshProUGUI>();
    }

    /// <summary>
    /// Displays an item stack on the badge
    /// </summary>
    /// <param name="i">The item stack to display</param>
    public virtual void SetItem(InventoryItem i)
    {
        if(i == null)
        {
            display.color = Color.clear;
            amount_text.text = "";
            return;
        }
        else
        {
            display.color = Color.white;
            display.sprite = i.GetSprite();
        }
        amount_text.text = "" + i.amount;
        if(showYieldInstead)
        {
            amount_text.text = "" + i.itemType.outputYield;
        }
    }

    public virtual void SetColor(Color color)
    {
        display.color = color;
    }
}
