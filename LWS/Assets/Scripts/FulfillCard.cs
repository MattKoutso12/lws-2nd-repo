using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FulfillCard : MonoBehaviour
{

    [SerializeField]
    public TextMeshProUGUI info_text, days_left;

    [SerializeField]
    public FulfillSlot slot;

    [SerializeField]
    public TextMeshProUGUI fulfillText;

    public OrderWrapper order;

    InventoryItem[] added;

    List<Item> done = new List<Item>();

    public bool closed = false;
    public void Init(OrderWrapper order)
    {
        this.order = order;

        // set up an array with the right item types, but none of them fulfilled.
        added = new InventoryItem[order.products.Count];
        for(int i = 0; i < order.products.Count; i++)
        {
            added[i] = new InventoryItem(order.products[i].itemType, 0);
        }
        UpdateText();
    }

    public void AddItem(InventoryItem item)
    {
        // ensure we keep everything in one stack. If there's no existing stack, then it's not required and thus shouldn't be put in the fulfill slot.
        foreach(InventoryItem i in added)
        {
            if(i.itemType.Equals(item.itemType))
            {
                i.amount += item.amount;
                if (CheckFulfillment()) StartCoroutine(CloseContract()); // see our status so far
                UpdateText();
                return;
            }
        }
    }

    private void OnGUI()
    {
        days_left.text = $"{order.daysRemaining}  DAYS LEFT";
        if (order.daysRemaining == 1)
            days_left.text = "<color=red>LAST DAY</color>";
    }

    bool CheckFulfillment()
    {
        for(int i = 0; i < order.products.Count; i++)
        {
            bool item_fulfilled = false;
            if (added[i].amount >= order.products[i].amount)
            {
                item_fulfilled = true;
                done.Add(added[i].itemType); // mark it as done.
            }


            if (!item_fulfilled) return false; // if we're missing even one requirement
        }

        return true; // if we had everything we need
    }

    IEnumerator CloseContract()
    {
        FROUI.Instance.orders_taken.Remove(order);
        PersistantManager.Instance.AddGold(order.reward);
        fulfillText.text = "+$" + (order.reward);
        fulfillText.gameObject.GetComponent<Animator>().Play("Sold");
        CouncilReputation.Instance.OrderScore();
        CouncilReputation.Instance.ClaudiaMiscTest(order);
        if(TutorialRunner.Instance.tutorialActive)
        {
            TutorialRunner.Instance.OrderCheck("Submit Order");
        }
        yield return new WaitForSeconds((float) 1.75);
        Destroy(this.gameObject);
    }

    void UpdateText()
    {
        string info = order.ColouredName + "\n\n";

        info += "<color=#bf9000>" + order.reward.ToString() + " Gold</color>\n";
        for (int i = 0; i < order.products.Count; i++)
        {
            string line = $"{order.products[i].itemType.getDisplayName()} {added[i].amount}/{order.products[i].amount}\n";
            if (done.Contains(added[i].itemType)) line = "<color=green>" + line + "</color>";
            info += line;
        }
        info_text.text = info;
    }

    public bool AskForAdd(InventoryItem item, out int amount_needed)
    {
        amount_needed = 0;
        if (closed) return false;

        if (done.Contains(item.itemType)) return false;

        for(int i = 0; i < order.products.Count; i++)
        {
            if(item.itemType.Equals(order.products[i].itemType)) // same type of item as required...
            {
                amount_needed = order.products[i].amount - added[i].amount; // send back the number of items required to finish off the order, which we'll use as a limit.
                return true;
            }
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
