using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FulfillSlot : ItemSlot
{

    [SerializeField]
    public FulfillCard card;

    [SerializeField]
    public UnityEvent OnFulfilled;

    protected override bool CanHoldThis(IDraggableObject objectToHold)
    {
        if(objectToHold is InventoryItem)
        {
            InventoryItem item = (InventoryItem)objectToHold;
            int newlimit;
            if (card.AskForAdd(item, out newlimit))
            {
                this.limit = newlimit; // establish the amount needed as the limit, so the player doesn't burn items
                return true;
            }
        }
        return false;
    }

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);

        card.AddItem((InventoryItem)GetHeldObject());
        this.holding = null; // schlorp the item into the fulfillment slot
        OnFulfilled?.Invoke();
    }
}
