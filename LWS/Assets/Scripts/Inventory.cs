using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player's inventory of items. Contains methods for manipulating what is in the inventory. 
/// </summary>
public class Inventory : IItemHolder
{

    public event EventHandler OnItemListChanged;

    private Action<InventoryItem> useItemAction;
    public ItemSlot [] inventorySlotArray;



    #region Inventory comment
    /// <summary>
    /// Constructor method for Inventory.
    /// </summary>
    /// <param name="useItemAction"> Item's use action. </param>
    /// <param name="inventorySlotCount"> Size of inventory. </param>
    #endregion
    public Inventory(Action<InventoryItem> useItemAction, int inventorySlotCount)
    {
        this.useItemAction = useItemAction;

        inventorySlotArray = new ItemSlot[inventorySlotCount];
        for (int i = 0; i < inventorySlotCount; i++)
        {
            inventorySlotArray[i] = new ItemSlot();
        }
        AddItem(new InventoryItem("water", 15));
        AddItem(new InventoryItem("moonleaf_seeds", 3));
        AddItem(new InventoryItem("miskweed_seeds", 1));
        AddItem(new InventoryItem("poppy_seeds", 1));

        
    }

    private Inventory(ItemSlot [] slots)
    {
        this.inventorySlotArray = slots;
        foreach (ItemSlot i in inventorySlotArray)
        {
            i.connected = this;

        }
    }

    public static Inventory MakeFromChildren(GameObject parent)
    {
        ItemSlot [] slots = parent.GetComponentsInChildren<ItemSlot>();
        foreach(ItemSlot slot in slots)
        {
            slot.SetID((uint)slot.transform.GetSiblingIndex() + 1); // +1 because it shouldn't ever be 0.     
        }
        return new Inventory(slots);
    }

    public ItemSlot GetEmptyInventorySlot()
    {
        foreach (ItemSlot inventorySlot in inventorySlotArray)
        {
            if (inventorySlot.IsEmpty())
            {
                return inventorySlot;
            }
        }
        //Debug.LogError("Cannot find an empty InventorySlot!");
        return null;
    }

    public ItemSlot GetInventorySlotWithItem(InventoryItem item)
    {
        foreach (ItemSlot inventorySlot in inventorySlotArray)
        {
            if (!inventorySlot.IsEmpty() && ((InventoryItem) inventorySlot.GetHeldObject()).itemType == item.itemType)
            {
                return inventorySlot;
            }
        }
        return null;
    }

    #region AddItem comment
    /// <summary>
    /// Adds an item to the inventory.
    /// </summary>
    /// <param name="item"> Item we want to add. </param>
    #endregion
    public void AddItem(InventoryItem item)
    {
        ItemSlot possibleStackSlot = GetInventorySlotWithItem(item);
        Debug.Log("ADDING " + item.itemType.getIDName());
        if (possibleStackSlot != null) Debug.Log("POSSIBLE STACK SLOT....");
        if(item.itemType.stackable && possibleStackSlot != null && possibleStackSlot.TryAddObject(item)) // if there's a place to stack this item, put it in
        {
            Debug.Log("Successfully added to existing stack");
            return;
        }
        else if (GetEmptyInventorySlot() != null && GetEmptyInventorySlot().TryAddObject(item)) // otherwise, if there's an empty slot, put it there.
        {
            Debug.Log("Successfully added to empty slot");
            item.SetItemHolder(this);
            OnItemListChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    #region  RemoveItem comment
    /// <summary>
    /// Removes item from inventory. 
    /// </summary>
    /// <param name="item"></param>
    #endregion
    public void RemoveItem(InventoryItem item)
    {

        ItemSlot slot = GetInventorySlotWithItem(item);
        if(slot != null)
        {
            slot.SetHolding(null);
            OnItemListChanged?.Invoke(this, EventArgs.Empty);
            //return true;
        }
        //return false;
    }

    public void AddItem(InventoryItem item, ItemSlot inventorySlot)
    {
        // Add Item to a specific Inventory slot
        item.SetItemHolder(this);
        inventorySlot.SetHolding(item);

        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    #region UseItem comment
    /// <summary>
    /// Uses designated item.
    /// </summary>
    /// <param name="item"> Item we want to use. </param>
    #endregion
    public void UseItem(InventoryItem item)
    {
        useItemAction(item);
    }

    #region  GetItemList comment
    /// <summary>
    /// Returns list of items in inventory.
    /// </summary>
    /// <returns> List of items in inventory. </returns>
    #endregion
    public InventoryItem [] GetItemList()
    {
        InventoryItem[] items = new InventoryItem[inventorySlotArray.Length];
        int i = 0;
        foreach(ItemSlot slot in inventorySlotArray)
        {
            if (slot.GetHeldObject() == null) continue;
            InventoryItem item = (InventoryItem)slot.GetHeldObject();
            items[i] = item;
            i++; // only increment when we have non-empty slots. So, at the end we can trim up the array :)
        }
        InventoryItem[] trimmed = new InventoryItem[i]; // you'd think this would be i+1, but since we increment i at the end of each loop it is not!
        Array.Copy(items, trimmed, trimmed.Length); // eliminate empty spaces
        return trimmed;
    }

    public ItemSlot[] GetInventorySlotArray()
    {
        return inventorySlotArray;
    }

    #region CanAddItem comment
    /// <summary>
    /// Checks whether there are empty slots in inventory.
    /// </summary>
    /// <returns> Whether there are empty slots. </returns>
    #endregion
    public bool CanAddItem()
    {
        return GetEmptyInventorySlot() != null;
    }

}