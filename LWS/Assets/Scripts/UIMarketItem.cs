using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UIMarketItem : Button
{

    [SerializeField]
    public string item_id = "moonleaf";

    [SerializeField]
    public TextMeshProUGUI item_text;

    [SerializeField]
    public Image item_icon;


    Item linked_item;

    Image background;

    [SerializeField]
    public MarketGraph graph;

    public void ItemInit()
    {
        linked_item = Item.GetItem(item_id);
    }

    protected override void Start()
    {
        base.Start();
        linked_item = Item.GetItem(item_id);
        background = GetComponent<Image>();
    }

    

    private void OnGUI()
    {
        if(linked_item != null)
        {
            item_icon.sprite = linked_item.sprite;
            item_text.text = linked_item.getDisplayName();
        }

        
    }

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        //background.maskable = false;
        if (linked_item == null) Debug.LogError("ERROR: No linked item on selection!");
        graph.SetSelected(linked_item);
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        //background.maskable = true;
    }
}
