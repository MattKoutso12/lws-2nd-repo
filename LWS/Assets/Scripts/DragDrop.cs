using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

/// <summary>
/// Handles dragging and dropping of items.
/// </summary>
public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private Image image;
    public InventoryItem item;
    private TextMeshProUGUI amountText;

    
    public void SetItemButton()
    {

        ItemButton itemButton = GetComponent<ItemButton>();
        Item temp = item.itemType;
        itemButton.item = temp;

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = .5f;
        canvasGroup.blocksRaycasts = false;
        UI_ItemDrag.Instance.Show(item);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        UI_ItemDrag.Instance.Hide();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            // Right click, split
            if (item != null)
            {
                // Has item
                if (item.IsStackable())
                {
                    // Is Stackable
                    if (item.amount > 1)
                    {
                        // More than 1
                        if (item.GetItemHolder().CanAddItem())
                        {
                            // Can split
                            int splitAmount = Mathf.FloorToInt(item.amount / 2f);
                            item.amount -= splitAmount;
                            InventoryItem duplicateItem = new InventoryItem(item.itemType, splitAmount);
                            item.GetItemHolder().AddItem(duplicateItem);
                        }
                    }
                }
            }
        }
    }

    #region SetSprite comment
    /// <summary>
    /// Sets this sprite to new sprite.
    /// </summary>
    /// <param name="newSprite"> Sprite we wish to change this to. </param>
    #endregion
    public void SetSprite(Sprite newSprite)
    {
        image.sprite = newSprite;
    }

    #region  SetAmountText comment
    /// <summary>
    /// Sets amount text to reflect the provided int.
    /// </summary>
    /// <param name="amount"> Int we want to set the amount text to. </param>
    #endregion
    public void SetAmountText(int amount)
    {
        if (amount <= 1)
        {
            amountText.text = "";
        }
        else
        {
            // More than 1
            amountText.text = amount.ToString();
        }
    }

    /// <summary>
    /// Disables DragDrop.
    /// </summary>
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Enables DragDrop
    /// </summary>
    public void Show()
    {
        gameObject.SetActive(true);
    }

    #region SetItem comment
    /// <summary>
    /// Sets the dragged item to the designated item.
    /// </summary>
    /// <param name="item"> Item we are dragging. </param>
    #endregion
    public void SetItem(InventoryItem item)
    {
        //set components before setting item
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
        image = transform.Find("Image").GetComponent<Image>();
        amountText = transform.Find("Amount Text").GetComponent<TextMeshProUGUI>();
        //set item itself
        this.item = item;
        SetSprite(item.GetSprite());
        SetAmountText(item.amount);
        SetItemButton();
    }

}