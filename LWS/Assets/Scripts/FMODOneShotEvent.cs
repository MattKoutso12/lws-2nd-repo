using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODOneShotEvent : MonoBehaviour
{
    [SerializeField]
    public string eventPath;


    [SerializeField]
    [Tooltip("If false, will play the sound globally. If true, it will play from the position of this object.")]
    public bool playFromThisObject = false;

    public void Play()
    {
        if (!playFromThisObject)
            FMODUnity.RuntimeManager.PlayOneShot(eventPath);
        else
            FMODUnity.RuntimeManager.PlayOneShotAttached(eventPath, this.gameObject);
    }
}
