using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

[RequireComponent(typeof(StudioEventEmitter))]
public class ChangingSongEmitter : MonoBehaviour
{

    [Tooltip("Put a list of your cycling event names here, in the order that you want them played. It will cycle to the next song each day, and loop.")]
    [FMODUnity.EventRef]
    public List<string> songCycle;

    [Tooltip("The song index we start on")]
    public int songIndex;

    FMOD.Studio.EventInstance []  songs;

    StudioEventEmitter emitter;

    private void Awake()
    {
        emitter = GetComponent<StudioEventEmitter>();
    }


    

    // Start is called before the first frame update
    void Start()
    {
        PersistantManager.OnDayChanged += PersistantManager_OnDayChanged;
        songs = new FMOD.Studio.EventInstance[songCycle.Count];
        for(int i = 0; i < songCycle.Count; i++)
        {
            songs[i] = FMODUnity.RuntimeManager.CreateInstance(songCycle[i]);
        }
    }

    private void OnDestroy()
    {
        PersistantManager.OnDayChanged -= PersistantManager_OnDayChanged;
    }

    private void PersistantManager_OnDayChanged(object sender, System.EventArgs e)
    {
        songs[songIndex].stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        songIndex++;
        if (songIndex >= songCycle.Count) songIndex = 0;
        songs[songIndex].start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
