using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionsAndParcels : MonoBehaviour
{
    public static PotionsAndParcels Instance;

    [SerializeField] GameObject shopSlotPrefab;

    private HashSet<Item> spawnedItems;
    private HashSet<Item> foundItems;

    private List<InventoryItem> forcedStacks = new List<InventoryItem>(); // stacks we override and put into the shop, like the 5x and 10x water
    private bool addedForcedStacks = false;

    void Awake()
    {
        spawnedItems = new HashSet<Item>();
    }

    // Start is called before the first frame update
    void Start()
    {
        forcedStacks.Clear(); // clean out the list, juuuust in case
        forcedStacks.Add(new InventoryItem(
            Item._items["water"],
            5
            ));

        forcedStacks.Add(new InventoryItem(
            Item._items["water"],
            10
            ));

        addedForcedStacks = false;
        UpdateInventory();
        PersistantManager.Instance.OnShopLevelUp += Instance_OnShopLevelUp;
    }

    private void Instance_OnShopLevelUp(object sender, System.EventArgs e)
    {
        bool hasNewOrder = false;
        foreach (Item item in Item._items.Values)
        {
            if(!(item is CraftedItem) && item.shop_level <= PersistantManager.Instance.shopLevel && !foundItems.Contains(item) && item.isSeed)
            {
                hasNewOrder = true;
                foundItems.Add(item);
            }
        }
        if (hasNewOrder)
        {
            Debug.Log("Dyu: Added new seed letter");
            LetterStack.Instance.letterStack.Add("New Seeds");
        }
    }

    private void UpdateInventory()
    {
        foreach(Item item in Item._items.Values)
        {
            if(!(item is CraftedItem) && item.shop_level <= PersistantManager.Instance.shopLevel && !spawnedItems.Contains(item) && item.isSeed)
            {
                try
                {
                    GameObject newShop = Instantiate(shopSlotPrefab, transform);
                    newShop.transform.SetSiblingIndex(0);
                    ShopSlot slot = newShop.GetComponent<ShopSlot>();
                    slot.SetHolding(new InventoryItem(item, 1));
                    slot.startsWith = item;
                    slot.starting_amount = 1;
                    spawnedItems.Add(item);
                }
                catch (System.Exception e)
                {
                    continue;
                }
            }
        }

        if (!addedForcedStacks)
        {
            foreach (InventoryItem forced in forcedStacks)
            {
                try
                {
                    GameObject newShop = Instantiate(shopSlotPrefab, transform);
                    newShop.transform.SetSiblingIndex(0);
                    ShopSlot slot = newShop.GetComponent<ShopSlot>();
                    slot.limit = forced.amount;
                    slot.SetHolding(forced);
                    slot.startsWith = forced.itemType;
                    slot.starting_amount = forced.amount;
                }
                catch (System.Exception e)
                {
                    continue; // just blow past errors and don't spawn them.
                }
            }
            addedForcedStacks = true;
        }
    }

    private void OnEnable()
    {
        UpdateInventory();
    }

}
