using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

[RequireComponent(typeof(StudioEventEmitter))]
public class FamiliarSoundEmitter : MonoBehaviour
{

    StudioEventEmitter emitter;

    // Start is called before the first frame update
    void Start()
    {
        emitter = GetComponent<StudioEventEmitter>();
        Crew.OnCrewViewed += TriggerCrewSound;
    }


    /// <summary>
    /// Play the appropriate sound of a familiar
    /// </summary>
    /// <param name="sender"></param>
    private void TriggerCrewSound(Crew sender)
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Familiars/Familiar LWS " + sender.FMOD_EventName);
    }
}
