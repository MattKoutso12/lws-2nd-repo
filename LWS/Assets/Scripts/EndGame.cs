using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;

public class EndGame : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI claudiaFieldThresh;
    [SerializeField] TextMeshProUGUI claudiaFieldRep;
    [SerializeField] TextMeshProUGUI claudiaFieldMisc;
    //[SerializeField] TextMeshProUGUI claudiaFieldLine;
    [SerializeField] TextMeshProUGUI lucasFieldThresh;
    [SerializeField] TextMeshProUGUI lucasFieldRep;
    [SerializeField] TextMeshProUGUI lucasFieldMisc;
    //[SerializeField] TextMeshProUGUI lucasFieldLine;
    [SerializeField] TextMeshProUGUI kojiFieldThresh;
    [SerializeField] TextMeshProUGUI kojiFieldRep;
    [SerializeField] TextMeshProUGUI kojiFieldMisc;
    //[SerializeField] TextMeshProUGUI kojiFieldLine;
    [SerializeField] TextMeshProUGUI emiliaFieldThresh;
    [SerializeField] TextMeshProUGUI emiliaFieldRep;
    [SerializeField] TextMeshProUGUI emiliaFieldMisc;
    //[SerializeField] TextMeshProUGUI emiliaFieldLine;
    [SerializeField] TextMeshProUGUI pClaudiaFieldThresh;
    [SerializeField] TextMeshProUGUI pClaudiaFieldRep;
    [SerializeField] TextMeshProUGUI pClaudiaFieldMisc;
    //[SerializeField] TextMeshProUGUI pClaudiaFieldLine;
    [SerializeField] TextMeshProUGUI pLucasFieldThresh;
    [SerializeField] TextMeshProUGUI pLucasFieldRep;
    [SerializeField] TextMeshProUGUI pLucasFieldMisc;
    //[SerializeField] TextMeshProUGUI pLucasFieldLine;
    [SerializeField] TextMeshProUGUI pKojiFieldThresh;
    [SerializeField] TextMeshProUGUI pKojiFieldRep;
    [SerializeField] TextMeshProUGUI pKojiFieldMisc;
    //[SerializeField] TextMeshProUGUI pKojiFieldLine;
    [SerializeField] TextMeshProUGUI pEmiliaFieldThresh;
    [SerializeField] TextMeshProUGUI pEmiliaFieldRep;
    [SerializeField] TextMeshProUGUI pEmiliaFieldMisc;
    //[SerializeField] TextMeshProUGUI pEmiliaFieldLine;
    [SerializeField] TextMeshProUGUI totalFieldScore;
    [SerializeField] TextMeshProUGUI totalFieldRemarks;

    [SerializeField]
    public GameObject claudiaScore; 

    [SerializeField]
    public GameObject lucasScore; 

    [SerializeField]
    public GameObject kojiScore; 

    [SerializeField]
    public GameObject emiliaScore; 

    [SerializeField]
    public GameObject claudiaScorePopup; 

    [SerializeField]
    public GameObject lucasScorePopup; 

    [SerializeField]
    public GameObject kojiScorePopup; 

    [SerializeField]
    public GameObject emiliaScorePopup; 

    [SerializeField]
    public GameObject totalScore;

    [SerializeField]
    public GameObject flyScreen;

    [SerializeField]
    public GameObject exteriorScreen;

    [SerializeField]
    public GameObject fadeScreen;

    [SerializeField]
    public GameObject endButton;
    
    private Transform endScreen;

    void Start()
    {
        PersistantManager.OnDayChanged += Instance_OnDayChanged;
        endScreen = this.gameObject.transform.GetChild(0);
        endScreen.gameObject.SetActive(false);
    }

    void Update()
    {
        /*
        if(Keyboard.current.pKey.wasPressedThisFrame)
        {
            PersistantManager.Instance.dayCount = 60;
        }
        */
    }

    private void Instance_OnDayChanged(object sender, System.EventArgs e)
    {
        if (PersistantManager.Instance.dayCount >= 61)
        {
            ButtonManager.Instance.HideAll();
            ButtonManager.Instance.HideUI();
            StartCoroutine(BeginFlyAnim());
        }
    }

    public void ClaudiaSummary()
    {
        //Run Claudia initial dialogue
        //DialogueManager.Instance.StartDialogue();
        DialougeManager.Instance.StartDialogue("LWSCEnd");

        claudiaFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.orderScore / CouncilReputation.Instance.orderThreshold) * 100f)).ToString() + "%";
        claudiaFieldRep.text = CouncilReputation.Instance.claudiaRep.ToString() + " / 3";
        claudiaFieldMisc.text = CouncilReputation.Instance.claudiaMiscString;
        pClaudiaFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.orderScore / CouncilReputation.Instance.orderThreshold) * 100f)).ToString() + "%";
        pClaudiaFieldRep.text = CouncilReputation.Instance.claudiaRep.ToString() + " / 3";
        pClaudiaFieldMisc.text = CouncilReputation.Instance.claudiaMiscString;
        float claudiaLine = CouncilReputation.Instance.orderScore / 48f;
        claudiaScorePopup.SetActive(true);
        /*
        switch(claudiaLine)
        {
            case float n when (n >= .75f):
            claudiaFieldLine.text = ":)"; 
            pClaudiaFieldLine.text = ":)"; 
            //Run Claudia contextual A++ dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .50f): 
            claudiaFieldLine.text = ":|"; 
            pClaudiaFieldLine.text = ":|"; 
            //Run Claudia contextual B dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .25f): 
            claudiaFieldLine.text = ":/"; 
            pClaudiaFieldLine.text = ":/"; 
            //Run Claudia contextual C dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            default: 
            claudiaFieldLine.text = ":("; 
            pClaudiaFieldLine.text = ":("; 
            //Run Claudia contextual F dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
        }
        */
        claudiaScore.SetActive(true);
    }

    public void LucasSummary()
    {
        //Run Lucas initial dialogue
        //DialogueManager.Instance.StartDialogue();
        DialougeManager.Instance.StartDialogue("LWSLEnd");

        lucasFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.familiarScore / CouncilReputation.Instance.familiarThreshold) * 100f)).ToString() + "%";
        lucasFieldRep.text = CouncilReputation.Instance.lucasRep.ToString() + " / 3";
        lucasFieldMisc.text = CouncilReputation.Instance.lucasMiscString;
        pLucasFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.familiarScore / CouncilReputation.Instance.familiarThreshold) * 100f)).ToString() + "%";
        pLucasFieldRep.text = CouncilReputation.Instance.lucasRep.ToString() + " / 3";
        pLucasFieldMisc.text = CouncilReputation.Instance.lucasMiscString;
        float lucasLine = CouncilReputation.Instance.familiarScore / 12f;
        lucasScorePopup.SetActive(true);
        /*
        switch(lucasLine)
        {
            case float n when (n >= .75f): 
            lucasFieldLine.text = ":)"; 
            pLucasFieldLine.text = ":)";
            //Run Lucas contextual A++ dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .50f): 
            lucasFieldLine.text = ":|"; 
            pLucasFieldLine.text = ":|";
            //Run Lucas contextual B dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .25f): 
            lucasFieldLine.text = ":/"; 
            pLucasFieldLine.text = ":/";
            //Run Lucas contextual C dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            default: 
            lucasFieldLine.text = ":("; 
            pLucasFieldLine.text = ":(";
            //Run Lucas contextual F dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
        }
        */
        lucasScore.SetActive(true);
    }

    public void KojiSummary()
    {
        //Run Koji initial dialogue
        //DialogueManager.Instance.StartDialogue();
        DialougeManager.Instance.StartDialogue("LWSKEnd");

        kojiFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.goldScore / CouncilReputation.Instance.goldThreshold) * 100f)).ToString() + "%";
        kojiFieldRep.text = CouncilReputation.Instance.kojiRep.ToString() + " / 2";
        kojiFieldMisc.text = CouncilReputation.Instance.kojiMiscString;
        pKojiFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.goldScore / CouncilReputation.Instance.goldThreshold) * 100f)).ToString() + "%";
        pKojiFieldRep.text = CouncilReputation.Instance.kojiRep.ToString() + " / 2";
        pKojiFieldMisc.text = CouncilReputation.Instance.kojiMiscString;
        float kojiLine = CouncilReputation.Instance.goldScore / 1200f;
        kojiScorePopup.SetActive(true);
        /*
        switch(kojiLine)
        {
            case float n when (n >= .75f): 
            kojiFieldLine.text = ":)"; 
            pKojiFieldLine.text = ":)"; 
            //Run Koji contextual A++ dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .50f): 
            kojiFieldLine.text = ":|"; 
            pKojiFieldLine.text = ":|";
            //Run Koji contextual B dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .25f): 
            kojiFieldLine.text = ":/"; 
            pKojiFieldLine.text = ":/";
            //Run Koji contextual C dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            default: 
            kojiFieldLine.text = ":("; 
            pKojiFieldLine.text = ":()";
            //Run Koji contextual F dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
        }
        */
        kojiScore.SetActive(true);
    }

    public void EmiliaSummary()
    {
        //Run Emilia initial dialogue
        //DialogueManager.Instance.StartDialogue();
        DialougeManager.Instance.StartDialogue("LWSEEnd");

        emiliaFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.upgradeScore / CouncilReputation.Instance.upgradeThreshold) * 100f)).ToString() + "%";
        emiliaFieldRep.text = CouncilReputation.Instance.emiliaRep.ToString() + " / 2";
        emiliaFieldMisc.text = CouncilReputation.Instance.emiliaMiscString;
        pEmiliaFieldThresh.text = ((int) Mathf.Round((CouncilReputation.Instance.upgradeScore / CouncilReputation.Instance.upgradeThreshold) * 100f)).ToString() + "%";
        pEmiliaFieldRep.text = CouncilReputation.Instance.emiliaRep.ToString() + " / 2";
        pEmiliaFieldMisc.text = CouncilReputation.Instance.emiliaMiscString;
        float emiliaLine = CouncilReputation.Instance.upgradeScore / 10f;
        emiliaScorePopup.SetActive(true);
        /*
        switch(emiliaLine)
        {
            case float n when (n >= .75f): 
            emiliaFieldLine.text = ":)"; 
            pEmiliaFieldLine.text = ":)"; 
            //Run Emilia contextual A++ dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .50f): 
            emiliaFieldLine.text = ":|"; 
            pEmiliaFieldLine.text = ":|"; 
            //Run Emilia contextual B dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            case float n when (n >= .25f): 
            emiliaFieldLine.text = ":/"; 
            pEmiliaFieldLine.text = ":/"; 
            //Run Emilia contextual C dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
            default: 
            emiliaFieldLine.text = ":("; 
            pEmiliaFieldLine.text = ":("; 
            //Run Emilia contextual F dialogue
            //DialogueManager.Instance.StartDialogue();
            break;
        }
        */
        emiliaScore.SetActive(true);
    }

    public void FinalSummary()
    {
        endScreen.GetChild(3).gameObject.SetActive(false);
        totalFieldScore.text = ((int) Mathf.Round((CouncilReputation.Instance.EndCalculation() / 2000f) * 100f)).ToString() + "%";
        float totalRemark = CouncilReputation.Instance.EndCalculation() / 2000f;
        switch(totalRemark)
        {
            case float n when (n >= .75f): totalFieldRemarks.text = "Excellent"; break;
            case float n when (n >= .50f): totalFieldRemarks.text = "Good"; break;
            case float n when (n >= .25f): totalFieldRemarks.text = "Ok"; break;
            default: totalFieldRemarks.text = "Poor"; break;
        }
        totalScore.SetActive(true);
        endButton.SetActive(true);
    }

    public void RollCredits()
    {
        SceneManager.LoadScene(sceneName: "Credits");
    }

    private IEnumerator BeginFlyAnim()
    {
        fadeScreen.SetActive(true);
        yield return new WaitForSeconds(7);
        flyScreen.SetActive(true);
        yield return new WaitForSeconds(10);
        flyScreen.SetActive(false);
        exteriorScreen.SetActive(true);
        yield return new WaitForSeconds(6);
        endScreen.gameObject.SetActive(true);
        exteriorScreen.SetActive(false);
        yield return new WaitForSeconds(2);
        fadeScreen.SetActive(false);
        ClaudiaSummary();
    }
}
