﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Handles UI for OrderDragDrop.
/// </summary>
public class UI_OrderDrag : MonoBehaviour
{
    public static UI_OrderDrag Instance { get; private set; }

    private Canvas canvas;
    private RectTransform rectTransform;
    private RectTransform parentRectTransform;
    private CanvasGroup canvasGroup;
    private OrderWrapper order;

    private void Awake()
    {
        Instance = this;

        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
        parentRectTransform = transform.parent.GetComponent<RectTransform>();

        Hide();
    }

    private void Update()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, Mouse.current.position.ReadValue(), null, out Vector2 localPoint);
        transform.localPosition = new Vector3(transform.position.x, localPoint.y);
    }

    public OrderWrapper GetOrder()
    {
        return order;
    }

    public void SetOrder(OrderWrapper order)
    {
        this.order = order;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    #region Show comment
    /// <summary>
    /// Enables the UI_OrderDrag and sets the dragged order.
    /// </summary>
    /// <param name="order"> Order to set. </param>
    #endregion
    public void Show(OrderWrapper order)
    {
        gameObject.SetActive(true);
        SetOrder(order);
        UpdatePosition();
    }
}