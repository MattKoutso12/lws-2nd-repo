using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Handles UI for the item inventory.
/// </summary>
public class UI_Invetory : MonoBehaviour
{

    [SerializeField] private Transform pfUI_Item;

    public Inventory inventory;
    private Transform itemSlotContainer;
    private Transform itemSlotTemplate;
    private Player player;

    private void Awake()
    {
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    #region SetInventory
    /// <summary>
    /// Sets inventory and refreshes inventory items.
    /// </summary>
    /// <param name="inventory"></param>
    #endregion
    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;

        inventory.OnItemListChanged += Inventory_OnItemListChanged;

        RefreshInventoryItems();
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }

    
    private void RefreshInventoryItems()
    {

        // we don't need this anymore- slots will take care of themselves.


        return; // NOTE: stoppiing here, remove this if we need to
        /*
        bool lilyPichu = false;
        if (!gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
            lilyPichu = true;
        }
        foreach (Transform child in itemSlotContainer)
        {
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }

        int x = 0;
        int y = 0;
        float itemSlotCellSize = 54f;
        foreach (Inventory.InventorySlot inventorySlot in inventory.GetInventorySlotArray())
        {
            InventoryItem item = inventorySlot.GetItem();

            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);

            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, -y * itemSlotCellSize);

            if (!inventorySlot.IsEmpty())
            {
                // Not Empty, has Item
                Transform uiItemTransform = Instantiate(pfUI_Item, itemSlotContainer);
                uiItemTransform.GetComponent<RectTransform>().anchoredPosition = itemSlotRectTransform.anchoredPosition;
                DragDrop uiItem = uiItemTransform.GetComponent<DragDrop>();
                uiItem.SetItem(item);
            }

            Inventory.InventorySlot tmpInventorySlot = inventorySlot;

            UI_ItemSlot uiItemSlot = itemSlotRectTransform.GetComponent<UI_ItemSlot>();
            uiItemSlot.SetOnDropAction(() => {
                // Dropped on this UI Item Slot
                InventoryItem draggedItem = UI_ItemDrag.Instance.GetItem();
                draggedItem.RemoveFromItemHolder();
                inventory.AddItem(draggedItem, tmpInventorySlot);
            });

            /*
            TextMeshProUGUI uiText = itemSlotRectTransform.Find("amountText").GetComponent<TextMeshProUGUI>();
            if (inventorySlot.IsEmpty()) {
                // Empty
                uiText.SetText("");
            } else {
                if (item.amount > 1) {
                    uiText.SetText(item.amount.ToString());
                } else {
                    uiText.SetText("");
                }
            }
            */
        /*
            x++;
            int itemRowMax = 2;
            if (x >= itemRowMax)
            {
                x = 0;
                y++;
            }
            if(lilyPichu)
            {
                gameObject.SetActive(false);
            }
            
        }
        */
    }
}