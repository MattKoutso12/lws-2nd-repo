﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Interface for manipulating FamiliarWrappers in a crew holder.
/// </summary>
public interface ICrewHolder
{
    void RemoveCrew(FamiliarWrapper crew);
    void AddCrew(FamiliarWrapper crew);
    bool CanAddCrew();
}