using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inherits from CraftingSystem. Contains the planter-specific grid size. 
/// </summary>
public class PlantingSystem : CraftingSystem
{
    
    /// <summary>
    /// Sets up a crafting system with the Planting type and a grid size of 2.
    /// </summary>
    /// <param name="id">The unique id of the system.</param>
    public PlantingSystem(int id) : base(id, 2) 
    {
        crafting_type = CraftedItem.CraftingType.PLANTING;
    }

    
}
