using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using System;


/// <summary>
/// An Item Editor window for use in engine. Creates new Items and crafting recipes from scratch and seamlessly loads them into the game world.
/// </summary>
[Serializable]
public class ItemEditor : EditorWindow
{
    string s_name = "";
    string s_idname = "";
    string s_usage = "";
    Sprite s_sprite;

    /* COMING SOON: PRESETS FOR ITEM TYPE COLORS
    [SerializeField]
    public static Dictionary<string, Color> colorPresets = new Dictionary<string, Color>();
    string ps_name = "";
    static readonly string PRESET_PATH = "Assets/Scripts/Editor/presets.json";
    */
 
    CraftedItem.CraftingType s_craftingType = CraftedItem.CraftingType.PLANTING;
    bool s_stackable = true;
    bool s_purchasable = false;
    int s_cost = 5;
    float s_sellValue = 3f;
    Color s_color = Color.white;
    

    bool s_craftable = false;
    int s_daysToCraft = 1;
    int s_level = 1;
    Item[] recipe = new Item[4];

    GUIStyle infoStyle;

    static string _info = "";
    static AnimBool _showInfoText, _hackyTimer;

    bool goAnyway = false;
    bool newChanges = true;

    /// <summary>
    /// Our main window.
    /// </summary>
    [MenuItem("Window/Item Editor")]
    public static void ShowWindow()
    {
        var window = EditorWindow.GetWindow(typeof(ItemEditor));
        window.position = new Rect(0, 0, 600, 740);
        
    }

    /// <summary>
    /// Sets up all the info text variables for fading warnings/errors/information at the bottom of the editor.
    /// </summary>
    private void OnEnable()
    {
        
        _showInfoText = new AnimBool(false);
        _hackyTimer = new AnimBool(false);
        _hackyTimer.speed = 0.2f;
        _showInfoText.valueChanged.AddListener(Repaint);
        _showInfoText.speed = 0.25f;

        // preset loading COMING SOON
        /*
        if (!System.IO.File.Exists(PRESET_PATH))
            System.IO.File.Create(PRESET_PATH);
        */
    }


    /// <summary>
    /// The main loop of the editor. Draws everything and saves control inputs.
    /// </summary>
    private void OnGUI()
    {

        EditorGUILayout.LabelField("Fields with '*' must be filled", EditorStyles.miniLabel);

        EditorGUILayout.BeginHorizontal(GUILayout.MaxHeight(150));
        EditorGUILayout.Separator();
        EditorGUILayout.BeginVertical(GUILayout.MaxWidth(200));



        // LEFT SIDE

        EditorGUILayout.PrefixLabel("Name*:");
        s_name = EditorGUILayout.TextField(s_name, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false), GUILayout.Width(300));

        EditorGUILayout.PrefixLabel("Level*:");
        s_level = EditorGUILayout.IntSlider(s_level, 1, 10);
        GUIStyle textAreaStyle = new GUIStyle(EditorStyles.textArea);
        textAreaStyle.wordWrap = true;
        EditorGUILayout.PrefixLabel("Use*:");

        s_usage = EditorGUILayout.TextArea(s_usage, textAreaStyle, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true), 
            GUILayout.MinHeight(30), GUILayout.MaxHeight(100), GUILayout.Width(300));

        s_stackable = EditorGUILayout.Toggle("Stackable*:", s_stackable);
        

        /*          PRESETS COMING SOON ====================
        string [] presetNames = new string[colorPresets.Count];
        colorPresets.Keys.CopyTo(presetNames, 0);

        EditorGUI.BeginChangeCheck();
        s_categoryIndex = EditorGUILayout.Popup("Item Category*:", s_categoryIndex, presetNames);
        if(EditorGUI.EndChangeCheck())
        {
            s_color = colorPresets[presetNames[s_categoryIndex]];
        }*/

        s_color = EditorGUILayout.ColorField("Tooltip Name Color:", s_color);

        /*
        // color preset maker NOT WORKING YET
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Create/Overwrite Color Preset");
        ps_name = EditorGUILayout.TextField(ps_name);

        GUI.SetNextControlName("newpresetbutton");
        if (GUILayout.Button("Set Preset"))
        {
            colorPresets[ps_name] = s_color;
            SavePresetFile();
            GUI.FocusControl("newpresetbutton");
        }

        EditorGUILayout.EndHorizontal();
        */
        EditorGUILayout.EndVertical();
        EditorGUILayout.Separator(); // padding



        // RIGHT SIDE


        EditorGUILayout.BeginVertical(GUILayout.Width(200), GUILayout.Width(200));
        s_sprite = (Sprite)EditorGUILayout.ObjectField(
            s_sprite,
            typeof(Sprite),
            false,
            GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false), GUILayout.Width(200), GUILayout.Height(200));
        EditorGUILayout.EndVertical();
        EditorGUILayout.Separator(); // padding
        EditorGUILayout.EndHorizontal();



        EditorGUILayout.Space(20, true);

        // Shop Data
        s_purchasable = EditorGUILayout.BeginToggleGroup("Purchasable", s_purchasable);
        if (s_purchasable)
        {
            s_cost = EditorGUILayout.IntField("Cost", s_cost);
            s_sellValue = EditorGUILayout.FloatField("Sell Value", s_sellValue);
            EditorGUILayout.Space(20, true);
        }
        EditorGUILayout.EndToggleGroup();

        
        // Crafting Data
        s_craftable = EditorGUILayout.BeginToggleGroup("Craftable:", s_craftable);
        if (s_craftable)
        {
            s_daysToCraft = EditorGUILayout.IntSlider("Days Required:", s_daysToCraft, 1, 60);
            s_craftingType = (CraftedItem.CraftingType)EditorGUILayout.EnumPopup("Crafting Method:", s_craftingType);

            GUILayout.Label("Recipe:", EditorStyles.label);
            EditorGUILayout.BeginHorizontal();
            for (int i = 0; i < 4; i++)
            {
                EditorGUILayout.BeginVertical();
                string slotname = "Slot " + (i + 1) + ":";

                if (s_craftingType == CraftedItem.CraftingType.PLANTING)
                {
                    if (i == 0) slotname = "Seed:";
                    else if (i == 1) slotname = "Water:";
                    else if (i == 2)
                    {
                        EditorGUILayout.EndVertical();
                        break;
                    }
                }

                EditorGUILayout.PrefixLabel(slotname);
                recipe[i] = (Item)EditorGUILayout.ObjectField(recipe[i], typeof(Item), false);
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndToggleGroup();
        EditorGUILayout.Separator();

        if(!GUI.changed)
        {
            newChanges = false;
        }
        else
        {
            newChanges = true;
            goAnyway = false;
        }

        GUI.SetNextControlName("createbutton");
        if (GUILayout.Button(goAnyway ? "Confirm?" : "Create Item", GUILayout.Height(50)))
        {

            if(FinalizeItem())
            {
                s_name = "";
                s_usage = "";
                s_sprite = null;
                s_purchasable = false;
                s_cost = 5;
                s_sellValue = 3f;
                recipe = new Item[4];
                GUI.FocusControl("createbutton");
            }
        }


        infoStyle = new GUIStyle(EditorStyles.boldLabel);
        infoStyle.alignment = TextAnchor.UpperCenter;
        infoStyle.richText = true;
        if (EditorGUILayout.BeginFadeGroup(_showInfoText.faded))
        {
            //EditorGUI.indentLevel++;
            EditorGUILayout.LabelField(_info, infoStyle, GUILayout.Height(300), GUILayout.Width(600));
        }
        EditorGUILayout.EndFadeGroup();

        
        
    }

    /// <summary>
    /// When the user selects something else in the editor, we reset the overwrite toggle.
    /// </summary>
    private void OnSelectionChange()
    {
        goAnyway = false;
    }


    /// <summary>
    /// NOT YET IN USE! Coming Soon:
    /// Fetches data from presets.json, checks for differences between that and the user's unsaved changes, and then updates the file non-destructively.
    /// </summary>
    /*
    void SavePresetFile()
    {
        Dictionary<string, Color> savedData = new Dictionary<string, Color>();
        EditorJsonUtility.FromJsonOverwrite(
            System.IO.File.ReadAllText(PRESET_PATH),
            savedData
            );

        foreach(string name in colorPresets.Keys)
        {
            if(savedData.ContainsKey(name))
            {
                if(!savedData[name].Equals(colorPresets[name]))
                {
                    savedData[name] = colorPresets[name];

                }
            }
            else
            {
                savedData[name] = colorPresets[name];
            }
        }

        string updated_data = EditorJsonUtility.ToJson(savedData);
        System.IO.File.WriteAllText(PRESET_PATH, updated_data); // write the new data to the json file

    }
    */

    /// <summary>
    /// Takes the user's entered data and creates an Item object out of it accordingly, telling the difference between if a CraftedItem is needed or if a simple Item is enough.
    /// </summary>
    /// <returns>True if the item was successfully created, false if there was something preventing it.</returns>
    bool FinalizeItem()
    {
        if (s_name.Trim().Equals(""))
            return false;
        Item newItem;
        string id_name = s_name.ToLower().Replace(" ", "_");

        if (s_craftable)
        {
            newItem = new CraftedItem(id_name,
                s_sprite,
                recipe,
                s_craftingType,
                sellPrice: (s_purchasable ? s_cost : 5),
                stackable: s_stackable,
                displayed_name: s_name,
                use_text: s_usage,
                level: s_level,
                days_to_craft: s_daysToCraft);
        }
        else
        {
            newItem = new Item(id_name,
                s_sprite,
                s_stackable,
                sellPrice: (s_purchasable ? s_cost : 5),
                displayed_name: s_name,
                use_text: s_usage,
                level: s_level);
        }


        newItem.textColor = s_color;

        // god this is hacky
        _hackyTimer.value = false;
        _hackyTimer.target = true;

        _showInfoText.value = true;
        _showInfoText.target = true;

        // the average human can read 250 words per minute, or a word every ~0.24s.
        // We will double that to 0.5s per word and add an extra base 0.3s (since 0.25s is the average reaction time to visual stimulus).

        // count words:
        int words = _info.Split(' ').Length;

        // This gives a grand total of:
        float holdTime = (0.5f) * words + (0.3f); // thus longer messages will stay for longer before fading.

        // Why so complicated? Because I fucking hate it when software fades away important text before I get a chance to read it and I have the reaction time of a corpse

        _hackyTimer.speed = 1f / holdTime;


        string reason = "";
        int code = IsItemValid(newItem, out reason); // check item validity
    

        if(code == -1)
        {
            _info = "<color=red>"+reason+"</color>";
            return false;
        }
        else if ((!goAnyway || newChanges) && code == 0)
        {
            _info = "<color=yellow>"+reason+"</color>";
            goAnyway = true;
            
            return false;
        }
        else
        {
            AssetDatabase.ReleaseCachedFileHandles();
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            AssetDatabase.CreateAsset(newItem, "Assets/Resources/Items/" + (id_name.Replace(":", "_")) + ".asset");
            _info = "Item " + newItem.getIDName() + " successfully written to Assets/Resources/Items/" + id_name.Replace(":", "_") + ".asset";
            _info = "<color=green>" + _info + "</color>";
            goAnyway = false;
            AssetDatabase.SaveAssets();
            AssetDatabase.ReleaseCachedFileHandles();
            AssetDatabase.Refresh();
            ProjectWindowUtil.ShowCreatedAsset(newItem);
            AssetDatabase.ReleaseCachedFileHandles();
            AssetDatabase.SaveAssets();
            return true;
        }





    }

    /// <summary>
    /// Checks if the item in question is ready to be written to a .asset file. Has special cases for different kinds of items (like CraftedItem).
    /// Returns -1 on failure, 0 to warn the user (and prompt them to hit "Create" a second time to go through with it), or 1 to signal complete success.
    /// </summary>
    /// <param name="item">The item to check</param>
    /// <param name="reason">An output string parameter which gives the reason for failure, context for a warning if we are warning the user,
    ///                      or "SUCCESS" if successful. </param>
    /// <returns>-1 if the item is invalid, 0 if the item shouldn't write right away but should warn the user, or 1 if the item is completely valid.</returns>
    int IsItemValid(Item item, out string reason)
    {
        int code = 1;
        reason = "";
        // ERRORS ==================================================
        // Check if item is null
        if(item == null)
        {
            reason = "Invalid Item: You created nothing. You have not created an item. You can't do that. I'm mad as shit!";
            return -1;
        }

        // Check item's sprite
        if(item.sprite == null)
        {
            reason += "Invalid Item: Items must have a sprite to display.\n";
            code = -1;
        }

        // Check if item name is empty or if it contains characters that can't be in a filename.
        if (item.getIDName().Trim().Equals("")
        || item.getIDName().IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) >= 0)
        {
            reason += string.Format("Invalid name: \"{0}\" Name cannot contain weird characters and cannot be empty\n", item.getIDName());
            code = -1;
        }


        // CRAFTED ITEM CASES
        if (item.GetType() == typeof(CraftedItem))
        {
            CraftedItem citem = (CraftedItem)item;

            bool empty = true;
            foreach (Item i in citem.recipe)
            {
                if (i != null)
                {
                    empty = false;
                    break;
                }
            }
            if (empty)
            {
                reason += "Invalid Item: Recipe is empty\n";
                code = -1;
            }

        }

        if(code == -1)
        {
            return -1; // we use this 'code' variable so we can tell the user about multiple errors at once. Only fatal errors (like null) will return then and there.
        }

        // if we have no errors, we move on to warnings.

        // WARNINGS ===============================================
        
        // if the item has the same name as another item.
        if(ItemExists(item.getIDName()))
        {
            reason += string.Format("Warning: Item name: {0} already exists. Creating this item will overwrite it.\n", item.getIDName());
            code = 0;
        }

        // Check use text
        if (item.getUseText().Trim().Equals(""))
        {
            reason += "Warning: Item has no usage text. The player will be left without Flavor and Lore!\n";
            code = 0;
        }

        // CRAFTED ITEM CASES
        if(item.GetType() == typeof(CraftedItem))
        {
            //CraftedItem citem = (CraftedItem)item;

            // nothing here yet fuck you, dear reader

        }

        if(code != 0)
        {
            reason = "Item created successfully";
            return 1;
        }
        else
        {
            return 0;
        }
    }

    private void OnValidate()
    {
        // when value is changed
        goAnyway = false;
    }

    /// <summary>
    /// Checks to see if an item with a certain name already exists as an asset file in Resources, since names must be unique.
    /// </summary>
    /// <param name="itemname">The ID Name of the item to check</param>
    /// <returns>True if this item already exists, false if it does not.</returns>
    bool ItemExists(string itemname)
    {
        string [] inFolder = AssetDatabase.FindAssets(itemname, new[] {"Assets/Resources/Items"});
        if (inFolder.Length >= 1) return true;
        return false;
    }

    /// <summary>
    /// Handles the fading of the text, runs multiple times per frame. Don't touch.
    /// </summary>
    private void Update()
    {
        if(_hackyTimer.faded == 1.0f)
        {
            _showInfoText.target = false;
            _hackyTimer.value = false;
            _hackyTimer.target = false;
        }
    }

    


   
}
