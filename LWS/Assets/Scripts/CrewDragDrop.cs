using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

/// <summary>
/// Handles dragging and dropping of crew members
/// </summary>
public class CrewDragDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{

    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private Image crewImage;
    private Sprite newSprite;
    private FamiliarWrapper worker;
    [SerializeField] private List<Crew> crews;
    private Testing testPush;

    /// <summary>
    /// Sets crewbutton for each crew member.
    /// </summary>
    public void SetItemButton()
    {
        foreach (Crew listedItem in crews)
        {
            if (listedItem.name == worker.name)
            {
                CrewButton itemButton = GetComponent<CrewButton>();
                itemButton.crew = worker;
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = .5f;
        canvasGroup.blocksRaycasts = false;
        UI_CrewDrag.Instance.Show(worker);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        UI_CrewDrag.Instance.Hide();
    }

    /// <summary>
    /// Sets image sprite to the dragged crew member's sprite.
    /// </summary>
    public void SetSprite()
    {
        newSprite = worker.GetSprite();
        crewImage.sprite = newSprite;
    }

    /// <summary>
    /// Disables this CrewDragDrop.
    /// </summary>
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    #region SetItem comment
    /// <summary>
    /// Sets CrewDragDrop's FamiliarWrapper to input.
    /// </summary>
    /// <param name="worker"> Crew member we want to set the CrewDragDrop to. </param>
    #endregion
    public void SetItem(FamiliarWrapper worker)
    {
        //set UI_Crew components before setting the item
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
        crewImage = transform.Find("CrewImage").GetComponent<Image>();

        //set item itself
        this.worker = worker;
        SetSprite();
        SetItemButton();
    }
}