using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RecipeBook : MonoBehaviour
{
    public static RecipeBook Instance;

    [SerializeField]
    public GameObject recipeSlotPrefab;

    [SerializeField]
    public Scrollbar scrollbar;

    private HashSet<CraftedItem> spawnedItems;

    private void Awake()
    {
        spawnedItems = new HashSet<CraftedItem>();
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateRecipes();
    }

    private void UpdateRecipes()
    {
        foreach(CraftedItem item in CraftedItem.AllRecipes.Keys)
        {
            if (item.crafting_type == CraftedItem.CraftingType.PLANTING) continue;
            // we can designate an item's recipe to show up at either a day or a level
            if ((item.shop_level <= PersistantManager.Instance.shopLevel || item.availableOnDay == PersistantManager.Instance.dayCount) && !spawnedItems.Contains(item))
            {
                try
                {
                    GameObject newRecipe = Instantiate(recipeSlotPrefab, transform);
                    newRecipe.transform.SetSiblingIndex(0);
                    RecipeSlot slot = newRecipe.GetComponent<RecipeSlot>();
                    slot.Init(item);
                    spawnedItems.Add(item);
                    //scrollbar.value = 1.0f;
                }
                catch (System.Exception e)
                {
                    continue;
                }
            }
        }
    }

    private void OnEnable()
    {
        scrollbar.value = 1.0f;
        UpdateRecipes();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
