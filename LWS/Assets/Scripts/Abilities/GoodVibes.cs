using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodVibes : Ability
{

    public GoodVibes()
    {
        name = "Good Vibes";
        description = "Upon activation, all other familiar's ability cooldowns reduce by one. This ability does not affect other instances of Good Vibes.";
        cooldown_total = 4;
        cooldown_current = 0;
        this.activation_total = 10;
        this.activation_current = activation_total;
        this.isExpired = false;
    }           //This familiar knows just how to motivate. When working, reduces the cooldowns of others (Does not apply to other Good Vibes).

    /// <summary>
    /// Procs here, if it's ready.
    /// </summary>
    public override void OnDayStart()
    {
        base.OnDayStart();
        if(cooldown_current == 0)
        {
            ReduceCurrentActiveCooldowns();
            OnResolve(this);
        }
    }

    /// <summary>
    /// Reduces the Ability cooldowns of Familiars currently attached to a CraftingSystem.
    /// </summary>
    void ReduceCurrentActiveCooldowns() // put this in its own method so it can be moved around more easily if we want
    {
        int[] ids = CraftingSystem.GetIDArray();
        foreach(int i in ids)
        {
            CraftingSystem system = CraftingSystem.GetCraftingSystem(i);
            if(system.manned)
            {
                Ability crew_ability = system.GetCrew()?.GetAbility();
                if(crew_ability != null && !(crew_ability is GoodVibes))
                {
                    crew_ability.Current_Cooldown -= 1;
                }
            }
        }
    }
}
