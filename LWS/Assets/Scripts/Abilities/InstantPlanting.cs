﻿using System.Collections;
using UnityEngine;


public class InstantPlanting : Ability
{

    public InstantPlanting()
    {
        this.name = "Instant Planter";
        this.description = "Upon activation, the planting time of this familiar's Planter will finish immediately.";
        this.cooldown_total = 4;
        this.cooldown_current = 0;
        this.activation_total = 5;
        this.activation_current = activation_total;
        this.isExpired = false;

        good_effects = new string[] { "-100% Days to Plant" };
    }           //When assigned to a planter, growing will finish in one day.



    /// <summary>
    /// If we are planting, finishes it instantly
    /// </summary>
    /// <param name="system"></param>
    public override void BeforeCrafting(ProductionSystem system)
    {
        base.BeforeCrafting(system);
        if(cooldown_current <= 0 && system.type == CraftedItem.CraftingType.PLANTING) //specifies planting system
        {
            system.AddDurationModifier(-1* system.GetCurrentDuration()); //forces Days to plant to 1
            OnResolve(this); // resets cooldown
        }
    }
}