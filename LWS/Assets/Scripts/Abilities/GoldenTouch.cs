using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenTouch : Ability
{

    public GoldenTouch()
    {
        this.name = "Golden Touch";
        this.description = "Upon fully crafting an item in a Crafting Table, Fulfulled Orders offer 25% more gold for a day.";
        this.cooldown_total = 3;
        this.cooldown_current = 0;
        this.activation_total = 15;
        this.activation_current = activation_total;
        this.isExpired = false;


        good_effects = new string[] { "+25% Gold from Fulfilled Orders after Crafting" };
    }           //Increases gold earned from orders by 25% for a day after crafting an item.

    /// <summary>
    /// Increases crafted item output by one.
    /// </summary>
    /// <param name="system"> Crafting system we are checking for an output. </param>
    /// <param name="output"> Item being output by the system. </param>
    /// <param name="inventory"> The player's inventory. </param>
    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if(cooldown_current <= 0 && system.type != CraftedItem.CraftingType.PLANTING)
        {
            PersistantManager.Instance.goldTouch = true;
            OnResolve(this);
        }
    }
}
