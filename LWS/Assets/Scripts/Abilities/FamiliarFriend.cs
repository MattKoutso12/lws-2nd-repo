using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// It's just Hex: Undying, but with cooldowns.
public class FamiliarFriend : Ability
{
    public FamiliarFriend()
    {
        name = "Familiar Friend";
        description = "Upon another familiar's ability activating, This familiar goes on cooldown instead, allowing the other familiar to act again.";
        cooldown_total = -1; // doesn't really apply to this one, since their cooldown is a different familiar's cooldown.
        cooldown_current = 0;
        this.activation_total = 15;
        this.activation_current = activation_total;
        this.isExpired = false;
        Ability.Resolved += MrPresidentGetDown;
    }               // This familiar helps relieve the burden of others. When another familiar uses their ability, this familiar takes their cooldown instead.



    /// <summary>
    /// Jumps in front of the cooldown for another familiar. Will trigger on the first cooldown that happens- might want to think of a way to make it random.
    /// </summary>
    /// <param name="sender">The ability we're taking the cooldown of</param>
    private void MrPresidentGetDown(Ability sender)
    {
        if(cooldown_current == 0 && !(sender is FamiliarFriend))
        {
            sender.Current_Cooldown = 0;
            cooldown_total = sender.Total_Cooldown;
            OnResolve(this);

        }
    }
}
