﻿using System.Collections;
using UnityEngine;



public class Trader : Ability
{
    public Trader()
    {
        this.name = "Trader";
        this.description = "Upon fully crafting an item in a Crafting Table, Selling Items offers 50% more gold for a day.";
        this.cooldown_total = 3;
        this.cooldown_current = 0;
        this.activation_total = 7;
        this.activation_current = activation_total;
        this.isExpired = false;


        good_effects = new string[] { "+50% Gold on day after Crafting" };
    }               //This familiar takes some extra time to make the most of what they have. +2 to crafting output, +1 to crafting time

    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if (cooldown_current <= 0 && system.type == CraftedItem.CraftingType.CRAFTING) //specifically crafting system
        {
            PersistantManager.Instance.trader = true;
            OnResolve(this);
        }
    }
}