﻿using System;
using System.Collections;
using UnityEngine;

public static class GlobalAbility
{
    static string[] abilitiesLevel1 =
    {
        "CarefulCultivate",
        "GoldenTouch",
        //"DodoManager",
        //"Economize",
        "FortunateCrafting",
        //"InstantPlanting",
        "LuckyPlanting",
        //"QuickPlant",
        "Resourceful",
        "SkilledCrafter",
        "Trader",
        //"GoodVibes",
        //"FamiliarFriend",
        //"MultiTasker"
    };

    static string[] abilitiesLevel2 =
    {

    };

    static string[] abilitiesLevel3 =
    {

    };

    static string[] abilitiesLevel4 =
    {

    };

    static string[] abilitiesLevel5 =
    {

    };

    public static Ability GetRandomAbility(int abilityLevel)
    {
        switch (abilityLevel)
        {
            case 1:
            return CreateAbility(abilitiesLevel1[UnityEngine.Random.Range(0, abilitiesLevel1.Length)]);
            case 2:
            return CreateAbility(abilitiesLevel1[UnityEngine.Random.Range(0, abilitiesLevel2.Length)]);
            case 3:
            return CreateAbility(abilitiesLevel1[UnityEngine.Random.Range(0, abilitiesLevel3.Length)]);
            case 4:
            return CreateAbility(abilitiesLevel1[UnityEngine.Random.Range(0, abilitiesLevel4.Length)]);
            case 5:
            return CreateAbility(abilitiesLevel1[UnityEngine.Random.Range(0, abilitiesLevel5.Length)]);
            default: 
            return new Versatile();
        }
    }

    public static Ability CreateAbility(string ability)
    {
        switch (ability)
        {
            case "CarefulCultivate":
                return new CarefullyCultivated();
            case "GoldenTouch":
                return new GoldenTouch();
            case "DodoManager":
                return new DodoManager();
            case "Economize":
                return new Economize();
            case "FortunateCrafting":
                return new FortunateCrafting();
            case "InstantPlanting":
                return new InstantPlanting();
            case "LuckyPlanting":
                return new LuckyPlanting();
            case "QuickPlant":
                return new QuickPlant();
            case "Resourceful":
                return new Resourceful();
            case "SkilledCrafter":
                return new SkilledCrafter();
            case "Trader":
                return new Trader();
            case "GoodVibes":
                return new GoodVibes();
            case "FamiliarFriend":
                return new FamiliarFriend();
            case "MultiTasker":
                return new MultiTasker();
        }
        return null;
    }
}