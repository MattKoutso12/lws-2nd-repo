using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarefullyCultivated : Ability
{
    new bool hasTriggered = false;

    public CarefullyCultivated()
    {
        this.name = "Carefully Cultivated";
        this.description = "Upon fully growing a plant in a Planter, recieve 2 additional items of the crafted item. This ability increases the planting time of the plant by 1 day.";
        this.cooldown_total = 3;
        this.cooldown_current = 0;
        this.activation_total = 7;
        this.activation_current = activation_total;
        this.isExpired = false;
        this.hasTriggered = false;

        good_effects = new string[] { "+2 Item Output on Planting" };

        bad_effects = new string[] { "+1 Days To Plant" };
    }       //Gives two additional grown items, but increases time to grow by one day.

    /// <summary>
    /// Increases growing time on a planting system by one, increases item output by two.
    /// </summary>
    /// <param name="system"></param>
    public override void BeforeCrafting(ProductionSystem system)
    {
        base.BeforeCrafting(system);
        if(cooldown_current <= 0 && system is PlantingSystem)
        {
            system.AddDurationModifier(1);
            cooldown_current = cooldown_total;
            hasTriggered = true;
        }
    }

    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if(cooldown_current <= 0 && system is PlantingSystem)
        {
            inventory.AddItem(new InventoryItem(output.itemType, 2));
            OnResolve(this);
        }
        else if(hasTriggered && system is PlantingSystem)
        {
            inventory.AddItem(new InventoryItem(output.itemType, 2));
            OnResolve(this);
            hasTriggered = false;
        }
    }
}
