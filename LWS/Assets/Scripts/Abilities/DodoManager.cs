using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DodoManager : Ability
{
    public DodoManager()
    {
        this.name = "Dodo Manager";
        this.description = "Upon fully crafting an item in a Crafting Table, planting times on all Planters will be reduced by 1.";
        this.cooldown_total = 4;
        this.cooldown_current = 0;
        this.activation_total = 10;
        this.isExpired = false;
        this.activation_current = activation_total;


        good_effects = new string[] { "-1 Days to Plant for all planters after Crafting" };
    }         // Upon crafting an item, reduce growth times for all planters by one day.

    /// <summary>
    /// Reduces growth times of all plantingsystems by 1 on crafting an item.
    /// </summary>
    /// <param name="system"> Crafting system we are checking. </param>
    /// <param name="output"> Item being output by the system. </param>
    /// <param name="inventory"> The player's inventory. </param>
    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if(cooldown_current <= 0 && !(system is PlantingSystem))
        {
            UI_PlantingSystem[] activePlanters = Object.FindObjectsOfType<UI_PlantingSystem>();
            foreach(UI_PlantingSystem p in activePlanters)
            {
                p.plantingSystem.ReduceDaysToCraft(1);
            }
        }
    }
}
