﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class Versatile : Ability
{
    public Versatile()
    {
        this.name = "Versatile";
        this.description = "Can craft or plant.";
        this.cooldown_total = 1;
        this.cooldown_current = 1;
        this.activation_total = 99;
        this.activation_current = activation_total;
        this.isExpired = false;
    }
}