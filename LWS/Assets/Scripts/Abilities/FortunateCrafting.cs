using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FortunateCrafting : Ability
{
    public FortunateCrafting()
    {
        this.name = "Fortunate Crafting";
        this.description = "Upon fully crafting an item in a Crafting Table, recieve an additional item of the crafted item.";
        this.cooldown_total = 2;
        this.cooldown_current = 0;
        this.activation_total = 15;
        this.activation_current = activation_total;
        this.isExpired = false;

        good_effects = new string[] { "+1 Item Output on Crafting" };
    }               //Increases number of crafted items by one.

    /// <summary>
    /// Increases crafted item output by one.
    /// </summary>
    /// <param name="system"> Crafting system we are increasing output of. </param>
    /// <param name="output"> Item being output by the system. </param>
    /// <param name="inventory"> The player's inventory. </param>
    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if(cooldown_current <= 0 && !(system is PlantingSystem))
        {
            inventory.AddItem(new InventoryItem(output.itemType, 1));
            OnResolve(this);
        }
    }
}
