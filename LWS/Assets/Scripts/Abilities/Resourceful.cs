﻿using System.Collections;
using UnityEngine;


public class Resourceful : Ability
{
    public Resourceful()
    {
        this.name = "Resourceful";
        this.description = "Upon fully growing a plant in a Planter, all water is returned to the inventory";
        this.cooldown_total = 3;
        this.cooldown_current = 0;
        this.activation_total = 10;
        this.activation_current = activation_total;
        this.isExpired = false;

        good_effects = new string[] { "+1 Water after Planting" };
    }       //Once every two nights, after having grown a plant, all the water used in that recipe is returned to your inventory


    /// <summary>
    /// if planting, returns all water used
    /// </summary>
    /// <param name="system"></param>
    /// <param name="output"></param>
    /// <param name="inventory"></param>
    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    { 
        base.OnOutputCreation(system, output, inventory);
        if(cooldown_current <= 0 && system.type == CraftedItem.CraftingType.PLANTING)
        {
            inventory.AddItem(new InventoryItem("water",1));
            OnResolve(this);
        }
    }
}