﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Economize : Ability
{
    

    public Economize()
    {
        this.name = "Economize";
        this.description = "Upon crafting a Level 1 item in a Crafting Table, gain an additional resource that was used in the recipe. This resource is chosen at random.";
        this.cooldown_total = 3;
        this.cooldown_current = 0;
        this.activation_total = 15;
        this.isExpired = false;
        this.activation_current = activation_total;


        good_effects = new string[] { "Get one ingredient back on Level 1 Crafting" };
    }       // Upon crafting a level 1 item, gain one extra resource from recipe, chosen at random


    private InventoryItem extra_resource = null; // we'll assign this in BeforeCrafting and then give it to the player when they craft.

    public override void BeforeCrafting(ProductionSystem system)
    {
        base.BeforeCrafting(system);
        if (system.type == CraftedItem.CraftingType.CRAFTING && system.GetCurrentResult() != null)
        {
            
            List<Item> items = new List<Item>(); // listing the items in current recipe use
            for(int i = 0; i < system.slots.Length; i++)
            {
                InventoryItem inSlot = system.GetContents(i);
                if(inSlot != null && inSlot.itemType.level == 1)
                {
                    items.Add(inSlot.itemType);
                }
            }

            Item chosen = items[Random.Range(0, items.Count)];
            extra_resource = new InventoryItem(chosen); // choose a random item and save it so we can add it when the player produces it
            
        }
    }



    /// <summary>
    /// Upon crafting a level 1 item, give the player an extra resource
    /// </summary>
    /// <param name="system">The CraftingSystem being produced at</param>
    /// <param name="output">The item being outputted</param>
    /// <param name="inventory">The Players inventory</param>
    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if (cooldown_current <= 0 && system.type == CraftedItem.CraftingType.CRAFTING) //specifically crafting system
        {
            if (extra_resource != null)
                inventory.AddItem(extra_resource);
            extra_resource = null;
            OnResolve(this);
        }
    }
}