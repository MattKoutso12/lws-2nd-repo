using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Ability
{
    protected string name;
    protected string description;
    protected int cooldown_total;
    protected int cooldown_current;
    protected int activation_total;
    protected int activation_current;
    protected bool isExpired;
    protected string[] good_effects = { };
    protected string[] bad_effects = { };


    // Ability events that are sent out, so we can know when abilities are triggered. Useful for synergistic abilities.
    // NOTE: Make SURE to invoke OnResolve when you trigger your ability!!!
    public delegate void AbilityEvent(Ability sender);
    public static event AbilityEvent Resolved;

    public Ability()
    {
        PersistantManager.OnDayChanged += DayHasChanged;
    }

    /// <summary>
    /// The name of this ability. Should be the same as the class name, really.
    /// </summary>
    public string Name { get => name; }

    /// <summary>
    /// The description of the ability
    /// </summary>
    public string Description { get => description;}

    /// <summary>
    /// The total number of days this ability needs to wait after it is triggered before it can be triggered again
    /// </summary>
    public int Total_Cooldown { get => cooldown_total; }

    public int Activation_Total { get => activation_total; }

    public int Activation_Current { get => activation_current; }


    public string [] Good_Effects
    {
        get
        {
            return good_effects;
        }
    }

    public string [] Bad_Effects
    {
        get
        {
            return bad_effects;
        }
    }

    /// <summary>
    /// A way to access/alter the current cooldown without goofing stuff up at runtime. Prevents the cooldown from going below zero
    /// </summary>
    public int Current_Cooldown
    {
        get
        {
            return cooldown_current;
        }

        set
        {
            cooldown_current = value;
            if (cooldown_current < 0) cooldown_current = 0;
        }
    }

    private void DayHasChanged(object sender, EventArgs e)
    {
        OnDayEnd();
        OnDayStart(); //call DayEnd and DayStart back to back. The purpose of this for ordering/stacking abilities.
    }

    /// <summary>
    /// Called when the "End Day" button is hit
    /// </summary>
    public virtual void OnDayEnd() { }

    /// <summary>
    /// Called right after OnDayEnd and decreases cooldowns. Works like LateUpdate.
    /// </summary>
    public virtual void OnDayStart() 
    {
        if (cooldown_current > 0) cooldown_current--; // reduce cooldown
    }

    /// <summary>
    /// Called when the crew that has this ability produces an output
    /// </summary>
    /// <param name="system">The CraftingSystem being produced at</param>
    /// <param name="output">The item being outputted</param>
    /// <param name="inventory">The Players inventory</param>
    public virtual void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory) { }

    /// <summary>
    /// Called right before crafting, so we can modify item production time, etc.
    /// </summary>
    /// <param name="system">The crafting system we're working on</param>
    public virtual void BeforeCrafting(ProductionSystem system) { }

    protected virtual void OnResolve(Ability sender)
    {
        Resolved?.Invoke(this);
        sender.activation_current--;
        if(sender.activation_current <= 0)
        {
            sender.isExpired = true;
        }
        sender.cooldown_current = sender.cooldown_total;
    }



}
