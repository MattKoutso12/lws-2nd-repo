using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiTasker : Ability
{
    public MultiTasker()
    {
        name = "Multitasker";
        description = "Upon fully growing a plant in a Planter, recieve an additional plant of the same level. This plant is selected at random.";
        cooldown_total = 3;
        cooldown_current = 0;
        this.activation_total = 7;
        this.activation_current = activation_total;
        this.isExpired = false;
    }              //This familiar has a knack for making something out of nothing. Produces an extra, random plant of the same level when harvesting.

    /// <summary>
    /// Makes a list of all items which match the criteria of being the same level as well as being something made using planting,
    /// then picks a random one from that list and gives it to the player.
    /// </summary>
    /// <param name="system"></param>
    /// <param name="output"></param>
    /// <param name="inventory"></param>
    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);

        if (Current_Cooldown == 0)
        {
            // holds all items of the same crafting type and same level. We'll choose a random one.
            List<CraftedItem> plantedItems = new List<CraftedItem>();

            // iterate through crafted items, get items which are planted
            foreach (CraftedItem item in CraftedItem.AllRecipes.Keys)
            {
                if (item.crafting_type == CraftedItem.CraftingType.PLANTING)
                {
                    if (item.level == output.itemType.level)
                        plantedItems.Add(item);
                }
            }

            if(plantedItems.Count > 0)
            {
                InventoryItem extraItem = new InventoryItem(plantedItems[Random.Range(0, plantedItems.Count)], 1);
                inventory.AddItem(extraItem); // get a random item from the list and add it
                OnResolve(this);
            }
        }
    }
}
