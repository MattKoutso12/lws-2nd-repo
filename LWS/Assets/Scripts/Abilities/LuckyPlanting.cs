using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyPlanting : Ability
{


    public LuckyPlanting()
    {
        this.name = "Lucky Planting";
        this.description = "Upon fully growing a plant in a Planter, recieve an additional item of the crafted item.";
        this.cooldown_total = 2;
        this.cooldown_current = 0;
        this.activation_total = 15;
        this.activation_current = activation_total;
        this.isExpired = false;


        good_effects = new string[] { "+1 Item Output on Planting" };
    }           //This familiar just seems to have a green thumb. +1 to planting output


    /// <summary>
    /// If we're working on a planting job, add 1 to the output amount. Very simple.
    /// </summary>
    /// <param name="system">The crafting system we're working on</param>
    /// <param name="output">The item we're about to output</param>
    /// <param name="inventory">The player's inventory</param>
    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if(cooldown_current <= 0 && system.type == CraftedItem.CraftingType.PLANTING) // if we're talking about a planting system here
        {
            inventory.AddItem(new InventoryItem(output.itemType, 1)); // add another item of the same type, with amount 1
            OnResolve(this);
        }
    }
}
