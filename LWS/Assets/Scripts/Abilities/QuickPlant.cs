using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickPlant : Ability
{

    public QuickPlant()
    {
        this.name = "Quick Plant";
        this.description = "Upon activation, the planting time of this familiar's Planter is reduced by 1";
        this.cooldown_total = 2;
        this.cooldown_current = 0;
        this.activation_total = 15;
        this.activation_current = activation_total;
        this.isExpired = false;


        good_effects = new string[] { "-1 Days to Plant" };
    }           //Reduces growing times by one day at assigned station.

    /// <summary>
    /// Reduces growing time on a planting system by one.
    /// </summary>
    /// <param name="system"> The crafting system we are checking. </param>
    public override void BeforeCrafting(ProductionSystem system)
    {
        base.BeforeCrafting(system);
        if(cooldown_current <= 0 && system.type == CraftedItem.CraftingType.PLANTING)
        {
            system.AddDurationModifier(-1);
            OnResolve(this);
        }
    }
}
