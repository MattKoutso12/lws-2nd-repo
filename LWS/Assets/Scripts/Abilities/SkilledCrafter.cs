using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkilledCrafter : Ability
{


    ProductionSystem current = null;

    public SkilledCrafter()
    {
        this.name = "Skilled Crafter";
        this.description = "Upon fully crafting an item in a Crafting Table, recieve 2 additional items of the crafted item. This ability increases the crafting time of the plant by 1 day.";
        this.cooldown_total = 5;
        this.cooldown_current = 0;
        this.activation_total = 7;
        this.activation_current = activation_total;
        this.isExpired = false;


        good_effects = new string[] { "+2 Item Output on Crafting" };

        bad_effects = new string[] { "+1 Days To Craft" };
    }       //This familiar takes some extra time to make the most of what they have. +2 to crafting output, +1 to crafting time

    public override void BeforeCrafting(ProductionSystem system)
    {
        base.BeforeCrafting(system);
        if(cooldown_current <= 0 && system.type == CraftedItem.CraftingType.CRAFTING) // check we're on a crafting table and that our cooldown is done.
        {
            current = system;
            system.AddDurationModifier(1); // reduce by a negative, so add.
            this.cooldown_current = cooldown_total;
        }
    }

    public override void OnOutputCreation(ProductionSystem system, InventoryItem output, Inventory inventory)
    {
        base.OnOutputCreation(system, output, inventory);
        if(current != null && system == current) // we have to be on the same crafting table we started on. You can't just pass the extra duration to a different station and then swap over.
        {
            InventoryItem copy = new InventoryItem(output.itemType, 2); // +2 to output
            inventory.AddItem(copy);
            current = null;
            OnResolve(this);
        }
    }
}
