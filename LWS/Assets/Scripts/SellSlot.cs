using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using FMODUnity;

public class SellSlot : ItemSlot
{
    /*
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            InventoryItem item = eventData.pointerDrag.GetComponent<DragDrop>().item;
            Debug.Log(item);
            Destroy(eventData.pointerDrag);
            PersistantManager.Instance.AddGold(item.itemType.SellPrice / 2);
        }
    }
    */
    [SerializeField]
    public TextMeshProUGUI soldText;

    public delegate void SellEvent(InventoryItem sold, int profit);
    public static event SellEvent OnSell;

    StudioEventEmitter emitter;

    private void Start()
    {
        emitter = GetComponent<StudioEventEmitter>();
    }


    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);
        if (new_holding == null) return; // if we're empty, since this will be called again when we delete the item
        InventoryItem tempItem = (InventoryItem)new_holding;
        int rawSellPrice = tempItem.itemType.SellPrice;

        if (soldText != null)
        {
            //int traderSellPrice = (int)(rawSellPrice + (rawSellPrice * .5));
            //if (traderSellPrice <= 0) traderSellPrice = 1;
            PersistantManager.Instance.AddGold(rawSellPrice * tempItem.amount);
            soldText.text = "+$" + (rawSellPrice * tempItem.amount);
            soldText.gameObject.GetComponent<Animator>().Play("Sold");
            emitter.Play();
            OnSell?.Invoke(tempItem, rawSellPrice * tempItem.amount);
        }
        else
        {
            PersistantManager.Instance.AddGold(rawSellPrice * tempItem.amount);
            soldText.text = "+$" + (rawSellPrice * tempItem.amount);
            soldText.gameObject.GetComponent<Animator>().Play("Sold");
            emitter.Play();
            OnSell?.Invoke(tempItem, rawSellPrice * tempItem.amount);
        }

        
        SetHolding(null);
    }

    /// <summary>
    /// Only allows you to sell inventory items, for now anyway
    /// </summary>
    protected override bool CanHoldThis(IDraggableObject objectToHold)
    {
        if (objectToHold == null) return true;
        if(base.CanHoldThis(objectToHold))
        {
            if(objectToHold.GetType() == typeof(InventoryItem)
                && this.IsEmpty()) // NOTE: right now, only inventory items can be sold

            {
                return true;
            }
        }
        return false;
    }
}
