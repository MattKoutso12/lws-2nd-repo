﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

/// <summary>
/// Handles dragging and dropping of an order.
/// </summary>
public class OrderDragDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private OrderWrapper order;
    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    [SerializeField] private List<Task> orders;


    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
    }

    #region SetOrder
    /// <summary>
    /// Sets the dragged order to the given order.
    /// </summary>
    /// <param name="order"> Order we want to set. </param>
    #endregion
    public void SetOrder(OrderWrapper order)
    {
        this.order = order;
        SetOrderButton();
    }

    private void SetOrderButton()
    {
        foreach (Task listedOrder in orders)
        {
            string orderName = listedOrder.Name;
            if(orderName == order.name)
            {
                OrderButton itemButton = GetComponent<OrderButton>();
                itemButton.order = order;
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = .5f;
        canvasGroup.blocksRaycasts = false;
        UI_OrderDrag.Instance.Show(order);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        UI_OrderDrag.Instance.Show(order);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public OrderWrapper GetOrder()
    {
        return order;
    }
}