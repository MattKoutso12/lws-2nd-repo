using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The interface which allows save functionality for an object.
/// </summary>
public interface Savable
{

    /*NOTE: I've changed the architecture of this system a little bit, since it was previously an abstract class which was a child of MonoBehaviour, which is discouraged heavily by the Unity documentation.
    *   It would be nice if we could have multiple inheritance (like C++ has) but since we can't, we have to make due with having this be an interface. This mostly works fine, but is a little annoying because
    *   we have to implement AddToList and RemoveFromList for every class. Making this an interface solves multiple bugs, though, and makes it easier to maintnence. This also means we can have savables which aren't
    *   MonoBehaviours, which is nice for things instanced only on the script side and not within the world.
    */



    /// <summary>
    /// Saves this object.
    /// </summary>
    /// <param name="data">The PlayerData to save this object's data to. Includes side effects.</param>
    public void Save(PlayerData data);

    /// <summary>
    /// Adds this savable object to SaveManager's savable list.
    /// </summary>
    public void AddToSavables(SaveManager manager);


    /// <summary>
    /// Removes this savable object from SaveManager's savable list
    /// </summary>
    public void RemoveFromSavables();
}