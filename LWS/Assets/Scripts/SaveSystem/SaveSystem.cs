using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public static class SaveSystem
{
    //passes various parameters to act as data sources
    public static void SavePlayer(ref string path, List<Savable> saveables)
    {
        //creates a binary formatter, a path name, a FileStream, and a PlayerData Object
        BinaryFormatter formatter = new BinaryFormatter();
        
        // TODO: Change file naming to include "header" attributes (date, gold, real life date)
        
        FileStream stream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
        PlayerData data = new PlayerData(saveables);

        //formats the PlayerData into the file and closes the file
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static PlayerData LoadPlayer(string path)
    {
        if(SceneManager.GetActiveScene().name == "Main Menu")
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/click 1");
            SceneManager.LoadScene(sceneName: "LWS");
        }

        //checks if file exists
        if (File.Exists(path))
        {
            //creates binary formatter and opens named file
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            //unformats the file into a PlayerData Object and closes file
            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();
        
            return data;
        } else
        {
            //alerts player to unfound file
            Debug.LogError("Save File not found in " + path);
            return null;
        }
    }
}
