using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class WorldLoader : MonoBehaviour
{
    public static WorldLoader Instance;

    // for purchased versions of everything
    public GameObject expansionTilePrefab, crafterPrefab, planterPrefab;


    // Start is called before the first frame update
    private void Awake()
    {
        Instance = this;
        ExpansionTile.purchasedTiles = new HashSet<ExpansionTile>();
        ExpansionTile.occupied = new List<Vector2>();
        TableBuyPreview.purchased_slots = new List<TableBuyPreview>();
    }


    /// <summary>
    /// We have to load the world stuff in a weird way because each part is going to require other parts to be initialized <i>in scene</i>, which we can't force.
    /// 
    /// The main case for this is with expansion tiles. We have to load the tiles, then wait a fraction of a second for them to actually load in Unity, and THEN we can actually put the tables on them.
    /// Otherwise, because the table slots (TableBuyPreview) are nested in their prefab, they won't have time to initialize (hit Awake)
    /// </summary>
    /// <param name="data">The playerdata we're loading</param>
    public async static void LoadWorld(PlayerData data)
    {
        Instance.LoadAllTiles(data.tiles);
        await System.Threading.Tasks.Task.Delay(50); // wait 100 milliseconds, then load. Using Threading instead of WaitForSeconds + Coroutines because I fear the Garbage Collection Man.
        Instance.LoadAllTables(data.tables);
    }

    // all purchased tiles
    public void LoadAllTiles(List<ExpansionTile.SerializableTile> tiles)
    {


        // if we're loading tiles, delete the starter tile
        if(tiles.Count > 0)
        {
            Destroy(GetComponentInChildren<ExpansionTile>().gameObject); // get outta here
        }
        List<ExpansionTile> generated = new List<ExpansionTile>();
        foreach (ExpansionTile.SerializableTile tile in tiles)
        {
            GameObject fresh = Instantiate(expansionTilePrefab, this.transform);
            ExpansionTile newtile = fresh.GetComponent<ExpansionTile>();

            newtile.isPurchased = true;
            newtile.gridPosition = new Vector2(tile.PosX, tile.PosY);
            generated.Add(newtile);
            ExpansionTile.occupied.Add(newtile.gridPosition);
            TableBuyPreview[] slots = newtile.GetComponentsInChildren<TableBuyPreview>();
            TableBuyPreview.all_slots.AddRange(slots);
        }

        // go over the tiles we just made and generate their linked tiles. Do this after all the tiles are placed so we don't layer tiles
        foreach (ExpansionTile tile in generated)
        {
            tile.GenerateLinkedTiles();
            tile.AdjustViewLimits();
            ExpansionTile.purchasedTiles.Add(tile);
        }

    }

    public void LoadAllTables(List<TableBuyPreview.SerializableTableSlot> tables)
    {
        foreach(TableBuyPreview.SerializableTableSlot table in tables)
        {
            foreach(TableBuyPreview preview in TableBuyPreview.all_slots)
            {
                if(!preview.alreadyPurchased && preview.transform.position.Equals(new Vector3(table.x, table.y, table.z)))
                {
                    GameObject newtable = Instantiate(preview.options[table.boughtIndex].buyable_prefab, preview.transform.parent, true);
                    newtable.GetComponent<RectTransform>().position = preview.transform.position;
                    preview.plus_button.gameObject.SetActive(false);
                    preview.caroselIndex = table.boughtIndex;
                    preview.alreadyPurchased = true;
                }
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
