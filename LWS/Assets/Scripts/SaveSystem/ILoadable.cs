﻿using System.Collections;
using UnityEngine;

public interface ILoadable 
{
    void Load();
}