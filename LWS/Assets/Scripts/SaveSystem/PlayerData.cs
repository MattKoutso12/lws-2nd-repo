using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int dayCount;
    public int goldCount;
    public int shopLevel;

    public float repMult;

    public int emiliaScore;
    public float upScore;
    public int eMisc;
    public int emilia;

    public int kojiScore;
    public float goldScore;
    public int kMisc;
    public int koji;

    public int claudiaScore;
    public float orderScore;
    //public OrderWrapper cMisc;
    public int claudia;

    public int lucasScore;
    public float famScore;
    //public FamiliarWrapper lMisc;
    public int lucas;

    public int familiarUpgradePrice, familiarCap;
    public int runningPriceCrafting, runningPricePlanting;
    

    public List<ItemSlot.SerializableItemSlot> itemSlotData;
    public List<CrewSlot.SerializableCrewSlot> crewSlotData;
    public List<OrderWrapper> availableOrders;
    public List<OrderWrapper> taken_orders;
    public List<FamiliarWrapper.SerializableFamiliarWrapper> serializableFamiliars;
    public List<ExpansionTile.SerializableTile> tiles;
    public List<TableBuyPreview.SerializableTableSlot> tables;


    public PlayerData(List<Savable> saveables)
    {
        itemSlotData = new List<ItemSlot.SerializableItemSlot>();
        crewSlotData = new List<CrewSlot.SerializableCrewSlot>();
        serializableFamiliars = new List<FamiliarWrapper.SerializableFamiliarWrapper>();
        familiarUpgradePrice = FamiliarHouse.familiarUpgradePrice;
        runningPriceCrafting = TableBuyPreview.runningPriceCrafting;
        tiles = new List<ExpansionTile.SerializableTile>();
        tables = new List<TableBuyPreview.SerializableTableSlot>();


        foreach(Savable sys in saveables)
        {
            sys.Save(this);
        }
    }
}
