﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance { get; private set; }

    public List<Savable> saveables;
    public string newGameFile;
    public string gameFile;

    public static bool isNewGame;
    public static bool inGame;

    public delegate void SaveEvent(SaveManager sender);
    public static event SaveEvent OnPreSave, OnPostSave, OnPreLoad, OnPostLoad;

    private void Awake()
    {
        Debug.Log("SAVEMANAGER AWAKE! " + this.GetInstanceID(), this);
        if (Instance == null)
        {
            Instance = this;
            saveables = new List<Savable>();
            DontDestroyOnLoad(gameObject); 
        }
        else
        {
            Destroy(gameObject); // get rid of this script instance
        }
    }

    private void OnDestroy()
    {
        PersistantManager.OnDayChanged -= OnDayChanged;
    }



    private void OnLevelWasLoaded(int level)
    {
        if(level == SceneManager.GetSceneByName("LWS").buildIndex)
        {
            if (!isNewGame)
                LoadPlayerData(gameFile);

            PersistantManager.OnDayChanged += OnDayChanged;
        }
        else
        {
            PersistantManager.OnDayChanged -= OnDayChanged;
        }
    }

    // Save on new day, but only if we're not in the tutorial
    private void OnDayChanged(object sender, System.EventArgs e)
    {
        if(!TutorialRunner.Instance.tutorialActive)
        {
            SavePlayerData();
        }
    }

    public void StartNewGame(string path)
    {
        SceneManager.LoadScene(sceneName: "LWS");
        isNewGame = true;
        newGameFile = path;
        inGame = true;
    }

    public void LoadGame(string path)
    {
        SceneManager.LoadScene(sceneName: "LWS");
        isNewGame = false; // Mark if we're starting a new game, for one-time events to get initialized
        gameFile = path;
        inGame = true;
    }

    /// <summary>
    /// Saves the game
    /// </summary>
    public void SavePlayerData()
    {
        if(gameFile.Equals(""))
        {
            isNewGame = false; // we're now in a "saved" game, so it's not new anymore
            PersistantManager.isNewGame = false;
            gameFile = newGameFile;
        }

        Instance.saveables = new List<Savable>(); // clear out old references. This prevents old/destroyed stuff from still being saved
        OnPreSave?.Invoke(this);
        Debug.Log("SAVEMANAGER SAVING: " + this.GetInstanceID());
        SaveSystem.SavePlayer(ref gameFile, Instance.saveables);
        OnPostSave?.Invoke(this);
        
    }

    /// <summary>
    /// Loads the relevant save file
    /// </summary>
    public void LoadPlayerData(string file)
    {
        OnPreLoad?.Invoke(this);
        PlayerData data = SaveSystem.LoadPlayer(file);

        PersistantManager.Instance.dayCount = data.dayCount;
        PersistantManager.Instance.goldAmount = data.goldCount;


        if(data.shopLevel > 0)
        {
            PersistantManager.Instance.shopLevel = data.shopLevel-1;
            PersistantManager.Instance.IncreaseShopLevel();
        }
        PersistantManager.Instance.shopLevel = data.shopLevel;
        FamiliarHouse.familiarUpgradePrice = data.familiarUpgradePrice;
        TableBuyPreview.runningPriceCrafting = data.runningPriceCrafting;
        TableBuyPreview.runningPricePlanting = data.runningPricePlanting;
        FamiliarHouse.instance.famCap = data.familiarCap;

        foreach(ItemSlot.SerializableItemSlot slot in data.itemSlotData)
        {
            // Take each slot and match it to a REAL slot.
            Debug.Log($"SLOT: {slot} {slot.id} : {slot.item_name}");
            try
            {
                if (slot.item_name.Equals("EMPTY")) continue;
                else ItemSlot.id_to_slot[slot.id].SetHolding(new InventoryItem(slot.item_name, slot.amount)); // put stuff in slot
            }catch(KeyNotFoundException e)
            {
                Debug.LogError($"Load Error: Item Slot {slot.id} not found. Failed to place item {slot.item_name} x{slot.amount} inside.");
            }
        }


        FamiliarWrapper.allfamiliars = new List<FamiliarWrapper>(); // 
        foreach(FamiliarWrapper.SerializableFamiliarWrapper fam in data.serializableFamiliars)
        {
            FamiliarWrapper temp = new FamiliarWrapper(fam);
            Player.Instance.GetCrewInventory().AddCrew(temp);
            //CrewSlot.crew_slots[fam.slotID].SetHolding(temp); // set the slot where we saved this guy
        }

        WorldLoader.LoadWorld(data);

        // Now, load ORDERS!
        FROUI.Instance.LoadFrom(data);


        OnPostLoad?.Invoke(this);
    }
}