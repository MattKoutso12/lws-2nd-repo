using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using TMPro;
using UnityEngine.SceneManagement;

public class LoadState : MonoBehaviour
{
    [SerializeField] protected string refName;
    [SerializeField] private TextMeshProUGUI display;
    [SerializeField] private GameObject delete;
    [SerializeField] private GameObject copy;

    [SerializeField] private LoadState[] loadStates;

    private void Awake()
    {
        UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        string path = Application.persistentDataPath + refName;
        if (File.Exists(path))
        {
            display.text = gameObject.name;
            //return;
            StringBuilder builder = new StringBuilder();
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.Open);
            PlayerData data = formatter.Deserialize(fs) as PlayerData;

            builder.Append("Gold: ").Append(data.goldCount).AppendLine();
            builder.Append("Day: ").Append(data.dayCount).AppendLine();

            display.text = builder.ToString();
            delete.SetActive(true);
            copy.SetActive(true);
        }
        else
        {
            display.text = "Start a New Game";
            delete.SetActive(false);
            copy.SetActive(false);
        }
    }

    public void StartGame()
    {
        string path = Application.persistentDataPath + refName;

        if (File.Exists(path))
        {
            SaveManager.Instance.LoadGame(path);
            PersistantManager.isNewGame = false;
        }
        else
        {
            SaveManager.Instance.StartNewGame(path);
            PersistantManager.isNewGame = true;
        }
    }

    public void CopyGame()
    {
        string path = Application.persistentDataPath + refName;
        foreach (LoadState state in loadStates)
        {
            string newPath = Application.persistentDataPath + state.refName;
            if (File.Exists(newPath))
            {
                continue;
            }
            else
            {
                FileStream fsSource = new FileStream(path, FileMode.Open, FileAccess.Read);

                    // Read the source file into a byte array.
                    byte[] bytes = new byte[fsSource.Length];
                    int numBytesToRead = (int)fsSource.Length;
                    int numBytesRead = 0;
                    while (numBytesToRead > 0)
                    {
                        // Read may return anything from 0 to numBytesToRead.
                        int n = fsSource.Read(bytes, numBytesRead, numBytesToRead);

                        // Break when the end of the file is reached.
                        if (n == 0)
                            break;

                        numBytesRead += n;
                        numBytesToRead -= n;
                    }
                    numBytesToRead = bytes.Length;

                // Write the byte array to the other FileStream.
                FileStream fsNew = new FileStream(newPath, FileMode.Create, FileAccess.Write);
                fsNew.Write(bytes, 0, numBytesToRead);
                fsSource.Close();
                fsNew.Close();
                state.UpdateDisplay();
                break;
            }
        }
    }

    public void DeleteGame()
    {
        string path = Application.persistentDataPath + refName;

        if (File.Exists(path))
        {
            File.Delete(path);
            UpdateDisplay();
        }
    }
}
