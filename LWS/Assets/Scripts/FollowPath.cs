using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates visual representation for a familiar being assigned to a station.
/// </summary>
public class FollowPath : MonoBehaviour
{

    //declare all familiars

    //fox stuff
    public Animator foxAnimator;
    public GameObject fox;
    public Transform[] CraftingTableWaypoints;
    public Transform[] PlanterWaypoints;
    public float moveSpeed = 2f;
    bool foxCrafting = false;
    bool foxPlanting = false;
    bool foxWillMove = false;
    bool foxCraftingDone = false;
    bool foxPlantingDone = false;
    int waypointIndex = 0;

    //cat stuff
       


    // Start is called before the first frame update
    void Start()
    {
        
        foxAnimator.SetBool("isMoving", false);
        fox.transform.position = CraftingTableWaypoints[waypointIndex].transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (foxWillMove == true && foxCrafting == true)
        {
            foxAnimator.SetBool("isMoving", true);
            CraftingTableMove();
        }

        if (foxWillMove == true && foxPlanting ==true)
        {
            foxAnimator.SetBool("isMoving", true);
            PlanterMove();
        }

        if (foxCraftingDone == true)
        {
            foxAnimator.SetBool("isMoving", false);
            StopMovingCrafting();
        }

        if (foxPlantingDone == true)
        {
            foxAnimator.SetBool("isMoving", false);
            StopMovingPlanting();
        }
    }

    //waypoint system for fox at crafting table
    private void CraftingTableMove()
    {

        if (waypointIndex == CraftingTableWaypoints.Length)
        {
            fox.transform.position = CraftingTableWaypoints[waypointIndex-1].transform.position;
            foxCraftingDone = true;
            foxWillMove = false;

        }

        if (waypointIndex <= CraftingTableWaypoints.Length - 1)
        {
            fox.transform.position = Vector2.MoveTowards(fox.transform.position,
                CraftingTableWaypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);
            
        }

        if (fox.transform.position == CraftingTableWaypoints[waypointIndex].transform.position)
        {
            waypointIndex += 1;
        }

    }

    //waypoint system for fox at planter
    private void PlanterMove()
    {

        if (waypointIndex == PlanterWaypoints.Length)
        {
            fox.transform.position = PlanterWaypoints[waypointIndex - 1].transform.position;
            foxPlantingDone = true;
            foxWillMove = false;

        }

        if (waypointIndex <= PlanterWaypoints.Length - 1)
        {
            fox.transform.position = Vector2.MoveTowards(fox.transform.position,
                PlanterWaypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);

        }

        if (fox.transform.position == PlanterWaypoints[waypointIndex].transform.position)
        {
            waypointIndex += 1;
        }

    }

    //stops the waypoint system at the end
    private void StopMovingCrafting()
    {
        
        fox.transform.position = CraftingTableWaypoints[waypointIndex - 1].transform.position;
    }

    //stops the waypoint system at the end
    private void StopMovingPlanting()
    {
        fox.transform.position = PlanterWaypoints[waypointIndex - 1].transform.position;
    }

    /// <summary>
    /// Sets familiar to craft.
    /// </summary>
    public void ToggleFoxCrafting()
    {
        foxWillMove = true;
        foxCrafting = true;
    }

    /// <summary>
    /// Sets familiar to plant.
    /// </summary>
    public void ToggleFoxPlanting()
    {
        foxWillMove = true;
        foxPlanting = true;

    }

    /// <summary>
    /// Returns familar to the familiar house waypoint.
    /// </summary>
    public void RestartFox()
    {
        foxCraftingDone = false;
        foxPlantingDone = false;
        foxPlanting = false;
        foxCrafting = false;
        foxWillMove = false;
        foxAnimator.SetBool("isMoving", false);
        waypointIndex = 0;
        fox.transform.position = CraftingTableWaypoints[waypointIndex].transform.position;
    }


}

