﻿using System.Collections;
using UnityEngine;

public class BuySlot : ItemSlot
{
    [SerializeField] UI_Invetory invent;

    private void Awake()
    {
        // do nothing. prevents saving buyslots (since it's unnecessary)
        if(id_to_slot.ContainsKey(_id))
        {
            Debug.Log("BuySlot: Unloading this...");
            id_to_slot.Remove(_id);
        }
    }


    protected override bool CanHoldThis(IDraggableObject objectToHold)
    {

        return false;


        if (objectToHold == null) return true;
        if (base.CanHoldThis(objectToHold))
        {
            if (objectToHold.GetType() == typeof(InventoryItem)
                && this.IsEmpty()) // NOTE: right now, only inventory items can be sold
            {
                return true;
            }
        }
        return false;
    }

    public override bool TryTransfer(UISlot other)
    {

        if (!(other is BuySlot))
            return base.TryTransfer(other);

        if(other is ItemSlot)
        {
            return base.TryTransfer((ItemSlot)other);
        }
        return false;
    }

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);
        if (new_holding == null) return; // if we're empty, since this will be called again when we delete the item
        InventoryItem tempItem = (InventoryItem)new_holding;

        if (PersistantManager.Instance.TryPurchase(tempItem.amount))
        {
            invent.inventory.AddItem(tempItem);
            SetHolding(null);
        }
    }
}