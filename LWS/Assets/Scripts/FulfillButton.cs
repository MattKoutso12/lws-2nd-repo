using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Button to fulfill an order.
/// </summary>
public class FulfillButton : MonoBehaviour
{
    private Transform fulfillButton;
    private Transform fulfillScreen;
    private ItemSlot fulfillSlot;
    [SerializeField] UI_OrderLine uiOrderLine;
    [SerializeField] UI_Invetory invetory;
    private OrderWrapper order;

    private void Awake()
    {
        fulfillScreen = transform.Find("FulfillScreen");
        fulfillButton = fulfillScreen.Find("FB");
        fulfillSlot = fulfillScreen.Find("ItemSlot").GetComponent<ItemSlot>();
    }

    private void Start()
    {
        OrderDragDrop oDD = gameObject.GetComponent<OrderDragDrop>();
        order = oDD.GetOrder();
    }

    /// <summary>
    /// Toggles fulfill button on ond off.
    /// </summary>
    public void ToggleFulfill()
    {
        if (fulfillScreen.gameObject.activeInHierarchy)
        {
            fulfillScreen.gameObject.SetActive(false);
        }
        else
        {
            fulfillScreen.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Attempts to fulfill an order by checking for and removed required items.
    /// </summary>
    public void Fulfill()
    {
        InventoryItem fulfillItem = (InventoryItem) fulfillSlot.GetHeldObject(); 
        Debug.Log("Fulfill slot contains " + fulfillItem);
        bool lilyPichu = false;
        if (!invetory.gameObject.activeInHierarchy)
        {
            invetory.gameObject.SetActive(true);
            lilyPichu = true;
        }


        bool fulfilled = false;
        foreach (InventoryItem l in order.products)
        {
            if(fulfillItem != null){
                if (l == fulfillItem)
                {
                    if(fulfillItem.IsStackable())
                    {
                        if(fulfillItem.amount > 1){
                            fulfillItem.amount -= 1;
                            invetory.inventory.AddItem(fulfillItem);
                            fulfillSlot.SetHolding(null);
                            fulfilled = true;
                            break;
                        }
                        else
                        {
                            fulfillSlot.SetHolding(null);
                            fulfilled = true;
                            break;
                        }
                    }
                    else
                    {
                        fulfillSlot.SetHolding(null);
                        fulfilled = true;
                        break;
                    }
                }
            }
            
        }

        if (lilyPichu)
        {
            invetory.gameObject.SetActive(false);
        }

        if (fulfilled)
        {
            PersistantManager.Instance.AddGold(order.reward);
            if(PersistantManager.Instance.goldTouch)
            {
                PersistantManager.Instance.AddGold((int) (order.reward * 0.25));
            }
            PersistantManager.Instance.HideTooltip();
            uiOrderLine.orderLine.RemoveOrder(order);
        }
    }
}