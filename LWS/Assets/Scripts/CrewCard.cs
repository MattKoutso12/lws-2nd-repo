using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Card for a hireable familiar.
/// </summary>
public class CrewCard : MonoBehaviour
{
    [SerializeField] List<Crew> crews; //List of comparative crew members
    [SerializeField] TextMeshProUGUI abilityText; //reference value to the ability text object
    [SerializeField] int cardLevel;
    [SerializeField] List<GameObject> dissolveList; //list of game objects to dissolve when hired


    [SerializeField] TextMeshProUGUI name_text, ability_description;

    [SerializeField] public CrewCardManager cCM; //reference to the CCM
    public UI_CrewInventory crewInvent; //reference to the crew inventory
    private FamiliarHouse fHouse; //reference to the familiar house
    private FamiliarWrapper crew; //reference to this cards specific crew member
    [SerializeField] GameObject renamePopup;
    [SerializeField] private Image image; //reference to this crew members specific profile picture
    [SerializeField] private GameObject fullError;


    public bool acceptedToday = false;
    private bool shouldDissolve = false;
    private float dissolveAmount = 0.4f;


    private void Start()
    {
        fHouse = FamiliarHouse.instance.gameObject.GetComponent<FamiliarHouse>();
        PersistantManager.OnDayChanged += Instance_OnDayChanged;
        crewInvent = cCM.crewInvent;

        foreach(GameObject toDissolve in dissolveList)
        {
            Material tempMat = Instantiate<Material>(toDissolve.GetComponent<Image>().material);
            tempMat.color = toDissolve.GetComponent<Image>().color;
            toDissolve.GetComponent<Image>().material = tempMat;
        }
    }

    private void Update()
    {
        if(shouldDissolve == true)
        {
            foreach (GameObject toDissolve in dissolveList)
            {
                toDissolve.GetComponent<Image>().material.SetFloat("_Thresh", dissolveAmount);
            }
            dissolveAmount += (Time.deltaTime / 2);
            if(dissolveAmount >= 0.8f)
            {
                dissolveAmount = 0.4f;
                shouldDissolve = false;
                foreach(GameObject toDissolve in dissolveList)
                {
                    toDissolve.GetComponent<Image>().material.SetFloat("_Thresh", 0.4f);
                }
                Detsroy();
            }

            
        }
    }

    private void Instance_OnDayChanged(object sender, System.EventArgs e)
    {
        acceptedToday = false;
    }

    /// <summary>
    /// Disables card.
    /// </summary>
    public void Detsroy()
    {
        gameObject.SetActive(false);
    }

    public void GenerateNewCrew()
    {
        int randInt = Random.Range(0, crews.Count);
        crew = new FamiliarWrapper(crews[randInt]);
        crew.name = RandomName();
        image.sprite = crew.crew.sprite;
        name_text.text = crew.name;
        SetAbilityText();

        crew.crew.TriggerViewEvent();
    }

    public string RandomName()
    {
        var lines = System.IO.File.ReadAllLines(Application.streamingAssetsPath + "/first-names.txt");
        return lines[Random.Range(0, lines.Length)];
    }

    public void GenerateNameInput()
    {
        renamePopup.transform.Find("NameInputField").Find("Text Area").Find("Placeholder").GetComponent<TextMeshProUGUI>().text = crew.name;
        renamePopup.SetActive(true);
    }

    /// <summary>
    /// Adds this crew member to the crew inventory.
    /// </summary>
    public void HireCrew()
    {
        string pending_name = renamePopup.transform.Find("NameInputField").Find("Text Area").Find("Text").GetComponent<TextMeshProUGUI>().text;
        if(!(pending_name.Length <= 1))
        {
            Debug.Log("NAME IS [" + pending_name + "]");
            crew.name = pending_name;
        }
        renamePopup.transform.Find("NameInputField").Find("Text Area").Find("Text").GetComponent<TextMeshProUGUI>().text = "";
        renamePopup.transform.Find("NameInputField").GetComponent<TMP_InputField>().text = "";
        renamePopup.SetActive(false);
        acceptedToday = true;
        PersistantManager.Instance.hiredCrews[cardLevel].Add(crew.crew);
        crewInvent.inventory.AddCrew(crew);
        CouncilReputation.Instance.FamiliarScore();
        CouncilReputation.Instance.LucasMiscTest(crew);

        shouldDissolve = true;

        FamiliarHouse.instance.famCount++;

        //checks if familiar house is active currently
        /*
        if (fHouse.gameObject.activeInHierarchy)
        {
            //sets the crew member to active
            fHouse.SetActive(crew);
        }
        else
        {
            //same thing
            fHouse.gameObject.SetActive(true);
            fHouse.SetActive(crew);
            fHouse.gameObject.SetActive(false);
        }
        */
    }

    // Randomly generates the crew member's ability
    private void SetAbilityText()
    {
        abilityText.text = "";
        ability_description.text = "";
        //abilityText.text = "Ability: " + crew.abilityName;
        //ability_description.text = crew.ability;
    }
}
