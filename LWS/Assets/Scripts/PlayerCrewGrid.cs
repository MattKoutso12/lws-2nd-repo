using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCrewGrid : MonoBehaviour
{


    CrewSlot[] grid;

    // Start is called before the first frame update
    void Start()
    {

        grid = GetComponentsInChildren<CrewSlot>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnGUI()
    {

        for(int i = 0; i < grid.Length; i++)
        {
            grid[i].ForceInternalID(i); // set up sequential IDs, which SHOULD bind us nicely to existing slots. SHOULD!!
        }
        
    }
}
