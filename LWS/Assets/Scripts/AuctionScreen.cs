using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AuctionScreen : MonoBehaviour
{
    public static AuctionScreen Instance { get; private set; }

    private bool on = false;
    private GameObject stockButton;
    private TextMeshProUGUI basePrice;
    private TextMeshProUGUI adjustedPrice;
    private TextMeshProUGUI stockName;
    [SerializeField] List<Item> itemsNonCraftable;
    private Item stockedItem;
    private int stockedAmt;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        stockButton = this.transform.GetChild(0).gameObject;
        basePrice = this.transform.GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>();
        adjustedPrice = this.transform.GetChild(1).GetChild(2).GetComponent<TextMeshProUGUI>();
        stockName = this.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();

        RefreshStock();

        this.gameObject.SetActive(false);
    }

    public void ToggleAuctionScreen()
    {   
        on = !on;
        this.gameObject.SetActive(on);

        if(on)
        {
            Player.Instance.SetMoveLock(true);
        }
        else
        {
            Player.Instance.SetMoveLock(false);
        }
    }

    public void RefreshStock()
    {
        stockButton.GetComponent<Image>().sprite = null;
        stockedItem = itemsNonCraftable[(UnityEngine.Random.Range(0, itemsNonCraftable.Count - 1))];

        stockedAmt = 3;
        stockName.text = stockedItem.getDisplayName();
        basePrice.text = ("<s>" + stockedItem.SellPrice.ToString() + "</s>");
        adjustedPrice.text = (stockedItem.SellPrice / 2).ToString();
        stockButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = stockedAmt.ToString();
        stockButton.GetComponent<Image>().sprite = stockedItem.sprite;
    }

    public void BuyStock()
    {
        if(PersistantManager.Instance.TryPurchase(stockedItem.SellPrice / 2) && stockedAmt > 0)
        {
            Player.Instance.GetInventory().AddItem(new InventoryItem(stockedItem, 1));
            stockedAmt--;
            stockButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = stockedAmt.ToString();
        }
    }

}
