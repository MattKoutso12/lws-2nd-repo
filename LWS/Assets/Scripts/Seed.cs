using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// Represents a seed that can be grown in a planter.
/// </summary>
[CreateAssetMenu(fileName = "New Seed", menuName = "Items/Seed")]
public class Seed : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private int buyPrice;
    [SerializeField] private string description;
    //[SerializeField] private ItemType itemCat;
    private Color textColor = Color.green;

    public string Name { get { return name; } }
    public string Description { get { return description; } set { description = value; } }
    public int BuyPrice { get { return buyPrice; } }
    //public ItemType ItemType { get { return itemCat; } }
    public Color Color { get { return textColor; } }

    #region ColouredName comment
    /// <summary>
    /// Returns hexadecimal color string of the seed.
    /// </summary>
    /// <value> Hexadecimal color string. </value>
    #endregion
    public string ColouredName
    {
        get
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(textColor);
            return $"<color=#{hexColour}>{Name}</color>";
        }
    }

    #region GetTooltipInfo
    /// <summary>
    /// Returns tooltip info of the seed.
    /// </summary>
    /// <returns> Tooltip info. </returns>
    #endregion
    public string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        builder.Append(ColouredName).AppendLine();
        builder.Append("Hire Price: ").Append(buyPrice).Append(" Gold").AppendLine();
        builder.Append("Ability: ").Append(Description).AppendLine();

        return builder.ToString();
    }
}
