using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


/// <summary>
/// Simple script which tracks the player's gold amount and displays it
/// using a TextMeshPro component.
/// </summary>
[RequireComponent(typeof(TextMeshProUGUI))]
public class GoldCounter : MonoBehaviour
{
    TextMeshProUGUI text;


    /// <summary>
    /// The text to put in front of the current gold amount
    /// </summary>
    [SerializeField]
    public string prepend;

    /// <summary>
    /// The text to put after the current gold amount
    /// </summary>
    [SerializeField]
    public string append;

    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    private void OnGUI()
    {
        text.text = prepend + PersistantManager.Instance.goldAmount + append;
    }
}
