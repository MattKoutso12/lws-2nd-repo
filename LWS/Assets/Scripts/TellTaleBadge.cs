using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TellTaleBadge : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ttbText;
    private void Awake()
    {
        DialougeCommands.OnSpawnTTB += UpdateText;
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        DialougeCommands.OnSpawnTTB -= UpdateText;
    }

    private void UpdateText(object sender, DialougeCommands.SpawnTTBEventArgs e)
    {
        gameObject.SetActive(true);
        ttbText.text = e.speaker + " is " + e.mood;
    }

    //Animation Events
    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
