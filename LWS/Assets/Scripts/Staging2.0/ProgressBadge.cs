using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProgressBadge : ItemBadge
{
    [SerializeField]
    public ItemBadge red_badge, normal_badge;

    [SerializeField]
    public Image mask;

    [SerializeField]
    public ProductionSystem system;

    [SerializeField]
    public float StrobeRate = 1.8f;

    bool strobe = false;

    public override void SetItem(InventoryItem i)
    {
        
    }



    private void Start()
    {
        system.OnProgressMade += OnProgress;
        
    }

    private void OnGUI()
    {
        InventoryItem item = system.GetCurrentResult();
        if(item != null)
        {
            amount_text.text = $"x{system.GetCurrentResult().itemType.outputYield}";
            red_badge.SetItem(item);
            normal_badge.SetItem(item);
            red_badge.SetColor(Color.red);
            strobe = true;
        }
        else
        {
            red_badge.SetItem(null);
            normal_badge.SetItem(null);
            SetProgress(0f);
            strobe = false;
        }
    }

    private void Update()
    {

        if(strobe)
        {
            float strobeVal = Mathf.Clamp01((Mathf.Sin(Time.time * StrobeRate) + 1f)/2f);
            red_badge.SetColor(new Color(1, strobeVal , strobeVal, 1));
            
        }
    }

    private void OnProgress(ProductionSystem sender, params object[] extras)
    {
        InventoryItem i = (InventoryItem) extras[1];
        red_badge.SetItem(i);
        normal_badge.SetItem(i);
        if (i != null)
        {
            red_badge.SetColor(Color.red);
            float prog = (float)((int)extras[0]) / (float)sender.GetCurrentFullDuration();
            SetProgress(prog);
        }
    }

    public void SetProgress(float f)
    {
        mask.fillAmount = f;
    }
}
