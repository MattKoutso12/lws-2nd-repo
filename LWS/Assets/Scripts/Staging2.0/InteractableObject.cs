using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Represents an object in the shop that can be interacted with
/// </summary>
[RequireComponent(typeof(Button))]
public class InteractableObject : MonoBehaviour
{
    [SerializeField]
    public InteractableSystem panel;

    public delegate void InteractableObjectEvent();
    public static event InteractableObjectEvent CloseAll;

    private void Start()
    {
        InteractableObject.CloseAll += InteractableObject_CloseAll;
    }

    private void InteractableObject_CloseAll()
    {
        this.Close();
    }

    public static void CloseAllPanels()
    {
        CloseAll?.Invoke();
    }

    public void Open()
    {
        CloseAllPanels();
        panel.gameObject.SetActive(true);

    }

    bool toggled = false;
    public void Toggle()
    {
        toggled = !toggled;

        if (toggled) Open();
        else if (!toggled) Close();
    }

    public void Close()
    {
        panel.gameObject.SetActive(false);
    }


}
