using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class TableBuyPreview : MonoBehaviour
{

    /// <summary>
    ///  A struct for a thing you can buy. Stores the prefab of the thing as well as the cost.
    /// </summary>
    [System.Serializable]
    public struct Option
    {
        public GameObject buyable_prefab;
        public int cost;
        public string prefab_type;
    }

    public static List<TableBuyPreview> purchased_slots = new List<TableBuyPreview>();
    public static List<TableBuyPreview> all_slots = new List<TableBuyPreview>();

    [SerializeField]
    public List<Option> options;

    [SerializeField]
    public TextMeshProUGUI priceText;

    [SerializeField]
    public Button plus_button;

    public delegate void BuyPreviewEvent(TableBuyPreview sender);
    public static event BuyPreviewEvent OnPreviewOpened;

    bool purchasingMode = false;
    public bool alreadyPurchased = false;

    public static int runningPriceCrafting = 30;
    public static int runningPricePlanting = 20;

    static int tablesBought = 0;
    
    public int caroselIndex = 0;

    Image preview;

    [System.Serializable]
    public struct SerializableTableSlot
    {
        public float x, y, z;
        public int boughtIndex;
    }

    private void Awake()
    {
        if(!all_slots.Contains(this))
            all_slots.Add(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        transform.GetChild(0).gameObject.SetActive(false);

        preview = GetComponent<Image>();

        TableBuyPreview.OnPreviewOpened += TableBuyPreview_OnPreviewOpened;
        ShiftPlanning.OnPlanningDisabled += ShiftPlanning_OnPlanningDisabled;

        if(alreadyPurchased)
        {
            purchased_slots.Add(this);
        }
    }

    private void OnDestroy()
    {
        TableBuyPreview.OnPreviewOpened -= TableBuyPreview_OnPreviewOpened;
        ShiftPlanning.OnPlanningDisabled -= ShiftPlanning_OnPlanningDisabled;
        all_slots.Remove(this);
    }

    private void ShiftPlanning_OnPlanningDisabled()
    {
        if(purchasingMode && !alreadyPurchased)
        {
            CloseBuyMode();
        }
    }

    private void TableBuyPreview_OnPreviewOpened(TableBuyPreview sender)
    {
        if(sender != this && purchasingMode && !alreadyPurchased)
        {
            CloseBuyMode();
        }
    }

    public SerializableTableSlot GetSerializableVersion()
    {
        SerializableTableSlot slot = new SerializableTableSlot();
        slot.x = this.transform.position.x;
        slot.y = this.transform.position.y;
        slot.z = this.transform.position.z;
        slot.boughtIndex = caroselIndex;
        return slot;
    }

    public void MoveCarosel(int direction)
    {
        if (options.Count <= 0)
            return;

        caroselIndex += direction;
        if(caroselIndex >= options.Count)
        {
            caroselIndex = 0; // wrap to the start of the list
        }

        if(caroselIndex < 0)
        {
            caroselIndex = options.Count - 1; // wrap to end of the list
        }

        DisplayPreview(options[caroselIndex].buyable_prefab); // get the prefab selected
    }

    void DisplayPreview(GameObject prefab)
    {
        Sprite preview = prefab.GetComponent<Image>().sprite;

        if(options[caroselIndex].prefab_type.Equals("planting"))
        {
            Option newOpt = options[caroselIndex];
            newOpt.cost = runningPricePlanting;
            options[caroselIndex] = newOpt;
            priceText.text = "<sprite=81> " + runningPricePlanting;
        }
        else if(options[caroselIndex].prefab_type.Equals("crafting"))
        {
            Option newOpt = options[caroselIndex];
            newOpt.cost = runningPriceCrafting;
            options[caroselIndex] = newOpt;
            priceText.text = "<sprite=81> " + runningPriceCrafting;
        }

        if(preview != null)
        {
            this.preview.sprite = preview;
            this.preview.GetComponent<RectTransform>().sizeDelta = prefab.GetComponent<RectTransform>().sizeDelta;
        }
            
    }

    public void OpenBuyMode()
    {
        if (alreadyPurchased) return;
        purchasingMode = true;
        transform.GetChild(0).gameObject.SetActive(true);
        DisplayPreview(options[0].buyable_prefab);
        plus_button.gameObject.SetActive(false);
        preview.enabled = true;
        caroselIndex = 0;
        OnPreviewOpened?.Invoke(this);
    }

    public void CloseBuyMode()
    {
        if (alreadyPurchased) return;
        purchasingMode = false;
        transform.GetChild(0).gameObject.SetActive(false);
        plus_button.gameObject.SetActive(true);
        preview.enabled = false;
    }

    public void Purchase()
    {
        if(PersistantManager.Instance.TryPurchase(options[caroselIndex].cost))
        {
            if(options[caroselIndex].prefab_type.Equals("planting"))
            {
                runningPricePlanting += 20;
            }
            else if(options[caroselIndex].prefab_type.Equals("crafting"))
            {
                runningPriceCrafting += 30;
            }
            else
            {
                Debug.Log("Unexpected upgrade prefab type, prices not increasing.");
            }
            GameObject newBench = Instantiate(options[caroselIndex].buyable_prefab, transform.parent);
            transform.GetChild(0).gameObject.SetActive(false);
            this.plus_button.gameObject.SetActive(false);
            alreadyPurchased = true;
            CouncilReputation.Instance.UpgradeScore();
            CouncilReputation.Instance.EmiliaMiscTest();
            PersistantManager.Instance.IncreaseShopLevel();
            purchased_slots.Add(this);
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
