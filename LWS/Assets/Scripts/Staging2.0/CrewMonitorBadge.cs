using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


/// <summary>
///  A simple script which shows the linked system's crew when it is manned, and is invisible otherwise.
/// </summary>
[RequireComponent(typeof(Image))]
public class CrewMonitorBadge : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    public ProductionSystem system;

    Image image;


    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }




    private void OnGUI()
    {
        FamiliarWrapper crew = system.GetCrew();
        if(crew != null)
        {
            image.color = Color.white;
            image.sprite = crew.crew.icon;
        }
        else
        {
            image.color = Color.clear;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PersistantManager.Instance.HideTooltip();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        FamiliarWrapper crew = system.GetCrew();

        if(crew != null)
            PersistantManager.Instance.ShowTooltip(crew);
    }
}
