using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// A slot which holds a familiar. Just a simple base class we can jump off of later if we need extra base functionality.
/// </summary>
public class FamiliarSlot : UISlot
{
    protected override bool CanHoldThis(IDraggableObject objectToHold)
    {
        if (objectToHold is FamiliarWrapper)
            return base.CanHoldThis(objectToHold);
        else return false;
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {

        base.OnBeginDrag(eventData);
        if(holding != null)
        {
            ((FamiliarWrapper)holding).crew.TriggerViewEvent();
        }
    }
}

public class WorkSlot : FamiliarSlot
{
    [SerializeField]
    public ProductionSystem linked_system;

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);
    }
}
