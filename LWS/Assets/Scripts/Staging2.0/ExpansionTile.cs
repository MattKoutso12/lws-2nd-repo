using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using TMPro;
using UnityEngine.Events;

public class ExpansionTile : MonoBehaviour
{
    public static readonly float X_SIZE = 110;
    public static readonly float Y_SIZE = 70;

    public static List<Vector2> occupied = new List<Vector2>();

    public static HashSet<ExpansionTile> purchasedTiles = new HashSet<ExpansionTile>();

    [SerializeField]
    public GameObject prefab;

    [SerializeField]
    public TextMeshProUGUI costText;

    [SerializeField]
    public Vector2 gridPosition;

    [SerializeField]
    public bool isPurchased;

    [Header("Open Sides")]
    [SerializeField]
    public bool North, South, East, West;

    public int price; // price in gold

    public static int running_price;

    public UnityEvent OnPurchased;

    Transform tile, plus;
    SortingGroup layer;

    [System.Serializable]
    public struct SerializableTile
    {
        public float PosX, PosY;
    }

    private void Awake()
    {

    }



    // Start is called before the first frame update
    void Start()
    {
        tile = transform.GetChild(0);
        plus = transform.GetChild(1);
        layer = GetComponent<SortingGroup>();
        occupied.Add(gridPosition);


        // PRICE EQUATION
        running_price = 50 * occupied.Count; 

        if (isPurchased)
        {
            tile.gameObject.SetActive(true);
            plus.gameObject.SetActive(false);

        }
        else
        {
            tile.gameObject.SetActive(false);
            plus.gameObject.SetActive(true);
        }

    }

    public SerializableTile GetSerializedVersion()
    {
        SerializableTile tile = new SerializableTile();
        tile.PosX = this.gridPosition.x;
        tile.PosY = this.gridPosition.y;
        return tile;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = GridToWorld(gridPosition); // snap to grid
        if (layer == null) layer = GetComponent<SortingGroup>();
        layer.sortingOrder = 100 - (int) transform.position.y;

        if (isPurchased)
        {
            tile.gameObject.SetActive(true);
            plus.gameObject.SetActive(false);
        }
        else
        {
            tile.gameObject.SetActive(false);
            plus.gameObject.SetActive(true);
        }
    }

    private void OnGUI()
    {
        if(!isPurchased)
        {
            // sprite 81 is the gold pile
            costText.text = "Buy Room:  <size=10> <sprite=81>" + price;

            if(PersistantManager.Instance.goldAmount >= price)
            {
                costText.color = Color.green;
            }
            else
            {
                costText.color = Color.red;
            }

        }
    }



    public void Purchase()
    {
        if(PersistantManager.Instance.TryPurchase(price))
        {
            isPurchased = true;
            GenerateLinkedTiles();
            AdjustViewLimits();
            purchasedTiles.Add(this);
            OnPurchased?.Invoke();
            PersistantManager.Instance.IncreaseShopLevel();
        }
        else
        {
            // Can't Afford! Put animations, etc, here.
        }


    }

    void OpenAllSides()
    {
        North = true;
        South = true;
        East = true;
        West = true;
    }

    public void AdjustViewLimits()
    {
        float minX;
        float minY, maxY;

        // HARDCODED MARGIN
        float margin = 30f;

        // make sure we don't go *more* restrictive than the already-existing limits.
        minX = Player.Instance.minX;
        minY = Player.Instance.minY;
        maxY = Player.Instance.maxY;

        // Find Limitss
        foreach(Vector2 o in occupied)
        {
            Vector2 position = GridToWorld(o); // get the world position

            if(position.x - margin < minX)
            {
                minX = position.x - margin;
            }

            if(position.y - margin < minY)
            {
                minY = position.y - margin;
            }

            if(position.y + margin > maxY)
            {
                maxY = position.y + margin;
            }
        }

        
        Player.Instance.minX = minX;
        Player.Instance.minY = minY;
        Player.Instance.maxY = maxY;

    }

    bool IsSpotOccupied(Vector2 pos)
    {

        foreach(Vector2 taken in occupied)
        {
            if(Mathf.Abs((taken - pos).magnitude) < 1f)
            {
                return true;
            }
        }

        return false;
    }

    bool generated = false;
    public void GenerateLinkedTiles()
    {
        if(North)
        {
            GenerateLinkedTile(gridPosition.x - 1f, gridPosition.y + 1f);
        }
        if(South)
        {
            GenerateLinkedTile(gridPosition.x + 1f, gridPosition.y - 1f);

        }
        if(East)
        {
            GenerateLinkedTile(gridPosition.x + 1f, gridPosition.y + 1f);
        }
        if(West)
        {
            GenerateLinkedTile(gridPosition.x - 1f, gridPosition.y - 1f);
        }
        generated = true;
    }

    public void GenerateLinkedTile(float PosX, float PosY)
    {
        // bounds
        if (PosX > 0 && PosY > -2) return;
        if (PosX > 1 || PosX < -9) return;
        if (PosY > -1 || PosY < -9) return;

        Vector2 pos = new Vector2(PosX, PosY);
        if (IsSpotOccupied(pos)) return;
        GameObject new_tile = Instantiate(prefab, this.transform.parent);
        ExpansionTile tile = new_tile.GetComponent<ExpansionTile>();
        tile.OpenAllSides();
        tile.isPurchased = false;
        tile.gridPosition = pos;
        tile.price = this.price + 50;

    }

    public static Vector3 GridToWorld(Vector2 gridPosition)
    {
        return new Vector3(gridPosition.x * X_SIZE, gridPosition.y * Y_SIZE);
    }
}
