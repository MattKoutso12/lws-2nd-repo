using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionSystem : InteractableSystem
{

    [SerializeField]
    public WorkSlot crewslot;

    [SerializeReference]
    public GameObject planning_version;

    FamiliarWrapper crew;

    InventoryItem current_result;
    int current_duration;
    int duration_modifier = 0;
    
    bool checkPlantedMoonleaf = false;
    public static bool checkOutputMoonleaf = false;
    bool checkCraftingPotion = false;
    public static bool checkOutputPotion = false;

    [SerializeField]
    public CraftedItem.CraftingType type;

    public delegate void ProductionEvent(ProductionSystem sender, params object[] extras);
    public event ProductionEvent OnProgressMade, OnPostCompletion;

    /// <summary>
    /// When the contents of a slot change, we track those changes here and refesh relevant info only when needed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="objects"></param>
    protected override void OnSlotContentsChanged(UISlot sender, params IDraggableObject[] objects)
    {
        base.OnSlotContentsChanged(sender, objects);
        InventoryItem item = null;
        current_duration = CheckRecipe(out item);
    }

    /// <summary>
    /// Initializes the system and subscribes to crew slot changes.
    /// </summary>
    protected override void Init()
    {
        base.Init();
        crewslot.OnContentsSet += OnCrewChanged;
        ShiftPlanning.OnPlanningEnabled += OnPlanningEnabled;
        ShiftPlanning.OnPlanningDisabled += OnPlanningDisabled;

        if(ShiftPlanning.InPlanningMode())
        {
            OnPlanningEnabled(); // if we're already in planning mode, open your crew slot screen! This happens when the player buys a table
        }
        else
        {
            OnPlanningDisabled();
        }
    }

    protected override void UnInit()
    {
        base.UnInit();
        crewslot.OnContentsSet -= OnCrewChanged;
        ShiftPlanning.OnPlanningEnabled -= OnPlanningEnabled;
        ShiftPlanning.OnPlanningDisabled -= OnPlanningDisabled;
    }



    /// <summary>
    /// When we exit planning mode
    /// </summary>
    private void OnPlanningDisabled()
    {
        if(planning_version != null)
            planning_version?.SetActive(false); // disable the planning version but don't necessarily close the crafting screen.
       // planning_version.transform.SetAsLastSibling(); // move it down in hierarchy so it isn't the active panel anymore
    }

    /// <summary>
    /// When we enter planning mode
    /// </summary>
    private void OnPlanningEnabled()
    {
        planning_version.SetActive(true);
        SetState(false);
        //planning_version.transform.SetAsFirstSibling(); // move it up in the hierarchy so it is the active panel and will be opened via SetState, which makes Child 0 active
    }

    /// <summary>
    /// Sets the crew when manned/un-manned
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="objects"></param>
    private void OnCrewChanged(UISlot sender, params IDraggableObject[] objects)
    {
        crew = (FamiliarWrapper) objects[1];
        if(crew != null)
        {
            
        }
    }

    /// <summary>
    /// The item currently being crafted, if any.
    /// </summary>
    /// <returns></returns>
    public InventoryItem GetCurrentResult()
    {
        return current_result;
    }

    public int GetCurrentDuration()
    {
        return current_duration + duration_modifier;
    }

    /// <summary>
    /// Modifies the duration of the current crafting recipe
    /// </summary>
    /// <param name="modifier">How many days to modify the recipe by</param>
    public void AddDurationModifier(int modifier)
    {
        if(current_duration != -1)
        {
            duration_modifier += modifier;
        }
        else
        {
            duration_modifier = 0;
        }
    }

    /// <summary>
    /// Checks the recipe's validity based on the crafting grid but doesn't produce the item. Side effect: updates current_result and estimated time
    /// </summary>
    /// <returns>The crafting result</returns>
    public int CheckRecipe(out InventoryItem item)
    {
        item = null;
        foreach (CraftedItem i in CraftedItem.AllRecipes.Keys)
        {
            if (i.crafting_type != type) continue;

            bool ready = true;
            for (int j = 0; j < slots.Length; j++)
            {
                Item slotContents = GetContents(j)?.itemType;
                Item[] recipe = CraftedItem.AllRecipes[i];

                if (recipe[j] != slotContents)
                {
                    ready = false;
                }

            }

            if (ready)
            {
                if(i.getIDName().Equals("moonleaf") && !checkPlantedMoonleaf && TutorialRunner.Instance.tutorialActive)
                {
                    checkPlantedMoonleaf = true;
                    TutorialRunner.Instance.PlantingCheck("plantSeed");
                }
                if(i.getIDName().Equals("potion_of_water_breathing") && !checkCraftingPotion && TutorialRunner.Instance.tutorialActive)
                {
                    checkCraftingPotion = true;
                    TutorialRunner.Instance.CraftingCheck("setCrafting");
                }

                if(current_result != null && current_result.itemType == i)
                {
                    item = current_result;
                    return current_duration; // days left
                }
                else
                {
                    current_result = new InventoryItem(i);
                    item = current_result;
                    current_duration = i.days_to_craft;
                    return i.days_to_craft;
                }
            }
        }


        current_result = null;
        current_duration = -1;
        return -1; // -1 days 
    }


    /// <summary>
    /// Refresh the current output. Should not need to use this often, if at all outside this class- but it's here as a nuclear option.
    /// </summary>
    public void Refresh()
    {
        InventoryItem item = null;
        CheckRecipe(out item);
    }

    /// <summary>
    /// Gives the <b>full</b> duration of the current recipe, so not the days remaining, but how many days this recipe has taken/will take in TOTAL.
    /// 
    /// In other words, it's the second number in "3/5 Days Left". When abilities are fully integrated, this will reflect ability changes
    /// </summary>
    /// <returns></returns>
    public int GetCurrentFullDuration()
    {
        InventoryItem item = null;
        CheckRecipe(out item);

        if (item == null) return -1;

        int dur = ((CraftedItem)item.itemType).days_to_craft;

        //TODO: Process ability modifications here

        return dur;

    }

    /// <summary>
    /// Gives the crew currently manning this system
    /// </summary>
    /// <returns></returns>
    public FamiliarWrapper GetCrew()
    {
        return crew;
    }

    /// <summary>
    /// Tries to produce the item. If its production is done, returns true. If it has more days left, advances the recipe by one day.
    /// </summary>
    /// <param name="output">The item being produced. If null, nothing is being produced.</param>
    /// <returns>True if ready, false otherwise.</returns>
    public bool WorkOnItem(out InventoryItem output)
    {
        output = null;
        int days = CheckRecipe(out output);
        if (crew == null) return false;
        if (days == 1 && output != null) // last day
        {
            if(output.itemType.getIDName().Equals("moonleaf") && !checkOutputMoonleaf && TutorialRunner.Instance.tutorialActive)
            {
                checkOutputMoonleaf = true;
            }
            if(output.itemType.getIDName().Equals("potion_of_water_breathing") && !checkOutputPotion && TutorialRunner.Instance.tutorialActive)
            {
                checkOutputPotion = true;
            }
            ConsumeSlotContents();
            Refresh();
            return true;
        }
        else if (days != -1 && output != null)
        {
            current_duration--;
            OnProgressMade?.Invoke(this, current_duration, output);
            return false;

        }
        return false;
    }

    /// <summary>
    /// Consumes the contents of all slots, never to be seen again
    /// </summary>
    void ConsumeSlotContents()
    {
        foreach(ItemSlot slot in slots)
        {
            slot.SetHolding(null);
        }
    }



}



