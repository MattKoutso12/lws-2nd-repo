using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class SoftScalingWorldPanel : MonoBehaviour
{
    RectTransform rect;
    Camera cam;
    float camSize;


    [SerializeField]
    [Tooltip("The resizing factor over the zoom. Both are normalized (0-1) percentages. Zoom percent is the x-axis, and resize factor is the y-axis.")]
    public AnimationCurve curve;

    // Start is called before the first frame update
    void Start()
    {
        rect = GetComponent<RectTransform>();
        cam = Camera.main;
        camSize = cam.orthographicSize;
        
    }

    float normalizedZoom;
    private void OnGUI()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        var zoomBounds = Player.Instance.GetZoomMinMax();
        camSize = cam.orthographicSize;
        normalizedZoom = 1f - ((camSize - zoomBounds.Item1) / (zoomBounds.Item2 - zoomBounds.Item1));

        float factor = curve.Evaluate(normalizedZoom);
        transform.localScale = new Vector3(factor, factor, factor);
    }
}
