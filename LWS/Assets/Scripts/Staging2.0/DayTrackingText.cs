using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class DayTrackingText : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The system to track")]
    public ProductionSystem track;

    TextMeshProUGUI text;
    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    private void OnGUI()
    {
        text.text = "";
        InventoryItem item = null;
        int days = track.GetCurrentFullDuration() - track.CheckRecipe(out item);
        if (item != null)
        {
            
            text.text = days + "/" + track.GetCurrentFullDuration() + " DAYS";
        }
    }

}
