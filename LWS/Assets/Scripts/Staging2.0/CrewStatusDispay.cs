using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
[RequireComponent(typeof(TextMeshProUGUI))]
public class CrewStatusDispay : MonoBehaviour
{

    TextMeshProUGUI textMesh;


    [SerializeField]
    public ProductionSystem system;

    [SerializeField]
    public Color notMannedColor, mannedColor;


    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<TextMeshProUGUI>();
    }

    private void OnGUI()
    {
        if(system != null)
        {
            if(system.GetCrew() != null)
            {
                textMesh.text = "";
                textMesh.color = mannedColor;
            }
            else if(system.GetCurrentFullDuration() != -1) // quick way to see if there's an item being produced
            {
                textMesh.text = "NEEDS CREW";
                textMesh.color = notMannedColor;
            }
        }
        else // give a more informative error
        {
            Debug.LogError("Error: No ProductionSystem specified for CrewStatusDisplay in " + transform.root.gameObject.name);
        }
    }
}
