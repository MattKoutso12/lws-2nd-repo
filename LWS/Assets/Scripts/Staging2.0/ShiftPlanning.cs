using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;


public class ShiftPlanning : MonoBehaviour
{

    static bool in_planning = true;

    public delegate void PlanningModeEvent();
    public static event PlanningModeEvent OnPlanningEnabled, OnPlanningDisabled;

    [SerializeField]
    public GameObject reward_slot_prefab;


    StudioEventEmitter song;
    

    /// <summary>
    /// See if we're currently in Planning Mode
    /// </summary>
    /// <returns></returns>
    public static bool InPlanningMode()
    {
        return in_planning;
    }

    public static void EnablePlanning()
    {
        if (!in_planning)
        {
            in_planning = true;
            OnPlanningEnabled?.Invoke();
            
            foreach (GameObject g in GameObject.FindGameObjectsWithTag("HideInPlanning"))
            {
                

                Canvas c;
                if(g.TryGetComponent<Canvas>(out c))
                {
                    c.enabled = false;
                }
            }

            foreach (GameObject g in GameObject.FindGameObjectsWithTag("ShowInPlanning"))
            {
                

                Canvas c;
                if (g.TryGetComponent<Canvas>(out c))
                {
                    c.enabled = true;
                }
            }

        }
    }

    public static void DisablePlanning()
    {
        
            in_planning = false;
            
            OnPlanningDisabled?.Invoke();
            foreach (GameObject g in GameObject.FindGameObjectsWithTag("HideInPlanning"))
            {
                Canvas c;
                if (g.TryGetComponent<Canvas>(out c))
                {
                    c.enabled = true;
                }
            }

            foreach (GameObject g in GameObject.FindGameObjectsWithTag("ShowInPlanning"))
            {
                Canvas c;
                if (g.TryGetComponent<Canvas>(out c))
                {
                    c.enabled = false;
                }
            }
        
    }

    // JUST FOR TESTING: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    private void Update()
    {
       

    }

    private void Start()
    {
        OnPlanningEnabled += ShiftPlanning_OnPlanningEnabled;
        OnPlanningDisabled += ShiftPlanning_OnPlanningDisabled;
        song = GetComponent<StudioEventEmitter>();
        DisablePlanning();

        Debug.Log("START CALLED - DISABLED PLANNING");
        
    }

    private void OnDestroy()
    {
        OnPlanningEnabled -= ShiftPlanning_OnPlanningEnabled;
        OnPlanningDisabled -= ShiftPlanning_OnPlanningDisabled;
    }

    private void ShiftPlanning_OnPlanningDisabled()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        song.Stop();
    }

    private void ShiftPlanning_OnPlanningEnabled()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        song.Play();
        SongManager.Instance.MusicDown();
    }

    public static void StartNightShift()
    {
        SongManager.Instance.MusicDown();
        DayToNightFeedback.Instance.gameObject.SetActive(true);
        DayToNightFeedback.Instance.FetchOutputs(); // do the output stuff, visuals
        //ProcessProduction(); // do the output stuff, logic wise
        //ButtonManager.Instance.AwakenChildYouHaveADestinyToLiveUpTo();
    }

    public static List<InventoryItem> ProcessProduction()
    {
        List<InventoryItem> doneTonight = new List<InventoryItem>();
        foreach(ProductionSystem system in ProductionSystem.all_systems)
        {
            InventoryItem output = null;
            if(system.WorkOnItem(out output))
            {
                output.amount = output.itemType.outputYield;
                doneTonight.Add(output);
            }
        }
        return doneTonight;
    }

    public void TogglePlanning()
    {
        if (in_planning)
        {
            SongManager.Instance.MusicUp();
            DisablePlanning();
        }
        else
            EnablePlanning();
    }

    public static void EndNightShift()
    {
        SongManager.Instance.MusicUp();
    }


}
