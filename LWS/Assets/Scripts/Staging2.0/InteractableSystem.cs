using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractableSystem : MonoBehaviour
{
    [SerializeField]
    public ItemSlot [] slots;

    public static HashSet<InteractableSystem> all_systems = new HashSet<InteractableSystem>();

    bool panel_active = false;

    private void Start()
    {

        Init();

    }

    protected virtual void Init()
    {

        Debug.Log("INITIALIZE THIS");
        all_systems.Add(this);

        foreach (ItemSlot slot in slots)
        {
            slot.OnContentsSet += OnSlotContentsChanged;
        }

        SetState(false);
    }

    private void OnDestroy()
    {
        UnInit();
    }

    protected virtual void UnInit()
    {
        foreach(ItemSlot slot in slots)
        {
            slot.OnContentsSet -= OnSlotContentsChanged;
        }
    }

    public ItemSlot GetSlot(int index)
    {
        if(index < slots.Length)
        {
            return slots[index];
        }
        return null;
    }

    public void Toggle()
    {
        if (!panel_active)
        {
            SetState(true);
            //PersistantManager.Instance.Mute(true);
        }
        else
        {
            SetState(false);
            //PersistantManager.Instance.Mute(false);
        }
    }


    /// <summary>
    /// Subscribes to the change events of child slots, so we can see when their contents are modified
    /// </summary>
    /// <param name="sender">The slot who is changed</param>
    /// <param name="objects">Relevant draggables- in this case, index 0 is the old item and index 1 is the new. </param>
    protected virtual void OnSlotContentsChanged(UISlot sender, params IDraggableObject [] objects)
    {
        // We override this in child classes n' stuff
    }

    public InventoryItem GetContents(int index)
    {
        return (InventoryItem) slots[index].GetHeldObject();
        // this can throw OutOfRange, on purpose so we don't confuse an empty slot with going out of bounds
    }



    public virtual void SetState(bool open)
    {
        if (ShiftPlanning.InPlanningMode() && open) return; // don't let the player open crafting during planning

        panel_active = open;
        transform.GetChild(0).gameObject.SetActive(open);
    }

    
}



