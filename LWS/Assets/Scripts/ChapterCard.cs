using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.EventSystems.EventTrigger;

public class ChapterCard : MonoBehaviour
{
    public bool[] statuses = new bool[3];
    public Color completeColor = new Color(0, 0.6f, 0);

    [SerializeField]List<List<string>> tasks = new List<List<string>>()
    {
        new List<string>(){"Use WASD to move around your shop", "Use your mouse to move", "Use the mouse wheel to zoom"},
        new List<string>(){"Using the crystal ball, Purchase a bag of moonleaf seeds", "Using your Planter, grow your new seeds", "Assign Rose to the planter and close for the day" },
        new List<string>(){"Using the crystal ball, Purchase a bucket of water", "Using the crafting table, Make a Potion of Waterbreathing", "Assign Rose to the crafting table and close for the day" },
        new List<string>(){"Open the order board", "Select the order", "Submit the order"}
    };

    int taskSet = 0;
    
    [Serializable]
    public struct entryStatus
    {
        public Image sigil;
        public Image check;
        public TextMeshProUGUI text;
    }

    [SerializeField] List<entryStatus> entries;


    public static ChapterCard Instance;



    private void Start()
    {
   
        Instance = this;

        // disable ourself if we didn't just start a new game.
        if (!PersistantManager.isNewGame) gameObject.SetActive(false);

        ResetSigils();
    }

    public void HideCard()
    {
        gameObject.SetActive(false);
    }

    void ResetSigils()
    {
        //reset sigils
        for (int i = 0; i < entries.Count; i++)
        {
            entries[i].sigil.color = Color.white;
            entries[i].text.color = Color.black;
            entries[i].text.text = tasks[taskSet][i];
            entries[i].check.enabled = false;
        }
    }

    public void SetStatus(int index)
    {
        statuses[index] = true;

        entries[index].check.enabled = true;
        entries[index].sigil.color = completeColor;
        entries[index].text.color = completeColor;



        if(statuses[0] && statuses[1] && statuses[2])
        {
            NextSet();
        }


    }

    private void OnGUI()
    {
        if (!TutorialRunner.Instance.tutorialActive) gameObject.SetActive(false);
    }

    public void NextSet()
    {
        taskSet++;
        if(taskSet >= tasks.Count)
        {
            gameObject.SetActive(false);
            return;
            // DONE WITH TUTORIAL
        }

        for(int i = 0; i < statuses.Length; i++)
        {
            statuses[i] = false; // reset all
        }

        ResetSigils();
    }


}
