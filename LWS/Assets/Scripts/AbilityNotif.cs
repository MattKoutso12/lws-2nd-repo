using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AbilityNotif : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;

    public void Init(string ability)
    {
        text.text = ability;
    }

    public void OnFinish()
    {
        Destroy(gameObject);
    }
}
