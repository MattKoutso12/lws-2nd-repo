using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PrologueRunner : MonoBehaviour
{
    [SerializeField] public GameObject dayScroll;
    [SerializeField] public GameObject letterReceive;
    [SerializeField] public GameObject letterOpen;
    [SerializeField] public GameObject doorWalk;
    [SerializeField] public GameObject fadeTransition;
    [SerializeField] public Letter prologueLetter;
    
    void Start()
    {
        if(PersistantManager.isNewGame){
            StartCoroutine(BeginPrologueAnim());
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Update()
    {
        if (Keyboard.current.spaceKey.wasPressedThisFrame)
        {
            DestroyAndRestoreMusic();
            if (PersistantManager.isNewGame)
            {
                DialougeManager.Instance.StartDialogue("LWST1");
            }
        }
    }

    void DestroyAndRestoreMusic()
    {
        SongManager.Instance.MusicUp();
        Destroy(this.gameObject);
    }

    private IEnumerator BeginPrologueAnim()
    {
        SongManager.Instance.MusicDown();
        letterReceive.SetActive(true);
        yield return new WaitForSeconds(2);
        fadeTransition.SetActive(true);
        yield return new WaitForSeconds(1);
        letterOpen.SetActive(true);
        yield return new WaitForSeconds(1);
        fadeTransition.SetActive(false);
        prologueLetter.OpenLetter();
        yield return new WaitForSeconds(20);
        prologueLetter.CloseLetter();
        yield return new WaitForSeconds(1);
        fadeTransition.SetActive(true);
        yield return new WaitForSeconds(1);
        letterOpen.SetActive(false);
        letterReceive.SetActive(false);
        dayScroll.SetActive(true);
        yield return new WaitForSeconds(1);
        fadeTransition.SetActive(false);
        yield return new WaitForSeconds(8);
        fadeTransition.SetActive(true);
        yield return new WaitForSeconds(1);
        doorWalk.SetActive(true);
        yield return new WaitForSeconds(1);
        fadeTransition.SetActive(false);
        yield return new WaitForSeconds(5);
        fadeTransition.SetActive(true);
        yield return new WaitForSeconds(1);
        doorWalk.SetActive(false);
        dayScroll.SetActive(false);
        yield return new WaitForSeconds(1);
        fadeTransition.SetActive(false);
        DestroyAndRestoreMusic();
        DialougeManager.Instance.StartDialogue("LWST1"); // start the dialogue
    }
}
