using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationScreen : MonoBehaviour
{
    [SerializeField]
    public bool toggled_on = false;

    Region currentRegion;



    [SerializeField]
    public GameObject party_setup_panel;

    [SerializeField]
    public GameObject crew_inventory;

    [SerializeField]
    public GameObject quest_list_area;

    [SerializeField]
    public GameObject questItemPrefab;

    [SerializeField]
    public List<Quest> quests;


    Canvas canvas;

    // Start is called before the first frame update
    void Start()
    {

        canvas = GetComponent<Canvas>();
        foreach(Quest q in quests)
        {
            GameObject questitem = Instantiate(questItemPrefab, quest_list_area.transform);

            UI_QuestItem qi = questitem.GetComponent<UI_QuestItem>();
            qi.Init(q);

        }
    
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Toggle()
    {
        toggled_on = !toggled_on;
        this.transform.GetChild(0).gameObject.SetActive(toggled_on);
    }

    public void SetState(bool isVisible)
    {
        canvas.enabled = isVisible;
        UI_QuestItem.Deselect();
    }



}
