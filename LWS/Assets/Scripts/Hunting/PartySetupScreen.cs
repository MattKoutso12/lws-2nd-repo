using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class PartySetupScreen : MonoBehaviour
{

    [SerializeField]
    public TextMeshProUGUI questName;

    [SerializeField]
    public GameObject activeView, inactiveView;

    [SerializeField]
    public TextMeshProUGUI recBrains, recBrawn, urBrains, urBrawn, riskText, log, description;

    [SerializeField]
    public GameObject reward_zone, item_badge_prefab;

    [SerializeField]
    public Button collect_button;

    [SerializeField]
    public Color highRisk, mediumRisk, lowRisk, noRisk;

    [SerializeField]
    public GameObject reward_slot_prefab;

    [SerializeField]
    public GameObject reward_collection_zone;

    

    PartySlot[] party_slots = new PartySlot[4];

    int partyBrains, partyBrawn;


    // Start is called before the first frame update
    void Start()
    {
        party_slots = GetComponentsInChildren<PartySlot>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SendQuest()
    {
        

        if(UI_QuestItem.getSelectedInstance() != null)
        {
            UI_QuestItem.getSelectedInstance().MakeActive(partyBrains, partyBrawn);
            UI_QuestItem.getSelectedInstance().log += "\t<b>Your party heads out for the wilds...</b>\n";
        }



    }

    public void OnQuestSelected()
    {
        
    }

    


    void AssessRisk(Quest quest)
    {

        int brainDiff = quest.GetRecommendedBrainsAndBrawn().Item1 - partyBrains;
        int brawnDiff = quest.GetRecommendedBrainsAndBrawn().Item2 - partyBrawn;

        int max = Mathf.Max(brainDiff, brawnDiff);

        string risk = "RISK LEVEL: ";
        // magic numbers here! I don't give a fuuuuuuuuuuuuuck
        if (brawnDiff >= 20)
        {
            // HIGH
            riskText.color = highRisk;
            risk += "HIGH";
        }
        else if(brawnDiff >= 10)
        {
            //MEDIUM
            riskText.color = mediumRisk;
            risk += "MEDIUM";
        }
        else if(brawnDiff > 0)
        {
            // LOW
            riskText.color = lowRisk;
            risk += "LOW";
        }
        else
        {
            // NONE
            riskText.color = noRisk;
            risk += "VERY LOW";


        }

        riskText.text = risk;



    }

    List<InventoryItem> lastSelected;
    void ShowPendingRewards(UI_QuestItem quest)
    {
        if (!quest.IsQuestActive())
            return;

        
        // clear
        for(int i = 0; i < reward_zone.transform.childCount; i++)
        {
            Destroy(reward_zone.transform.GetChild(i).gameObject); // HACK: lol this whole thing is hacky. Very gross, dishonorable even
               
        }
        
        lastSelected = quest.pending_rewards;

        foreach(InventoryItem i in quest.pending_rewards)
        {
            if (i == null) continue;
            GameObject badge = Instantiate(item_badge_prefab, reward_zone.transform);
            ItemBadge itemBadge = badge.GetComponent<ItemBadge>();
            itemBadge.SetItem(i);
        }
    }

    public void CollectRewards()
    {
        UI_QuestItem finished_quest = UI_QuestItem.getSelectedInstance();

        for(int i = 0; i < finished_quest.pending_rewards.Count; i++)
        {
            GameObject rs = Instantiate(reward_slot_prefab, reward_collection_zone.transform);
            RewardSlot slot = rs.GetComponent<RewardSlot>();
            slot.SetHolding(finished_quest.pending_rewards[i]);
            slot.SetAmountText(finished_quest.pending_rewards[i].amount);
        }
        finished_quest.ResetQuest(); // reset this quest
    }

    private void OnGUI()
    {
        if (UI_QuestItem.getSelectedQuest() != null)
        {
            if (UI_QuestItem.getSelectedInstance().IsQuestActive())
            {
                log.text = UI_QuestItem.getSelectedInstance().log;
                activeView.SetActive(true);
                inactiveView.SetActive(false);
                
                ShowPendingRewards(UI_QuestItem.getSelectedInstance());

                if (UI_QuestItem.getSelectedInstance().isFinished)
                    collect_button.gameObject.SetActive(true);
                else collect_button.gameObject.SetActive(false);
            }
            else
            {
                activeView.SetActive(false);
                inactiveView.SetActive(true);
                Quest thisQuest = UI_QuestItem.getSelectedQuest();
                description.text = UI_QuestItem.getSelectedQuest().description;
                int brains = 0;
                int brawn = 0;
                for (int i = 0; i < party_slots.Length; i++)
                {
                    PartySlot c = party_slots[i];
                    c.SetLinked(UI_QuestItem.getSelectedInstance());
                    if (c.GetHeldObject() != null)
                    {
                        Crew crew = ((FamiliarWrapper)c.GetHeldObject()).crew;
                        if (crew != null)
                        {
                            UI_QuestItem.getSelectedInstance().party[i] = (FamiliarWrapper)party_slots[i].GetHeldObject();
                            brains += crew.brains;
                            brawn += crew.brawn;
                        }
                    }
                }


                partyBrains = brains;
                partyBrawn = brawn;
                urBrains.text = "" + partyBrains;
                urBrawn.text = "" + partyBrawn;



                questName.text = thisQuest.name;
                recBrains.text = thisQuest.GetRecommendedBrainsAndBrawn().Item1.ToString();
                recBrawn.text = thisQuest.GetRecommendedBrainsAndBrawn().Item2.ToString();
                AssessRisk(thisQuest);
            }
        }
        else
        {
            activeView.SetActive(false);
            inactiveView.SetActive(false);
        }




        
    }
}
