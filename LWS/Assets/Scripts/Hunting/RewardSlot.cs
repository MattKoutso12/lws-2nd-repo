using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simply an ItemSlot that destroys itself when the item is taken out
/// </summary>
public class RewardSlot : ItemSlot
{
    private void Start()
    {
        if (startsWith != null && starting_amount != 0)
        {
            this.SetHolding(new InventoryItem(startsWith, starting_amount));
        }
    }

    public void SetAmountText(int amount)
    {
        amount_text.text = "" + amount;
    }

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);


        if (new_holding == null)
        {
            Destroy(this.gameObject);
            return;
        }
        amount_text.text = "" + ((InventoryItem)new_holding).amount;



    }
}
