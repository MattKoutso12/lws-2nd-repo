using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartySlot : UISlot
{
    [HideInInspector]
    public UI_QuestItem current_linked;
    
    [SerializeField]
    public int index;

    private void Update()
    {
        if (current_linked != null && !current_linked.IsQuestActive())
        {
            this.holding = current_linked?.party[index];
        }
    }

    public void SetLinked(UI_QuestItem to_link)
    {
        if (to_link == null) return;
        current_linked = to_link;
        this.holding = current_linked.party[index];
    }
    
    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);
        if(current_linked != null)
            current_linked.party[index] = (FamiliarWrapper)new_holding;

    }

    protected override bool CanHoldThis(IDraggableObject objectToHold)
    {

        if (!(objectToHold is FamiliarWrapper)) return false;
        return base.CanHoldThis(objectToHold);
    }
}
