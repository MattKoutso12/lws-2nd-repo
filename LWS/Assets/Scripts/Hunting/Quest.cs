using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Quest", menuName ="Hunting/Quest")]
public class Quest : ScriptableObject
{
    [SerializeField]
    public string name;

    [SerializeField]
    public string description;

    [SerializeField]
    public int questLengthInDays = 2;

    [SerializeField]
    [Tooltip("The amount of time it will take the party to start the quest, and how long it will take them to get home after success (or defeat)")]
    public int travelTimeInDays = 0;

    [SerializeField]
    [Tooltip("The number of encounters the party will face throughout this quest")]
    public int numEncounters;

    [SerializeField]
    public int recbrains, recbrawn;

    [SerializeField]
    public int unlockCost;

    /// <summary>
    /// Only used for inspector functionality. Just lets us designate a day for an encounter that will 100% happen.
    /// </summary>
    [System.Serializable]
    public struct HardcodedEncounter
    {
        public Encounter encounter;
        public int onDay;
    }

    [SerializeField]
    [Tooltip("Encounters that MUST occur during this quest. We can also designate when they will happen. Use this for main quest objectives and such")]
    public List<HardcodedEncounter> guaranteedEncounters;

    [SerializeField]
    [Tooltip("The things that may or may not happen throughout our quest.")]
    public Encounter[] possibleEncounters;

    
    public (int, int) GetRecommendedBrainsAndBrawn()
    {
        if(recbrains != 0 && recbrawn != 0)
        {
            return (recbrains, recbrawn);
        }


        int runningBrains = 0;
        int runningBrawn = 0;

        foreach(Encounter e in possibleEncounters)
        {
            runningBrains += e.brains_check;
            runningBrawn += e.brawn_check;
        }

        foreach(HardcodedEncounter e in guaranteedEncounters)
        {
            runningBrains += e.encounter.brains_check;
            runningBrawn += e.encounter.brawn_check;
        }

        runningBrains /= possibleEncounters.Length + guaranteedEncounters.Count;
        runningBrawn /= possibleEncounters.Length + guaranteedEncounters.Count;

        

        return (runningBrains, runningBrawn);

    }

    



}
