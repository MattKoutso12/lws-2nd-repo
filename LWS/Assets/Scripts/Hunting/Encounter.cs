using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags] public enum RollExtras
{
    // Coming soon: Extra interacting with bit flags (like familiars that do better in certain environments, etc.)
    None = 0,
    BrainsRoll = 1 << 0,
    BrawnRoll = 1 << 1,
}


[CreateAssetMenu(fileName = "New Encounter", menuName = "Hunting/Encounter")]
public class Encounter : ScriptableObject
{

    [SerializeField]
    [Range(0, 100)]
    public int brains_check;

    [SerializeField]
    [Range(0, 100)]
    public int brawn_check;

    public enum RollRequirement
    {
        MustSucceedOne,
        MustSucceedBoth
    }

    [SerializeField]
    [Tooltip("If we require both brains and brawn checks, do they need to only succeed one, or both to pass?")]
    public RollRequirement requirement; 

    [SerializeField]
    [Tooltip("Will the party be 'defeated' if they fail this roll?")]
    public bool goHomeOnFailure = false;

    [SerializeField]
    [Tooltip("The possible log entries when your party starts this encounter")]
    public string[] possibleLogsStart;

    [SerializeField]
    [Tooltip("The possible log entries when your party succeeds this encounter")]
    public string[] possibleLogsSuccess;

    [SerializeField]
    [Tooltip("The possible log entries when your party fails this encounter. Leave empty for perception checks where the familiars wouldn't know they missed something (common brain checks)")]
    public string[] possibleLogsFailure;

    [System.Serializable]
    public struct Reward
    {
        public Item item;
        public int amount;
    }

    [Tooltip("The rewards you offer the player if they succeed")]
    [SerializeField]
    public Reward[] rewards;


    /// <summary>
    /// An event that fires during an encounter
    /// </summary>
    /// <param name="encounter">The encounter at hand</param>
    /// <param name="current_roll">This is a little weird, so here's how this goes: we send in a <b>reference</b> to the roll with the event, so anyone who recieves the event can <b>alter the roll</b> and it will be considered in success/failure!!</param>
    /// <param name="extras">Extra parameters in case we need them. Bit flags should probably be used here. Just like current_roll, uses references- so ANY CHANGES TO THESE on the other end of the event will be seen here. We can use these for weird modifiers, like retrying a roll or something.</param>
    public delegate void EncounterEvent(Encounter encounter, ref int current_roll, RollExtras extras);

    public static event EncounterEvent OnRoll, OnRollSuccess, OnRollFailure; // setting up a bunch of events so we can easily integrate with abilities! Radical!!



    public bool RollCheck(int brains_modifier, int brawn_modifier)
    {
        bool brain_pass = RollBrains(brains_modifier);
        bool brawn_pass = RollBrawn(brawn_modifier);

        if (brains_check > 0 && brawn_check > 0) // if both checks are active
        {
            if ((!brain_pass || !brawn_pass) && requirement == RollRequirement.MustSucceedBoth) return false; // if we fail one but must pass both, fail
            if ((!brain_pass && !brawn_pass) && requirement == RollRequirement.MustSucceedOne) return false; // if we fail both but must pass one, fail
            return true;
        }
        else if (brains_check > 0) return brain_pass; // if the brain check is active, return that result
        else if (brawn_check > 0) return brawn_pass; // if the brawn check is active, return that result
        else
        {
            return true; // if both are difficulty = 0, we pass no matter what (we can use this for flavor text and other stuff)
        }


    }


    public string GetRandomIn(string [] possibles, string speaker = "")
    {
        int index = Random.Range(0, possibles.Length);
        

        if(possibles.Length > 0)
        {
            string result = possibles[index];
            if(speaker != "")
            {
                result = speaker + ": " + result;
            }
            return result + "\n";
        }

        return "";
    }


    /// <param name="modifier">Your party's total brains</param>
    /// <returns>True if you succeed the check</returns>
    public bool RollBrains(int modifier, RollExtras extras = RollExtras.BrainsRoll)
    {
        int roll = Random.Range(1, 101); // 101 because it's exclusive, so the max is 100.
        roll += modifier; // add modifier
        OnRoll?.Invoke(this, ref roll, extras); 

        

        if (roll > brains_check)
        {
            OnRollSuccess?.Invoke(this, ref roll, extras);
            return true;
        }

        OnRollFailure?.Invoke(this, ref roll, extras);
        return false;
    }

    /// <param name="modifier">Your party's total brawn</param>
    /// <returns>True if you succeed the check</returns>
    public bool RollBrawn(int modifier, RollExtras extras = RollExtras.BrawnRoll)
    {
        int roll = Random.Range(1, 101); // 101 because it's exclusive, so the max is 100.
        roll += modifier; // add modifier
        OnRoll?.Invoke(this, ref roll, extras);
        if (roll > brawn_check)
        {
            OnRollSuccess?.Invoke(this, ref roll, extras);
            return true;
        }

        OnRollFailure?.Invoke(this, ref roll, extras);
        return false;
    }

    /// <summary>
    /// Gets an array of inventory items which are the spoils of this encounter. Only call this if we succeeded.
    /// </summary>
    /// <returns>The rewards of this encounter</returns>
    public InventoryItem [] GenerateRewards()
    {
        InventoryItem[] temp = new InventoryItem[rewards.Length];

        for(int i = 0; i < rewards.Length; i++)
        {
            Reward r = rewards[i];
            temp[i] = new InventoryItem(r.item, r.amount);
        }

        return temp;

    }

    
}
