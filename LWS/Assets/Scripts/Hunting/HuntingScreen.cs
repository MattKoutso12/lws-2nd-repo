using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using FMODUnity;


public class HuntingScreen : MonoBehaviour
{

    [SerializeField]
    public bool toggled_on = false;

    [SerializeField]
    public GameObject swampsButton;

    [SerializeField]
    public GameObject mountaincavesButton;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.GetChild(0).gameObject.SetActive(toggled_on);
        PersistantManager.Instance.OnShopLevelUp += Instance_OnShopLevelUp;
    }

    private void Instance_OnShopLevelUp(object sender, System.EventArgs e)
    {
        if(PersistantManager.Instance.shopLevel >= 10 && !swampsButton.activeSelf)
        {
            swampsButton.SetActive(true);
        }
        if(PersistantManager.Instance.shopLevel >= 12 && !mountaincavesButton.activeSelf)
        {
            mountaincavesButton.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Toggle()
    {
        toggled_on = !toggled_on;
        this.transform.GetChild(0).gameObject.SetActive(toggled_on);

        if (toggled_on)
        {

            Player.Instance.SetMoveLock(true);
        }
        else
        {
            Player.Instance.SetMoveLock(false);
        }
    }
}
