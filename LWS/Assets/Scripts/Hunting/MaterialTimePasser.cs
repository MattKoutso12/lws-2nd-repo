using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Passes Time.time to a material. Does not pass the time, like, the expression. I mean I guess anything passes the time in a way. Life is temporary if you really think about it
/// </summary>
public class MaterialTimePasser : MonoBehaviour
{


    [SerializeField]
    public Material material;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        material.SetFloat("_Time", Time.time);
    }
}
