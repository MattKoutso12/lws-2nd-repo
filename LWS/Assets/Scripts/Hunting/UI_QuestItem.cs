using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class UI_QuestItem : MonoBehaviour
{


    [SerializeField]
    public TextMeshProUGUI quest_name, recommended_brains, recommended_brawn, text_duration, unlock_text;


    Quest myQuest;


    static Quest selected_quest = null;
    static UI_QuestItem selected_instance = null;

    bool active_quest = false;

    int days_remaining = -1;
    public FamiliarWrapper[] party = new FamiliarWrapper[4];
    public string log;
    int partyBrains, partyBrawn;
    int encounters_remaining;
    int avg_encounters_in_day;
    public List<InventoryItem> pending_rewards = new List<InventoryItem>();
    public bool isFinished = false;
    public bool failed = false;
    public void Init(Quest quest)
    {
        myQuest = quest;
        SetText();
        PersistantManager.OnDayChanged += Instance_OnDayChanged;
        for(int i = 0; i < party.Length; i++)
        {
            party[i] = null;
        }
    }

    private void Instance_OnDayChanged(object sender, System.EventArgs e)
    {
        if(active_quest)
        {
            days_remaining--;

            // CALCULATE DAY'S WORTH OF ENCOUNTERS
            AdvanceQuest();
        }
    }

    // if it's repeatable
    public void ResetQuest()
    {
        active_quest = false;
    }

    public void AdvanceQuest()
    {
        if (!active_quest) return;

        int dayNum = myQuest.questLengthInDays - days_remaining;


        log += "\nDAY " + dayNum + "\n";

        Stack<Encounter> todaysEncounters = new Stack<Encounter>();

        foreach(Quest.HardcodedEncounter encounter in myQuest.guaranteedEncounters)
        {
            if(encounter.onDay == dayNum)
            {
                todaysEncounters.Push(encounter.encounter);
            }
        }

        if(encounters_remaining > 0)
        {
            int randomNum = Random.Range(avg_encounters_in_day-1, avg_encounters_in_day + 1);
            if(randomNum > encounters_remaining)
            {
                randomNum = encounters_remaining;
            }

            if (randomNum <= 0)
                randomNum = 1;

            for (int i = 0; i < randomNum; i++)
            {
                int randomIndex = Random.Range(0, myQuest.possibleEncounters.Length); // generate encounter
                ProcessEncounter(myQuest.possibleEncounters[randomIndex]);
            }
        }

        while(todaysEncounters.Count > 0)
        {
            ProcessEncounter(todaysEncounters.Pop());
        }

        if(days_remaining == 0)
        {
            SendHome(true);
        }
    }

    


    void ProcessEncounter(Encounter e)
    {
        int speakerIndex = Random.Range(0, tempParty.Count);
        
        for(int i = 0; i < party.Length; i++)
            Debug.Log(party[i]);

        
        

        //log += party[speakerIndex]?.name + " : ";
        string speaker = tempParty[speakerIndex].ColouredName;
        string pending = "";
        pending += e.GetRandomIn(e.possibleLogsStart, speaker);
        if (e.RollCheck(partyBrains, partyBrawn))
        {
            // win
            pending += e.GetRandomIn(e.possibleLogsSuccess, speaker);
            foreach(InventoryItem r in e.GenerateRewards())
            {
                if (r != null) pending_rewards.Add(r);
            }
        }
        else
        {
            // lose
            pending += e.GetRandomIn(e.possibleLogsFailure, speaker);

            if (e.goHomeOnFailure)
            {
                SendHome(false);
            }
        }

        if(pending == "")
        {
            pending += "Nothing eventful happened...";
        }
        else
        {
            pending = ProcessPartyString(pending, speaker);
            log += pending;
        }
    }


    /// <summary>
    /// Takes references we put in to other familiars and replaces them contextually. This will be upgraded in the future, it's crude for now
    /// </summary>
    /// <param name="toProcess">The log string being processed</param>
    /// <param name="speaker"> The name of the familiar writing the log (so they don't speak in the 3rd person)</param>
    /// <returns>The processed version of this string</returns>
    string ProcessPartyString(string toProcess, string speaker)
    {
        string pending = toProcess;
        if(pending.Contains("{strongest}"))
        {
            List<FamiliarWrapper> byBrawn = tempParty.OrderByDescending(m => m.crew.brawn).ToList(); // sort the party by brawn
            if(byBrawn[0] != null)
            {
                if (byBrawn[0].ColouredName.Equals(speaker))
                    pending = pending.Replace("{strongest}", "I"); // if the speaker is the strongest, talk in the first person ("They were so strong, not even {strongest} could beat them" becomes "They were so strong, not even I could beat them" because the speaker has the most brawn)
                pending = pending.Replace("{strongest}", byBrawn[0].ColouredName);
            }    
        }
        if(pending.Contains("{smartest}"))
        {
            List<FamiliarWrapper> byBrains = tempParty.OrderByDescending(m => m.crew.brains).ToList(); // sort the party by brawn
            if (byBrains[0] != null)
            {
                if (byBrains[0].ColouredName.Equals(speaker))
                    pending = pending.Replace("{smartest}", "I"); // if the speaker is the strongest, talk in the first person ("They were so strong, not even {strongest} could beat them" becomes "They were so strong, not even I could beat them" because the speaker has the most brawn)
                pending = pending.Replace("{smartest}", byBrains[0].ColouredName);
            }
        }
        if(pending.Contains("{random}"))
        {
            if(tempParty.Count == 1)
            {
                pending = pending.Replace("{random}", "I");
            }
            else
            {
                FamiliarWrapper member = tempParty[Random.Range(0, tempParty.Count)];
                if (member.ColouredName.Equals(speaker))
                    pending.Replace("{random}", "I");
                else
                    pending.Replace("{random}", member.ColouredName);
            }
        }


        return pending;

    }

    void SendHome(bool victory)
    {
        isFinished = true;
        if (victory)
        {
            log += "<i> QUEST VICTORY </i>";

        }
        else
        {
            log += "<i> QUEST FAILED </i>";
        }
                        
    }

    void SetText()
    {
        quest_name.text = myQuest.name;

        (int, int) bandb = myQuest.GetRecommendedBrainsAndBrawn();

        recommended_brains.text = "" + bandb.Item1;
        recommended_brawn.text = "" + bandb.Item2;
        text_duration.text = myQuest.questLengthInDays + " Days";
        unlock_text.text = myQuest.unlockCost + " Gold";
        if(myQuest.unlockCost <= 0)
        {
            Destroy(unlock_text.transform.parent.gameObject);
        }
    }

    public bool IsQuestActive()
    {
        return active_quest;
    }

    List<FamiliarWrapper> tempParty;

    public void MakeActive(int partyBrains, int partyBrawn)
    {
        tempParty = new List<FamiliarWrapper>();
        bool empty = true;
        for(int i =0; i < party.Length; i++)
        {
            if (party[i] != null)
            {
                empty = false;
                tempParty.Add(party[i]);
            }
        }

        if (empty) return; // no party!

        active_quest = true;
        this.partyBrains = partyBrains;
        this.partyBrawn = partyBrawn;
        days_remaining = myQuest.questLengthInDays;
        pending_rewards = new List<InventoryItem>();
        isFinished = false;
        failed = false;

        
        


        //this.party = party;
        log = "";
        encounters_remaining = myQuest.numEncounters;
        avg_encounters_in_day = myQuest.numEncounters / myQuest.questLengthInDays;
    }

    public void AttemptUnlock()
    {
        if(PersistantManager.Instance.TryPurchase(myQuest.unlockCost))
        {
            Destroy(unlock_text.transform.parent.gameObject);
        }
    }


    public static Quest getSelectedQuest()
    {
        return selected_quest;
    }

    public static UI_QuestItem getSelectedInstance()
    {
        return selected_instance;
    }

    public void OnSelected()
    {
        selected_quest = myQuest;
        selected_instance = this;
    }

    public static void Deselect()
    {
        selected_quest = null;
        
    }


}
