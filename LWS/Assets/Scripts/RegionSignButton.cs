using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegionSignButton : MonoBehaviour
{

    bool ready_to_spawn = true;

    [SerializeField] GameObject fullError;

    [SerializeField]
    public List<Crew> possible_familiars = new List<Crew>();

    [SerializeField]
    public GameObject locationText;

    [SerializeField]
    public GameObject lockedText;

    [SerializeField]
    public Sprite sprite_locked, sprite_unlocked;

    [SerializeField]
    public int levelToActivate;

    [SerializeField]
    public CrewCard crew_card;

    private Button thisButton;

    private void Start()
    {
        PersistantManager.OnDayChanged += Instance_OnDayChanged;
        PersistantManager.Instance.OnShopLevelUp += OnLevelUp;
        thisButton = this.gameObject.GetComponent<Button>();
        if(levelToActivate >= 1)
        {
            thisButton.interactable = false;
            locationText.SetActive(false);
            lockedText.SetActive(true);
        }
    }

    /// <summary>
    /// When IncreaseShopLevel() is called, we check to see if we're getting unlocked. Better than OnDayChanged because we'll get it immediately.
    /// </summary>
    private void OnLevelUp(object sender, System.EventArgs e)
    {
        if(levelToActivate <= PersistantManager.Instance.shopLevel)
        {
            ActivateRegion();
        }
    }

    private void Instance_OnDayChanged(object sender, System.EventArgs e)
    {
        crew_card?.gameObject.SetActive(false);
        ready_to_spawn = true;
    }

    private void OnDestroy()
    {
        PersistantManager.OnDayChanged -= Instance_OnDayChanged;
        PersistantManager.Instance.OnShopLevelUp -= OnLevelUp;
    }

    public void TryToSpawn()
    {
        if (FamiliarHouse.instance.famCount >= FamiliarHouse.instance.famCap)
        {
            fullError.SetActive(true);
            return;
        }
        if (ready_to_spawn)
        {
            crew_card.GenerateNewCrew();
            ready_to_spawn = false;
        }
        
        if(!crew_card.acceptedToday)
        {
            crew_card.gameObject.SetActive(true);

        }
    }

    public void ActivateRegion()
    {
        lockedText.SetActive(false);
        locationText.SetActive(true);
        thisButton.interactable = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
