using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using TMPro;

public class ItemSlot : UISlot, IPointerClickHandler
{

    [SerializeField]
    public Item startsWith;

    [SerializeField]
    public int starting_amount = 0;

    [SerializeField]
    public TextMeshProUGUI amount_text;

    [SerializeField]
    public int limit = -1; // no limit by default

    [HideInInspector]
    public Inventory connected;

    public static Dictionary<uint, ItemSlot> id_to_slot;

    public delegate void ItemSlotEvent(InventoryItem item);
    public static event ItemSlotEvent ShowRecipeEvent;

    static uint runningID = 0;
    public uint _id;

    static HashSet<Transform> parents = new HashSet<Transform>(); // we store unique parents in here, and use them for indexing the ID.


    [System.Serializable]
    public struct SerializableItemSlot
    {
        public string item_name;
        public uint id;
        public int amount;
    }



    private void Awake()
    {
        if (this.GetType() == typeof(ShopSlot)) Debug.Log("UH OH! THIS IS A BUY SLOT STILL USING AWAKE!");

        if (id_to_slot == null) id_to_slot = new Dictionary<uint, ItemSlot>(); // initalize if we have to

        // if we haven't seen this parent before (i.e. we are the first ItemSlot child of this parent to hit Awake)...

        if (!parents.Contains(transform.parent) || _id == 0)
        {
            // ... then we will initialize ALL children of that parent IN SEQUENCE based on their heirarchy position!
            parents.Add(transform.parent);
            int parentID = (int)(transform.parent.position.sqrMagnitude);

            // "Apartment Number" system. First digit is the parent's "ID", the rest is the child's sibling index.
            // We need this so IDs are both unique overall and the same every time you load the game.
            ItemSlot[] siblings = transform.parent.GetComponentsInChildren<ItemSlot>();
            for (int i = 0; i < siblings.Length; i++)
            {
                // initialize siblings!
                uint newID = uint.Parse(parentID + "" + i); // this should always be unique
                //if (id_to_slot.ContainsKey(newID) && id_to_slot[newID].GetInstanceID() != siblings[i].GetInstanceID()) Debug.LogError($"DUPLICATE ID!!! {siblings[i].name} and {id_to_slot[newID].name} on {newID}!!!");
                siblings[i]._id = newID;
                id_to_slot[newID] = siblings[i];
            }


        }

        Debug.Log("POSITION: (" + name + ") " + _id); // we're trying this apeshit method lol



        // do this in awake so we ensure everybody has their IDs in order before we access them

    }

    /// <summary>
    /// Sets the ID of this item slot to a new ID. Used in inventory, for forcing our IDs
    /// </summary>
    /// <param name="newID">The new ID</param>
    public void SetID(uint newID)
    {
        if(_id != 0)
        {
            id_to_slot.Remove(_id); // remove old reference
        }

        if(id_to_slot.ContainsKey(newID))
        {
            Debug.LogError($"ERROR: ID {newID} ALREADY EXISTS! ({this})");
            return;
        }


        _id = newID;
        id_to_slot[newID] = this;
    }

    /// <summary>
    /// Gives a version which can be put into .json easily
    /// </summary>
    /// <returns> An SerializedItemSlot which holds item data for save files </returns>
    public SerializableItemSlot GetSerializedVersion()
    {
        SerializableItemSlot serializable = new SerializableItemSlot();
        
        
        if (GetHeldObject() == null)
        {
            Debug.Log("SERIALIZING EMPTY SLOT " + this._id);
            serializable.item_name = "EMPTY"; // this means it's empty
            serializable.amount = 0;
            serializable.id = this._id;
            return serializable; // return an empty item
        }

        InventoryItem item = (InventoryItem)GetHeldObject();
        serializable.item_name = item.itemType.GetRawName(); // get the id name
        serializable.amount = item.amount;
        serializable.id = this._id;
        Debug.Log($"SERIALIZING SLOT {this.name} HOLDING {serializable.item_name} x{serializable.amount} AT ID {serializable.id}!");
        return serializable;
    }

    private void Start()
    {
        


        if (startsWith != null && starting_amount != 0 && SaveManager.isNewGame)
        {
            this.SetHolding(new InventoryItem(startsWith, starting_amount));
        }
        else
        {
            amount_text.text = "";
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        runningID = 0;
    }

    private void OnDestroy()
    {
        id_to_slot.Remove(this._id);
    }

    public ItemSlot GetSlotFromID(uint id)
    {
        if(id_to_slot.ContainsKey(id))
        {
            return id_to_slot[id];
        }
        else
        {
            Debug.LogError("ERROR: Missing key for slot ID: " + id);
            return null;
        }
    }


    /// <summary>
    /// Gives the amount of the InventoryItem this ItemSlot holds.
    /// </summary>
    /// <returns></returns>
    public int GetAmount()
    {
        if (GetHeldObject() == null) return 0;
        else return ((InventoryItem)GetHeldObject()).amount;
    }


    public int TryLimitedTransfer(ItemSlot other)
    {
        if (!other.CanHoldThis((InventoryItem)this.GetHeldObject())) return -1;
        if (other == this) return -1;
        InventoryItem held = (InventoryItem)GetHeldObject();
        InventoryItem otherheld = (InventoryItem)other.GetHeldObject();
        InventoryItem remainder = null;
        if (held == null) return 0;
        if (this.limit == -1 && other.limit == -1) return 0;

        // ONLY LET THE PLAYER *SWAP* IN ITEMS WITH NO REMAINDER! If there is a remainder, we'd have no slot to send the swapped item to... can of worms
        if(!held.CanMergeWith(otherheld))
        {
            if (other.limit != -1 && GetAmount() <= other.limit)
            {
                // if WE have a limit and can't accept the swapped amount:
                if (this.limit != -1 && otherheld.amount > this.limit) return -1;


                // SWAP!
                InventoryItem temp = held;
                this.SetHolding(otherheld);
                other.SetHolding(temp);
                return 1;
            }
            else
            {
                return -1;
            }

        }

        if (other.limit == -1) return 0;

        if(GetAmount() > other.limit - other.GetAmount())
        {
            int room = other.limit - other.GetAmount();
            if(GetAmount() - room >= 0)
            {
                remainder = new InventoryItem(held.itemType, GetAmount() - room);
                held.amount = room;

                if (remainder.amount == 0)
                    SetHolding(null);
                else
                    SetHolding(remainder);
                
                other.SetHolding(held.CombinedWith(otherheld));


                return 1;
            }
        }

        return 0;


    }


    protected override void OnGUI()
    {
        base.OnGUI();
        if(!IsEmpty())
        {
            string newtext = ""+ GetAmount();
            int amount = GetAmount();
            // I literally don't even know why I'm putting this here. Honestly just for fun
            if (amount >= 1000000000)
            {
                float newAmountNum = ((float)amount) / 1000000000; // how many thousand of this item we have
                newtext = newAmountNum.ToString("D2") + "B";
            }
            else if (amount >= 1000000)
            {
                float newAmountNum = ((float)amount) / 1000000; // how many thousand of this item we have
                newtext = newAmountNum.ToString("D2") + "M";
            }
            else if (amount >= 1000)
            {
                float newAmountNum = ((float)amount) / 1000; // how many thousand of this item we have
                newtext = newAmountNum.ToString("D2") + "k";
            }


            amount_text.text = newtext;
            if (amount == 0) amount_text.text = "";
        }
    }


    public override bool TryTransfer(UISlot other)
    {
        if (other.GetType() == typeof(ShopSlot)) return false;
        if (other is ItemSlot)
        {
            int result = TryLimitedTransfer((ItemSlot)other);
            switch(result)
            {
                case -1:
                    return false;
                case 0:
                    return base.TryTransfer(other);
                case 1:
                    return true;
                default:
                    return false;
            }
        }
        else return false;

    }

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);
        if(new_holding == null)
        {
            amount_text.text = ""; // essentially make the text invisible if this slot is empty
        }
        else
        {
            InventoryItem tempItem = (InventoryItem)new_holding;

            if(tempItem.amount == 0)
            {
                Debug.LogError("Uh, not sure an item should be reduced to zero but somehow still in existance");
                this.holding = null; // you'll hold nothing and like it
                return;
            }


            amount_text.faceColor = Color.white;
            string newtext = tempItem.amount.ToString();

            // I literally don't even know why I'm putting this here. Honestly just for fun
            if(tempItem.amount >= 1000000000)
            {
                float newAmountNum = ((float)tempItem.amount) / 1000000000; // how many thousand of this item we have
                newtext = newAmountNum.ToString("D2") + "B";
            }
            else if (tempItem.amount >= 1000000)
            {
                float newAmountNum = ((float)tempItem.amount) / 1000000; // how many thousand of this item we have
                newtext = newAmountNum.ToString("D2") + "M";
            }
            else if (tempItem.amount >= 1000)
            {
                float newAmountNum = ((float)tempItem.amount) / 1000; // how many thousand of this item we have
                newtext = newAmountNum.ToString("D2") + "k";
            }


            amount_text.text = newtext;
            if (tempItem.amount == 0) amount_text.text = "";
        }

    }

    protected override bool CanHoldThis(IDraggableObject objectToHold)
    {
        if(!(objectToHold is InventoryItem))
        {
            return false;
        }
        return true;
    }

    private void OnDrawGizmos()
    {
        if (startsWith != null && startsWith.sprite != null)
        {
            Gizmos.color = Color.white;
            //Gizmos.DrawGUITexture(((RectTransform)transform).rect, startsWith.sprite.texture);
            // new Rect(transform.GetChild(0).GetChild(0).position, Vector2.one * -50f)

            // freaky debug math
            Gizmos.DrawGUITexture(new Rect(
                (Vector2)transform.position+(((RectTransform)transform).sizeDelta)/2,
                ((RectTransform)transform).sizeDelta*-1), startsWith.sprite.texture);
        }
    }



    public void OnPointerClick(PointerEventData eventData)
    {
        // SPLIT ON RIGHT CLICK
        if(eventData.button == PointerEventData.InputButton.Right)
        {
            if (GetHeldObject() == null)
            {
                return;
            }
            InventoryItem splitoff = new InventoryItem(((InventoryItem)holding).itemType, ((InventoryItem)holding).amount / 2);
            if (splitoff.amount <= 0) return; // if we're splitting nothing off (i.e. when our stack is, like, 1)
            InventoryItem tempItem = (InventoryItem)holding;
            Debug.Log("CLICKED!");

            if (connected == null) return;

            ItemSlot empty = connected.GetEmptyInventorySlot();
            if(empty != null)
            {
                empty.SetHolding(splitoff);
                tempItem.amount -= splitoff.amount;
                this.SetHolding(tempItem);
                return;
            }

        }
        else if(eventData.button == PointerEventData.InputButton.Left && Keyboard.current.leftAltKey.wasPressedThisFrame) // VIEW RECIPE
        {
            Debug.Log("ALTCLICKED");
            InventoryItem tempHeld = (InventoryItem)GetHeldObject();
            if(tempHeld.itemType is CraftedItem)
            {
                Debug.Log("OPENING RECIPES");
                CraftedItem item = (CraftedItem)tempHeld.itemType;
                ButtonManager.Instance.ToggleRecipes();
                CraftingPreview.Instance.SetPreviewedItem(item);
            }
        }
    }


}

