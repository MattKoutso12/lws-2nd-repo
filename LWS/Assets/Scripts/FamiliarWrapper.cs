﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using UnityEngine;

[Serializable]
public class FamiliarWrapper: IToolTipable, IDraggableObject
{
    public string name;
    public string displayName;
    private int hirePrice;
    public string ability;
    public string abilityName;
    [NonSerialized] private Sprite sprite;
    [NonSerialized] private Color textColor;
    private int level;

    [NonSerialized] private ICrewHolder crewHolder;
    public int current_slot_id = -1;
    public int current_slot_internal_id = -1;

    [NonSerialized] public Crew crew;
    [NonSerialized] public Ability perk;

    public static List<FamiliarWrapper> allfamiliars = new List<FamiliarWrapper>();

    [System.Serializable]
    public struct SerializableFamiliarWrapper
    {
        public string name;
        public string species;
        public string displayName;
        public int slotID;
    }


    public FamiliarWrapper(Crew crew)
    {
        level = crew.Level;
        displayName = crew.Name;
        name = crew.Name;
        hirePrice = crew.HirePrice;
        perk = GlobalAbility.GetRandomAbility(level);
        ability = perk.Description;
        abilityName = perk.Name;
        sprite = crew.Sprite;
        textColor = Color.yellow;
        this.crew = crew;
        allfamiliars.Add(this);
        Debug.Log("AF Added: " + this);
    }

    public FamiliarWrapper(SerializableFamiliarWrapper from)
    {
        foreach(Crew c in PersistantManager.Instance.allCrews)
        {
            if(c.species.Equals(from.species))
            {
                this.crew = c;
                break;
            }
        }

        if (this.crew == null) Debug.LogError("CREW NOT FOUND: " + from.species);

        this.name = from.name;
        this.displayName = from.displayName;
        this.textColor = Color.yellow;
        allfamiliars.Add(this);
    }
    
    

    #region ColouredName comment
    /// <summary>
    /// Returns string representation of hexdecimal color.
    /// </summary>
    /// <value> String hexadecimal color. </value>
    #endregion
    public string ColouredName
    {
        get
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(textColor);
            return $"<color=#{hexColour}>{name}</color>";
        }
    }

    public Ability GetAbility()
    {
        return perk;
    }

    public SerializableFamiliarWrapper GetSerializedVersion()
    {
        SerializableFamiliarWrapper fam = new SerializableFamiliarWrapper();
        fam.displayName = this.displayName;
        fam.name = name;
        fam.species = this.crew.species;
        fam.slotID = this.current_slot_internal_id;
        return fam;
    }

    #region GetTooltipInfo comment
    /// <summary>
    /// Returns tooltip info for a familiar.
    /// </summary>
    /// <returns> Tooltip info for familiar. </returns>
    #endregion
    public string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        //builder.Append(ColouredName).AppendLine();
        //builder.Append("Hire Price: ").Append(hirePrice).Append(" Gold").AppendLine();
        //builder.Append("Ability: ").Append(ability).AppendLine();

        return builder.ToString();
    }


    // IDraggableObject implementations
    public Sprite GetSprite()
    {
        return crew.icon;
    }

    public void SetCrewHolder(ICrewHolder crewHolder)
    {
        this.crewHolder = crewHolder;
    }

    public void SetDisplayName(string newName)
    {
        this.displayName = newName;
    }


    /// <summary>
    /// Removes familiar from its current ItemHolder.
    /// </summary>
    public void RemoveFromItemHolder()
    {
        if (crewHolder != null)
        {
            // Remove from current Item Holder
            crewHolder.RemoveCrew(this);
        }
    }

    public bool CanMergeWith(IDraggableObject other)
    {
        return false;
    }

    public IDraggableObject CombinedWith(IDraggableObject other)
    {
        return null;
    }

    public override string ToString()
    {
        return $"FamiliarWrapper {displayName} ({name}, {crew.species}) @ {this.current_slot_id}";
    }
}