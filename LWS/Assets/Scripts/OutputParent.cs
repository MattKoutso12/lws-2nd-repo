using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class OutputParent : MonoBehaviour
{
    private Image thisImage;
    Animator anim;
    InventoryItem currentStack;

    [SerializeField]
    public ParticleSystem particles;

    [SerializeField]
    public DayToNightFeedback DayToNightFeedback;

    [SerializeField]
    public Material itemParticleMaterial;

    bool onAlternateTexture = false;

    void Start()
    {
        thisImage = transform.Find("OutputImage").GetComponent<Image>();
       // thisImage.transform.localScale = new Vector3(.0f, .0f, 1f);
        anim = GetComponent<Animator>();
        itemParticleMaterial.mainTexture = null;
    }

    public void SetCurrentOutput(InventoryItem item)
    {
        // sets up our output item
        currentStack = item;
        thisImage.sprite = item.itemType.sprite;
        itemParticleMaterial.mainTexture = item.itemType.sprite.texture;


    }

    /// <summary>
    /// Caches the texture of the next item for quick loading. We genuinely need this if we don't wanna use a sprite sheet
    /// </summary>
    /// <param name="item">The next item to be produced</param>
    public void SetNextOutput(InventoryItem item)
    {
        if(onAlternateTexture)
        {
            itemParticleMaterial.mainTexture = item.itemType.sprite.texture;
        }
        else
        {
            itemParticleMaterial.SetTexture("_AltTex", item.itemType.sprite.texture);
        }
    }

    public void PlayCompleteAnimation()
    {
        anim.SetTrigger("ProcessItem");
        particles.gameObject.SetActive(true);
    }

    /// <summary>
    /// Triggered by an animation event. Tells the particles to emit.
    /// </summary>
    void OnOutputCompleted()
    {
        ParticleSystem.Particle[] ptls = new ParticleSystem.Particle[currentStack.amount];
        ParticleSystem.Particle particle = new ParticleSystem.Particle();

        ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();

        StartCoroutine(EmissionCoroutine(currentStack.amount, 0.1f));
        hitsLeft = currentStack.amount;



    }

    IEnumerator EmissionCoroutine(float amount, float staggering)
    {
        for(int i = 0; i < amount; i++)
        {
            particles.Emit(1);
            yield return new WaitForSeconds(staggering);
        }
    }


    int hitsLeft = 0;
    public void OnTargetHit()
    {
        if(hitsLeft > 0)
        {
            Player.Instance.GetInventory().AddItem(new InventoryItem(currentStack.itemType));
            hitsLeft--;
        }

        if (hitsLeft <= 0)
        {
            currentStack = null;
            particles.Stop();
            particles.gameObject.SetActive(false);
            DayToNightFeedback.AdvanceToNext();
        }
    }
    

    void Update()
    {
       
    }

    private void OnGUI()
    {
        if(currentStack == null)
        {
            thisImage.color = Color.clear;
        }
        else
        {
            thisImage.color = Color.white;
        }
    }


}
