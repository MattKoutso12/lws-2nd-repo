using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// Represents an order.
/// </summary>
[CreateAssetMenu(fileName = "New Task", menuName = "Items/Tasks")]
public class Task : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private string orderInfo;
    [SerializeField] public int level = 1;
    [SerializeField] public int give_on_shop_Level = 0;
    [SerializeField] public int orderLength;
    [SerializeField] private int reward;
    [SerializeField] public List<Product> products;

    [Serializable]
    public struct Product
    {
        public Item item;
        public int amount;
    }



    private Color textColor = Color.red;
    //[SerializeField] private ItemType itemCat;

    public string Name { get { return name; } }

    public string OrderInfo { get {return orderInfo; } }

    public int Reward { get { return reward; } }

    public List<InventoryItem> ItemList { 
        get {
            List<InventoryItem> items = new List<InventoryItem>();
            foreach(Product p in products)
            {
                items.Add(new InventoryItem(p.item, p.amount));
            }
            return items;
        } 
    }

    public int OrderLength { get { return orderLength; } }

    //public ItemType ItemType { get { return itemCat; } }

    public Color Color { get { return textColor; } }
}
