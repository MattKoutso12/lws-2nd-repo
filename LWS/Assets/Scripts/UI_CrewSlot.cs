using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Handles UI for a single crew slot.
/// </summary>
public class UI_CrewSlot: MonoBehaviour, IDropHandler
{
    public event EventHandler<OnItemDroppedEventArgs> OnItemDropped;
    public Action onDropAction;
    public int crafting_system_id = -1; // -1 means it is not bound to a system and just holds crew (for the crew roster on the right side of staging).


    CraftingSystem system;

    private void Start()
    {
        //system = GetComponentInParent<UI_CraftingSystem>().craftingSystem;
        system = CraftingSystem.GetCraftingSystem(crafting_system_id);
        
    }

    public class OnItemDroppedEventArgs : EventArgs
    {
        public FamiliarWrapper worker;
        public int x;
        public CraftingSystem system;
    }

    private int x;
    public void SetOnDropAction(Action onDropAction)
    {
        this.onDropAction = onDropAction;
    }

    public void OnDrop(PointerEventData eventData)
    {
        UI_CrewDrag.Instance.Hide();
        FamiliarWrapper worker = UI_CrewDrag.Instance.GetWorker();
        OnItemDropped?.Invoke(this, new OnItemDroppedEventArgs { worker = worker, x = x, system = system });
        if(onDropAction != null)
        {
            onDropAction();
        }
    }

    public void SetX(int x)
    {
        this.x = x;
    }
}
