using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// System handling the process of crafting InventoryItems.
/// </summary>
public class CraftingSystem : IItemHolder, ICrewHolder
{
    public static readonly int DEFAULT_GRID_SIZE = 4;
    public event EventHandler OnGridChanged; //event to signify when the display changes
    public InventoryItem[] itemArray; //the items in aforementioned item slots
    private CraftingSlot[] craftingSlots;
    private InventoryItem output = null; //the item in the output slot
    private CraftedItem last_recipe; //the recipe we made progress on last night, if there was one. This way, if the player fucks with the items in a system, they don't lose progress so long as we're trying to make the same thing two or more nights in a row.
    private FamiliarWrapper crew; //ignore this

    public static Dictionary<CraftingSystem, CraftedItem> ready_to_craft = new Dictionary<CraftingSystem, CraftedItem>();

    static Dictionary<int, CraftingSystem> unique_systems = new Dictionary<int, CraftingSystem>();

    public readonly int id;
    public bool manned = false;
    protected int num_slots = 4;
    protected int days_remaining = -1;
    protected int days_total = -1;

    //List<FamiliarWrapper> active_crew = new List<FamiliarWrapper>();


    CraftedItem.CraftingType _type;
    public CraftedItem.CraftingType crafting_type
    {
        get
        {
            return _type;
        }

        protected set
        {
            _type = value;
        }
    }


    #region CraftingSystem comment
    /// <summary>
    /// Constructor method for CraftingSystem.
    /// </summary>
    /// <param name="id"> ID assigned to this system. </param>
    #endregion
    public CraftingSystem(int id, int num_slots = 4)
    {
        //creates new array
        itemArray = new InventoryItem[num_slots];
        craftingSlots = new CraftingSlot[num_slots];
        this.num_slots = num_slots;
        this.id = id;
        unique_systems[id] = this;
        this.crafting_type = CraftedItem.CraftingType.CRAFTING;
    }

    #region GetCraftingSystem comment
    /// <summary>
    /// Gets the crafting system associated with a certain ID, or null if one does not exist.
    /// Designed to be fully agnostic towards the existance of a crafting system- so you can safely look for an ID and not
    /// care if you necessarily get one.
    /// </summary>
    /// <param name="id">The unique ID of the crafting system.</param>
    /// <returns>The CraftingSystem with the provided ID, or null if one does not exist.</returns>
    #endregion
    public static CraftingSystem GetCraftingSystem(int id)
    {
        if (!unique_systems.ContainsKey(id)) return null;
        return unique_systems[id];
        
    }

    /// <summary>
    /// A method which we need to override in order to specify grid sizes. This is the only way to preserve polymorphism.
    /// </summary>
    /// <returns>The number of slots belonging to this CraftingSystem</returns>
    public int GetGridSize()
    {
        return num_slots;
    }


    /// <summary>
    /// </summary>
    /// <returns>Number of unique crafting systems in the game world.</returns>
    public static int GetNumCraftingSystems()
    {
        return unique_systems.Values.Count;
    }

    /// <summary>
    /// Provides an array of all the unique IDs corresponding to CraftingSystems. You can use this to iterate through all CraftingSystems.
    /// </summary>
    /// <returns>An array of all ID values, which can be used in GetCraftingSystem(int id)</returns>
    public static int [] GetIDArray()
    {
        int[] ids = new int[unique_systems.Keys.Count];
        unique_systems.Keys.CopyTo(ids, 0);
        return ids;
    }

    #region IsEmpty comment
    /// <summary>
    /// Checks whether an item slot is empty.
    /// </summary>
    /// <param name="x"> Item slot we wish to check. </param>
    /// <returns> Boolean representing whether the item slot is empty. </returns>
    #endregion
    public bool IsEmpty(int x)
    {
        return GetItem(x) == null;
    }

    /// <summary>
    /// </summary>
    /// <returns>True if a Familiar is assigned to this CraftingSystem</returns>
    public bool IsCrewEmpty()
    {
        return crew == null;
    }

    #region GetItem comment
    /// <summary>
    /// Returns the InventoryItem in a specific slot.
    /// </summary>
    /// <param name="x"> Item slot we wish to check. </param>
    /// <returns> InventoryItem in designated slot. </returns>
    #endregion
    public InventoryItem GetItem(int x)
    {
        return (InventoryItem) craftingSlots[x]?.GetHeldObject();
    }

    #region SetItem comment
    /// <summary>
    /// Places designated InventoryItem in designated slot.
    /// </summary>
    /// <param name="item"> Item we wish to insert. </param>
    /// <param name="x"> Slot we wish to insert into. </param>
    #endregion
    public void SetItem(InventoryItem item, int x)
    {
        //checks if item is a thing
        if(item != null)
        {
            //removes item from previous slot
            item.RemoveFromItemHolder();
            //adds it to this slot
            item.SetItemHolder(this);
        }
        //makes a reference to item
        craftingSlots[x].SetHolding(item);

        //checks if an output can be created
        CreateOutput();
        
        //fixes display
        OnGridChanged?.Invoke(this, EventArgs.Empty);
    }

    /// <summary>
    /// Binds a crafting slot to a table, so the CraftingSystem will then watch this slot and use it for recipes.
    /// </summary>
    /// <param name="slot">The slot to bind. A slot should only be set once.</param>
    /// <param name="x">The index of the slot (left to right in GUI/recipes)</param>
    public void SetSlot(CraftingSlot slot, int x)
    {
        if (x >= craftingSlots.Length) return;
        craftingSlots[x] = slot;
    }

    /// <summary>
    /// Sets the crew of this CraftingSystem to a certain familiar.
    /// </summary>
    /// <param name="crew">The familiar who should work this System.</param>
    public void SetCrew(FamiliarWrapper crew)
    {
        this.crew = crew;
        if (crew != null) manned = true;
        else manned = false;
    }

    #region IncreaseItemAmount comment
    /// <summary>
    /// Increments number of items in designated slot.
    /// </summary>
    /// <param name="x"> Slot we wish to add to. </param>
    #endregion
    protected void IncreaseItemAmount(int x)
    {
        GetItem(x).amount++;
        OnGridChanged?.Invoke(this, EventArgs.Empty);
    }

    #region DecreaseItemAmount
    /// <summary>
    /// Decrements number of items in designated slot.
    /// </summary>
    /// <param name="x"> Slot we wish to subtract from. </param>
    #endregion
    protected void DecreaseItemAmount(int x)
    {
        if (GetItem(x) != null)
        {
            InventoryItem tempItem = GetItem(x);
            tempItem.amount--;
            if (tempItem.amount <= 0)
            {
                RemoveItem(x);
            }
            else
            {
                SetItem(tempItem, x);
            }
            OnGridChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    #region RemoveItem
    /// <summary>
    /// Removes item from designated slot.
    /// </summary>
    /// <param name="x"> Slot we wish to remove from. </param>
    #endregion
    protected void RemoveItem(int x)
    {
        SetItem(null, x);
    }

    /// <summary>
    /// Gets the crew currently assigned here, if any.
    /// </summary>
    /// <returns>The crew assigned here, or null if none exist.</returns>
    public FamiliarWrapper GetCrew()
    {
        return crew;
    }

    #region TryAddItem comment
    /// <summary>
    /// Attempts to add designated InventoryItem to designated slot. 
    /// </summary>
    /// <param name="item"> Item we want to add. </param>
    /// <param name="x"> Slot we want to add to. </param>
    /// <returns> Whether or item was successfully added. </returns>
    #endregion
    public bool TryAddItem(InventoryItem item,int x)
    {
        //checks if slot x is empty
        if (IsEmpty(x))
        {
            //places item
            SetItem(item, x);
            return true;
        }
        else
        {
            //checks if item is already in slot x and if its the same item

            if(item.CanMergeWith(GetItem(x)))
            {
                return false;
            }
            else if(item.itemType == GetItem(x).itemType)
            {
                //adds 1 to items amount
                IncreaseItemAmount(x);
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    #region RemoveItem comment
    /// <summary>
    /// Removes designated InventoryItem from system.
    /// </summary>
    /// <param name="item"> Item we want to remove. </param>
    #endregion
    public void RemoveItem(InventoryItem item)
    {
        for(int x = 0; x < GetGridSize(); x++)
        {
            if(GetItem(x) == item)
            {
               RemoveItem(x);
            }
        }
    }

    // Not implemented
    public void AddItem(InventoryItem item)
    {
        throw new System.NotImplementedException();
    }

    // Returns false
    public bool CanAddItem()
    {
        return false;
    }

    #region GetRecipeOutput comment
    //generates the recipe result
    /// <summary>
    /// Generates result of a recipe.
    /// </summary>
    /// <returns> Item resulting from recipe. </returns>
    #endregion
    public CraftedItem GetRecipeOutput()
    {
        Dictionary<CraftedItem, Item[]> recipes = CraftedItem.AllRecipes;
        //cycles through the keys in the recipe dictionary
        Debug.Log(GetGridSize() + ", TYPE: " + crafting_type);
        foreach (CraftedItem recipeItem in recipes.Keys)
        {
            if (recipeItem.crafting_type != crafting_type)
            {
                Debug.Log("NOT THE SAME TYPE! " + recipeItem.crafting_type + " vs " + crafting_type + " for " + recipeItem.getDisplayName());
                continue; // skip this loop if it's not the right kind of crafting
            }

                //pulls out the recipe corresponding with the item
                Item[] recipe = recipes[recipeItem];
            if (recipe.Length == 0) continue;
            //indicator of completion
            bool completeRecipe = true;

            //cycles through x (while x is less than the number of item slots)
            for (int x = 0; x < GetGridSize(); x++)
            {
                //checks if recipe item x is a thing
                if (recipe[x] != null)
                {
                    Debug.Log("Checking for " + recipe[x].getDisplayName() + " in slot " + x);
                    //checks if item slot x is empty or not corresponding to the recipe item
                    if (IsEmpty(x) || GetItem(x).itemType != recipe[x])
                    {
                        Debug.Log("Found " + GetItem(x)?.itemType.getDisplayName() + ", which is not the same");
                        //if empty or doesn't match recipe item, returns false
                        completeRecipe = false;
                    }
                    else
                    {
                        Debug.Log(GetItem(x)?.itemType.getDisplayName() + " was in slot " + x + ". STATUS: " + completeRecipe);
                    }
                }
                else
                {
                    if(!IsEmpty(x))
                    {
                        completeRecipe = false;
                    }
                }
            }
            //if recipe is complete, corresponding item
            if (completeRecipe)
            {
                ready_to_craft[this] = recipeItem;
                Debug.Log("ready to craft " + recipeItem.getDisplayName());
                return recipeItem;
            }
        }

        //else, return nothing
        ready_to_craft[this] = null;
        return null;
    }

    #region CreateOutput comment
    /// <summary>
    /// Sets CraftedItem output to this system's output.
    /// </summary>
    #endregion
    public void CreateOutput()
    {
        //assigns the recipe output
        CraftedItem recipeOutput = GetRecipeOutput();

        //if output is of type none
        if (recipeOutput == null)
        {
            output = null;
        }
        else
        {
            //else output becomes corresponding item
            output = new InventoryItem(recipeOutput);
            output.SetItemHolder(this);

        }
    }

    /// <summary>
    /// Manually subtracts an amount of days from the current recipe's production timeline. If you want to add days to production, just pass in a negative number.
    /// 
    /// DO NOT CALL THIS METHOD <b>DURING</b> THE DAY! This is only to be called in places like <see cref="Ability.OnDayEnd"/> and never in a place where the player could alter what the
    /// recipe is!
    /// </summary>
    /// <param name="amt">The amount of days to reduce</param>
    public void ReduceDaysToCraft(int amt)
    {
        // because this method will likely be called before crafting, we need to consider the case where this is the first day a recipe has been progressed.
        // Thus, if that's the case, go ahead and set up the output to the thing that is gonna be crafted.
        if(days_remaining == -1)
        {
            CraftedItem possible_output = GetRecipeOutput();
            if (possible_output != null)
            {
                days_remaining = possible_output.days_to_craft;
                days_total = days_remaining;
            }
            else return; // if there's nothing to be outputted, then forget about it. There's no pending production time to change.
        }

        days_total -= amt;
        days_remaining -= amt;
        if(days_remaining <= 0){
            days_remaining = 1;
            days_total = 1;
        } 
    }

    /// <summary>
    /// Manually sets the number of days to craft the current item in production. Used for Abilities which modify the amount of time production takes.
    /// Keep in mind that this may be overridden if the recipe changes, and also that crafting times will take a minimum of one day.
    /// </summary>
    /// <param name="days">The new amount of days the recipe should take to produce.</param>
    public void SetDaysToCraft(int days)
    {
        if (days_remaining == -1) return;
        days_remaining = days;
        if (days_remaining <= 0) days_remaining = 1;

    }

    /// <summary>
    /// Attempts to produce the output designated by the recipe, and if successful will output it to the "output" parameter.
    /// 
    /// May fail if there is no crew manning the system, if there is still time left on the recipe, or if there's insufficient
    /// materials/wrong materials for a recipe.
    /// </summary>
    /// <param name="output">The InventoryItem which is produced by this recipe.</param>
    /// <returns>True if the output was produced, or false otherwise. If true, output will not be null.</returns>
    public bool TryProduceOutput(out InventoryItem output)
    {
        output = null;
        if(manned)
        {
            CraftedItem currentRecipe = GetRecipeOutput();
            if(currentRecipe != null)
            {
                if(days_remaining <= 0)
                {
                    days_remaining = currentRecipe.GetDaysToCraft();
                    days_total = days_remaining;
                    last_recipe = currentRecipe;
                }


                if(days_remaining <= 1)
                {
                    output = new InventoryItem(currentRecipe);
                    days_remaining = -1;
                    days_total = -1;
                    last_recipe = null;
                    return true;
                }
                else if(last_recipe == null || !last_recipe.Equals(currentRecipe))
                {
                    last_recipe = currentRecipe;
                    days_remaining = days_total - 1; //if we're trying to make this recipe for the first day in a row, set our days remaining to what they should be. -1 because we're including the day we're taking right now
                }
                else if(last_recipe.Equals(currentRecipe))
                {
                    days_remaining--;
                }
            }
        }
        return false;
    }

    //returns the output item
    #region GetOutputItem
    /// <summary>
    /// Returns item output.
    /// </summary>
    /// <returns> Item output of system. </returns>
    #endregion
    public InventoryItem GetOutputItem()
    {
        return output;
    }
    
    #region ConsumeRecipeItems comment
    /// <summary>
    /// Decrements number of each item in display.
    /// </summary>
    #endregion
    public void ConsumeRecipeItems()
    {
        for(int x = 0; x < GetGridSize(); x++)
        {
            DecreaseItemAmount(x);
        }
    }

    // Sets assigned crew to null.
    public void RemoveCrew(FamiliarWrapper crew)
    {
        SetCrew(null);
    }

    public int GetDaysRemaining()
    {
        return days_remaining;
    }

    public int GetDaysTotal()
    {
        return days_total;
    }

    // Not implemented.
    public void AddCrew(FamiliarWrapper crew)
    {
        throw new NotImplementedException();
    }

    // Returns false.
    public bool CanAddCrew()
    {
        return false;
    }
}
