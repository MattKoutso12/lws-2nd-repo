using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class MarketGraph : MonoBehaviour
{
    RectTransform rect;
    Image image;

    //[SerializeField]
    //public List<int> priceHistory = new List<int>();

    const int DAY_SPAN = 7; // graph spans 7 days.
    int[] pricesNow = new int[DAY_SPAN]; //  each day's prices from the last 5 days.


    int recentHigh = -1;
    int recentLow = -1;

    int allTimeHigh = -1;
    int allTimeLow = -1;

    const float V_PADDING_PERCENT = 0.1f; // leave 10% for padding recent highs and lows with this price

    public Item selected_item = null;

    // Start is called before the first frame update
    void Start()
    {
        rect = GetComponent<RectTransform>();
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        UpdateGraph();
    }


    /// <summary>
    /// Select a commodity
    /// </summary>
    /// <param name="item"></param>
    public void SetSelected(Item item)
    {
        selected_item = item;
        UpdateGraph();
    }

    public int lineSize = 2;

    void UpdateGraph()
    {
        Texture2D graph = new Texture2D(300, 150);
        graph.filterMode = FilterMode.Point;
        //graph.alphaIsTransparency = true;
        graph.wrapMode = TextureWrapMode.Clamp;
        for(int i = 0; i < graph.height; i++)
        {
            for(int j = 0; j < graph.width; j++)
            {
                graph.SetPixel(j, i, Color.clear);
            }
        }


        for(int i = 0; i < 4; i++)
        {
            int y = (graph.height / 4) * i;
            PlotHorizontalLine(graph, y, Color.grey, dotted: true);
        }
        PlotHorizontalLine(graph, graph.height - 1, Color.grey, dotted: true);
        PlotVerticalLine(graph, graph.width - 1, Color.grey, dotted: true);

        if(allTimeHigh != -1)
        {
            PlotHorizontalLine(graph, (int)PriceToYCoordinate(allTimeHigh) + graph.height/3, Color.cyan, true);
        }

        if(allTimeLow != -1)
        {
            PlotHorizontalLine(graph, (int)PriceToYCoordinate(allTimeLow) + graph.height/3, Color.yellow, true);
        }

        List<int> priceHistory = new List<int>();
        if(selected_item != null)
            priceHistory = selected_item.GetPriceHistory();

        Vector2 lastPos = Vector2.zero;
        bool lastWasDown = false;
        for(int i = 0; i < DAY_SPAN; i++)
        {

            PlotVerticalLine(graph, i * 50, Color.grey, true);

            // get the price at this day
            int priceHere;
            if (priceHistory.Count >= DAY_SPAN)
                priceHere = priceHistory[(priceHistory.Count - DAY_SPAN) + i];
            else
            {
                try
                {
                    priceHere = priceHistory[i];
                }catch(System.ArgumentOutOfRangeException)
                {
                    continue; // end our drawin' if we're out of pricing data.
                }
            }
            Vector2 pos = new Vector2(i * (50), PriceToYCoordinate(priceHere));
            if(lastPos == Vector2.zero)
            {
                lastPos = pos;
                continue;
            }


            Vector2Int p1, p2;

            p1 = Vector2Int.RoundToInt(lastPos);
            p2 = Vector2Int.RoundToInt(pos);



            PlotVerticalLine(graph, p1.x, Color.grey, true);

            //p1.y += graph.height / 3;
            //p2.y += graph.height / 3;


            // Bresenham's Line Algorithm
            // https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm

            Color lineColor = up;
            if (p1.y > p2.y) lineColor = down;
            for (int j = 0; j < lineSize; j++)
            {
                p1.y -= j;
                p2.y -= j; // draw multiple lines to THICKEN!

                if (Mathf.Abs(p2.y - p1.y) < Mathf.Abs(p2.x - p1.x))
                {
                    if (p1.x > p2.x)
                    {
                        PlotLow(graph, p2.x, p2.y, p1.x, p1.y, lineColor);
                    }
                    else
                    {
                        PlotLow(graph, p1.x, p1.y, p2.x, p2.y, lineColor);
                    }
                }
                else
                {
                    if (p1.y > p2.y)
                    {
                        PlotHigh(graph, p2.x, p2.y, p1.x, p1.y, lineColor);
                    }
                    else
                    {
                        PlotHigh(graph, p1.x, p1.y, p2.x, p2.y, lineColor);
                    }
                }
            }

            PlotTriangle(graph, p2, 4, Color.blue, p2.y < p1.y);
            PlotTriangle(graph, p1, 4, Color.blue, lastWasDown);

            lastPos = pos;
            if (p2.y < p1.y) lastWasDown = true;
            else lastWasDown = false;   
        }

        // Draw a line 
        //graph.Resize(1200, 660);
        graph.Apply();
        image.sprite = Sprite.Create(graph, new Rect(0, 0, graph.width, graph.height), Vector2.zero);


        

        //image.preserveAspect = true;
    }

    void PlotTriangle(Texture2D graph, Vector2Int center, int size, Color color, bool upsidedown = false)
    {
        Vector2Int tempCenter = center;

        if(upsidedown)
        {
            tempCenter.y += size / 2;
        }
        else
        {
            tempCenter.y -= size / 2;
        }
        tempCenter.x += size / 2;

        for (int i = 0; i < size; i++)
        {
            for(int j = i-size; j < size - i; j++)
            {
                graph.SetPixel(tempCenter.x + (j - size/2),
                    (upsidedown ? tempCenter.y - i : tempCenter.y + i),
                    color);
            }
        }
    }

    void PlotVerticalLine(Texture2D graph, int x, Color color, bool dotted = false)
    {
        for(int y = 0; y < graph.height; y++)
        {
            if (!dotted || y % 2 == 0)
                graph.SetPixel(x, y, color);
        }
    }

    void PlotHorizontalLine(Texture2D graph, int y, Color color, bool dotted = false)
    {

        for(int x = 0; x < graph.width; x++)
        {
            if(!dotted || x%2==0)
                graph.SetPixel(x, y, color);
        }
    }

    [SerializeField]
    public Color up = Color.green;
    [SerializeField]
    public Color down = Color.red;

    void PlotLow(Texture2D graph, int x1, int y1, int x2, int y2, Color? optionalColor = null)
    {
        int dx = x2 - x1;
        int dy = y2 - y1;

        int D = (2 * dy) - dx;
        int y = y1;
        int yi = 1;
        Color lineColor;
        if(dy <  0)
        {
            yi = -1;
            dy = -1 * dy;
        }

        if (optionalColor != null)
        {
            lineColor = (Color)optionalColor;
        }
        else
        {
            lineColor = up;
        }

        for (int x = x1; x < x2; x++)
        {
            graph.SetPixel(x, y, lineColor);
            if (D > 0)
            {
                y += yi;
                D += (2 * (dy-dx));
            }
            else D += 2 * dy;
        }
    }

    void PlotHigh(Texture2D graph, int x1, int y1, int x2, int y2, Color? optionalColor = null)
    {
        int dx = x2 - x1;
        int dy = y2 - y1;

        int D = (2 * dy) - dx;
        int x = x1;
        int xi = 1;
        Color lineColor = up;
        
        if (dx < 0)
        {
            xi = -1;
            dx = -1 * dx;
        }

        if(optionalColor != null)
        {
            lineColor = (Color)optionalColor;
        }

        for (int y = y1; y < y2; y++)
        {
            graph.SetPixel(x, y, lineColor);
            if (D > 0)
            {
                x += xi;
                D += (2 * (dx - dy));
            }
            else D += 2 * dx;
        }
    }


    const int TEMPORARY_MAX_PRICE = 100; // right now, don't adjust graph size, just say nothing will go over 100 gold.
    float PriceToYCoordinate(int price)
    {
        float t_height = 100*(1f-(V_PADDING_PERCENT*2));
        float coord = (((float)price / (float)selected_item.allTimeHigh) * t_height) + (V_PADDING_PERCENT * t_height);
        return coord;
    }

    /*
    public void addPrice(int price)
    {
        priceHistory.Add(price);
        if(allTimeHigh == -1 || price > allTimeHigh)
        {
            allTimeHigh = price;
        }

        if(allTimeLow == -1 || price < allTimeLow)
        {
            allTimeLow = price;
        }
    }
    */
}
