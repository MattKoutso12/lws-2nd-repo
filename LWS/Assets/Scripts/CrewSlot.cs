using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CrewSlot : UISlot
{
    public static Dictionary<int, CrewSlot> crew_slots = new Dictionary<int, CrewSlot>();

    public static Dictionary<int, List<CrewSlot>> internal_crew_slots = new Dictionary<int, List<CrewSlot>>();

    public static Dictionary<int, IDraggableObject> linked_contents = new Dictionary<int, IDraggableObject>();

    [SerializeField] public int id = -1;

    CraftingSystem mySystem;

    [SerializeField] public Crew startWithCrew;

    

    public List<Crew> crewtypes;

    /// <summary>
    /// INTERNAL ID CONVENTIONS:
    /// 
    /// We're doing this like apartment numbers, so an id of 105 doesn't mean it's the 105th slot!
    /// 
    /// IDs < 100: The player's main crew inventory
    /// 100 to 199: Party Slots
    /// 
    /// </summary>
    [SerializeField]
    [Tooltip("Use this to force the internal ID. We will use conventions for this, see CrewSlot.cs for more info. LEAVE AT -1 UNLESS YOU UNDERSTAND HOW TO USE THIS")]
    public int forcedInternalID = -1;

    static int runningInternalID = 0;

    

    int internal_id;

    public uint _id;
    static HashSet<Transform> parents = new HashSet<Transform>(); // we store unique parents in here, and use them for indexing the ID.
    public static Dictionary<uint, CrewSlot> id_to_slot = new Dictionary<uint, CrewSlot>();

    public delegate void CrewSlotEvent(int internal_id, FamiliarWrapper crew);
    public static event CrewSlotEvent OnCrewChanged;

    [System.Serializable]
    public struct SerializableCrewSlot
    {
        public FamiliarWrapper.SerializableFamiliarWrapper crew;
        public int id;
    }

    private void Awake()
    {
        display = transform.GetChild(0).GetChild(0).GetComponent<Image>();

        // if we haven't seen this parent before (i.e. we are the first ItemSlot child of this parent to hit Awake)...

        if (!parents.Contains(transform.parent) || _id == 0)
        {
            // ... then we will initialize ALL children of that parent IN SEQUENCE based on their heirarchy position!
            parents.Add(transform.parent);
            int parentID = (int)(transform.parent.position.sqrMagnitude);

            // "Apartment Number" system. First digit is the parent's "ID", the rest is the child's sibling index.
            // We need this so IDs are both unique overall and the same every time you load the game.
            CrewSlot [] siblings = transform.parent.GetComponentsInChildren<CrewSlot>();
            for (int i = 0; i < siblings.Length; i++)
            {
                // initialize siblings!
                uint newID = uint.Parse(parentID + "" + i); // this should always be unique
                //if (id_to_slot.ContainsKey(newID) && id_to_slot[newID].GetInstanceID() != siblings[i].GetInstanceID()) Debug.LogError($"DUPLICATE ID!!! {siblings[i].name} and {id_to_slot[newID].name} on {newID}!!!");
                siblings[i]._id = newID;
                id_to_slot[newID] = siblings[i];
            }


        }

        Debug.Log("POSITION: (" + name + ") " + _id); // we're trying this apeshit method lol




        // if you have any questions on my methods here please go watch Christopher Nolan's 'The Prestige'. Also, spoiler alert for The Prestige.
        if (forcedInternalID == -1)
            internal_id = runningInternalID++; // set our ID and increment it for the next guy
        else
            internal_id = forcedInternalID;
        
        
        if (!internal_crew_slots.ContainsKey(internal_id) || internal_crew_slots[internal_id] == null)
            internal_crew_slots[internal_id] = new List<CrewSlot>();
        internal_crew_slots[internal_id].Add(this);
        linked_contents[internal_id] = this.holding;
    }

    public SerializableCrewSlot GetSerializedVersion()
    {
        SerializableCrewSlot slot = new SerializableCrewSlot();
        if (IsEmpty())
        {
            slot.id = 0;
            return slot;
        }
        slot.crew = ((FamiliarWrapper)this.GetHeldObject()).GetSerializedVersion();
        slot.id = this.internal_id;

        Debug.Log("SAVING: " + slot.crew.displayName + " " + slot.id);


        return slot;
    }


    // Start is called before the first frame update
    void Start()
    {
        mySystem = CraftingSystem.GetCraftingSystem(id);
        if(startWithCrew != null)
        {
            this.holding = new FamiliarWrapper(startWithCrew);
            linked_contents[internal_id] = this.holding;
        }

        OnCrewChanged += CrewSlot_OnCrewChanged;
    }


    public void ForceInternalID(int new_id)
    {
        internal_crew_slots[internal_id].Remove(this); // unbind us from our old binding
        this.internal_id = new_id;
        if (internal_crew_slots[internal_id] == null)
            internal_crew_slots[internal_id] = new List<CrewSlot>();
        internal_crew_slots[internal_id].Add(this);
        if(linked_contents.ContainsKey(internal_id))
        {
            holding = linked_contents[internal_id];
        }
    }

    private void CrewSlot_OnCrewChanged(int internal_id, FamiliarWrapper crew)
    {
        

        if (internal_id != this.internal_id) return; // not us

        // otherwise, it IS us

       
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        if(holding != null)
        {
            ((FamiliarWrapper)holding).crew.TriggerViewEvent();
        }
    }

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);

        Debug.Log($"CONTENTS CHANGED AT ID: {internal_id} from {old_holding} to {new_holding} ");
        OnCrewChanged?.Invoke(internal_id, (FamiliarWrapper)new_holding);

        linked_contents[internal_id] = new_holding;

        //((FamiliarWrapper)new_holding).current_slot_internal_id = internal_id;

        foreach(CrewSlot slot in internal_crew_slots[internal_id]) // fix desync
        {
            if (slot == this) continue;

            slot.holding = new_holding;
        }

        if (id == -1) return;
        if(new_holding != null)
        {
            FamiliarWrapper crew = (FamiliarWrapper)GetHeldObject();

            mySystem.manned = true;
            mySystem.SetCrew(crew);
        }
        else
        {
            mySystem.manned = false;
            mySystem.SetCrew(null);
        }
    }

    

    // Update is called once per frame
    void Update()
    {
        if (this.holding != linked_contents[internal_id])
            this.holding = linked_contents[internal_id];
    }


}
