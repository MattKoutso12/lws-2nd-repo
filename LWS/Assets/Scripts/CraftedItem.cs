﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Crafted Item", menuName = "Items/CraftedItem")]
/// <summary>
/// Represents an item obtained through crafting; inherits from Item.
/// </summary>
public class CraftedItem : Item
{
    // now we store recipes in CraftedItem because that way we can keep track of everything that is crafted from ground zero. Data on items which are crafted is from CraftedItem.
    public static Dictionary<CraftedItem, Item[]> AllRecipes = new Dictionary<CraftedItem, Item[]>();

    
    public static readonly string[] SLOT_COLORS =
    {
        "#B100BA", // the purple slot
        "#E04674", // the pink-ish slot
        "#FAF087", // the yellow slot
        "#008080ff" // the teal slot
    };

    public static readonly string SEED_SLOT_HEX = "#64A63D";
    public static readonly string WATER_SLOT_HEX = "#004EBA";

    public enum CraftingType
    {
        CRAFTING,
        PLANTING
    }


    public int availableOnDay = 0;

    [SerializeField]
    public CraftingType crafting_type;

    [SerializeField]
    public Item[] recipe; //NOTE: 4 slots is currently hardcoded as the max. Go here if we want to change that.

    [SerializeField]
    public int days_to_craft = 1;

    #region CraftedItem comment
    /// <summary>
    /// Constructor function for a CraftedItem
    /// </summary>
    /// <param name="item_name"> Name of item. </param>
    /// <param name="sprite"> Sprite of item. </param>
    /// <param name="recipe"> Recipe of item. </param>
    /// <param name="crafting_type"> Whether planted or crafted. </param>
    /// <param name="stackable"> Whether stackable or not. </param>
    /// <param name="sellPrice"> Selling price. </param>
    /// <param name="displayed_name"></param>
    /// <param name="use_text"></param>
    #endregion
    public CraftedItem(string item_name, Sprite sprite, Item[] recipe, CraftingType crafting_type,  bool stackable = true, int sellPrice = 5, string displayed_name = "", string use_text = "", int level = 1, int days_to_craft = 1) 
        : base(item_name, sprite, stackable, sellPrice, displayed_name, use_text, level)
    {
        this.recipe = recipe;
        AllRecipes.Add(this, recipe);
        this.crafting_type = crafting_type;
        this.days_to_craft = days_to_craft;
    }

    /// <summary>
    /// Intitializes all non-empty recipes.
    /// </summary>
    public new void Init()
    {
        base.Init();
        Debug.Log("Initializing Recipe for " + getIDName());
        if (AllRecipes.ContainsKey(this)) return;
        foreach (Item i in recipe)
        {
            if(i != null)
            {
                AllRecipes.Add(this, recipe);
                break;
            }
        }
        
    }


    

    public override int GetCurrentMarketPrice()
    {
        int tempPrice = 1;
        foreach(Item i in recipe)
        {
            if (i != null)
                tempPrice += i.GetCurrentMarketPrice();
        }

        if (tempPrice < price_min) tempPrice = price_min;
        if (tempPrice > price_max) tempPrice = price_max;
        float craftingFactor = 1.25f; // value of labor is 25% of the cumulative value of the ingredients, for now

        if (crafting_type == CraftingType.PLANTING) craftingFactor = 1.0f;

        return Mathf.CeilToInt(tempPrice * 1.10f);
    }

    public override void GenerateWeightedPrice()
    {
        int newPrice = GetCurrentMarketPrice();
        GetPriceHistory().Add(newPrice);
        if (newPrice > allTimeHigh) allTimeHigh = newPrice;
        if (newPrice < allTimeLow) allTimeLow = newPrice;
    }

    public int GetDaysToCraft() { return days_to_craft; }

    #region GetTooltipInfo comment
    /// <summary>
    /// Returns tooltip information for a CraftedItem.
    /// </summary>
    /// <returns> String representing tooltip info. </returns>
    #endregion
    public override string GetTooltipInfo()
    {
        string basicText = base.GetTooltipInfo();
        //creates a stringbuilder object
        StringBuilder builder = new StringBuilder();
        builder.Append(basicText); // append all the normal Item info



        builder.Append("\tALT + \t<sprite=66> to <b>VIEW RECIPE</b>");



        /*
        if (crafting_type == CraftingType.PLANTING)
            builder.Append("<color=black>Growing (Planter): <b>").AppendLine();
        else
            builder.Append("<color=black>Ingredients (Crafting): </color><b>").AppendLine();


        for(int i = 0; i < recipe.Length; i++)
        {

            if (crafting_type == CraftingType.CRAFTING)
                builder.Append($"<color={SLOT_COLORS[i]}>#{i+1}: ");

            else if (crafting_type == CraftingType.PLANTING)
            {
                if (i == 0)
                    builder.Append($"<color={SEED_SLOT_HEX}>Seeds: \t");
                else if (i == 1)
                    builder.Append($"<color={WATER_SLOT_HEX}>Water With: \t");
                else if(i >= 2)
                {
                    builder.AppendLine();
                    break;
                }
            }    

            builder.AppendLine("\t"+(recipe[i] == null ? "Empty" : recipe[i].getDisplayName()) + "</color>");

            /*
            builder.Append("\t");
            if(recipe[i] != null)
            {
                builder.Append(recipe[i].getDisplayName());
            }
            else
            {
                builder.Append("Empty");
            }
            builder.AppendLine("</color>"); 
        }
        builder.AppendLine("</b>");
        */
        /*

        foreach (string ingredient in ingred)
        {
            builder.Append(ingredient);
            if(i == recipe.Length -1)
            {
                builder.Append("</color>").AppendLine();
            } else if(ingred[i] != ingred[ingred.Length - 1])
            {
                builder.AppendLine();
            }
            i++;
        }
        */


        //returns the built up string
        return builder.ToString();
    }
}