using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;


// InventoryItem now represents an IN WORLD INSTANCE of an Item. Item is the blueprint for InventoryItem, it acts as a wrapper.
/// <summary>
/// An in-world instance of an item. 
/// </summary>
[Serializable]
public class InventoryItem : IDraggableObject, IToolTipable, ISerializable
{
    // this is the new way. Items will hold data concerning items. What a concept
    public readonly Item itemType;



    public int amount = 1;
    private IItemHolder itemHolder;
    public bool buyable = false;

    #region InventoryItem comment
    /// <summary>
    /// Constructor method for InventoryItem.
    /// </summary>
    /// <param name="item_type"> The item itself. </param>
    /// <param name="amount"> The amount of the item. </param>
    #endregion
    public InventoryItem(Item item_type, int amount = 1)
    {
        this.itemType = item_type;
        this.amount = amount;
    }

    #region InventoryItem comment
    /// <summary>
    /// Constructor method taking a string name for quick instancing.
    /// </summary>
    /// <param name="item_name"> Name of item. </param>
    /// <param name="amount"> Amount of the item. </param>
    #endregion
    public InventoryItem(string item_name, int amount = 1)
    {
        if(Item.TryGetItem(item_name, out itemType))
        {
            this.amount = amount;
        }
        else
        {
            Debug.LogError("Item Error: Definition " + item_name + " does not exist!");

        }
    }


    /// <summary>
    /// The constructor called during deserialization
    /// </summary>
    /// <param name="info">The object holdingg our serialization info</param>
    /// <param name="context"></param>
    protected InventoryItem(SerializationInfo info, StreamingContext context)
    {
        string item_name = info.GetString("item_name");
        int amount = info.GetInt32("amount");
        if (Item.TryGetItem(item_name, out itemType))
        {
            this.amount = amount;
        }
        else
        {
            Debug.LogError("Item Error: Definition " + item_name + " does not exist!");

        }
    }

    public void SetItemHolder(IItemHolder itemHolder)
    {
        this.itemHolder = itemHolder;
    }

    public IItemHolder GetItemHolder()
    {
        return itemHolder;
    }

    /// <summary>
    /// Removes item from current item holder.
    /// </summary>
    public void RemoveFromItemHolder()
    {
        if (itemHolder != null)
        {
            // Remove from current Item Holder
            itemHolder.RemoveItem(this);
        }
    }

    #region MoveToAnotherItemHolder comment
    /// <summary>
    /// Moves item from current holder to a new holder.
    /// </summary>
    /// <param name="newItemHolder"> Holder we ant to move the item to. </param>
    #endregion
    public void MoveToAnotherItemHolder(IItemHolder newItemHolder)
    {
        RemoveFromItemHolder();
        // Add to new Item Holder
        newItemHolder.AddItem(this);
    }

    #region GetSprite comment
    /// <summary>
    /// Returns sprite of this item. 
    /// </summary>
    /// <returns> Sprite of the item. </returns>
    #endregion
    public Sprite GetSprite()
    {
        if(itemType.sprite == null)
        {
            Debug.LogError("Item Error: Definition for " + itemType.getIDName() + " has no sprite!");
            return null;
        }
        return itemType.sprite;
    }

    #region IsStackable comment
    /// <summary>
    /// Returns whether or not the item can stack with others of itself.
    /// </summary>
    /// <returns> Whether the item can stack. </returns>
    #endregion
    public bool IsStackable()
    {
        return itemType.stackable;
    }


    public override string ToString()
    {
        return itemType.ToString();
    }


    // UI STUFF

    public string ColouredName => ((IToolTipable)itemType).ColouredName;


    public bool CanMergeWith(IDraggableObject other)
    {
        if (other == null)
        {
            return true;
        }

        if (!(other is InventoryItem)) return false;

        return this.itemType.Equals(((InventoryItem)other).itemType);

    }

    public string GetTooltipInfo()
    {
        return ((IToolTipable)itemType).GetTooltipInfo();
    }

    public IDraggableObject CombinedWith(IDraggableObject other)
    {
        if (other == null)
        {
            return this;
        }
        
        if (other is InventoryItem)
        {
            InventoryItem temp = (InventoryItem)other;
            if (temp.itemType.Equals(this.itemType))
            {
                temp.amount += this.amount;
                return temp;
            }
        }
        return null;
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        Debug.Log("Saving Item: " + itemType.getIDName());
        info.AddValue("item_name", itemType.getIDName());
        info.AddValue("amount", amount);
    }
}
