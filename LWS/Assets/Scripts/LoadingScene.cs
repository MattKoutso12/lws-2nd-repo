using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

/// <summary>
/// Handles the cutscene which plays between the main menu and LWS scenes.
/// </summary>
public class LoadingScene : MonoBehaviour
{
    //all variables

    public GameObject startButton;
    
    //public Camera cam;

    public GameObject witch;
    public float moveSpeed = 2f;
    public Transform[] MapWaypoints;
    int waypointIndex = 0;
    public float startTimer = 5;
    public bool willMove;
    public bool startGame;
    

    // Start is called before the first frame update
    void Start()
    {
        witch.transform.position = MapWaypoints[waypointIndex].transform.position;
        
        
        
        startButton.SetActive(false);
        willMove = true;
        startGame = false;
    //    cam.orthographicSize = 5f;
    }



    void Update()
    {
        
        startTimer -= Time.deltaTime;
       
        if (willMove == true)
        {
            CameraMoving();
        }

        if (startGame == true)
        {
            
            startButton.SetActive(true);
        //    cam.orthographicSize = 3f;
        }

    }


    /// <summary>
    /// Switches to the LWS scene.
    /// </summary>
    public void StartGame()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/click 1");
        SceneManager.LoadScene(sceneName: "LWS");
    }
    
    /// <summary>
    /// Moves witch along preset waypoints.
    /// </summary>
    public void CameraMoving()
    {
        if (waypointIndex <= MapWaypoints.Length - 1)
        {
            witch.transform.position = Vector2.MoveTowards(witch.transform.position,
                MapWaypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);

        }

        if (witch.transform.position == MapWaypoints[waypointIndex].transform.position)
        {
            waypointIndex += 1;
        }

        if (waypointIndex == MapWaypoints.Length)
        {
            willMove = false;
            startGame = true;
        }
    }
    
}