using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

/// <summary>
/// Handles methods for various Buttons.
/// </summary>
public class ButtonManager : MonoBehaviour
{
    public GameObject[] EnabledOnStart;

    [SerializeField] UI_CraftingSystem craftingTester; //crafting system 1
    [SerializeField] GameObject crafter;
    [SerializeField] UI_PlantingSystem plantSystem; //planting system 1
    [SerializeField] GameObject planter;
    [SerializeField] GameObject invent;
    [SerializeField] GameObject testing; //players inventory reference
    [SerializeField] UI_CrewInventory crew; //players crew inventory reference
    [SerializeField] GameObject crewInventry;
    [SerializeField] GameObject buySeedScreen; //reference to the PP menu
    [SerializeField] public Player player; //reference to the player object
    [SerializeField] GameObject crewCard; //reference to the CC prefab
    [SerializeField] GameObject fHouse; //reference to the familiar house
    [SerializeField] public GameObject recipe; //reference to the recipe book
    [SerializeField] Button hideButton; //the button that hides the Inv/rep menu
    [SerializeField] GameObject tabTail; //the rolldown of the WAND menu
    [SerializeField] GameObject tabHead; //the top of the WAND menu
    [SerializeField] GameObject staging; //the reference to the staging screen
    [SerializeField] UIStaging stagingSystem; //the reference to the staging system
    [SerializeField] GameObject helpGuideImage; //the help image
    [SerializeField] GameObject controlsImage; //the controls image
    [SerializeField] GameObject hud; //the HUD image
    [SerializeField] GameObject dayToNight; //the day to night feedback canvas element
    protected List<GameObject> systems; //List of systems involved in the game
    [SerializeField] CsPsCreator[] csPss; //array of CsPsCs
    [SerializeField] Spawner spawner; //Familiar spirit orb spawner
    [SerializeField] GameObject chapterCard;

    [SerializeField] GameObject blocker;

    public static ButtonManager Instance;

    //Dialogue related
    [SerializeField] GameObject raycastBlocker;
    public float speed;

    //audio
     FMOD.Studio.EventInstance gameMusic;

    public void SetTalking(bool active)
    {

        if(!active)
        {
            raycastBlocker.SetActive(false);
            Player.Instance.SetMoveLock(false);
            chapterCard.SetActive(true);
            crewInventry.SetActive(true);
            hud.SetActive(true);
            tabHead.SetActive(true);
            DCanvController.Instance.EnableCanvas(false);
            return;
        }

        raycastBlocker.SetActive(true);
        Player.Instance.SetMoveLock(true);
        hud.SetActive(false);
        chapterCard.SetActive(false);
        crewInventry.SetActive(false);
        tabHead.SetActive(false);
        fHouse.SetActive(false);
        buySeedScreen.SetActive(false);
        DCanvController.Instance.EnableCanvas(true);
    }

    //On Awake, creates the systems List and subscribes the proper spawning events to the spawning function
    private void Awake()
    {
        Instance = this;
        systems = new List<GameObject>();

        //cycles through the CsPss
        foreach (CsPsCreator csPs in csPss)
        {
            //subscribes the spawn event to the spawn function
            csPs.OnSystemSpawn += BM_OnSystemSpawn;
        }

        //audio
        //gameMusic = FMODUnity.RuntimeManager.CreateInstance("event:LWS/Music/All Song Parts");
        //gameMusic.start();
    }

    void Start() 
    {
        PersistantManager.OnDayChanged += OnDayChanged;        
    }

    private void OnDayChanged(object sender, EventArgs e)
    {
        Debug.Log("NEW DAY");
    }

    //Gets the type of system passed into the function and checked; added to the systems List and assigned to the correct variables
    private void BM_OnSystemSpawn(object sender, CsPsCreator.OnSystemSpawnedEventArgs e)
    {
        //if system is crafting
        if(e.x != null)
        {
            systems.Add(e.x.gameObject);
            craftingTester = e.x;
        }
        //if system is planting
        if(e.y != null)
        {
            systems.Add(e.y.gameObject);
            plantSystem = e.y;
        }
    }
    
    /// <summary>
    /// Runs the processes for when a day passes. Deactivates active systems, activates staging,
    /// retrieves all item outputs, deactivates active visuals, disables booleans, and pauses the game.
    /// </summary>
    public void Sleep()
    {
        //Debug.Log("ZZZZZZZ");
        //deactivates all systems
        foreach(GameObject sys in systems)
        {
            //Debug.Log(sys.name);
            sys.SetActive(false);
        }

        //activates staging
        staging.SetActive(true);

        //deactivates the other visuals
        hud.SetActive(false);
        tabHead.SetActive(false);
        fHouse.SetActive(false);

        //turns off booleans
        PersistantManager.Instance.craftingVisible = false;
        PersistantManager.Instance.fHouseVisible = false;
        PersistantManager.Instance.helpGuideVisible = false;
        PersistantManager.Instance.plantingVisible = false;
        PersistantManager.Instance.seedScreenVisible = false;

        //"pauses" the game
        Player.Instance.SetMoveLock(true);

    }

    public void UnSleep()
    {
        //deactivates staging
        staging.SetActive(false);

        hud.SetActive(true);
        tabHead.SetActive(true);

        //"unpauses" the game
        Player.Instance.SetMoveLock(false);
    }

    public void Mute(bool active)
    {
        gameObject.GetComponent<StudioEventEmitter>().enabled = active;
    }

    /// <summary>
    /// Runs the processes for when a day begins. Unpauses the game, activates visuals, progresses crafting times,
    /// deactivates staging, increments days, and activates the ROUI.
    /// </summary>
    public void AwakenChildYouHaveADestinyToLiveUpTo()
    {
        Player.Instance.transform.position = new Vector3(100, -100, Player.Instance.transform.position.z);

        PersistantManager.Instance.goldTouch = false;
        PersistantManager.Instance.trader = false;

        //advances council missions
        CouncilReputation.Instance.RunCouncilTests();

        //"unpauses" the game
        Player.Instance.SetMoveLock(false);

        //activates the other visuals
        hud.SetActive(true);
        tabHead.SetActive(true);

        //spawns a new spirit orb
        //spawner.SpawnObject();

        AuctionScreen.Instance.RefreshStock();

        //increases day
        PersistantManager.Instance.IncreaseDay();

        //activates the ROUI
        //roui.SetActive(true);
    }


    /// <summary>
    /// Enables the crafting table display.
    /// </summary>
    public void ToggleCrafting()
    {
        Debug.Log("Toggled Crafting");
        PersistantManager.Instance.craftingVisible = !PersistantManager.Instance.craftingVisible;
        craftingTester.gameObject.SetActive(PersistantManager.Instance.craftingVisible);
        
        /*
        if (PersistantManager.Instance.craftingVisible)
        {
            craftingTester.gameObject.GetComponent<Button>().enabled = false;
        }
        else
        {
            craftingTester.gameObject.GetComponent<Button>().enabled = true;
        }
        */

        //Sound for menu
        //FMODUnity.RuntimeManager.PlayOneShot("event:/LWS Potions");

        if (PersistantManager.Instance.craftingVisible)
        { 
            gameMusic.setParameterByName("Soften Sound", 1);
        }
        else
        {
            gameMusic.setParameterByName("Soften Sound", 0);
        }
        
    }

    /// <summary>
    /// Enables the 1st planter's display.
    /// </summary>
    public void TogglePlanting()
    {
        PersistantManager.Instance.plantingVisible = !PersistantManager.Instance.plantingVisible;
        plantSystem.gameObject.SetActive(PersistantManager.Instance.plantingVisible);

        /*
        if (PersistantManager.Instance.plantingVisible)
        {
            planter.GetComponent<Button>().enabled = false;
        }
        else
        {
            planter.GetComponent<Button>().enabled = true;
        }
        */

        //Sound for menu
        //FMODUnity.RuntimeManager.PlayOneShot("event:/LWS Digging");
    }

    /// <summary>
    /// Enables the raycast blocker.
    /// </summary>
    public void ToggleBlocker()
    {
        PersistantManager.Instance.blockerVisible = !PersistantManager.Instance.blockerVisible;
        blocker.gameObject.SetActive(PersistantManager.Instance.blockerVisible);
    }

    /// <summary>
    /// Enables the inventory display.
    /// </summary>
    public void EnableInventory()
    {
        PersistantManager.Instance.inventoryVisible = true;
        testing.SetActive(PersistantManager.Instance.inventoryVisible);
        invent.SetActive(PersistantManager.Instance.inventoryVisible);

        //opens tail
        PersistantManager.Instance.tabTailVisi = true;
        tabTail.gameObject.SetActive(PersistantManager.Instance.tabTailVisi);

        //hides hide button
        PersistantManager.Instance.hideVisi = true;
        hideButton.gameObject.SetActive(PersistantManager.Instance.hideVisi);

        //hides all else
        PersistantManager.Instance.recipesVisible = false;
        recipe.gameObject.SetActive(PersistantManager.Instance.recipesVisible);

        //Sound for menu
        //FMODUnity.RuntimeManager.PlayOneShot("event:/click 7");
    }



    /// <summary>
    /// Enables the recipe book display.
    /// </summary>
    public void ToggleRecipes()
    {
        PersistantManager.Instance.recipesVisible = true;
        recipe.gameObject.SetActive(PersistantManager.Instance.recipesVisible);

        //opens tail
        PersistantManager.Instance.tabTailVisi = true;
        tabTail.gameObject.SetActive(PersistantManager.Instance.tabTailVisi);

        //hides hide button
        PersistantManager.Instance.hideVisi = true;
        hideButton.gameObject.SetActive(PersistantManager.Instance.hideVisi);

        //hides all else
        PersistantManager.Instance.inventoryVisible = false;
        testing.gameObject.SetActive(PersistantManager.Instance.inventoryVisible);
        PersistantManager.Instance.seedScreenVisible = false;
        buySeedScreen.gameObject.SetActive(PersistantManager.Instance.seedScreenVisible);

        //Sound for menu
        //FMODUnity.RuntimeManager.PlayOneShot("event:/click 7");
    }

    /// <summary>
    /// Enables the potions and parcels display.
    /// </summary>
    public void ToggleBuyingSeedOn()
    {
        PersistantManager.Instance.seedScreenVisible = true;
        buySeedScreen.gameObject.SetActive(PersistantManager.Instance.seedScreenVisible);

        //tail
        //PersistantManager.Instance.tabTailVisi = true;
        //tabTail.gameObject.SetActive(PersistantManager.Instance.tabTailVisi);

        //hide
        //PersistantManager.Instance.hideVisi = true;
        //hideButton.gameObject.SetActive(PersistantManager.Instance.hideVisi);

        //Sound for menu
        //FMODUnity.RuntimeManager.PlayOneShot("event:/coins");
    }

    /// <summary>
    /// Disables multiple common displays. tabTail, hideButton, inventory, potions and parcels, and recipes are disabled.
    /// </summary>
    public void HideAll()
    {

        //tail
        PersistantManager.Instance.tabTailVisi = false;
        tabTail.gameObject.SetActive(PersistantManager.Instance.tabTailVisi);

        //hide
        PersistantManager.Instance.hideVisi = false;
        hideButton.gameObject.SetActive(PersistantManager.Instance.hideVisi);

        //all else false
        PersistantManager.Instance.inventoryVisible = false;
        testing.gameObject.SetActive(PersistantManager.Instance.inventoryVisible);
        PersistantManager.Instance.seedScreenVisible = false;
        buySeedScreen.gameObject.SetActive(PersistantManager.Instance.seedScreenVisible);
        PersistantManager.Instance.recipesVisible = false;
        recipe.gameObject.SetActive(PersistantManager.Instance.recipesVisible); ;

        //Sound for menu
        //FMODUnity.RuntimeManager.PlayOneShot("event:/click 2");
    }

    /// <summary>
    /// Disables the potions and parcels display.
    /// </summary>
    public void HidePotionsParcels()
    {
        PersistantManager.Instance.seedScreenVisible = false;
        buySeedScreen.gameObject.SetActive(PersistantManager.Instance.seedScreenVisible);
    }

    /// <summary>
    /// Enables the player help guide display.
    /// </summary>
    public void ToggleHelpGuide()
    {
        PersistantManager.Instance.helpGuideVisible = true;
        helpGuideImage.gameObject.SetActive(PersistantManager.Instance.helpGuideVisible);
    }

    /// <summary>
    /// Hides the player help guide display.
    /// </summary>
    public void HideHelpGuide()
    {
        PersistantManager.Instance.helpGuideVisible = false;
        helpGuideImage.gameObject.SetActive(PersistantManager.Instance.helpGuideVisible);
    }

    /// <summary>
    /// Enables the player controls display.
    /// </summary>
    public void ToggleControls()
    {
        PersistantManager.Instance.controlsVisible = true;
        controlsImage.gameObject.SetActive(PersistantManager.Instance.controlsVisible);
    }

    /// <summary>
    /// Hides the player controls display.
    /// </summary>
    public void HideControls()
    {
        PersistantManager.Instance.controlsVisible = false;
        controlsImage.gameObject.SetActive(PersistantManager.Instance.controlsVisible);
    }

    /// <summary>
    /// Toggles the familiar house display on or off.
    /// </summary>
    public void ToggleFHouse()
    {
        FamiliarHouse.instance.OpenFHouse();
        //FMODUnity.RuntimeManager.PlayOneShot("event:/LWS Fox");
    }

    /// <summary>
    /// Hides UI elements, for use at the end of the game
    /// </summary>
    public void HideUI()
    {
        tabTail.SetActive(false);
        hud.SetActive(false);
    }
}
