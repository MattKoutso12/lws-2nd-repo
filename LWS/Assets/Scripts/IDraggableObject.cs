using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDraggableObject
{

    public bool CanMergeWith(IDraggableObject other);

    public IDraggableObject CombinedWith(IDraggableObject other);

    public Sprite GetSprite();

}
