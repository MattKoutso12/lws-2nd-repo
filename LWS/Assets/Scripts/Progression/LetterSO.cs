﻿using System;
using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Letter", menuName = "Letter")]
public class LetterSO : ScriptableObject
{
    public string title;
    public string upper;
    public string lower;
    public string greeting;
    public string salutation;
    public int textSize = 20;

    public LetterBody bodyImages;
    [Serializable]
    public struct LetterBody
    {
        public Sprite topFlapOpen;
        public Sprite topFlapClosed;
        public Sprite bottomFlapOpen;
        public Sprite bottomFlapClosed;
        public Sprite middle;
    }
}
