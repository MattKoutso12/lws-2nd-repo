using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Letter : MonoBehaviour
{
    public LetterSO text;
    public bool lastInStack;

    [SerializeField] TextMeshProUGUI upper;
    [SerializeField] TextMeshProUGUI lower;
    [SerializeField] TextMeshProUGUI greeting;
    [SerializeField] TextMeshProUGUI salutation;

    Animator animator;
    public GameObject raycastBlocker;

    [SerializeField]private LetterBody letterBody;

    [Serializable]
    public struct LetterBody
    {
        public Image middle;
        public Image bottomFlap;
        public Image topFlap;
    }

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        letterBody.middle.sprite = text.bodyImages.middle;
        letterBody.topFlap.sprite = text.bodyImages.topFlapClosed;
        letterBody.bottomFlap.sprite = text.bodyImages.bottomFlapClosed;
    }

    public void OpenLetter()
    {
        animator.SetTrigger("Open");
    }

    public void CloseLetter()
    {
        animator.SetTrigger("Close");
        if(lastInStack)
        {
            raycastBlocker.SetActive(false);
        }
    }

    //anim events
    public void UpdateTexts()
    {
        upper.text = text.upper;
        lower.text = text.lower;
        upper.fontSize = text.textSize;
        lower.fontSize = text.textSize;
        salutation.text = text.salutation;
        greeting.text = text.greeting;
    }

    public void Detsroy()
    {
        Destroy(gameObject);
    }

    public void TopFlapOpenSwap()
    {
        letterBody.topFlap.sprite = text.bodyImages.topFlapOpen;
    }

    public void BottomFlapOpenSwap()
    {
        letterBody.bottomFlap.sprite = text.bodyImages.bottomFlapOpen;
    }

    public void CloseSwap()
    {
        letterBody.topFlap.sprite = text.bodyImages.topFlapClosed;
        letterBody.bottomFlap.sprite = text.bodyImages.bottomFlapClosed;
    }
}
