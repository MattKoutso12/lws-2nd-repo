using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LetterRaycastBlocker : MonoBehaviour
{

    private void Awake()
    {
        DialougeCommands.OnStackEnded += OnStackEnded;
        DialougeCommands.OnTutoSkipped += OnTutoSkipped;
        TutorialRunner.OnTutoFinished += OnTutoSkipped;
        PersistantManager.OnPostShopLevelUp += OnPostShopLevelUp;
        gameObject.SetActive(false);
    }

    private void OnPostShopLevelUp(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }

    private void OnStackEnded(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }

    private void OnTutoSkipped(object sender, EventArgs e)
    {
        Debug.Log("Des: Tuto Skipped");
        gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        DialougeCommands.OnStackEnded -= OnStackEnded;
        DialougeCommands.OnTutoSkipped -= OnTutoSkipped;
        TutorialRunner.OnTutoFinished -= OnTutoSkipped;
        PersistantManager.OnPostShopLevelUp -= OnPostShopLevelUp;
    }

    private void OnEnable()
    {
        if (TutorialRunner.Instance.tutorialActive)
        {
            gameObject.SetActive(false);
            return;
        }
        if (LetterStack.Instance.letterStack.Count > 0)
        {
            LetterStack.Instance.CheckForLetters();
            Player.Instance.SetMoveLock(true);
        } 
        else gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Player.Instance?.SetMoveLock(false);
    }
}
