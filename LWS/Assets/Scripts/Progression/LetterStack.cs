using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterStack : MonoBehaviour
{
    public static LetterStack Instance;
    public List<LetterSO> list = new List<LetterSO>();
    [SerializeField]
    public GameObject letterPrefab;

    public GameObject raycastBlocker; //Letter Canvas's raycast Blocker

    public List<string> letterStack = new List<string>();
    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }

    public void CheckForLetters()
    {
        for(int i = 0; i < letterStack.Count; i++)
        {
            Debug.Log("Dyu: " + letterStack[i]);
            if (i == 0)
            {
                MakeLetter(letterStack[i], true);
            }
            else
            {
                MakeLetter(letterStack[i], false);
            }
        }
        letterStack.Clear();
    }

    public void MakeLetter(string subject, bool last)
    {
        foreach(LetterSO letter in list)
        {
            if(letter.title == subject)
            {
                GameObject newLetter = Instantiate(letterPrefab, transform);
                Letter mail = newLetter.GetComponent<Letter>();
                mail.text = letter;
                mail.lastInStack = last;
                mail.raycastBlocker = raycastBlocker;
            }
        }
    }

}
