﻿using System.Collections;
using UnityEngine;

     /// <summary>
     /// Interface for anything that needs to display a tooltip.
     /// </summary>
   public interface IToolTipable
   {
        string GetTooltipInfo();
        string ColouredName { get; }
   }