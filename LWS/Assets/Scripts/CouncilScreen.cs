using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CouncilScreen : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI claudiaDays;
    [SerializeField] TextMeshProUGUI claudiaOrders;
    [SerializeField] TextMeshProUGUI lucasDays;
    [SerializeField] TextMeshProUGUI lucasFamiliars;
    [SerializeField] TextMeshProUGUI kojiDays;
    [SerializeField] TextMeshProUGUI kojiGold;
    [SerializeField] TextMeshProUGUI emiliaDays;
    [SerializeField] TextMeshProUGUI emiliaUpgrades;

    private bool on = false;


    void Update()
    {
        /*
        if(Input.GetKeyDown(KeyCode.L))
        {
            PersistantManager.Instance.dayCount = 60;
            Debug.Log("Skipped to day 60. The end is nigh.");
        }
        */
    }

    public void ToggleCouncilScreen()
    {
        UpdateDays();
        UpdateCouncil();

        on = !on;
        this.transform.GetChild(0).gameObject.SetActive(on);
    } 

    public void UpdateDays()
    {
        claudiaDays.text = Mathf.Clamp(((60 / (CouncilReputation.Instance.claudiaMeetings + 1)) - CouncilReputation.Instance.claudiaInterval), 0, 9999).ToString();
        lucasDays.text = Mathf.Clamp(((60 / (CouncilReputation.Instance.lucasMeetings + 1)) - CouncilReputation.Instance.lucasInterval), 0, 9999).ToString();
        kojiDays.text = Mathf.Clamp(((60 / (CouncilReputation.Instance.kojiMeetings + 1)) - CouncilReputation.Instance.kojiInterval), 0, 9999).ToString();
        emiliaDays.text = Mathf.Clamp(((60 / (CouncilReputation.Instance.emiliaMeetings + 1)) - CouncilReputation.Instance.emiliaInterval), 0, 9999).ToString();
    }

    public void UpdateCouncil()
    {
        claudiaOrders.text = Mathf.Clamp((16 - CouncilReputation.Instance.orderTest), 0, 9999).ToString();
        lucasFamiliars.text = Mathf.Clamp((3 - CouncilReputation.Instance.familiarTest), 0, 9999).ToString();
        kojiGold.text = Mathf.Clamp((600 - CouncilReputation.Instance.goldTest), 0, 9999).ToString();
        emiliaUpgrades.text = Mathf.Clamp((5 - CouncilReputation.Instance.upgradeTest), 0, 9999).ToString(); 
    }
}
