﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Card representing an order the player can take.
/// </summary>
public class OrderCard : MonoBehaviour
{
    public OrderWrapper order;
    public int index = 0;
    [SerializeField] TextMeshProUGUI taskText;
    private Toggle toggle;

    public bool isSelected = false;
    public bool ripped = false;

    private void Awake()
    {
        PersistantManager.Instance.OnPreDayChanged += Instance_OnPreDayChanged;
    }

    public void Init(Task task)
    {
        order = new OrderWrapper(task);
        toggle = GetComponentInChildren<Toggle>();
        SetText();
        toggle.isOn = false;
    }

    /// <summary>
    /// Warning: For initializing a task via save data ONLY
    /// </summary>
    /// <param name="wrapper">The order data to initialize</param>
    public void Init(OrderWrapper wrapper)
    {
        order = wrapper;
        toggle = GetComponentInChildren<Toggle>();
        SetText();
        toggle.isOn = false;
    }

    private void Instance_OnPreDayChanged(object sender, EventArgs e)
    {
        SetText();
        if(order.daysRemaining <= 0)
        {
            ripped = true;
        }
    }


    private void SetText()
    {
        taskText.text = order.GetTooltipInfo();
    }

    /// <summary>
    /// Adds the order to the player's accepted orders. 
    /// </summary>
    public void TakeOrder()
    {
        // this order must be going to the gym the way it's Becoming Ripped !!1
        ripped = true;

        transform.GetChild(1).gameObject.SetActive(false); // disable the foreground "card" element, leaving only the ripped part


    }

    #region SetStatus comment
    /// <summary>
    /// Selects or deselects an order to take.
    /// </summary>
    /// <param name="isSelected"> Whether the order is selected. </param>
    #endregion
    public void SetStatus(bool isSelected)
    {
        this.isSelected = isSelected;
        
    }


}