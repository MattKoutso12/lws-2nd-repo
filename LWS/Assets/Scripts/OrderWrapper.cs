﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// Wrapper class for an order. 
/// </summary>
[Serializable]
public class OrderWrapper: IToolTipable
{
    [SerializeField]
    public string name;

    [SerializeField]
    public int daysRemaining;


    [SerializeField]
    public int reward;

    [SerializeField]
    public string orderInfo;

    [SerializeField]
    public List<InventoryItem> products;

    [SerializeField]
    private float[] rawColor = new float[4] { 1.0f, 1.0f, 1.0f, 1.0f };

    
    
    // interpret raw numbers as colors. We can't save colors in save data, so we do this
    public Color textColor
    {
        get
        {
            return new Color(rawColor[0], rawColor[1], rawColor[2], rawColor[3]);
        }

        set
        {
            rawColor = new float[4] { value.r, value.g, value.b, value.a };
        }
    }
    //private ItemType itemCat;

    #region OrderWrapper comment
    /// <summary>
    /// Constructor method for OrderWrapper.
    /// </summary>
    /// <param name="order"> The order itself. </param>
    #endregion
    public OrderWrapper(Task order)
    {
        name = order.Name;
        daysRemaining = order.OrderLength;
        orderInfo = order.OrderInfo;
        reward = order.Reward;
        //itemCat = order.ItemType;
        products = order.ItemList;
        textColor = order.Color;

        PersistantManager.Instance.OnPreDayChanged += Instance_OnPreDayChanged;
    }

    private void Instance_OnPreDayChanged(object sender, EventArgs e)
    {
        daysRemaining--;
    }

    #region ColouredName comment
    /// <summary>
    /// Returns hexadecimal string representing color.
    /// </summary>
    /// <value> Hexidecimal color string. </value>
    #endregion
    public string ColouredName
    {
        get
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(textColor);
            return $"<color=#{hexColour}>{name}</color>";
        }
    }

    #region GetTooltipInfo comment
    /// <summary>
    /// Returns tooltip info for this order.
    /// </summary>
    /// <returns> Tooltip info for the order. </returns>
    #endregion
    public string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        builder.Append(ColouredName).AppendLine();

        builder.Append("Requires:").AppendLine();

        foreach (InventoryItem listedItem in products)
        {
            builder.Append(listedItem.itemType.getDisplayName()).Append(" x").Append(listedItem.amount).AppendLine();
        }

        builder.Append("<color=black>").Append(daysRemaining).Append(" Days").Append("<sprite=\"IMG_0327\" name=\"IMG_0327_0\">").Append(" Left").Append("</color>").AppendLine();
        builder.Append("Sell Price: ").Append(reward).Append(" Gold").Append("<sprite=\"IC_Money\" name=\"IC_Money_0\">").AppendLine();

        return builder.ToString();
    }


}