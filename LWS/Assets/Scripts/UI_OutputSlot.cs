using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System.Text;

public class UI_OutputSlot : MonoBehaviour, IToolTipable, IPointerEnterHandler, IPointerExitHandler
{
    public static Dictionary<int, UI_OutputSlot> output_displays = new Dictionary<int, UI_OutputSlot>();

    [SerializeField] public int id;

    CraftingSystem mySystem;

    [SerializeField] public float not_manned_opacity = 0.2f;

    private Color NOT_MANNED_COLOR;



    [SerializeField]
    public Image display;

    public string ColouredName
    {
        get
        {
            return "TO " + (mySystem is PlantingSystem ? "PLANT" : "CRAFT")+": " + mySystem.GetOutputItem().ColouredName;
        }
    }

    private void Awake()
    {
        NOT_MANNED_COLOR = Color.white * new Color(1, 1, 1, not_manned_opacity); // Basically just 50% opacity
    }


    // Start is called before the first frame update
    void Start()
    {
        mySystem = CraftingSystem.GetCraftingSystem(id);
    }

    private void OnEnable()
    {
        mySystem = CraftingSystem.GetCraftingSystem(id);
    }

    /// <summary>
    /// Displays an item as the output
    /// </summary>
    /// <param name="item">The item which will be displayed</param>
    public void SetDisplay(Item item)
    {
        display.sprite = item.sprite;

    }

    private void OnGUI()
    {
        if(mySystem == null)
        {
            mySystem = CraftingSystem.GetCraftingSystem(id);
            return; // try again next frame
        }


        if(mySystem.GetOutputItem() != null)
        {
            SetDisplay(mySystem.GetOutputItem().itemType);
            display.color = Color.white;
            if(mySystem.manned)
            {
                display.color = Color.white;
            }
            else
            {
                display.color = NOT_MANNED_COLOR;
            }
        }
        else
        {
            display.color = Color.clear;
        }


        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    const string GOOD = "#0D7E16";
    const string BAD = "#A92424";
    public string GetTooltipInfo()
    {
        
        
        StringBuilder builder = new StringBuilder();
        builder.Append(mySystem.GetOutputItem().itemType.getUseText()).AppendLine().AppendLine();

        if (mySystem.GetDaysRemaining() != -1)
            builder.Append(mySystem.GetDaysRemaining()+1 + " Days Left").AppendLine();
        else
        {
            builder.Append("\tRECIPE NOT STARTED").AppendLine();
            builder.Append("\t"+((CraftedItem)mySystem.GetOutputItem().itemType).GetDaysToCraft() + " Days To Craft").AppendLine();
        }

        builder.AppendLine().AppendLine();

        if (mySystem.manned)
        {
            builder.Append(mySystem.GetCrew().ColouredName + " is working on this").AppendLine();

            string[] good = mySystem.GetCrew().GetAbility().Good_Effects;
            
            foreach(string e in good)
            {
                builder.Append("\t<color="+GOOD+">" + e+"</color>").AppendLine();
            }

            string[] bad = mySystem.GetCrew().GetAbility().Bad_Effects;

            foreach (string e in bad)
            {
                builder.Append("\t<color="+BAD+">" + e + "</color>").AppendLine();
            }
        }
        else
        {
            builder.Append("<color=red>Nobody</color> is working on this");
        }

        return builder.ToString();
        
    }

    public CraftingSystem GetSystem()
    {
        return mySystem;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PersistantManager.Instance.HideTooltip();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (mySystem != null && mySystem.GetOutputItem() != null)
        {
            PersistantManager.Instance.ShowTooltip(this);
        }
    }
}
