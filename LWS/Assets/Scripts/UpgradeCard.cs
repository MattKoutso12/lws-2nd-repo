using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Not currently implemented. Handles buying upgrades from the shop.
/// </summary>
public class UpgradeCard : MonoBehaviour
{

    [SerializeField] string upgrade;
    [SerializeField] int scalar;
    [SerializeField] TextMeshProUGUI upgradeText;
    int level;
    bool maxLevel = false;

    [SerializeField] UIStaging staging;
    [SerializeField] FamiliarHouse house;

    public event EventHandler OnBoughtUpgrade;

    private void Awake()
    {
        level = KeyItems.upgradeItemsActive[upgrade] + 1;
        if (upgrade != "HBag" && upgrade != "HMap")
        {
            if (level > 6)
            {
                level = 6;
                maxLevel = true;
            }
        }
        else
        {
            if (level > 5)
            {
                level = 5;
                maxLevel = true;
            }
        }
        SetText();
    }

    private void Start()
    {
        OnBoughtUpgrade += BoughtUpgrade;
    }

    private void BoughtUpgrade(object sender, EventArgs e)
    {
        //Update the cards text boxes
        Debug.Log("Bought Upgrade");
        KeyItems.upgradeItemsActive[upgrade] = KeyItems.upgradeItemsActive[upgrade] + 1;
        level = level + 1;
        SetText();

        CouncilReputation.Instance.UpgradeScore();

        if((string)sender == "Crafter")
        {
            staging.staging.cUpgradeMulti += 1;
        }

        if ((string)sender == "Planter")
        {
            staging.staging.pUpgradeMulti += 1;
        }

        if ((string)sender == "FHouse")
        {
            house.famCap += 3;
        }
    }

    public void BuyUpgrade()
    {
        if(((upgrade != "FHouse" && upgrade != "HMap") && level < 6) || ((upgrade == "FHouse" || upgrade == "HMap") && level < 5))
        {
            if (PersistantManager.Instance.TryPurchase(scalar * level))
            {
                OnBoughtUpgrade?.Invoke(upgrade, EventArgs.Empty);
            }
        }
    }

    private void SetText()
    {        
        StringBuilder builder = new StringBuilder();

        if (!maxLevel)
        {
            builder.Append("<size=21.25>").Append("Next Level: ").Append(level).AppendLine();
            builder.Append("Cost: ").Append(scalar * level).Append("</size>");
        }
        else
        {
            builder.Append("<size=21.25>").Append("Max Level Reached").AppendLine();
        }
        upgradeText.text = builder.ToString();
    }
}