using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class CouncilReputation : MonoBehaviour, Savable
{
    public static CouncilReputation Instance { get; private set; }

    [SerializeField] public CouncilScreen councilScreen;

    public float repMultiplier = 1f;
    public float orderScore = 0f;
    public float goldScore = 0f;
    public float familiarScore = 0f;
    public float upgradeScore = 0f;

    private float orderTot = 0f;
    private float goldTot = 0f;
    private float familiarTot = 0f;
    private float upgradeTot = 0f;

    public float orderTest = 0f;
    public float goldTest = 0f;
    public float familiarTest = 0f;
    public float upgradeTest = 0f;

    private float orderTemp = 0f;
    private float goldTemp = 0f;
    private float familiarTemp = 0f;
    private float upgradeTemp = 0f;

    public int claudiaInterval = 0;
    public int lucasInterval = 1;
    public int kojiInterval = 0;
    public int emiliaInterval = 1;

    public int claudiaMeetings = 3;
    public int lucasMeetings = 3;
    public int kojiMeetings = 2;
    public int emiliaMeetings = 2;

    public int claudiaRep = 0;
    public int lucasRep = 0;
    public int kojiRep = 0;
    public int emiliaRep = 0;

    public int claudiaPMCount = 0;
    public int lucasPMCount = 0;
    public int kojiPMCount = 0;
    public int emiliaPMCount = 0;

    public readonly float orderThreshold = 48f;
    public readonly float goldThreshold = 1200f;
    public readonly float familiarThreshold = 12f;
    public readonly float upgradeThreshold = 10f;

    public OrderWrapper claudiaMisc = null;
    public string claudiaMiscString = "";
    public FamiliarWrapper lucasMisc = null;
    public string lucasMiscString = "";
    public int kojiMisc = 0;
    public string kojiMiscString = "";
    public int emiliaMisc = 2;
    public string emiliaMiscString = "";
    
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        SaveManager.OnPreSave += AddToSavables;
    }

    private void OnDestroy()
    {
        SaveManager.OnPreSave -= AddToSavables;
    }

    public void OrderScore()
    {
        orderScore++;
        orderTest++;
        orderTemp++;
    }

    public void GoldScore(int gold)
    {
        goldScore += gold;
        goldTest += gold;
        goldTemp += gold;

    }

    public void UpgradeScore()
    {
        upgradeScore++;
        upgradeTest++;
        upgradeTemp++;
    }

    public void FamiliarScore()
    {
        familiarScore++;
        familiarTest++;
        familiarTemp++;
    }

    public void RunCouncilTests()
    {
        ClaudiaTest();
        LucasTest();
        KojiTest();
        EmiliaTest();
    }

    public float EndCalculation()
    {
        float orderTotal = Mathf.Clamp((orderScore / orderThreshold) * 250f, 0f, 250f);
        float goldTotal = Mathf.Clamp((goldScore / goldThreshold) * 250f, 0f, 250f);
        float familiarTotal = Mathf.Clamp((familiarScore / familiarThreshold) * 250f, 0f, 250f);
        float upgradeTotal = Mathf.Clamp((upgradeScore / upgradeThreshold) * 250f, 0f, 250f);

        return (orderTotal + goldTotal + familiarTotal + upgradeTotal) * repMultiplier;
    }

    public float [] GetScoreCalc()
    {
        float orderTotal = Mathf.Clamp((orderScore / orderThreshold) * 250f, 0f, 250f);
        float goldTotal = Mathf.Clamp((goldScore / goldThreshold) * 250f, 0f, 250f);
        float familiarTotal = Mathf.Clamp((familiarScore / familiarThreshold) * 250f, 0f, 250f);
        float upgradeTotal = Mathf.Clamp((upgradeScore / upgradeThreshold) * 250f, 0f, 250f);

        return new float []{ orderTotal, goldTotal, familiarTotal, upgradeTotal};
    }


    private void ClaudiaTest()
    {
        orderTot += orderTemp;
        claudiaInterval++;
        if (orderTot >= 10 && PersistantManager.Instance.dayCount == 15)
        {
            DialougeManager.Instance.StartDialogue("LWSCPM1");
            claudiaPMCount++;
            orderTot = 0;
        }
        if (orderTot >= 20 && PersistantManager.Instance.dayCount == 30)
        {
            DialougeManager.Instance.StartDialogue("LWSCPM2");
            claudiaPMCount++;
            orderTot = 0;
        }
        if (orderTot >= 30 && PersistantManager.Instance.dayCount == 45)
        {
            DialougeManager.Instance.StartDialogue("LWSCPM3");
            claudiaPMCount++;
            orderTot = 0;
        }
        if (PersistantManager.Instance.dayCount == 60)
        {
            //DialougeManager.Instance.StartDialogue("LWSCEnd");
        }
        if (claudiaInterval == (60 / (claudiaMeetings + 1)))
        {
            if (orderTest / 16f >= 1f)
            {
                repMultiplier += 0.1f;
                claudiaRep++;
            }
            claudiaInterval = 0;
            orderTest = 0f;
        }
        orderTemp = 0;
    }

    private void LucasTest()
    {
        familiarTot += familiarTemp;
        lucasInterval++;
        if (familiarTot >= 4 && PersistantManager.Instance.dayCount == 10)
        {
            DialougeManager.Instance.StartDialogue("LWSLPM1");
            lucasPMCount++;
            familiarTot = 0;
        }
        if (familiarTot >= 8 && PersistantManager.Instance.dayCount == 35)
        {
            DialougeManager.Instance.StartDialogue("LWSLPM2");
            lucasPMCount++;
            familiarTot = 0;
        }
        if (familiarTot >= 12 && PersistantManager.Instance.dayCount == 50)
        {
            DialougeManager.Instance.StartDialogue("LWSLPM3");
            lucasPMCount++;
            familiarTot = 0;
        }
        if (PersistantManager.Instance.dayCount == 60)
        {
            //DialougeManager.Instance.StartDialogue("LWSLEnd");
        }
        if (lucasInterval == (60 / (lucasMeetings + 1)))
        {
            if (familiarTest / 3f >= 1f)
            {
                repMultiplier += 0.1f;
                lucasRep++;
            }     
            lucasInterval = 0;
            familiarTest = 0f; 
        }
        familiarTemp = 0;
    }

    private void KojiTest()
    {
        goldTot += goldTemp;
        kojiInterval++;
        if (goldTot >= 200 && PersistantManager.Instance.dayCount == 20)
        {
            DialougeManager.Instance.StartDialogue("LWSKPM1");
            kojiPMCount++;
            goldTot = 0;
        }
        if (goldTot >= 550 && PersistantManager.Instance.dayCount == 40)
        {
            DialougeManager.Instance.StartDialogue("LWSKPM2");
            kojiPMCount++;
            goldTot = 0;
        }
        if (PersistantManager.Instance.dayCount == 60)
        {
            //DialougeManager.Instance.StartDialogue("LWSKEnd");
        }
        if (kojiInterval == (60 / (kojiMeetings + 1)))
        {
            if(goldTest / 600f >= 1f)
            {
                repMultiplier += 0.1f;
                lucasRep++;
            }
            kojiInterval = 0;
            goldTest = 0f;
        }
        goldTemp = 0;
    }

    private void EmiliaTest()
    {
        upgradeTot += upgradeTemp;
        emiliaInterval++;
        if (upgradeTot >= 5 && PersistantManager.Instance.dayCount == 25)
        {
            DialougeManager.Instance.StartDialogue("LWSEPM1");
            emiliaPMCount++;
            upgradeTot = 0;
        }
        if (upgradeTot >= 20 && PersistantManager.Instance.dayCount == 55)
        {
            DialougeManager.Instance.StartDialogue("LWSEPM2");
            emiliaPMCount++;
            upgradeTot = 0;
        }
        if (PersistantManager.Instance.dayCount == 60)
        {
            //DialougeManager.Instance.StartDialogue("LWSEEnd");
        }
        if (upgradeTest / 5f >= 1f && emiliaInterval == (60 / (emiliaMeetings + 1)))
        {
            if(upgradeTest / 5f >= 1f)
            {
                repMultiplier += 0.1f;
                emiliaRep++;
            }
            emiliaInterval = 0;
            upgradeTest = 0f;
        }
        upgradeTemp = 0;
    }

    public void ClaudiaMiscTest(OrderWrapper input)
    {
        if(claudiaMisc != null)
        {
            if(claudiaMisc.reward < input.reward)
            {
                claudiaMisc = input;
                claudiaMiscString = input.name;
            }
        }
        else
        {
            claudiaMisc = input;
            claudiaMiscString = input.name;
        }
    }

    public void LucasMiscTest(FamiliarWrapper input)
    {
        if(lucasMisc == null)
        {
            lucasMisc = input;
            lucasMiscString = input.name;
        }
    }

    public void KojiMiscTest(int input)
    {
        if(kojiMisc != 0)
        {
            if(kojiMisc < input)
            {
                kojiMisc = input;
                kojiMiscString = input.ToString();
            }
        }
        else
        {
            kojiMisc = input;
            kojiMiscString = input.ToString();
        }
    }

    public void EmiliaMiscTest()
    {
        emiliaMisc++;
        emiliaMiscString = emiliaMisc.ToString();
    }

    public void Save(PlayerData data)
    {
        data.repMult = repMultiplier;

        data.emiliaScore = emiliaRep;
        data.upScore = upgradeScore;
        data.eMisc = emiliaMisc;
        data.emilia = emiliaInterval;

        data.kojiScore = kojiRep;
        data.goldScore = goldScore;
        data.kMisc = kojiMisc;
        data.koji = emiliaInterval;

        data.lucasScore = lucasRep;
        data.famScore = familiarScore;
        //data.lMisc = lucasMisc;
        data.lucas = emiliaInterval;

        data.claudiaScore = claudiaRep;
        data.orderScore = orderScore;
        //data.cMisc = claudiaMisc;
        data.claudia = emiliaInterval;
    }

    public void AddToSavables(SaveManager manager)
    {
        manager.saveables.Add(this);
    }

    public void RemoveFromSavables()
    {
        SaveManager.Instance.saveables.Remove(this);
    }
}
