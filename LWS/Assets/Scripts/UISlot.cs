using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// A UI Slot holds a DraggableObject and makes sure that things don't get out of whack (i.e. duplication, etc.)
/// 
/// It also displays the item.
/// </summary>
public class UISlot : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{

    protected IDraggableObject holding = null;
    protected bool dragging = false;

    [SerializeField]
    public Image display;

    public delegate void SlotChangeEvent(UISlot sender, params IDraggableObject[] objects);
    public event SlotChangeEvent OnContentsSet;

    static readonly Color COLOR_WHILE_DRAGGING = Color.white * new Color(1, 1, 1, 0.2f); // change opacity of the old item while dragging


    private void Awake()
    {

        
    }

    

    public bool TryAddObject(IDraggableObject o)
    {
        if (o == null)
            return true;

        if (o.CanMergeWith(holding))
        {
            if(holding == null)
            {
                SetHolding(o);
                return true;
            }
            else
            {
                SetHolding(o.CombinedWith(this.GetHeldObject())); //merge
                return true;
            }
        }
        else if (o is FamiliarWrapper)
        {
            SetHolding(o);
            return true;
        }
        else return false;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Returns the object held, or null if the slot is empty</returns>
    public IDraggableObject GetHeldObject()
    {
        return holding;
    }

    public bool IsEmpty()
    {
        if (holding == null) return true;
        return false;
    }




    //DRAG HANDLERS

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("Beginning Drag with holding: " + holding);
        if (holding == null) return;

        dragging = true;
        UICursorManager.SetDragSprite(holding.GetSprite());
        UICursorManager.grabbing = true;
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        
    }

    

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("Ending drag with holding: " + holding);
        if (holding == null) return;

        dragging = false;
        
        foreach(GameObject o in eventData.hovered)
        {
            Debug.Log("Trying to drop on: " + o.name);
            UISlot slotUnderCursor;
            if(o.TryGetComponent<UISlot>(out slotUnderCursor))
            {
                
                if (this.TryTransfer(slotUnderCursor))
                {
                    UICursorManager.grabbing = false;
                    UICursorManager.ResetDragSprite();
                    return;
                }
            }
        }

        UICursorManager.grabbing = false;
        UICursorManager.ResetDragSprite();
    }

    public virtual bool TryTransfer(UISlot other)
    {
        // if same slot
        if (other == this)
        {
            Debug.Log("Same Slot");
            return false;
        }

        if (!other.CanHoldThis(this.holding))
            return false;

        IDraggableObject other_contents = other.holding;
        // if empty
        if (other_contents == null)
        {

            other.OnObjectAdded(this.holding);
            other.SetHolding(this.holding);
            this.SetHolding(null);
            Debug.Log("Other was empty- transfer success");
            return true;
        }
        else if (holding.CanMergeWith(other.holding))
        {
            other.OnObjectAdded(this.holding);
            other.SetHolding(this.holding.CombinedWith(other.holding));
            this.SetHolding(null);
            Debug.Log("Could merge. Transfer success");
            return true;
        }
        else
        {
            IDraggableObject temp;
            temp = this.GetHeldObject();
            this.SetHolding(other.GetHeldObject());
            other.SetHolding(temp);
            Debug.Log("Swapped Objects. Transfer Success");
            return true;
        }

        Debug.Log("Couldn't merge " + holding + " with " + other.holding + ". Failure.");
        return false;
    }

    /// <summary>
    /// Set the contents of this slot manually. Will overwrite anything that is there previously, use with caution.
    /// </summary>
    /// <param name="o">The object to put into this slot</param>
    public void SetHolding(IDraggableObject o)
    {
        if (o != null && !CanHoldThis(o)) return;
        IDraggableObject old = this.holding;
        this.holding = o;
        OnContentsChanged(old, this.holding);
        OnContentsSet?.Invoke(this, old, this.holding);
    }

    /// <summary>
    /// Sets the contents of the slot without triggering OnContentsChanged or OnContentsSet
    /// </summary>
    /// <param name="o"></param>
    public void SetHoldingWithoutNotify(IDraggableObject o)
    {
        if (o != null && !CanHoldThis(o)) return;
        this.holding = o;
    }

    protected virtual void OnGUI()
    {
        
        // show object sprite!
        if(display != null)
        {
            display.sprite = holding?.GetSprite();
            if (holding == null || holding.GetSprite() == null)
                display.color = Color.clear; // avoid white squares on null values.
            else if (dragging)
                display.color = COLOR_WHILE_DRAGGING;
            else
                display.color = Color.white;
        }
    }


    /// <summary>
    /// Override this guy to add specific functionality to child classes. Called BEFORE slot's contents are changed (o is the object which is about to be added)!
    /// This is so you can recognize merges versus other changes.
    /// </summary>
    /// <param name="o">The object which is being added to this slot</param>
    protected virtual void OnObjectAdded(IDraggableObject o)
    {

    }

    protected virtual void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {

    }

    /// <summary>
    /// Override this method to tell it what kind of object it can hold. This method is used in all transferring, so anything you put in an override here
    /// will be LAW!
    /// </summary>
    /// <param name="objectToHold">The object which may or may not be able to be held</param>
    /// <returns>A boolean representing if this object can be held.</returns>
    protected virtual bool CanHoldThis(IDraggableObject objectToHold)
    {
        return true; // by default, as long as it's an IDraggableObject, it can be held.
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(holding != null && holding is IToolTipable)
        {
            PersistantManager.Instance.ShowTooltip((IToolTipable)holding);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (holding != null && holding is IToolTipable)
        {
            PersistantManager.Instance.HideTooltip();
        }
    }
}
