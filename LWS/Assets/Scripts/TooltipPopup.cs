﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

/// <summary>
/// A tooltip popup reflecting a hovered-over object.
/// </summary>
public class TooltipPopup : MonoBehaviour
{
    [SerializeField] public GameObject popupCanvasObject;
    [SerializeField] public RectTransform popupObject;
    [SerializeField] public TextMeshProUGUI titleText, infoText;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float padding;
    [SerializeField] public Image icon;
    private Canvas popupCanvas;

    [SerializeField] public Sprite craftingTable, plantingTable;
    private void Awake()
    {
        popupCanvas = popupCanvasObject.GetComponent<Canvas>();
    }

    private void Start()
    {
        popupCanvas.gameObject.SetActive(false);
    }

    private void Update()
    {
        FollowCursor();
    }

    private void FollowCursor()
    {
        if (!popupCanvasObject.activeSelf)
        {
            return;
        }
        Vector3 tempOffset = offset;
        if (Mouse.current.position.ReadValue().x < Screen.width / 2)
        {
            //tempOffset.x -= 50; // move it just a tiny bit so it's on the other side of the mouse
            tempOffset.x *= -1; // flip on the other side of the screen.
        }

        Vector3 newPos = new Vector3(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y, tempOffset.z) + tempOffset;
        newPos.z = 0f;
        float rightEdgeToScreenEdgeDistance = Screen.width - (newPos.x + popupObject.rect.width * popupCanvas.scaleFactor / 2) - padding;
        if (rightEdgeToScreenEdgeDistance < 0)
        {
            newPos.x += rightEdgeToScreenEdgeDistance;
        }
        float leftEdgeToScreenEdgeDistance = 0 - (newPos.x - popupObject.rect.width * popupCanvas.scaleFactor / 2) + padding;
        if (leftEdgeToScreenEdgeDistance > 0)
        {
            newPos.x += leftEdgeToScreenEdgeDistance;
        }
        float topEdgeToScreenEdgeDistance = Screen.height - (newPos.y + popupObject.rect.height * popupCanvas.scaleFactor) - padding;
        if (topEdgeToScreenEdgeDistance < 0)
        {
            newPos.y += topEdgeToScreenEdgeDistance;
        }
        popupObject.transform.position = newPos;
    }

    #region DisplayInfo comment
    /// <summary>
    /// Displays tooltip info.
    /// </summary>
    /// <param name="tool"> ToolTippable object. </param>
    #endregion
    public void DisplayInfo(IToolTipable tool)
    {
        titleText.SetText(tool.ColouredName);



        StringBuilder builder = new StringBuilder();

        //builder.Append("<size=20>").Append(tool.ColouredName).Append("</size>").AppendLine();
        builder.Append(tool.GetTooltipInfo());

        infoText.text = builder.ToString();

        if(tool is IDraggableObject)
        {
            icon.sprite = ((IDraggableObject)tool).GetSprite();
            if(icon.sprite != null)
                icon.color = new Color(1, 1, 1, 0.10f);
        }
        else if(tool is UI_OutputSlot)
        {
            CraftingSystem system = ((UI_OutputSlot)tool).GetSystem();
            if(system != null)
            {
                if(system is PlantingSystem)
                {
                    icon.sprite = plantingTable;
                }
                else
                {
                    icon.sprite = craftingTable;
                }
            }
        }
        else
        {
            icon.sprite = null;
            icon.color = new Color(1, 1, 1, 0);
        }

        popupCanvasObject.SetActive(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(popupObject);
    }

    /// <summary>
    /// Hides tooltip info.
    /// </summary>
    public void HideInfo()
    {
        popupCanvasObject.SetActive(false);
    }
}