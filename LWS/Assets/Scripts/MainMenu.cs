using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

/// <summary>
/// Handles buttons on main menu.
/// </summary>
public class MainMenu : MonoBehaviour
{
    //all variables

    //public GameObject startGame;
    //public GameObject tutorial;
    //public GameObject options;

    //public GameObject camera;
    //public float moveSpeed = 2f;
    //public Transform[] MapWaypoints;
    //int waypointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
    //    camera.transform.position = MapWaypoints[waypointIndex].transform.position;
    }



    void Update()
    {
    //   CameraMoving();
    }


    /// <summary>
    /// Switches to the loading scene.
    /// </summary>
    public void StartGame()
    {

    }

    /// <summary>
    /// Not yet implemented. Switches to the tutorial scene.
    /// </summary>
    public void StartTutorial()
    {

    }

    /*
    //funtcion to switch to options scene (could be a popup window?)
    public void CameraMoving()
    {
        if (waypointIndex <= MapWaypoints.Length - 1)
        {
            camera.transform.position = Vector2.MoveTowards(camera.transform.position,
                MapWaypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);

        }

        if (camera.transform.position == MapWaypoints[waypointIndex].transform.position)
        {
            waypointIndex += 1;
        }

        if (waypointIndex == MapWaypoints.Length - 1)
        {
            waypointIndex = 0;
        }
    }
    */

}
