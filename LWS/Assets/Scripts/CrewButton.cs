using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

#region CrewButton comment
/// <summary>
/// Displays tooltip for a crew.
/// </summary>
#endregion
public class CrewButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] public FamiliarWrapper crew; //The reference crew member

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Shows the tooltip info for this crew
        PersistantManager.Instance.ShowTooltip(crew);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Hides the tooltip info for this crew
        PersistantManager.Instance.HideTooltip();
    }
}
