﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStaging : MonoBehaviour
{
    private Transform[] crewSlots;
    private Transform[] outputSlots;
    public Staging staging;
    private Transform itemContainer;
    private Transform crewContainer;
    [SerializeField] Transform pfUI_Crew;
    [SerializeField] Transform pfUI_Output;
    private void Awake()
    {
        itemContainer = transform.Find("Item Container");
        crewContainer = transform.Find("Crew Container");

        crewSlots = new Transform[6];
        outputSlots = new Transform[6];

        for (int i = 0; i < 6; i++)
        {
            //crewSlots[i] = transform.Find("CrewSlots").transform.Find("CS" + i);
            //UI_CrewSlot crewSlot = crewSlots[i].GetComponent<UI_CrewSlot>();
            //crewSlot.SetX(i);
            //crewSlot.OnItemDropped += UI_Staging_OnItemDropped;
        }
        
        

    }
    
    public void SetStaging(Staging staging)
    {
        this.staging = staging;
        staging.OnGridChanged += Staging_OnGridChanged;
    }

    private void Staging_OnGridChanged(object sender, EventArgs e)
    {
        UpdateVisual();
    }

    private void UI_Staging_OnItemDropped(object sender, UI_CrewSlot.OnItemDroppedEventArgs e)
    {
        //staging.TryAddCrew(e.worker, e.system);
        staging.TryAddCrew(e.worker, e.x);
        if(e.worker.current_slot_id != -1)
        {
            CraftingSystem.GetCraftingSystem(e.worker.current_slot_id).manned = false;
        }
        e.worker.current_slot_id = e.system.id;
        e.system.manned = true;
        //e.system.SetCrew(e.worker);
       

    }

    public void UpdateVisual()
    {
        /*
        foreach(Transform child in crewContainer)
        {
            Destroy(child.gameObject);
        }*/
        /*
        foreach(Transform child in itemContainer)
        {
            Destroy(child.gameObject);
        }
        */

        for(int i = 0; i < 6; i++)
        {
            if(!staging.IsCrewEmpty(i))
            {
                CreateCrew(staging.GetCrew(i), i);
            }

            if (!staging.IsEmpty(i))
            {
                //CreateItemOutput(staging.GetItem(i), i);
            }

        }
    }

    private void CreateCrew(FamiliarWrapper crew , int x)
    {
        Transform crewTransform = Instantiate(pfUI_Crew, crewContainer);
        RectTransform itemRectTransform = crewTransform.GetComponent<RectTransform>();
        Transform crewSlotTransform = crewSlots[x];
        itemRectTransform.anchoredPosition = crewSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        crewTransform.GetComponent<CrewDragDrop>().SetItem(crew);
    }
    private void CreateItemOutput(InventoryItem item, int x)
    {
        Debug.Log("putting item " + item.itemType.getIDName() + " in slot " + x);
        Transform itemTransform = Instantiate(pfUI_Output, itemContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        Transform outputSlotTransform = outputSlots[x];
        itemRectTransform.anchoredPosition = outputSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        itemTransform.localScale = Vector3.one * 1.5f;
        itemTransform.GetComponent<OutputSetter>().SetItem(item);
    }
}