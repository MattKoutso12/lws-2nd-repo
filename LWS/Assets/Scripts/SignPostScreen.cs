using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMOD;
public class SignPostScreen : MonoBehaviour
{
    bool is_visible = false;
    Animator animator;

    [SerializeField]
    public GameObject realScreen;

    GraphicRaycaster ray;

    bool locked = false;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        ray = GetComponent<GraphicRaycaster>();
    }


    // Start is called before the first frame update
    void Start()
    {
        transform.GetChild(0).gameObject.SetActive(is_visible);
    }

    //Toggles the sign post screen.
    public void Toggle()
    {

        if (locked) return;
        is_visible = !is_visible;
        animator.SetTrigger("toggled");

        if (!is_visible) ray.enabled = false;
        else ray.enabled = true;

        locked = true;
    }


    /// <summary>
    /// Called by an Animation Event at the frame where we can no longer see the background behind the bushes.
    /// 
    /// Toggles the "screen" bit (the sign and background) at this point.
    /// </summary>
    public void OnCoveredUp()
    {
        locked = false;
        if (is_visible)
            transform.GetChild(0).gameObject.SetActive(true);
        else
            transform.GetChild(0).gameObject.SetActive(false);
      //whateever 




    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
