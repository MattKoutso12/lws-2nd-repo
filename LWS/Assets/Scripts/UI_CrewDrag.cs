using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using TMPro;

/// <summary>
/// Handles UI for dragging a familiar.
/// </summary>
public class UI_CrewDrag : MonoBehaviour
{

    public static UI_CrewDrag Instance { get; private set; }

    private Canvas canvas;
    private RectTransform rectTransform;
    private RectTransform parentRectTransform;
    private CanvasGroup canvasGroup;
    private Image image;
    private FamiliarWrapper worker;

    private void Awake()
    {
        Instance = this;

        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
        image = transform.GetComponentInChildren<Image>();
        parentRectTransform = transform.parent.GetComponent<RectTransform>();
        Hide();
    }

    private void Update()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, Mouse.current.position.ReadValue(), null, out Vector2 localPoint);
        transform.localPosition = localPoint;
    }

    public FamiliarWrapper GetWorker()
    {
        return worker;
    }

    public void SetWorker(FamiliarWrapper worker)
    {
        this.worker = worker;
    }

    public void SetSprite(Sprite sprite)
    {
        image.sprite = sprite;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show(FamiliarWrapper worker)
    {
        gameObject.SetActive(true);

        SetWorker(worker);
        SetSprite(worker.GetSprite());
        UpdatePosition();
    }

}
