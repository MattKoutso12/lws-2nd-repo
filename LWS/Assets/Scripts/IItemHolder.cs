﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Interface for manipulating items in a item holder.
/// </summary>
public interface IItemHolder
{
    void RemoveItem(InventoryItem item);
    void AddItem(InventoryItem item);
    bool CanAddItem();
}