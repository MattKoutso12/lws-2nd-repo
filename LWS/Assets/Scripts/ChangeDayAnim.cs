using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeDayAnim : MonoBehaviour
{
    [SerializeField] Image arrow;

    public static event EventHandler OnAnimEnded;

    public void EndAnim()
    {
        Debug.Log("Day Changed");
        OnAnimEnded?.Invoke(this, EventArgs.Empty);
    }

    public void BlockRaycast()
    {
        ButtonManager.Instance.ToggleBlocker();
    }

    public void ShowHideArrow()
    {
        PersistantManager.Instance.arrowVisi = !PersistantManager.Instance.arrowVisi;
        arrow.gameObject.SetActive(PersistantManager.Instance.arrowVisi);
    }
}
