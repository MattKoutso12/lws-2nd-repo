using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class FH_CloseUpPanel : MonoBehaviour
{
    [SerializeField]
    public TextMeshProUGUI ability_text, brains_text, brawn_text, cd_text, activation_text;

    [SerializeField]
    public Image icon;

    public static FH_CloseUpPanel Instance;

    FamiliarWrapper viewed_familiar;

    public void ShowFamiliar(FamiliarWrapper fam)
    {
        transform.GetChild(0).gameObject.SetActive(true);

        icon.sprite = fam.crew.icon;
        ability_text.text = "";
        //ability_text.text = fam.ability;
        activation_text.text = fam.perk.Activation_Current + " uses left";
        brains_text.text = "" + fam.crew.brains;
        brawn_text.text = "" + fam.crew.brawn;

        if(fam.perk.Current_Cooldown <= 0)
        {
            cd_text.text = "Ready to Trigger";
        }
        else
        {
            cd_text.text = "" + fam.perk.Current_Cooldown + " days remaining";
        }
    }

    public void Hide()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }
}
