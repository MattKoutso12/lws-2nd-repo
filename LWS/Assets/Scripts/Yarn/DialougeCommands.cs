using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Yarn.Unity;
using UnityEngine.UI;


public class DialougeCommands: MonoBehaviour
{

    //begin tutorial
    //hide stuff while convo
    public DialogueRunner dialogueRunner;
    public ButtonManager buttonManager;
    public DialogueImages[] images;
    public Transform cameraPos;
    public TextMeshProUGUI nameTextBox;
    private bool hasTTBUpdated = true;

    [Serializable]
    public struct DialogueImages
    {
        public string imagePath;
        public GameObject[] images;
    }

    [Serializable]
    public struct Speaker
    {
        public string name;
        public Sprite[] images;
    }

    [SerializeField] Crew rose;
    public Speaker[] speakers;
    public Image speakerImage;

    [SerializeField] private Image blackScreen;

    public VariableStorageBehaviour variables;

    public static event EventHandler OnTutoSkipped;
    public static event EventHandler OnTutoStarted;
    public static event EventHandler OnStackEnded;

    public class SpawnTTBEventArgs : EventArgs
    {
        public string speaker;
        public string mood;
    }
    readonly private List<string> emotions = new List<string> { "<color=#ffcc00>happy</color>", "<color=#e60000>upset</color>", "<color=#4e4d80>disappointed</color>", "<color=#00e6e6>pleased</color>", "<color=#808080>indifferent</color>" };
    // 0 - happy
    // 1 - upset
    // 2 - disappointed
    // 3 - pleased
    // 4 - indifferent
    public static EventHandler<SpawnTTBEventArgs> OnSpawnTTB;
    public void Awake()
    {
        variables = dialogueRunner.VariableStorage;
        dialogueRunner.AddCommandHandler<double>("change_claudia_phase", ChangeCPhase);
        dialogueRunner.AddCommandHandler<double>("change_lucas_phase", ChangeLPhase);
        dialogueRunner.AddCommandHandler<double>("change_emilia_phase", ChangeEPhase);
        dialogueRunner.AddCommandHandler<double>("change_koji_phase", ChangeKPhase);
        dialogueRunner.AddCommandHandler<double>("change_tutorial_phase", ChangeTPhase);
        dialogueRunner.AddCommandHandler<string>("give_key_item", GiveKeyItem);
        dialogueRunner.AddCommandHandler<bool>("ender", EndDialogue);
        dialogueRunner.AddCommandHandler<bool>("beginTuto", BeginTutorial);
        dialogueRunner.AddCommandHandler<bool>("skipTuto", SkipTutorial);
        dialogueRunner.AddCommandHandler<int>("spawnTTB", SpawnTTB);
        dialogueRunner.AddCommandHandler<int>("cycle_claudia_portrait", CycleCPortrait);
        dialogueRunner.AddCommandHandler<int>("cycle_lucas_portrait", CycleLPortrait);
        dialogueRunner.AddCommandHandler<int>("cycle_emilia_portrait", CycleEPortrait);
        dialogueRunner.AddCommandHandler<int>("cycle_koji_portrait", CycleKPortrait);
        cameraPos = buttonManager.player.gameObject.GetComponent<Transform>();
    }

    public void SpawnTTB(int x)
    {
            OnSpawnTTB?.Invoke(this, new SpawnTTBEventArgs { speaker = nameTextBox.text, mood = emotions[x] });
            hasTTBUpdated = true;
    }

    public void SkipTutorial(bool x)
    {
        TutorialRunner.Instance.skipTutorial = x;
        PersistantManager.Instance.IncreaseShopLevel();
        OnTutoSkipped?.Invoke(this, EventArgs.Empty);
    }

    public void BeginTutorial(bool x)
    {
        TutorialRunner.Instance.BeginTutorial();
        OnTutoStarted?.Invoke(this, EventArgs.Empty);
    }

    public void GiveKeyItem(string key)
    {
        KeyItems.keyItemsActive[key.ToString()] = true;
        Debug.Log(key + ": " + KeyItems.keyItemsActive[key.ToString()]);
        Debug.Log(Type.GetType(key));
        //onKeyItemGiven?.Invoke(key.ToString(), EventArgs.Empty);
    }

    public void EndDialogue(bool active)
    {
        if (DialougeManager.Instance.dialogs.Count > 0)
        {
            String nextDialog = DialougeManager.Instance.dialogs[0];
            DialougeManager.Instance.dialogs.RemoveAt(0);
            DialougeManager.Instance.StartDialogue(nextDialog);
        }
        OnStackEnded?.Invoke(this, EventArgs.Empty);
        Player.Instance.SetMoveLock(false);
    }

    //remember to have yarn "wait x" when doing this, where x is duration + 1;
    IEnumerator CameraLerpFromTo(Vector3 pos1, Vector3 pos2, float duration)
    {
        blackScreen.color = new Color(.07f, .06f, .06f, 0);
        for(float t = 0f; t < duration; t += Time.deltaTime)
        {
            cameraPos.position = Vector3.Lerp(pos1, pos2, t / duration);
            yield return 0;
        }
        cameraPos.position = pos2;
        blackScreen.color = new Color(.07f, .06f, .06f, .8f);
    }


    private GameObject GetImage(string title, int index, string mood = null)
    {
        foreach(Speaker speaker in speakers)
        {
            if(speaker.name.Equals(title))
            {
                nameTextBox.text = speaker.name; // auto-set the name
                if(index < speaker.images.Length)
                    speakerImage.sprite = speaker.images[index];
                return speakerImage.gameObject;
            }
        }

        foreach(DialogueImages d in images)
        {
            if(d.imagePath.Equals(title))
            {
                if(index < d.images.Length)
                {
                    return d.images[index];
                }
            }
        }
        return null;
    }

    public void ChangeTPhase(double phase)
    {
        Debug.Log("Dtf: " + phase);
        switch (phase)
        {
            #region Starting dialogue
            case 1: //LWST1 (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                break;
            case 1.1:
                GetImage("Claudia", 0).SetActive(false);
                GetImage("Tutorial", 0);
                break;
            case 2:
                GetImage("Claudia", 0).SetActive(false);
                Debug.Log("Dtf: Movable");
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Crafting Tutorial
            case 3: //LWSTCrafting (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                GetImage("Crafting", 0).SetActive(true);
                nameTextBox.text = "Claudia";
                break;
            case 3.1:
                GetImage("Crafting", 1).SetActive(true);
                GetImage("Claudia", 0).SetActive(false);
                GetImage("Claudia", 1).SetActive(true);
                break;
            case 3.2:
                GetImage("Crafting", 1).SetActive(false);
                GetImage("Crafting", 2).SetActive(true);
                break;
            case 3.3:
                GetImage("Claudia", 1).SetActive(false);
                GetImage("Claudia", 0).SetActive(true);
                GetImage("Crafting", 2).SetActive(false);
                GetImage("Crafting", 3).SetActive(true);
                break;
            case 4:
                Debug.Log("Dtf: Movable");
                GetImage("Claudia", 0).SetActive(false);
                GetImage("Crafting", 3).SetActive(false);
                GetImage("Crafting", 0).SetActive(false);
                buttonManager.SetTalking(false);
                TutorialRunner.Instance.craftingTable.GetComponentInChildren<ProductionSystem>().Toggle();
                break;
            #endregion
            #region Planting Tutorial
            case 5: //LWSTPlanting (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                GetImage("Planter", 0).SetActive(true);
                nameTextBox.text = "Claudia";
                break;
            case 5.1:
                GetImage("Planter", 1).SetActive(true);
                break;
            case 5.2:
                GetImage("Planter", 1).SetActive(false);
                GetImage("Planter", 2).SetActive(true);
                break;
            case 5.3:
                GetImage("Planter", 2).SetActive(false);
                GetImage("Planter", 3).SetActive(true);
                break;
            case 6:
                Debug.Log("Dtf: Movable");
                GetImage("Claudia", 0).SetActive(false);
                GetImage("Planter", 3).SetActive(false);
                GetImage("Planter", 0).SetActive(false);
                buttonManager.SetTalking(false);
                //TutorialRunner.Instance.plantingTable.GetComponent<ProductionSystem>().Toggle();
                break;
            #endregion
            #region P&P Tutorial
            case 7: //LWSTP&P (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                GetImage("PnP", 0).SetActive(true);
                GetImage("Claudia", 0).SetActive(true);
                nameTextBox.text = "Claudia";
                buttonManager.SetTalking(true);
                break;
            case 7.1:
                GetImage("PnP", 1).SetActive(true);
                break;
            case 8:
                Debug.Log("Dtf: Movable");
                GetImage("Claudia", 0).SetActive(false);
                GetImage("PnP", 1).SetActive(false);
                GetImage("PnP", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Staging Tutorial
            case 9: //LWSTStaging (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                nameTextBox.text = "Claudia";
                break;
            case 9.1:
                GetImage("Staging", 0).SetActive(true);
                GetImage("Staging", 1).SetActive(true);
                break;
            case 9.2:
                GetImage("Staging", 0).SetActive(false);
                GetImage("Staging", 1).SetActive(false);
                GetImage("Staging", 2).SetActive(true);
                break;
            case 9.3:
                GetImage("Staging", 2).SetActive(false);
                GetImage("General", 3).SetActive(true);
                break;
            case 9.4:
                GetImage("General", 3).SetActive(false);
                GetImage("Staging", 3).SetActive(true);
                break;
            case 10:
                Debug.Log("Dtf: Movable");
                GetImage("Staging", 3).SetActive(false);
                GetImage("Claudia", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Familiar Acquisition Tutorial
            case 11: //LWSTFamAqui (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Tutorial", 0);
                GetImage("FamiliarAqui", 0).SetActive(true);
                break;
            case 11.1:
                GetImage("FamiliarAqui", 2).SetActive(true);
                break;
            case 11.2:
                GetImage("FamiliarAqui", 2).SetActive(false);
                GetImage("FamiliarAqui", 0).SetActive(false);
                GetImage("FamiliarAqui", 1).SetActive(true);
                GetImage("FamiliarAqui", 3).SetActive(true);
                break;
            case 11.4:
                GetImage("FamiliarAqui", 1).SetActive(false);
                GetImage("FamiliarAqui", 3).SetActive(false);
                GetImage("FamiliarAqui", 0).SetActive(true);
                break;
            case 12:
                Debug.Log("Dtf: Movable");
                buttonManager.SetTalking(false);
                GetImage("FamiliarAqui", 0).SetActive(false);
                
                break;
            #endregion
            #region Ending Tutorial
            case 13: //LWSTEnd (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Claudia", 1).SetActive(true);
                break;
            case 14:
                Debug.Log("Dtf: Movable");
                buttonManager.SetTalking(false);
                GetImage("Claudia", 1).SetActive(false);
                break;
            #endregion
            #region Player to Planter Tutorial Transition
            case 15: //LWST2 (Player to planting) (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                GetImage("Claudia", 0).SetActive(true);
                nameTextBox.text = "Claudia";
                buttonManager.SetTalking(true);
                break;
            case 15.1:
                GetImage("General", 0).SetActive(true);
                break;
            case 15.2:
                GetImage("General", 0).SetActive(false);
                GetImage("General", 2).SetActive(true);
                break;
            case 15.3:
                GetImage("General", 2).SetActive(false);
                GetImage("Staging", 3).SetActive(true);
                break;
            case 16:
                Debug.Log("Dtf: Movable");
                GetImage("Claudia", 0).SetActive(false);
                GetImage("Staging", 3).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Planter to Crafting Tutorial Transition
            case 17: //LWST3 (planting to crafting) (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Claudia", 2).SetActive(true);
                break;
            case 17.1:
                GetImage("General", 1).SetActive(true);
                break;
            case 18:
                Debug.Log("Dtf: Movable");
                buttonManager.SetTalking(false);
                GetImage("General", 1).SetActive(false);
                GetImage("Claudia", 2).SetActive(false);
                break;
            #endregion
            #region Crafting to Order Tutorial Transition
            case 19: //LWST4 (crafting to orders) (starts the dialouge)
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Claudia", 2).SetActive(true);
                break;
            case 19.1:
                GetImage("General", 4).SetActive(true);
                break;
            case 20:
                Debug.Log("Dtf: Movable");
                buttonManager.SetTalking(false);
                GetImage("General", 4).SetActive(false);
                GetImage("Claudia", 2).SetActive(false);
                break;
            #endregion
            #region Hunting Tutorial
            case 21:
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Tutorial", 0);
                GetImage("Hunting", 0).SetActive(true);
                break;
            case 21.1:
                GetImage("Hunting", 3).SetActive(true);
                break;
            case 21.2:
                GetImage("Hunting", 0).SetActive(false);
                GetImage("Hunting", 3).SetActive(false);
                GetImage("Hunting", 1).SetActive(true);
                break;
            case 21.3:
                GetImage("Hunting", 4).SetActive(true);
                break;
            case 21.4:
                GetImage("Hunting", 4).SetActive(false);
                GetImage("Hunting", 1).SetActive(false);
                GetImage("Hunting", 2).SetActive(true);
                GetImage("Hunting", 5).SetActive(true);
                break;
            case 21.5:
                GetImage("Hunting", 5).SetActive(false);
                GetImage("Hunting", 6).SetActive(true);
                break;
            case 21.6:
                GetImage("Hunting", 6).SetActive(false);
                GetImage("Hunting", 7).SetActive(true);
                break;
            case 22:
                Debug.Log("Dtf: Movable");
                GetImage("Hunting", 2).SetActive(false);
                GetImage("Hunting", 7).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Orders Tutorial
            case 23:
                Debug.Log("Dtf: UnMovable");
                buttonManager.SetTalking(true);
                GetImage("Tutorial", 0);
                GetImage("Orders", 0).SetActive(true);
                GetImage("Orders", 1).SetActive(true);
                break;
            case 23.2:
                GetImage("Orders", 4).SetActive(true);
                break;
            case 23.3:
                GetImage("Orders", 4).SetActive(false);
                GetImage("Orders", 5).SetActive(true);
                break;
            case 23.4:
                GetImage("Orders", 0).SetActive(false);
                GetImage("Orders", 1).SetActive(false);
                GetImage("Orders", 2).SetActive(true);
                GetImage("Orders", 3).SetActive(true);
                GetImage("Orders", 5).SetActive(false);
                GetImage("Orders", 6).SetActive(true);
                break;
            case 23.5:
                GetImage("Orders", 6).SetActive(false);
                break;
            case 23.6:
                GetImage("Orders", 2).SetActive(false);
                GetImage("Orders", 3).SetActive(false);
                GetImage("Orders", 0).SetActive(true);
                GetImage("Orders", 1).SetActive(true);
                break;
            case 24:
                Debug.Log("Dtf: Movable");
                GetImage("Orders", 0).SetActive(false);
                GetImage("Orders", 1).SetActive(false);
                buttonManager.SetTalking(false);
                break;
                #endregion
        }
    }

    public void ChangeCPhase(double phase)
    {
        switch (phase)
        {
            #region Private Meeting 1
            case 1:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                break;
            case 1.2:
                GetImage("Meetings", 0).SetActive(true);
                break;
            case 1.3:
                GetImage("Meetings", 0).SetActive(false);
                break;
            case 2:
                GetImage("Claudia", 0).SetActive(false);
                CrewInventory.Instance.AddCrew(new FamiliarWrapper(rose));
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Private Meeting 2
            case 3:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                break;
            case 3.2:
                GetImage("Meetings", 1).SetActive(true);
                break;
            case 3.3:
                GetImage("Meetings", 1).SetActive(false);
                break;
            case 4:
                GetImage("Claudia", 0).SetActive(false);
                CrewInventory.Instance.AddCrew(new FamiliarWrapper(rose));
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Private Meeting 3
            case 5:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                break;
            case 6:
                GetImage("Claudia", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region End Meeting
            case 7:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Claudia", 0).SetActive(true);
                variables.SetValue("$claudMeets", CouncilReputation.Instance.claudiaPMCount);
                break;
            case 7.1:
                break;
            case 8:
                GetImage("Claudia", 0).SetActive(false);
                buttonManager.SetTalking(false);
                buttonManager.HideUI();
                break;
                #endregion
        }
    }

    public void ChangeLPhase(double phase)
    {
        switch (phase)
        {
            #region Private Meeting 1
            case 1:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 1.1:
                GetImage("Lucas", 0).SetActive(false);
                StartCoroutine(CameraLerpFromTo(cameraPos.position, new Vector3(-35.029f, 129.4641f, 0f), 3));
                break;
            case 1.2:
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 2:
                GetImage("Lucas", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Private Meeting 2
            case 3:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 3.1:
                GetImage("Lucas", 0).SetActive(false);
                StartCoroutine(CameraLerpFromTo(cameraPos.position, new Vector3(-94.81f, 35.50f, 0f), 3));
                break;
            case 3.2:
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 4:
                GetImage("Lucas", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Private Meeting 3
            case 5:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 5.1:
                GetImage("Lucas", 0).SetActive(false);
                StartCoroutine(CameraLerpFromTo(cameraPos.position, new Vector3(-33.5f, 87.8f, 0f), 3));
                break;
            case 5.2:
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 6:
                GetImage("Lucas", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region End Meeting
            case 7:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Lucas", 0).SetActive(true);
                variables.SetValue("$lucasMeets", CouncilReputation.Instance.lucasPMCount);
                break;
            case 8:
                GetImage("Lucas", 0).SetActive(false);
                buttonManager.SetTalking(false);
                buttonManager.HideUI();
                break;
            #endregion
            #region Encounter 1
            case 9:
                buttonManager.SetTalking(true);
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 9.1:
                GetImage("Lucas", 0).SetActive(false);
                break;
            case 9.2:
                StartCoroutine(CameraLerpFromTo(cameraPos.position, new Vector3(-33.5f, 87.8f, 0f), 5));
                break;
            case 9.3:
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 10:
                GetImage("Lucas", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Encounter 2
            case 11:
                buttonManager.SetTalking(true);
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 11.1:
                GetImage("Meetings", 2).SetActive(true);
                break;
            case 11.2:
                GetImage("Meetings", 2).SetActive(false);
                break;
            case 11.3:
                GetImage("Meetings", 3).SetActive(true);
                break;
            case 11.4:
                GetImage("Meetings", 3).SetActive(false);
                break;
            case 11.5:
                GetImage("Meetings", 4).SetActive(true);
                break;
            case 11.6:
                GetImage("Meetings", 4).SetActive(false);
                break;
            case 12:
                GetImage("Lucas", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
                #endregion
        }
    }

    public void ChangeKPhase(double phase)
    {
        switch (phase)
        {
            #region Private Meeting 1
            case 1:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Koji", 0).SetActive(true);
                break;
            case 2:
                GetImage("Koji", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Private Meeting 2
            case 3:
                buttonManager.SetTalking(true);
                GetImage("Koji", 0).SetActive(true);
                break;
            case 4:
                GetImage("Koji", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region End Meeting
            case 5:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Koji", 0).SetActive(true);
                variables.SetValue("$kojiMeets", CouncilReputation.Instance.kojiPMCount);
                break;
            case 6:
                GetImage("Koji", 0).SetActive(false);
                buttonManager.SetTalking(false);
                buttonManager.HideUI();
                break;
                #endregion
        }
    }

    public void ChangeEPhase(double phase)
    {
        switch (phase)
        {
            #region Private Meeting 1
            case 1:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Emilia", 0).SetActive(true);
                break;
            case 2:
                GetImage("Emilia", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region Private Meeting 2
            case 3:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Emilia", 0).SetActive(true);
                break;
            case 4:
                GetImage("Emilia", 0).SetActive(false);
                buttonManager.SetTalking(false);
                break;
            #endregion
            #region End Meeting
            case 5:
                hasTTBUpdated = false;
                buttonManager.SetTalking(true);
                GetImage("Emilia", 0).SetActive(true);
                variables.SetValue("$EmiliaMeets", CouncilReputation.Instance.emiliaPMCount);
                break;
            case 6:
                GetImage("Emilia", 0).SetActive(false);
                buttonManager.SetTalking(false);
                buttonManager.HideUI();
                break;
                #endregion

        }
    }

    public void CycleCPortrait(int port)
    {
        switch (port)
        {
            case 1:
                GetImage("Claudia", 1).SetActive(false);
                GetImage("Claudia", 2).SetActive(false);
                GetImage("Claudia", 0).SetActive(true);
                break;
            case 2:
                GetImage("Claudia", 2).SetActive(false);
                GetImage("Claudia", 0).SetActive(false);
                GetImage("Claudia", 1).SetActive(true);
                break;
            case 3:
                GetImage("Claudia", 1).SetActive(false);
                GetImage("Claudia", 0).SetActive(false);
                GetImage("Claudia", 2).SetActive(true);
                break;
        }
    }

    public void CycleLPortrait(int port)
    {
        switch (port)
        {
            case 1:
                GetImage("Lucas", 1).SetActive(false);
                GetImage("Lucas", 2).SetActive(false);
                GetImage("Lucas", 0).SetActive(true);
                break;
            case 2:
                GetImage("Lucas", 2).SetActive(false);
                GetImage("Lucas", 0).SetActive(false);
                GetImage("Lucas", 1).SetActive(true);
                break;
            case 3:
                GetImage("Lucas", 1).SetActive(false);
                GetImage("Lucas", 0).SetActive(false);
                GetImage("Lucas", 2).SetActive(true);
                break;
        }
    }

    public void CycleEPortrait(int port)
    {
        switch (port)
        {
            case 1:
                GetImage("Emilia", 1).SetActive(false);
                GetImage("Emilia", 2).SetActive(false);
                GetImage("Emilia", 0).SetActive(true);
                break;
            case 2:
                GetImage("Emilia", 2).SetActive(false);
                GetImage("Emilia", 0).SetActive(false);
                GetImage("Emilia", 1).SetActive(true);
                break;
            case 3:
                GetImage("Emilia", 1).SetActive(false);
                GetImage("Emilia", 0).SetActive(false);
                GetImage("Emilia", 2).SetActive(true);
                break;
        }
    }

    public void CycleKPortrait(int port)
    {
        switch (port)
        {
            case 1:
                GetImage("Koji", 1).SetActive(false);
                GetImage("Koji", 2).SetActive(false);
                GetImage("Koji", 0).SetActive(true);
                break;
            case 2:
                GetImage("Koji", 2).SetActive(false);
                GetImage("Koji", 0).SetActive(false);
                GetImage("Koji", 1).SetActive(true);
                break;
            case 3:
                GetImage("Koji", 1).SetActive(false);
                GetImage("Koji", 0).SetActive(false);
                GetImage("Koji", 2).SetActive(true);
                break;
        }
    }
}