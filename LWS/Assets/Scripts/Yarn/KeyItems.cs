using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Keeps Track of Key Item Details
/// </summary>
public static class KeyItems
{
    public static Dictionary<string, bool> keyItemsActive = new Dictionary<string, bool>()
    {
        {"Key", false},
        {"Shovel", false},
        {"Beaker", false},
        {"ARecipe", false},
        {"NRecipe", false},
        {"IRecipe", false},
        {"Hat", false},
        {"Ring", false},
        {"Amulet", false},
        {"FEdge", false},
        {"FShrine", false},
        {"Lake", false},
        {"Woods", false},
        {"Ruins", false},
        {"IWR", false},
        {"HMap", false},
        {"Grassland", false},
        {"Swamp", false},
        {"AForest", false},
        {"Caves", false},
        {"Volcano", false}
    };

    public static Dictionary<string, int> upgradeItemsActive = new Dictionary<string, int>()
    {
        {"Crafter", 0},
        {"Planter", 0},
        {"HBag", 0},
        {"FHouse", 0},
        {"HMap", 0}
    };
}
