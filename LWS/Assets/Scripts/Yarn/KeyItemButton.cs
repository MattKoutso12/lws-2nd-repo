using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItemButton : MonoBehaviour
{
    public string key;

    public void Select()
    {
        KeyItemManager.Instance.Select(key);
    }
}
