﻿using System.Collections;
using UnityEngine;
using TMPro;
using System.Collections.Generic;
using System;

public class KeyItemManager : MonoBehaviour
{
    public static KeyItemManager Instance { get; private set; }
    [SerializeField] GameObject prefab;
    [SerializeField] TextMeshProUGUI infoText;
    [SerializeField] DialougeCommands buttonGen;
    [SerializeField] Transform parent;

    private Dictionary<string, string> keyItemDeats = new Dictionary<string, string>()
    {
        {"Key", "Test"},
        {"Shovel", "Test"},
        {"Beaker", "Test"},
        {"ARecipe", "Test"},
        {"NRecipe", "Test"},
        {"IRecipe", "Test"},
        {"Hat", "Test"},
        {"Ring", "Test"},
        {"Amulet", "Test"},
        {"FEdge", "Test"},
        {"FShrine", "Test"},
        {"Lake", "Test"},
        {"Woods", "Test"},
        {"Ruins", "Test"},
        {"IWR", "Test"},
        {"HMap", "Test"},
        {"Grassland", "Test"},
        {"Swamp", "Test"},
        {"AForest", "Test"},
        {"Caves", "Test"},
        {"Volcano", "Test"}
    };

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        //buttonGen.onKeyItemGiven += OnKeyItemGiven;
    }

    private void OnKeyItemGiven(object sender, EventArgs e)
    {
        GameObject newButton = Instantiate(prefab, new Vector3(0,0,0), Quaternion.identity, parent);
        newButton.GetComponent<KeyItemButton>().key = (string)sender;
    }

    public void Select(string key)
    {
        infoText.text = keyItemDeats[key];
    }
}