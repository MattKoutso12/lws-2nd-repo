using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn;
using Yarn.Unity;

public class DialougeManager : MonoBehaviour
{
    public static DialougeManager Instance { get; private set; }

    [SerializeReference]
    public DialogueRunner dRunn;

    public List<string> dialogs = new List<string>();

    public bool crafterTutFired = false;
    public bool planterTutFired = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void StartDialogue(string node)
    {
        Player.Instance.SetMoveLock(true);
        try
        {
            dRunn.StartDialogue(node);
        }
        catch (NotImplementedException)
        {
            dialogs.Add(node);
        }
    }

}
