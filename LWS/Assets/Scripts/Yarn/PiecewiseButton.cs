using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// NOTE: I've made a couple changes to piecewise button to aid in hookup stuff. Now, piecewise button absolutely must have a button component and will automatically add OnPress as a listener.
[RequireComponent(typeof(Button))]
public class PiecewiseButton : MonoBehaviour
{
    [SerializeField] private bool isInTutorial;
    bool tutorialFired = false;
    public string yarnScript;
    public string context;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnPress); // Add the listener to the button automatically now
    }

    /// <summary>
    /// Attach to the Tutorial Implements button component
    /// *****REQUIRES A BUTTON COMPONENT******
    /// </summary>
    public void OnPress()
    {
        if (!PersistantManager.isNewGame) return; // if we're not even on the tutorial anymore, forget about it entirely.


        //NOTE: might put tips back in post-launch, but right now honestly they're just kind of annoying if you just want to skip the tutorial
       // if (TutorialRunner.Instance.skipTutorial) // tips active?
       //     TipManager.Instance.AddSeenContext(context); // tell the tip we've seen a certain thing


        if (!TutorialRunner.Instance.tutorialActive) return;

        if(isInTutorial)
        {
            if (tutorialFired || !TutorialRunner.Instance.tutorialActive) return;
        }
        else
        {
            if (tutorialFired) return;
        }
        if(gameObject.TryGetComponent(out ProductionSystem sys))
        {
            if (sys.type == CraftedItem.CraftingType.CRAFTING)
            {
                if (!DialougeManager.Instance.crafterTutFired)
                {
                    DialougeManager.Instance.crafterTutFired = true;
                }
                else return;
            }
            if (sys.type == CraftedItem.CraftingType.PLANTING)
            {
                if (!DialougeManager.Instance.planterTutFired)
                {
                    DialougeManager.Instance.planterTutFired = true;
                }
                else return;
            }
        }
        tutorialFired = true;
        DialougeManager.Instance.StartDialogue(yarnScript);
    }
}
