using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DCanvController : MonoBehaviour
{
    public static DCanvController Instance; // just make this a singleton so we can control it
    Canvas canvas;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        if(PersistantManager.isNewGame)
        {
            canvas.enabled = true;
        }
        else
        {
            canvas.enabled = false;
        }

    }

    /// <summary>
    /// Enables the Dialogue Canvas, for talking
    /// </summary>
    /// <param name="enabled">if it should be enabled</param>
    public void EnableCanvas(bool enabled)
    {
        if (canvas == null) canvas = GetComponent<Canvas>();
        canvas.enabled = enabled;
    }

    public bool IsCanvasOn()
    {
        return canvas.enabled;
    }
}
