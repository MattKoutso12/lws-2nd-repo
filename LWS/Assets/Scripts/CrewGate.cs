using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrewGate : MonoBehaviour
{
    private Animator anim;
    [Tooltip("Refers to the reference ID of this gate; this value should be at least 1")]
    [SerializeField] private int gateID;
    private bool closed;

    public bool Closed { get => closed; set => closed = value; }

    public int GetGateID()
    {
        return gateID;
    }

    private void Awake()
    {
        anim = GetComponent<Animator>();
        Closed = true;
    }

    public void OpenGate()
    {
        Closed = false;
        anim.SetTrigger("Open");
    }

    public void CloseGate()
    {
        Closed = true;
        anim.SetTrigger("Close");
    }
}
