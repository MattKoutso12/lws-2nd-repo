using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles enabling and disabling buttons UI.
/// </summary>
public class UIButtonManagerTest : MonoBehaviour
{
    //All variables
    public GameObject inventoryBG;
    public GameObject recipeBookBG;
    public GameObject seedShopBG;
    public GameObject upgradeShopBG;
    public GameObject tabTailBG;
    public GameObject hideButton;





    // Start is called before the first frame update
    void Start()
    {
        SetTabSystem();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SetTabSystem()
    {
        inventoryBG.SetActive(false);
        recipeBookBG.SetActive(false);
        seedShopBG.SetActive(false);
        upgradeShopBG.SetActive(false);
        tabTailBG.SetActive(false);
        hideButton.SetActive(false);
    }

    /// <summary>
    /// Enables all inventory UI and disables other UI.
    /// </summary>
    public void InventorySet()
    {
        //set all these to active
        tabTailBG.SetActive(true);
        inventoryBG.SetActive(true);
        hideButton.SetActive(true);

        //set all these to false
        recipeBookBG.SetActive(false);
        seedShopBG.SetActive(false);
        upgradeShopBG.SetActive(false);
    }

    /// <summary>
    /// Enables all recipebook UI and disables other UI.
    /// </summary>
    public void RecipeSet()
    {
        //set all these to active
        tabTailBG.SetActive(true);
        recipeBookBG.SetActive(true);
        hideButton.SetActive(true);

        //set all these to false
        inventoryBG.SetActive(false);
        seedShopBG.SetActive(false);
        upgradeShopBG.SetActive(false);
    }


    /// <summary>
    /// Enables all seed shop UI and disables other UI.
    /// </summary>
    public void SeedSet()
    {
        //set all these to active
        tabTailBG.SetActive(true);
        seedShopBG.SetActive(true);
        hideButton.SetActive(true);

        //set all these to false
        inventoryBG.SetActive(false);
        recipeBookBG.SetActive(false);
        upgradeShopBG.SetActive(false);
    }

    /// <summary>
    /// Enables all upgrade shop UI and disables other UI.
    /// </summary>
    public void UpgradeSet()
    {
        //set all these to active
        tabTailBG.SetActive(true);
        upgradeShopBG.SetActive(true);
        hideButton.SetActive(true);

        //set all these to false
        inventoryBG.SetActive(false);
        recipeBookBG.SetActive(false);
        seedShopBG.SetActive(false);

    }

    /// <summary>
    /// Hides all inventory, recipebook, and shop UI.
    /// </summary>
    public void HideAll()
    {
        //set all to false
        inventoryBG.SetActive(false);
        recipeBookBG.SetActive(false);
        seedShopBG.SetActive(false);
        upgradeShopBG.SetActive(false);
        tabTailBG.SetActive(false);
        hideButton.SetActive(false);
    }
}
