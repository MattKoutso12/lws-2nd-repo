using System;
using UnityEngine;

/// <summary>
/// UI handling for a crafting system.
/// </summary>
public class UI_CraftingSystem : MonoBehaviour
{
    [SerializeField] private Transform pfUI_Item;
    private Transform[] slotTransformArray;
    private Transform outputSlotTransform;
    public UIStaging uiStaging;
    private Transform itemContainer;
    public CraftingSystem craftingSystem;
    Transform outputContainer;
    [Tooltip("Maps this UI Crafting System to a unique crafting system.")]
    [SerializeField] public int unique_id;
    [SerializeField] private Transform pfUI_Output;

    private void Awake()
    {
        itemContainer = transform.Find("Item Container");
        outputContainer = transform.Find("Output Container");


        slotTransformArray = new Transform[CraftingSystem.DEFAULT_GRID_SIZE];
        outputSlotTransform = transform.Find("OutputSlot");
    }

    private void Start() 
    {
        SetCraftingSystem(new CraftingSystem(unique_id));
        uiStaging.staging.OnCraftingChanged += On_Crafting_Changed;
        this.gameObject.SetActive(false);

        RectTransform rt = GetComponent<RectTransform>();
        
    }

    public void undoCrafting()
    {
        ButtonManager.Instance.ToggleCrafting();
    }

    private void On_Crafting_Changed(object sender, EventArgs e)
    {
    }

    #region SetCraftingSystem
    /// <summary>
    /// Sets the craftingsystem this UI is representing.
    /// </summary>
    /// <param name="craftingSystem"> Crafting system beign represented. </param>
    #endregion
    public void SetCraftingSystem(CraftingSystem craftingSystem)
    {
        this.craftingSystem = craftingSystem;
        craftingSystem.OnGridChanged += CraftingSystem_OnGridChanged;
    }

    private void CraftingSystem_OnGridChanged(object sender, EventArgs e)
    {
    }

    private void OnGUI()
    {
        
    }

    private void UI_CraftingSystem_OnItemDropped(object sender, UI_CraftingItemSlot.OnItemDroppedEventArgs e)
    {
        craftingSystem.TryAddItem(e.item, e.x);
    }

    private void CreateItem(int x, InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Item, itemContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = slotTransformArray[x].GetComponent<RectTransform>().anchoredPosition;
        itemTransform.GetComponent<DragDrop>().SetItem(item);
    }
    private void CreateItemOutput(InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Output, outputContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = outputSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        //itemTransform.localScale = Vector3.one * 1.5f;
        itemTransform.GetComponent<OutputSetter>().SetItem(item);
    }
}
