using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SetGlobalParameter : MonoBehaviour
{
    private FMOD.Studio.EventInstance instance;
    
    [FMODUnity.EventRef]
    public string fmodEvent;

    [SerializeField]
    [Range(0f, 1f)]
    private float gameMusic;



    void Start()
    {
        instance = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
        instance.start();
    }

   
    void Update()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("event:/All Song Parts", gameMusic);
    }
}
