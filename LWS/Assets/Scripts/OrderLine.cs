﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents the list of accepted orders to add or remove from.
/// </summary>
public class OrderLine
{
    public event EventHandler OnOrderListChanged;
    public List<OrderWrapper> orders;

    /// <summary>
    /// Constructor method for OrderLine.
    /// </summary>
    public OrderLine()
    {
        orders = new List<OrderWrapper>();
    }

    #region AddOrder comment
    /// <summary>
    /// Adds an order to the list of accepted orders.
    /// </summary>
    /// <param name="order"> Order to be added. </param>
    #endregion
    public void AddOrder(OrderWrapper order)
    {
        orders.Add(order);
        PersistantManager.Instance.orderWrappers.Add(order);
        OnOrderListChanged?.Invoke(this, EventArgs.Empty);
    }

    #region RemoveOrder comment
    /// <summary>
    /// Removes an order from the list of accepted orders.
    /// </summary>
    /// <param name="order"> Order to be removed. </param>
    #endregion
    public void RemoveOrder(OrderWrapper order)
    {
        orders.Remove(order);
        PersistantManager.Instance.orderWrappers.Remove(order);
        OnOrderListChanged?.Invoke(this, EventArgs.Empty);
    }
}