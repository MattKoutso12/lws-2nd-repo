using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayToNightFeedback : MonoBehaviour
{
    public static DayToNightFeedback Instance { get; private set; }

    private Camera feedbackCamera;
    private Player player;
    private List<GameObject> localSystems;
    private Button closeShop;
    private int recipeLength;
    private int recipeLengthRemaining;
    [SerializeField] UIStaging stagingSystem;
    [SerializeField] GameObject outputImagePrefab;
    [SerializeField] ParticleSystem particles;
    [SerializeField] GameObject particle_renderer;
    [SerializeField] OutputParent bubble;

    void Start()
    {
        Instance = this;
        localSystems = new List<GameObject>();
        feedbackCamera = Camera.main;
        player = Player.Instance;
        closeShop = transform.Find("CloseShopButton").GetComponent<Button>();
        this.gameObject.SetActive(false);
    }


    /*
     * NEW PLAN:
     * 
     * 1. Get a list of all our output stacks (possibly also their locations)
     * 2. Begin a new coroutine to manage the overall output-getting process. This must be a coroutine so we can yield until certain things happen.
     * 
     * 3. For each output:
     *  -Set foreground sprite to current item
     *  -Start bubble animation with a parameter for "fullness".
     *  -Bubble anim has an event which will tell us when it is done. Wait for this to happen.
     *  -Check if we just completed an item. If so, do particle stuff. OnParticleAttracted should tell us when output reaches inventory. When it does, give the player the item (IMPORTANT)!
     * 
     * 4. After we've hit every output, make the bubble shrink away.
     * 5. Set close shop active = true
     * 
     */

    




    /// <summary>
    /// An adaptation of FetchOutputs for the new staging. Also is more straightforward as a result.
    /// 
    /// Will grab outputs from all production systems.
    /// </summary>
    public void FetchOutputs()
    {

        // loop thru all the systems, grab their pending outputs.
        List<ProductionSystem> readySystems = new List<ProductionSystem>();
        foreach (ProductionSystem system in ProductionSystem.all_systems)
        {
            InventoryItem item;
            system.CheckRecipe(out item);
            if (item != null && system.crewslot.GetHeldObject() != null)
            {
                readySystems.Add(system); // add the system if it's making something
            }
        }

        
        StartCoroutine(OutputRoutine(readySystems));
        
    }

    public Vector3 TranslateWorldToPlayerPoint(Vector3 translate)
    {
        return new Vector3(translate.x + 270, translate.y + 260, 0);
    }


    /// <summary>
    /// An adaptation of OutputRoutine for ProductionSystems
    /// </summary>
    /// <param name="systems">All systems which will produce something overnight</param>
    IEnumerator OutputRoutine(List<ProductionSystem> systems)
    {
        closeShop.gameObject.SetActive(false);
        SongManager.Instance.MusicDown();
        List<InventoryItem> outputs = ShiftPlanning.ProcessProduction(); //Process production must be called here due to race conditions. We should probably change this, maybe change the day/night feedback to animations rather than nested coroutines so we can do this on one thread
        outputs.Add(new InventoryItem(Item.GetItem("moonleaf"), 3));
        outputs.Add(new InventoryItem(Item.GetItem("water"), 8));
        for(int i = 0; i < outputs.Count; i++)
        {
            InventoryItem item = outputs[i];
            if (i < outputs.Count - 1) bubble.SetNextOutput(outputs[i + 1]);

            ShowCompleteAnimation(item);
            readyForNext = false;
            particle_renderer.SetActive(true);
            while(!readyForNext)
            {
                yield return null;
            }
            particle_renderer.SetActive(false);
        }
        closeShop.gameObject.SetActive(true);
    }

    bool readyForNext = false;
    void ShowCompleteAnimation(InventoryItem item)
    {
        bubble.SetCurrentOutput(item);
        bubble.PlayCompleteAnimation();
    }

    public void AdvanceToNext()
    {
        readyForNext = true;
    }

    /// <summary>
    /// An adaptation of WaitToFinish for the new staging/ProductionSystems. Makes stuff waaayyy easier and less reliant on GetComponent/Find/etc.
    /// </summary>
    /// <param name="system">The system whose output is being processed</param>
    /// <param name="totalSystems">The total number of systems</param>
    IEnumerator WaitToFinish(ProductionSystem system, float totalSystems)
    {
        // Looks different from the old WaitToFinish but does the exact same thing. New ProductionSystems let us grab everything we need at once
        InventoryItem temp;
        recipeLengthRemaining = system.CheckRecipe(out temp); // get the remaining time and item being worked on
        recipeLength = system.GetCurrentFullDuration();

        // move cam to active system
        Vector3 worldPoint = feedbackCamera.ScreenToWorldPoint(system.transform.position);
        Vector3 playerPoint = TranslateWorldToPlayerPoint(worldPoint); // extra Vector3 for readability
        player.transform.position = new Vector3(playerPoint.x, playerPoint.y, player.transform.position.z);

        //do the progress stuff
        //bubble.Play("Filling");

        //This is probably bad but we have to have the actual logic of getting the item in here. Otherwise, by the time it checks what the thing is crafting, it'll already be crafted since this coroutine will go parallel to
        // the main thread which would call ProcessProduction(). Therefore, the first item will be crafted with the animation no problem, but any items after that will be done crafting by the time they get here.


        if (totalSystems == 1f)
        {
            yield return new WaitForSeconds(5);
        }
        else
        {
            yield return new WaitForSeconds(10f / totalSystems);
        }

    }

    /*
    IEnumerator MoveTo(Vector2 sysVect) {
        Vector2 playerVect = new Vector2(player.transform.position.x, player.transform.position.y);
        float stepDistance = 1f / Vector2.Distance(playerVect, sysVect);
        float stepsTaken = 0;
        while(stepsTaken <= 1)
        {
            stepsTaken += stepDistance;
            player.transform.position = Vector2.Lerp(playerVect, sysVect, stepDistance);
            yield return null;  
        }
        player.transform.position = sysVect;     
    }
    */
}
