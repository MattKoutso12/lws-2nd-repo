﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Handles UI for the OrderLine.
/// </summary>
public class UI_OrderLine : MonoBehaviour
{
    private Transform orderContainer;
    private Transform orderTemplate;
    public OrderLine orderLine;
    [SerializeField] private Transform pfUI_Order;
    [SerializeField] private float padding;
    [SerializeField] private float size;

    private void Awake()
    {
        orderContainer = transform.Find("OrderContainer");
        orderTemplate = orderContainer.Find("OrderTemplate");
        orderTemplate.gameObject.SetActive(false);
    }

    #region SetOrderLine
    /// <summary>
    /// Sets the order line and refreshes the orders.
    /// </summary>
    /// <param name="orderLine"></param>
    #endregion
    public void SetOrderLine(OrderLine orderLine)
    {
        this.orderLine = orderLine;
        orderLine.OnOrderListChanged += OrderLine_OnOrderListChanged;
        RefreshOrders();
    }

    private void OrderLine_OnOrderListChanged(object sender, System.EventArgs e)
    {
        RefreshOrders();
    }

    private void RefreshOrders()
    {
        foreach(Transform child in orderContainer)
        {
            if (child == orderTemplate) continue;
            Destroy(child.gameObject);
        }

        foreach(OrderWrapper order in orderLine.orders)
        {
            CreateOrder(order, orderLine.orders.IndexOf(order));
        }
    }

    private void CreateOrder(OrderWrapper order, int orderIndex)
    {
        Transform orderTransform = Instantiate(orderTemplate, orderContainer);

        RectTransform rectTransform = orderTransform.GetComponent<RectTransform>();
        
        rectTransform.gameObject.SetActive(true);

        float y = orderContainer.GetComponent<RectTransform>().anchoredPosition.y;

        float x = orderIndex * size + padding;

        rectTransform.anchoredPosition = new Vector2(x, y);

        orderTransform.GetComponent<OrderDragDrop>().SetOrder(order);
    }


}