using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollLockingPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler 
{


    public void OnPointerEnter(PointerEventData eventData)
    {
        Player.Instance.SetMoveLock(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Player.Instance.SetMoveLock(false);
    }

}
