using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CraftingPreview : MonoBehaviour
{

    public static CraftingPreview Instance;

    RecipeSlot[] slots;

    [SerializeField]
    public Image background;

    [SerializeField]
    public Sprite crafting, planting;

    [SerializeField]
    public RecipeSlot seeds, water, output;

    [SerializeField]
    public GameObject craftingslots;

    [SerializeField]
    public TextMeshProUGUI header, days_to_craft, amount_crafted;

    [SerializeField]
    public Color water_color;


    /// <summary>
    /// Sets the item to see the crafting recipe of
    /// </summary>
    /// <param name="item">The item to see</param>
    public void SetPreviewedItem(CraftedItem item)
    {
        if (item != null)
        {

            MakeVisible(true);
        }
        else
        {
            MakeVisible(false);
            return;
        }

        seeds.SetItem(null);
        water.SetItem(null);

        // this is all very hacky. We'll need to change it if we want to do anything different with planting
        if (item.crafting_type == CraftedItem.CraftingType.CRAFTING)
            SetPlantingMode(false);
        else
            SetPlantingMode(true);


        days_to_craft.SetText("Takes " + item.days_to_craft + " day" + (item.days_to_craft > 1 ? "s":""));

        if (item.crafting_type == CraftedItem.CraftingType.PLANTING)
        {
            seeds.SetItem(item.recipe[0]);
            water.SetItem(item.recipe[1]);
        }
        else
        {
            for (int i = 0; i < item.recipe.Length; i++)
            {
                slots[i].SetItem(item.recipe[i]);
            }

        }

        

        output.SetItem(item);
        amount_crafted.text = ""+item.outputYield;

    }

    public void SetPlantingMode(bool isPlanting)
    {
        if(isPlanting)
        {
            background.sprite = planting;
            //slots[0].gameObject.GetComponent<Image>().color *= new Color(1,1,1, 0);
            //slots[3].gameObject.GetComponent<Image>().color *= new Color(1, 1, 1, 0);
            seeds.gameObject.SetActive(true);
            water.gameObject.SetActive(true);
            header.SetText("How To Plant:");
            craftingslots.SetActive(false);
        }
        else
        {
            background.sprite = crafting;
            header.SetText("How To Craft:");
            //slots[0].gameObject.GetComponent<Image>().color += new Color(0, 0, 0, 1);
            //slots[3].gameObject.GetComponent<Image>().color += new Color(0, 0, 0, 1);
            //slots[2].gameObject.GetComponent<Image>().color = Color.white;
            seeds.gameObject.SetActive(false);
            water.gameObject.SetActive(false);
            craftingslots.SetActive(true);
        }
    }

    public void MakeVisible(bool visible)
    {
        transform.GetChild(0).gameObject.SetActive(visible);
    }


    private void OnEnable()
    {
        Instance = this;

    }

    // Start is called before the first frame update
    void Start()
    {
        slots = GetComponentsInChildren<RecipeSlot>();
        MakeVisible(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
