using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains the method to display the crew card
/// </summary>
public class CrewCardToggler : MonoBehaviour
{
    bool hasSpawned = false; //a check to make spawning/randomization happen only once
    public GameObject crewCard; //the referred to crewcard
    public Transform parent; //the location of the spawner
    private GameObject spawnedCard; //the reference to the spawned card

    /// <summary>
    /// Displays crew card if it exists, creates one if it doesn't.
    /// </summary>
    public void ToggleCC()
    {
        //assigns spawner
        Transform spawner = parent; //.Find("Spawner").transform;

        //checks the aforementioned solo spawn check
        if(hasSpawned)
        {
            //if true, just show card
            spawnedCard.SetActive(true);
        }

        //checks the aforementioned solo spawn check
        if(!hasSpawned)
        {
            //if false, creates a new card and sets check
            spawnedCard = Instantiate(crewCard, spawner.position, spawner.rotation, parent);
            hasSpawned = true;
        }
    }
}
