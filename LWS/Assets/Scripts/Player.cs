using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Yarn.Unity;
using UnityEngine.SceneManagement;
/// <summary>
/// Player movement and action script.
/// </summary>
public class Player : MonoBehaviour
{

    public static Player Instance { get; private set; }

    [SerializeField] private DialogueRunner dRun;

    private Inventory inventory;
    private CrewInventory crewInvent;
    
    private UI_CraftingSystem uiCraftingSystem;

    [SerializeField] float speed;

    [SerializeField] GameObject info;
    
    private bool begin;
    [SerializeField] bool helpVisible;

    [SerializeField] GameObject help;

    [SerializeField] float zoomSpeed;
    [SerializeField] float zoomMin;
    [SerializeField] float zoomMax;

    [SerializeField] public float minX;
    [SerializeField] public float minY;
    [SerializeField] public float maxX;
    [SerializeField] public float maxY;

    [SerializeField]
    public GameObject mainInventoryGrid;

    [SerializeField] 
    public GameObject crewInventoryGrid;

    public float speedCoefficient;

    private Camera playerCam;

    bool locked = false;

    bool dragging = false;

    private bool checkMoveKeys = false;
    private bool checkMoveClick = false;
    private bool checkZoom = false;

    private Vector3 mousePosStart;

    private Vector3 mousePos;


    public InputAction scrollAction;

    private float scrollValue;

    private Vector3 moveDir;

    private void Awake()
    {
        Instance = this;
        crewInvent = CrewInventory.MakeFromChildren(crewInventoryGrid);
        inventory = Inventory.MakeFromChildren(mainInventoryGrid);

    }

    private void Start()
    {
        Debug.Log("START CALLED");
        
        // initialize inventory & singleton
        //scrollAction.performed += ctx => Scroll();

        //scrollAction.Enable();


        begin = true;
        playerCam = Camera.main;
        speedCoefficient = speed / playerCam.orthographicSize;
    }

    #region UseItem comment
    /// <summary>
    /// Uses a designated inventory item.
    /// </summary>
    /// <param name="inventoryItem"> Item to be used. </param>
    #endregion
    public void UseItem(InventoryItem inventoryItem)
    {
        Debug.Log("Use Item: " + inventoryItem);
    }

    #region UseCrew comment
    /// <summary>
    /// Uses a designcated familiar.
    /// </summary>
    /// <param name="crew"> Familiar to be used. </param>
    #endregion
    public void UseCrew(FamiliarWrapper crew)
    {
        Debug.Log("Crew:  " + crew);
    }


    /// <summary>
    /// A quick way to stop the player from zooming/moving around.
    /// </summary>
    /// <param name="locked">If the player's movement is locked</param>
    public void SetMoveLock(bool locked)
    {
        this.locked = locked;
        moveDir = Vector2.zero;
    }

    /// <summary>
    /// See if the player is move-locked
    /// </summary>
    /// <returns></returns>
    public bool IsMoveLocked()
    {
        return this.locked;
    }

    /// <returns>A tuple with zoom minimum and maximum values, respectively.</returns>
    public (float, float) GetZoomMinMax()
    {
        return (zoomMin, zoomMax);
    }


    private void Update()
    {
        // if we're dragging or are locked.
        // changed from just if(locked) to fix a bug where dragging an item breaks you out of movelock (because when you finish dragging, it calls SetMoveLock(false)).
        if (locked || UICursorManager.grabbing) return;

        if(!UICursorManager.instance.GetGrabbing())
        {
            if(Mouse.current.leftButton.isPressed)
            {
                mousePos = (playerCam.ScreenToWorldPoint(Mouse.current.position.ReadValue())) - playerCam.transform.position;
                if(!dragging)
                {
                    dragging = true;
                    mousePosStart = playerCam.ScreenToWorldPoint(Mouse.current.position.ReadValue());
                }
            }
            else
            {
                dragging = false;
            }
            if(dragging)
            {
                transform.position = new Vector3(Mathf.Clamp((mousePosStart - mousePos).x, minX, maxX), Mathf.Clamp((mousePosStart - mousePos).y, minY, maxY), transform.position.z);
                if(!checkMoveClick && TutorialRunner.Instance.tutorialActive)
                {
                    checkMoveClick = true;
                    TutorialRunner.Instance.PlayerCheck("moveMouse");
                }
                
            }
        }
        
        transform.position += moveDir * speed * Time.deltaTime;
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minX, maxX), Mathf.Clamp(transform.position.y, minY, maxY), transform.position.z);

        /*
        if((Input.GetAxis("Mouse ScrollWheel") > 0) && playerCam.orthographicSize > zoomMin && Time.timeScale != 0){
            playerCam.orthographicSize -= zoomSpeed;
            speed = (playerCam.orthographicSize * speedCoefficient);
        }
        else if((Input.GetAxis("Mouse ScrollWheel") < 0) && playerCam.orthographicSize < zoomMax && Time.timeScale != 0){
            playerCam.orthographicSize += zoomSpeed;
            speed = (playerCam.orthographicSize * speedCoefficient);
        }
        */
    
        /*if(begin)
        {
            help.SetActive(false);
            info.SetActive(true);
            if (Input.GetKeyUp(KeyCode.Space))
            {
                help.SetActive(false);
                info.SetActive(false);
                begin = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && !begin)
        {
            helpVisible = !helpVisible;
            help.SetActive(helpVisible);
        }*/
    }

    //Action on using the scroll wheel, handles zooming in and out
    void OldScroll()
    {
        if (locked) return;
        if(!checkZoom && TutorialRunner.Instance.tutorialActive)
        {
            checkZoom = true;
            TutorialRunner.Instance.PlayerCheck("zoom");
        }
        scrollValue = scrollAction.ReadValue<float>();
        if(scrollValue > 0 && playerCam.orthographicSize > zoomMin && Time.timeScale != 0)
        {
            playerCam.orthographicSize -= zoomSpeed;
            speed = (playerCam.orthographicSize * speedCoefficient);
        }
        else if(scrollValue <= 0 && playerCam.orthographicSize < zoomMax && Time.timeScale != 0)
        {
            playerCam.orthographicSize += zoomSpeed;
            speed = (playerCam.orthographicSize * speedCoefficient);
        }
    }

    public void Scroll(InputAction.CallbackContext ctx)
    {
        if (locked) return;
        if (!ctx.performed) return;
        if (!checkZoom && TutorialRunner.Instance.tutorialActive)
        {
            checkZoom = true;
            TutorialRunner.Instance.PlayerCheck("zoom");
        }
        scrollValue = ctx.ReadValue<float>();
        if (scrollValue > 0 && playerCam.orthographicSize > zoomMin && Time.timeScale != 0)
        {
            playerCam.orthographicSize -= zoomSpeed;
            speed = (playerCam.orthographicSize * speedCoefficient);
        }
        else if (scrollValue <= 0 && playerCam.orthographicSize < zoomMax && Time.timeScale != 0)
        {
            playerCam.orthographicSize += zoomSpeed;
            speed = (playerCam.orthographicSize * speedCoefficient);
        }
    }
    

    public void Move(InputAction.CallbackContext ctx)
    {
        if (locked) return;
        moveDir = ctx.ReadValue<Vector2>();
        if(!checkMoveKeys && TutorialRunner.Instance.tutorialActive)
        {
            checkMoveKeys = true;
            TutorialRunner.Instance.PlayerCheck("moveKeys");
        }
    }

    public Inventory GetInventory()
    {
        return inventory;
    }

    public CrewInventory GetCrewInventory()
    {
        return crewInvent;
    }

}
