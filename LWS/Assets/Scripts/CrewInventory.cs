﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains all obtained familiars, as well as functions allowing to access and alter the list.
/// </summary>
public class CrewInventory : ICrewHolder
{
    public event EventHandler OnItemListChanged;

    public static List<FamiliarWrapper> crewList;
    private Action<FamiliarWrapper> useItemAction;
    public CrewSlot[] inventorySlotArray;

    public static CrewInventory Instance;

    #region CrewInventory comment
    /// <summary>
    /// Constructor method for CrewInventory.
    /// </summary>
    /// <param name="useItemAction"> Action for familiar use. </param>
    /// <param name="inventorySlotCount"> Size of CrewInventory. </param>
    #endregion
    public CrewInventory(Action<FamiliarWrapper> useItemAction, int inventorySlotCount)
    {
        this.useItemAction = useItemAction;
        crewList = new List<FamiliarWrapper>();

        inventorySlotArray = new CrewSlot[inventorySlotCount];
        for (int i = 0; i < inventorySlotCount; i++)
        {
            inventorySlotArray[i] = new CrewSlot();
        }
    }

    private CrewInventory(CrewSlot [] slots)
    {
        this.inventorySlotArray = slots;
        crewList = new List<FamiliarWrapper>();
    }

    public static CrewInventory MakeFromChildren(GameObject parent)
    {
        CrewSlot [] slots = parent.transform.GetChild(0).GetComponentsInChildren<CrewSlot>();
        return new CrewInventory(slots);
    }

    #region GetEmptyInventorySlot comment
    /// <summary>
    /// Returns the first empty InventorySlot.
    /// </summary>
    /// <returns> Empty slot in CrewInventory. </returns>
    #endregion
    public CrewSlot GetEmptyInventorySlot()
    {
        foreach (CrewSlot inventorySlot in inventorySlotArray)
        {
            if (inventorySlot.IsEmpty())
            {
                return inventorySlot;
            }
        }
        Debug.LogError("Cannot find an empty InventorySlot!");
        return null;
    }

    #region GetInventorySlotWithItem comment
    /// <summary>
    /// Attempts to return slot with a specific familiar.
    /// </summary>
    /// <param name="crew"> Familiar we wish to find slot of. </param>
    /// <returns> Slot with designated familiar. </returns>
    #endregion
    public CrewSlot GetInventorySlotWithItem(FamiliarWrapper crew)
    {
        foreach (CrewSlot inventorySlot in inventorySlotArray)
        {
            if (inventorySlot.GetHeldObject() == crew)
            {
                return inventorySlot;
            }
        }
        Debug.LogError("Cannot find Crew " + crew + " in a InventorySlot!");
        return null;
    }

    #region AddCrew comment
    /// <summary>
    /// Adds familiar to CrewInventory.
    /// </summary>
    /// <param name="crew"> Familiar we want to add. </param>
    #endregion
    public void AddCrew(FamiliarWrapper crew)
    {
        if (GetEmptyInventorySlot() != null && GetEmptyInventorySlot().TryAddObject(crew))
        {
            crewList.Add(crew);
            crew.SetCrewHolder(this);
            OnItemListChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    #region RemoveCrew comment
    /// <summary>
    /// Removes designated familiar from CrewInventory.
    /// </summary>
    /// <param name="crew"> Familiar we want to remove. </param>
    #endregion
    public void RemoveCrew(FamiliarWrapper crew)
    {
        CrewSlot removeSlot = GetInventorySlotWithItem(crew);
        if(removeSlot)
        {
            removeSlot.SetHolding(null);
            crewList.Remove(crew);
            OnItemListChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    #region AddItem comment
    /// <summary>
    /// Adds designated familiar to a specific InventorySlot.
    /// </summary>
    /// <param name="crew"> Familiar we want to add. </param>
    /// <param name="inventorySlot"> Slot we want to add to. </param>
    #endregion
    public void AddItem(FamiliarWrapper crew, CrewSlot inventorySlot)
    {
        // Add Item to a specific Inventory slot
        crewList.Add(crew);
        crew.SetCrewHolder(this);
        inventorySlot.TryAddObject(crew);

        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    #region UseItem comment
    /// <summary>
    /// Activates action of designated familiar.
    /// </summary>
    /// <param name="crew"> Familiar we want to use. </param>
    #endregion
    public void UseItem(FamiliarWrapper crew)
    {
        useItemAction(crew);
    }

    #region GetItemList comment
    /// <summary>
    /// Returns list of familiars.
    /// </summary>
    /// <returns> List of familiars. </returns>
    #endregion
    public static List<FamiliarWrapper> GetItemList()
    {
        return crewList;
    }

    #region GetInventorySlotArray comment
    /// <summary>
    /// Returns array of inventory slots.
    /// </summary>
    /// <returns> Array of inventory slots. </returns>
    #endregion
    public CrewSlot[] GetInventorySlotArray()
    {
        return inventorySlotArray;
    }

    #region CanAddCrew comment
    /// <summary>
    /// Checks whether it's possible to add more familiars.
    /// </summary>
    /// <returns> Whether we can add more familiars or not. </returns>
    #endregion
    public bool CanAddCrew()
    {
        return GetEmptyInventorySlot() != null;
    }

    /*
     * Represents a single Inventory Slot
     * */
    public class InventorySlot
    {

        private int index;
        private FamiliarWrapper crew;

        public InventorySlot(int index)
        {
            this.index = index;
        }

        public FamiliarWrapper GetItem()
        {
            return crew;
        }

        public void SetItem(FamiliarWrapper crew)
        {
            this.crew = crew;
        }

        public void RemoveItem()
        {
            crew = null;
        }

        public bool IsEmpty()
        {
            return crew == null;
        }

    }

}