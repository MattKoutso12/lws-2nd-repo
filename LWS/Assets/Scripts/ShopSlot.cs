﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class ShopSlot : ItemSlot
{

    [SerializeField]
    public UnityEvent OnPurchase;

    private bool checkPurchaseMoonleaf = false;
    private bool checkPurchaseWater = false;

    private void Awake()
    {
        Debug.Log("Starting slot! ID: " + _id);
        // do nothing. prevents adding to id_to_slot
        if(id_to_slot.ContainsKey(_id))
        {
            id_to_slot.Remove(_id);
        }

    }

    protected override void OnGUI()
    {
        base.OnGUI();
        if (this.starting_amount > 1)
            this.amount_text.text = $"x{this.starting_amount}";
        else
            this.amount_text.text = "";

    }

    public override bool TryTransfer(UISlot other)
    {
        if (other is SellSlot || other is ShopSlot) return false;
        if (!other.IsEmpty()) return false;
        if (PersistantManager.Instance.TryPurchase(((InventoryItem)this.GetHeldObject()).itemType.SellPrice * this.GetAmount()))
        {
            if(((InventoryItem)this.GetHeldObject()).itemType.getIDName().Equals("moonleaf_seeds") && !checkPurchaseMoonleaf && TutorialRunner.Instance.tutorialActive)
            {
                checkPurchaseMoonleaf = true;
                TutorialRunner.Instance.PlantingCheck("buySeed");
            }
            if(((InventoryItem)this.GetHeldObject()).itemType.getIDName().Equals("water") && !checkPurchaseWater && TutorialRunner.Instance.tutorialActive)
            {
                checkPurchaseWater = true;
                TutorialRunner.Instance.CraftingCheck("buyWater");
            }
            return base.TryTransfer(other);
        } 

        return false;
    }

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);

        if(new_holding == null)
        {
            OnPurchase?.Invoke();
            this.holding = new InventoryItem(startsWith, starting_amount);
        }
        
    }
}