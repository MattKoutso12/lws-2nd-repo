﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// Currently not in use. Represents a material.
/// </summary>
[CreateAssetMenu(fileName = "New Material", menuName = "Items/Materials")]
public class LWSMaterial : Item
{
    [SerializeField] private int length;
    [SerializeField] private string useText = "Use:";

    public LWSMaterial(string item_name, Sprite sprite) : base(item_name, sprite)
    {
    }

    public int Length { get { return length; } }


}