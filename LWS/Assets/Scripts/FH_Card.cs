using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class FH_Card : MonoBehaviour
{
    public FamiliarWrapper crew;

    [SerializeField]
    public Image portrait, background;

    [SerializeField]
    public TMP_InputField name_text;

    public void Init(FamiliarWrapper fam)
    {
        crew = fam;
        portrait.sprite = fam.crew.sprite;
        name_text.text = crew.name;

    }

    public void ShowCloseUp()
    {
        FH_CloseUpPanel.Instance.ShowFamiliar(crew);
        crew.crew.TriggerViewEvent();
    }

    public void SetName(string name)
    {
        crew.name = name;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
