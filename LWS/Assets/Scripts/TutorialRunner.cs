using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TutorialRunner : MonoBehaviour
{
    public static TutorialRunner Instance;

    [SerializeField] GameObject plantingTable;
    public GameObject craftingTable;
    [SerializeField] GameObject inventoryButton;
    [SerializeField] GameObject recipeButton;
    [SerializeField] GameObject stagingDoor;
    [SerializeField] GameObject stagingIndicator;
    [SerializeField] GameObject orderButton;
    [SerializeField] GameObject familiarHouse;
    [SerializeField] GameObject familiarIndicator;
    [SerializeField] GameObject signpost;
    [SerializeField] GameObject signpostIndicator;
    [SerializeField] GameObject crystalBall;
    [SerializeField] GameObject crystalIndicator;
    [SerializeField] GameObject huntingTable;
    [SerializeField] GameObject huntingIndicator;
    [SerializeField] GameObject shopSlotContainer;
    [SerializeField] GameObject chapterCard;

    public static EventHandler OnTutoFinished;

    public List<ShopSlot> shopSlots;
    private ShopSlot moonleafSeedSlot;
    private ShopSlot waterSlot;

    public bool tutorialActive = false;
    public bool skipTutorial = false;

    private bool playerTask1 = false;
    private bool playerTask2 = false;
    private bool playerTask3 = false;
    private bool playerComplete = false;

    private bool planterTask1 = false;
    private bool planterTask2 = false;
    private bool planterTask3 = false;
    private bool planterComplete = false;

    private bool crafterTask1 = false;
    private bool crafterTask2 = false;
    private bool crafterTask3 = false;
    private bool crafterTask4 = false;
    private bool crafterComplete = false;

    private bool orderTask1 = false;
    private bool orderTask2 = false;
    private bool orderTask3 = false;
    private bool orderComplete = false;

    private void Awake()
    {
        Instance = this;
    }

    public void BeginTutorial()
    {
        if (!PersistantManager.isNewGame) return;
        


        playerTask1 = false;
        playerTask2 = false;
        playerTask3 = false;
        playerComplete = false;

        planterTask1 = false;
        planterTask2 = false;
        planterTask3 = false;
        planterComplete = false;

        crafterTask1 = false;
        crafterTask2 = false;
        crafterTask3 = false;
        crafterTask4 = false;
        crafterComplete = false;

        orderTask1 = false;
        orderTask2 = false;
        orderTask3 = false;
        orderComplete = false;

        DisableAll();
        tutorialActive = true;
    }

    public void PopulateShopArray()
    {
        foreach(Transform child in shopSlotContainer.transform)
        {
            if(child.GetComponent<ShopSlot>())
            {
                shopSlots.Add(child.GetComponent<ShopSlot>());
            }
        }

        foreach(ShopSlot slot in shopSlots)
        {
            if(slot.startsWith.getIDName().Equals("moonleaf_seeds"))
            {
                moonleafSeedSlot = slot;
            }
            if(slot.startsWith.getIDName().Equals("water"))
            {
                waterSlot = slot;
            }
            slot.transform.gameObject.SetActive(false);
        }
    }

    public void DisableAll()
    {
        plantingTable.SetActive(false);
        craftingTable.SetActive(false);
        inventoryButton.SetActive(false);
        recipeButton.SetActive(false);
        stagingDoor.SetActive(false);
        orderButton.SetActive(false);
        familiarHouse.SetActive(false);
        signpost.SetActive(false);
        crystalBall.SetActive(false);
        huntingTable.SetActive(false);
        familiarIndicator.SetActive(false);
        stagingIndicator.SetActive(false);
        signpostIndicator.SetActive(false);
        crystalIndicator.SetActive(false);
        huntingIndicator.SetActive(false);
    }

    public void EnableAll()
    {
        plantingTable.SetActive(true);
        craftingTable.SetActive(true);
        inventoryButton.SetActive(true);
        recipeButton.SetActive(true);
        stagingDoor.SetActive(true);
        orderButton.SetActive(true);
        familiarHouse.SetActive(true);
        signpost.SetActive(true);
        crystalBall.SetActive(true);
        //huntingTable.SetActive(true);
        familiarIndicator.SetActive(true);
        stagingIndicator.SetActive(true);
        signpostIndicator.SetActive(true);
        crystalIndicator.SetActive(true);
        //huntingIndicator.SetActive(true);
        foreach(ShopSlot slot in shopSlots)
        {
            slot.transform.gameObject.SetActive(true);
        }
    }

    private void ProgressTutorial()
    {
        if (!tutorialActive) return;
        if(playerComplete)
        {
            PopulateShopArray();
            crystalBall.SetActive(true);
            crystalIndicator.SetActive(true);
            moonleafSeedSlot.transform.gameObject.SetActive(true);
            inventoryButton.SetActive(true);
            if(planterComplete)
            {
                moonleafSeedSlot.transform.gameObject.SetActive(false);
                stagingDoor.SetActive(false);
                stagingIndicator.SetActive(false);
                recipeButton.SetActive(true);
                waterSlot.transform.gameObject.SetActive(true);
                if (crafterComplete)
                {
                    orderButton.SetActive(true);
                    stagingDoor.SetActive(false);
                    stagingIndicator.SetActive(false);
                    if(orderComplete)
                    {
                        EnableAll();
                        tutorialActive = false;
                        chapterCard.SetActive(false);
                    }
                }
            }
        }
    }

    private void TriggerEvent(int status)
    {
        ChapterCard.Instance.SetStatus(status);
    }

    public void PlayerCheck(string check)
    {
        if (!tutorialActive || playerComplete) return;

        switch(check)
        {
            case "moveKeys":
                playerTask1 = true;
                TriggerEvent(0);
                break;
            case "moveMouse":
                playerTask2 = true;
                TriggerEvent(1);
                break;
            case "zoom":
                playerTask3 = true;
                TriggerEvent(2);
                break;
            default:
                Debug.Log("Invalid check");
                break;
        }

        if(playerTask1 && playerTask2 && playerTask3)
        {
            Debug.Log("PLAYER TUTORIAL COMPLETE");
            DialougeManager.Instance.StartDialogue("LWST2");
            playerComplete = true;
            ProgressTutorial();
        }
    }

    public void PlantingCheck(string check)
    {
        if (!tutorialActive || planterComplete) return;
        switch (check)
        {
            case "buySeed":
                planterTask1 = true;
                plantingTable.SetActive(true);
                moonleafSeedSlot.transform.gameObject.SetActive(false);
                TriggerEvent(0);
                break;
            case "plantSeed":
                planterTask2 = true;
                stagingDoor.SetActive(true);
                stagingIndicator.SetActive(true);
                TriggerEvent(1);
                break;
            case "producePlant":
                planterTask3 = true;
                TriggerEvent(2);
                break;
            default:
                Debug.Log("Invalid check");
                break;
        }

        if(planterTask1 && planterTask2 && planterTask3)
        {
            Debug.Log("PLANTER TUTORIAL COMPLETE");
            DialougeManager.Instance.StartDialogue("LWST3");
            planterComplete = true;
            ProgressTutorial();
        }

    }

    public void CraftingCheck(string check)
    {
        if (!tutorialActive || crafterComplete) return;


        switch (check)
        {
            /*
            case "checkRecipe":
                crafterTask1 = true;
                break;
            */
            case "buyWater":
                crafterTask2 = true;
                craftingTable.SetActive(true);
                TriggerEvent(0);
                break;
            case "setCrafting":
                crafterTask3 = true;
                stagingDoor.SetActive(true);
                stagingIndicator.SetActive(true);
                TriggerEvent(1);
                break;
            case "produceCraft":
                crafterTask4 = true;
                TriggerEvent(2);
                break;
            default:
                Debug.Log("Invalid check");
                break;
        }
        //crafterTask1 && 
        if (crafterTask2 && crafterTask3 && crafterTask4)
        {
            Debug.Log("CRAFTER TUTORIAL COMPLETE");
            DialougeManager.Instance.StartDialogue("LWST4");
            crafterComplete = true;
            ProgressTutorial();
        }
    }

    public void OrderCheck(string check)
    {
        if (!tutorialActive || orderComplete) return;
        switch(check)
        {
            case "openOrders":
                orderTask1 = true;
                TriggerEvent(0);
                break;
            case "selectOrders":
                orderTask2 = true;
                TriggerEvent(1);
                break;
            case "Submit Order":
                orderTask3 = true;
                TriggerEvent(2);
                break;
            default:
                Debug.Log("Invalid check");
                break;
        }

        if(orderTask1 && orderTask2 && orderTask3)
        {
            Debug.Log("ORDER TUTORIAL COMPLETE");
            DialougeManager.Instance.StartDialogue("LWSTEnd");
            orderComplete = true;

            //tutorial is over
            EnableAll();
            tutorialActive = false;
            PersistantManager.Instance.IncreaseShopLevel();
            OnTutoFinished?.Invoke(this, EventArgs.Empty);
        }
    }

}
