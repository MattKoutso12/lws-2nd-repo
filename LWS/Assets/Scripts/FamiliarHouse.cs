using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class FamiliarHouse : MonoBehaviour
{
    //This script will control all the individual images in the familiar house
    //and hold all the necessary functions

    //all variables
    [SerializeField] List<string> fNames;
    [SerializeField] GameObject localParent;
    [SerializeField] TextMeshProUGUI upgradeText;

    public static FamiliarHouse instance;
    private CrewCardManager cCM;
    private CrewInventory cInvent;

    public int famCap = 1;
    public int famCount = 0;

    public static int familiarUpgradePrice = 20;

    public Dictionary<string, bool> familiarsToStatus = new Dictionary<string, bool>();
    public Dictionary<string, GameObject> familiarLocals = new Dictionary<string, GameObject>();
    public Dictionary<string, FamiliarWrapper> familiarDict = new Dictionary<string, FamiliarWrapper>();

    [SerializeField] private List<CrewGate> crewGates; //6,5,4,3,2,1,7,0,8,9,10,11


    [SerializeField]
    public GameObject crewcard_prefab;

    [SerializeField]
    public GameObject crewcard_zone;

    private void Awake()
    {
        if (instance == null) instance = this;
        gameObject.SetActive(false);

        cCM = GameObject.Find("CrewCardManager").GetComponent<CrewCardManager>();
    }

    public void OpenFHouse()
    {
        gameObject.SetActive(true);
        Debug.Log("DEG: Opening familiar house");
        OpenAvailableGates();
    }

    public void CloseFHouse()
    {
        Debug.Log("DEG: Closing familiar house");
        foreach (CrewGate gate in crewGates)
        {
            if(!gate.Closed)
            {
                Debug.Log("DEG: Closing gate " + gate.GetGateID());
                gate.CloseGate();
            }
        }
        StartCoroutine(WaitedClose());
    }

    private IEnumerator WaitedClose()
    {
        yield return new WaitForSeconds(1.5f);
        gameObject.SetActive(false);
    }

    private void OpenAvailableGates()
    {
        foreach (CrewGate gate in crewGates)
        {
            if (gate.GetGateID() <= famCap)
            {
                Debug.Log("DEG: Opening gate " + gate.GetGateID());
                gate.OpenGate();
                Debug.Log("DEG: Gate " + gate.GetGateID() + " should be opened");
            }
        }
    }


    // Move lock the player when they open the FamiliarHouse
    private void OnEnable()
    {
        Player.Instance?.SetMoveLock(true);
    }

    private void OnDisable()
    {
        Player.Instance?.SetMoveLock(false);
    }

    #region ViewCloseUp comment
    /// <summary>
    /// Enables popup information for clicked on familiar.
    /// </summary>
    /// <param name="localName"> Name of familiar we want to popup. </param>
    #endregion
    public void ViewCloseUp(string localName)
    {
        /*
        foreach(string n in familiarLocals.Keys)
        {
            if(n == localName)
            {
                GameObject localChild = familiarLocals[n].transform.GetChild(3).gameObject;
                TextMeshProUGUI objectText = localChild.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
                objectText.text = familiarDict[n].ability;
                localChild.SetActive(!localChild.activeInHierarchy);
            }
        }
        */
        
    }

    public void NameChange(GameObject thisObject)
    {
        foreach(string n in familiarLocals.Keys)
        {
            if (thisObject.transform.parent.name.Equals(n))
            {
                cInvent = cCM.crewInvent.inventory;
                CrewSlot oldSlot = cInvent.GetInventorySlotWithItem(familiarDict[n]);
                cInvent.RemoveCrew(familiarDict[n]);
                familiarDict[n].SetDisplayName(thisObject.transform.GetComponent<InputField>().text);
                cInvent.AddItem(familiarDict[n], oldSlot);
            }

        }
    }

    private List<FamiliarWrapper> crew = new List<FamiliarWrapper>();
    private void OnGUI()
    {
        foreach(FamiliarWrapper wrap in FamiliarWrapper.allfamiliars)
        {
            if(!crew.Contains(wrap))
            {
                if (wrap == null) continue;
                if (wrap.crew.name.Equals("Rose")) continue;
                crew.Add(wrap);
                FH_Card c = Instantiate(crewcard_prefab, crewcard_zone.transform).GetComponent<FH_Card>();
                c.Init(wrap);
            }
        }
    }

    public void UpgradeFamiliarCapacity()
    {
        if(PersistantManager.Instance.TryPurchase(familiarUpgradePrice) && famCap < 12)
        {
            famCap++;
            familiarUpgradePrice += 20;
            upgradeText.text = "Next Level: " + (familiarUpgradePrice / 20) + "\nCost: " + familiarUpgradePrice;
            OpenAvailableGates();
        }
    }
}