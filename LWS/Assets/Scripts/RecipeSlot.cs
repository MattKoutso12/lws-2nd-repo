using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;

public class RecipeSlot : MonoBehaviour,  IPointerEnterHandler, IPointerExitHandler, IToolTipable
{

    [SerializeField]
    public Image display;

    Item displayedItem;

    bool checkSeenWaterbreathing = false;

    public string ColouredName
    {
        get
        {
            return displayedItem.ColouredName;
        }
    }
    string colName;
    public void Init(CraftedItem item)
    {
        SetItem(item);
    }


    public void SetItem(Item item)
    {
        displayedItem = item;
        if (item != null)
        {
            display.sprite = item.sprite;
            display.color = Color.white;
        }
        else
        {
            display.sprite = null;
            display.color = Color.clear;
        }
    }

    public void ShowRecipe()
    {
        if(displayedItem != null && displayedItem is CraftedItem)
        {
            CraftingPreview.Instance.SetPreviewedItem((CraftedItem)displayedItem);
            /*
            if(displayedItem.getIDName().Equals("potion_of_water_breathing") && !checkSeenWaterbreathing && TutorialRunner.Instance.tutorialActive)
            {
                checkSeenWaterbreathing = true;
                TutorialRunner.Instance.CraftingCheck("checkRecipe");
            }
            */
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (displayedItem == null) return;
        PersistantManager.Instance.ShowTooltip(this);
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
        PersistantManager.Instance.HideTooltip();
        
    }

    public string GetTooltipInfo()
    {
        //creates a stringbuilder object
        StringBuilder builder = new StringBuilder();

        //adds the item's recipe list, and sell price to the tooltip info
        builder.Append("Retail Price: ").Append(displayedItem.SellPrice).Append(" Gold").AppendLine();
        builder.Append("<i>Usage: " + displayedItem.getUseText()+ "</i>").AppendLine();
        builder.AppendLine().Append("\t\t<sprite=66> to <b>VIEW RECIPE</b>").AppendLine();
        //returns the built up string
        return builder.ToString();
    }
}
