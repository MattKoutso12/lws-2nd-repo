using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullError : MonoBehaviour
{
    public void TurnOff()
    {
        gameObject.SetActive(false);
    }
}
