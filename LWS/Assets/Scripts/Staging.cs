﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staging: IItemHolder, ICrewHolder
{
    public event EventHandler OnGridChanged;
    public event EventHandler OnPlantingChanged;
    public event EventHandler OnCraftingChanged;
    private InventoryItem[] outputArray;
    private FamiliarWrapper[] familiarArray;
    private GameObject invent;
    private UI_Invetory iS;
    private int[] lengthArray;
    private Dictionary<GameObject, InventoryItem> outputs;

    public int pUpgradeMulti = 1;
    public int cUpgradeMulti = 1;

    Dictionary<CraftingSystem, Item> output_today;

    

    public Staging(GameObject invent)
    {
        this.invent = invent;
        iS = invent.GetComponent<UI_Invetory>();
        outputArray = new InventoryItem[6];
        familiarArray = new FamiliarWrapper[6];
        lengthArray = new int[6];
        outputs = new Dictionary<GameObject, InventoryItem>();
        output_today = new Dictionary<CraftingSystem, Item>();
    }

    public bool IsEmpty(int x)
    {
        return outputArray[x] == null;
    }

    public bool IsCrewEmpty(int x)
    {
        return familiarArray[x] == null;
    }

    public InventoryItem GetItem(int x)
    {
        return outputArray[x];
    }

    public FamiliarWrapper GetCrew(int x)
    {
        return familiarArray[x];
    }

    public InventoryItem RetrieveSpecificOutput(GameObject system)
    {
        return outputs[system];
    }

    public void RetrieveOutputs(List<GameObject> systems)
    {
        int i = 0;
        foreach(GameObject sys in systems)
        {
            if(sys.TryGetComponent(out UI_CraftingSystem component))
            {
                if(sys.GetComponent<UI_CraftingSystem>().craftingSystem.GetOutputItem() != null)
                {
                    SetItem(sys.GetComponent<UI_CraftingSystem>().craftingSystem.GetOutputItem(), i);
                    outputs[sys] = sys.GetComponent<UI_CraftingSystem>().craftingSystem.GetOutputItem();
                }
                else
                {
                    SetItem(null, i);
                    outputs[sys] = null;
                }
            }

            if(sys.TryGetComponent(out UI_PlantingSystem componento))
            {
                if(sys.GetComponent<UI_PlantingSystem>().plantingSystem.GetOutputItem() != null)
                {
                    SetItem(sys.GetComponent<UI_PlantingSystem>().plantingSystem.GetOutputItem(), i);
                    outputs[sys] = sys.GetComponent<UI_PlantingSystem>().plantingSystem.GetOutputItem();
                    sys.GetComponent<UI_PlantingSystem>().DestroyItemOutput();
                }
                else
                {
                    SetItem(null, i);
                    outputs[sys] = null;
                }
            }
            i++;
        }
    }


    public void Lobster()
    {

        Debug.Log("LOBSTER CALL");

        List<CraftingSystem> lobsterList = new List<CraftingSystem>(CraftingSystem.ready_to_craft.Keys);
        foreach(CraftingSystem system in lobsterList)
        {
            if(system.manned)
            {
                //system.GetCrew().GetAbility()?.BeforeCrafting(system);
                //if(system.GetCrew().GetAbility()?.isExpired){ Player.Instance.crewInvent.RemoveCrew(system.GetCrew()) }
                InventoryItem possibleOutput;
                if(system.TryProduceOutput(out possibleOutput))
                {
                    system.ConsumeRecipeItems();
                    if(system.GetType() != typeof(PlantingSystem))
                    {
                        for(int i = 0; i < cUpgradeMulti; i++)
                        {
                            Debug.Log("Crafting");
                            iS.inventory.AddItem(possibleOutput);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < pUpgradeMulti; i++)
                        {
                            Debug.Log("Planting");
                            iS.inventory.AddItem(possibleOutput);
                        }
                    }

                    //system.GetCrew().GetAbility()?.OnOutputCreation(system, possibleOutput, iS.inventory);
                    //if(system.GetCrew().GetAbility()?.isExpired){ Player.Instance.crewInvent.RemoveCrew(system.GetCrew()) }
                }
            }
        }
    }



    public void SetItem(InventoryItem item, int x)
    {
        if (item != null)
        {
            item.RemoveFromItemHolder();
            item.SetItemHolder(this);
        }
        outputArray[x] = item;
        if(item != null)
        {
            lengthArray[x] = item.amount;
        }
        OnGridChanged?.Invoke(this, EventArgs.Empty);
    }

    public bool TryAddCrew(FamiliarWrapper crew, int x)
    {
        if (IsCrewEmpty(x))
        {
            SetCrew(crew, x);
            return true;
        }
        else
        {
            if (crew.name == GetCrew(x).name)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public bool TryAddCrew(FamiliarWrapper crew, CraftingSystem system)
    {
        if(system.IsCrewEmpty())
        {
            system.SetCrew(crew);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetCrew(FamiliarWrapper crew, int x)
    {
        if (crew != null)
        {
            crew.RemoveFromItemHolder();
            crew.SetCrewHolder(this);
        }
        familiarArray[x] = crew;
        OnGridChanged?.Invoke(this, EventArgs.Empty);
    }

    public void AddItem(InventoryItem item)
    {
        throw new NotImplementedException();
    }

    public bool CanAddItem()
    {
        return false;
    }

    public void RemoveItem(InventoryItem item)
    {
        for(int x = 0; x < 6; x++)
        {
            if(GetItem(x) == item)
            {
               RemoveItem(x);
            }
        }
    }

    public void RemoveItem(int x)
    {
        SetItem(null, x);
    }

    
    public void RemoveCrew(int x)
    {
        SetCrew(null, x);
    }

    public void RemoveCrew(FamiliarWrapper crew)
    {
        for(int x = 0; x < 6; x++)
        {
            if (GetCrew(x) == crew)
            {
                RemoveCrew(x);
            }
        }
    }

    public void AddCrew(FamiliarWrapper crew)
    {
        throw new NotImplementedException();
    }

    public bool CanAddCrew()
    {
        return true;
    }
}