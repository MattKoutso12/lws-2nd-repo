﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Keeps track of the UI_CrewInventory
/// </summary>
public class CrewCardManager : MonoBehaviour
{
    [SerializeField] public UI_CrewInventory crewInvent;
}