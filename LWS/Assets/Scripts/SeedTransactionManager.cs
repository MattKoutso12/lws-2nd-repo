using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedTransactionManager : MonoBehaviour
{
    [SerializeField] public Item item; //specific itemtype associated with this transaction
    [SerializeField] public Inventory inventory; //UI Inventory for the item to go to

    public delegate void TransactionEvent(InventoryItem item, int cost); // negative cost if we sell
    public static event TransactionEvent OnBuy;

    private void Start()
    {
        inventory = Player.Instance.GetInventory();
    }

    public void Buy()
    {
            PersistantManager.Instance.RemoveGold(item.SellPrice);
            InventoryItem purchased_item = new InventoryItem(item);
            inventory.AddItem(purchased_item);
            OnBuy?.Invoke(purchased_item, item.SellPrice);
    }
}
