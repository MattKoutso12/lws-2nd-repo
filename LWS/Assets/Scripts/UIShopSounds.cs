using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShopSounds : MonoBehaviour
{
    AudioSource audio;

    [SerializeField]
    public bool shouldVaryPitch = false;

    [Tooltip("Random audio clips to choose")]
    [SerializeField]
    public AudioClip[] PlayedOnBuy;

    float original_pitch;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        original_pitch = audio.pitch;
    }

    // Start is called before the first frame update
    void Start()
    {
        SeedTransactionManager.OnBuy += PlayPurchaseSound;
        SellSlot.OnSell += PlayPurchaseSound;
    }

    private void PlayPurchaseSound(InventoryItem item, int cost)
    {
        audio.pitch = original_pitch + Random.Range(-0.1f, 0.1f); // vary the pitch a tiny bit for variety
        audio.PlayOneShot(PlayedOnBuy[Random.Range(0, PlayedOnBuy.Length)]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
