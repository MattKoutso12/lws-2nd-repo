using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShopLevelHandler : MonoBehaviour
{
    public uint levelRequirement;
    public UnityEvent WhenCorrectLevel;

    bool hitReqAlready = false;

    // Start is called before the first frame update
    void Start()
    {
        PersistantManager.Instance.OnShopLevelUp += Instance_OnShopLevelUp;
        
    }


    private void OnDestroy()
    {
        
        PersistantManager.Instance.OnShopLevelUp -= Instance_OnShopLevelUp;
    }


    private void Instance_OnShopLevelUp(object sender, System.EventArgs e)
    {
        if(!hitReqAlready && PersistantManager.Instance.shopLevel >= levelRequirement)
        {
            WhenCorrectLevel?.Invoke();
        }
    }

    public void RunDialouge(string node)
    {
        DialougeManager.Instance.StartDialogue(node);
        hitReqAlready = true;
    }
}
