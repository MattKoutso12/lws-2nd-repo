using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles UI for PlantingSystem.
/// </summary>
public class UI_PlantingSystem : MonoBehaviour
{
    [SerializeField] private Transform pfUI_Item;
    public UIStaging uiStaging;
    private Transform[] slotTransformArray;
    private Transform outputSlotTransform;
    private Transform itemContainer;
    public PlantingSystem plantingSystem;
    [SerializeField] private Transform pfUI_Crew;
    [SerializeField] private Transform pfUI_Output;

    [SerializeField] public int unique_id;

    Transform outputContainer;

    private void Awake()
    {
        Transform gridContainer = transform.Find("Grid Container");
        itemContainer = transform.Find("Item Container");
        outputContainer = transform.Find("Output Container");

        slotTransformArray = new Transform[2];

        for (int i = 0; i < 2; i++)
        {
            slotTransformArray[i] = gridContainer.Find("grid" + i);
            UI_PlantingItemSlot plantingItemSlot = slotTransformArray[i].GetComponent<UI_PlantingItemSlot>();
            plantingItemSlot.SetX(i);
            plantingItemSlot.OnItemDropped += UI_PlantingSystem_OnItemDropped;
        }

        outputSlotTransform = transform.Find("OutputSlot");

    }
    
    private void Start() 
    {
        SetPlantingSystem(new PlantingSystem(unique_id));
        uiStaging.staging.OnPlantingChanged += On_Planting_Changed;
        this.gameObject.SetActive(false);
    }

    public void undoPlanting()
    {
        ButtonManager.Instance.TogglePlanting();
    }

    private void On_Planting_Changed(object sender, EventArgs e)
    {
        //UpdateVisual();
    }

    #region SetPlantingSystem comment
    /// <summary>
    /// Sets the planting system and calls UpdateVisual.
    /// </summary>
    /// <param name="plantingSystem"> Planting system being set. </param>
    #endregion
    public void SetPlantingSystem(PlantingSystem plantingSystem)
    {
        this.plantingSystem = plantingSystem;
        plantingSystem.OnGridChanged += PlantingSystem_OnGridChanged;
        //UpdateVisual();
    }

    private void PlantingSystem_OnGridChanged(object sender, EventArgs e)
    {
        //UpdateVisual();
    }

    private void UpdateVisual()
    {
        DestroyItemOutput();
        foreach (Transform child in itemContainer)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < PlantingSystem.DEFAULT_GRID_SIZE; i++)
        {
            if (!plantingSystem.IsEmpty(i))
            {
                CreateItem(i, plantingSystem.GetItem(i));
            }
        }
        if (plantingSystem.GetOutputItem() != null)
        {
            CreateItemOutput(plantingSystem.GetOutputItem());
        }
    }

    private void UI_PlantingSystem_OnItemDropped(object sender, UI_PlantingItemSlot.OnItemDroppedEventArgs e)
    {
        plantingSystem.TryAddItem(e.item, e.x);
    }

    private void CreateItem(int x, InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Item, itemContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = slotTransformArray[x].GetComponent<RectTransform>().anchoredPosition;
        itemTransform.GetComponent<DragDrop>().SetItem(item);
    }

    private void CreateItemOutput(InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Output, outputContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = outputSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        //itemTransform.localScale = Vector3.one * 1.5f;

        
        itemTransform.GetComponent<OutputSetter>().SetItem(item);
    }

    /// <summary>
    /// Removes any current item output by destroying the UI_Output containing it.
    /// </summary>
    public void DestroyItemOutput()
    {
        Transform outputContainer = transform.Find("Output Container");
        int outputChildCount = outputContainer.childCount;
        for(int i = 0; i < outputChildCount; i++)
        {
            Destroy(outputContainer.transform.GetChild(i).gameObject);
        }
    }
    
}