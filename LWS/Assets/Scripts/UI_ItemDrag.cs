using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.InputSystem;

public class UI_ItemDrag : MonoBehaviour
{

    public static UI_ItemDrag Instance { get; private set; }

    private Canvas canvas;
    private RectTransform rectTransform;
    private RectTransform parentRectTransform;
    private CanvasGroup canvasGroup;
    private Image image;
    private InventoryItem item;
    private TextMeshProUGUI amountText;

    private void Awake()
    {
        Instance = this;

        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
        image = transform.Find("Image").GetComponent<Image>();
        amountText = transform.Find("Amount Text").GetComponent<TextMeshProUGUI>();
        parentRectTransform = transform.parent.GetComponent<RectTransform>();

        Hide();
    }

    private void Update()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, Mouse.current.position.ReadValue(), null, out Vector2 localPoint);
        transform.localPosition = localPoint;
    }

    public InventoryItem GetItem()
    {
        return item;
    }

    public void SetItem(InventoryItem item)
    {
        this.item = item;
    }

    public void SetSprite(Sprite sprite)
    {
        image.sprite = sprite;
    }

    public void SetAmountText(int amount)
    {
        if (amount <= 1)
        {
            amountText.text = "";
        }
        else
        {
            // More than 1
            amountText.text = amount.ToString();
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    #region Show comment
    /// <summary>
    /// Enables the UI_ItemDrag and sets the item being dragged.
    /// </summary>
    /// <param name="item"> Item being dragged. </param>
    #endregion
    public void Show(InventoryItem item)
    {
        gameObject.SetActive(true);

        SetItem(item);
        SetSprite(item.GetSprite());
        SetAmountText(item.amount);
        UpdatePosition();
        transform.parent.SetAsLastSibling();
    }

}
