using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipAttachment : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IToolTipable
{

    [SerializeField]
    public string Name = "Lorem Ipsum";

    [SerializeField]
    public string Body = "Put a valid description here!";

    [SerializeField]
    public Color NameColor;

    [SerializeField]
    [Tooltip("The amount of time the cursor needs to stay over this thing to trigger the tooltip (in seconds)")]
    public float delay = 0;
    public string ColouredName => $"<{getHexFrom(NameColor)}>{Name}</{getHexFrom(NameColor)}>";



    public string GetTooltipInfo()
    {
        return Body;
    }



    float timeInside = 0;
    bool insideZone = false;
    public void OnPointerEnter(PointerEventData eventData)
    {
        timeInside = 0;
        insideZone = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        insideZone = false;
        PersistantManager.Instance.HideTooltip();
    }

    private void OnGUI()
    {
        if(insideZone)
        {
            timeInside += Time.deltaTime;
            if (timeInside >= delay)
            {
                PersistantManager.Instance.ShowTooltip(this);
            }
        }

        
    }

    /// <summary>
    /// For some reason we can't just get a hex tag from a color via a built in method? Idk why so we implement ourselves
    /// </summary>
    /// <param name="color"></param>
    /// <returns>A hexadecimal representation of this color. Needed for TextMeshPro tags.</returns>
    string getHexFrom(Color color)
    {
        return "color=blue"; // not implemented yet
    }

    
}
