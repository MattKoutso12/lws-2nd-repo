using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// This gets attached to the panel which holds the TipPanel prefabs. Each one of those prefabs links to a section of the
/// tutorial, and this script manages the spawning and holding of said prefabs.
/// </summary>
public class TipManager : MonoBehaviour
{
    public static TipManager Instance;

    [SerializeField]
    public GameObject tipPanelPrefab;

    [Tooltip("Link context names to Yarn tutorial paths. Context names will be displayed on the signs for tips.")]
    [SerializeField]
    public List<TutorialContextPairing> tutorialContextPairings;

    HashSet<string> seenContexts;

    // Set up the tip manager singleton
    private void Awake()
    {
        if (Instance == null) Instance = this;
        seenContexts = new HashSet<string>();
    }


    /// <summary>
    /// Checks for repeated tips- if the tip hasn't been seen, then spawn and initialize it.
    /// </summary>
    /// <param name="contextName">The name of the tip's context. Will be displayed on the sign GUI</param>
    /// <param name="tutorialDialogue">The Yarn path of the tutorial to trigger</param>
    public void SpawnTipWithContext(string contextName)
    {
        TutorialContextPairing pair;
        if(IsValidContext(contextName, out pair) && !HasContextBeenSeen(contextName))
        {
            GameObject newlySpawned = Instantiate(tipPanelPrefab, this.transform);
            TipPanel tip = newlySpawned.GetComponent<TipPanel>();
            AddSeenContext(contextName);
            tip.contextName = contextName;
            tip.tutorialDialogue = pair.YarnScene;
        }
    }


    /// <summary>
    /// Check if a certain context has been seen, so you don't trigger spawning the tip again
    /// </summary>
    /// <param name="context">The context to look for</param>
    /// <returns>True if the context has already been seen, false if not</returns>
    public bool HasContextBeenSeen(string context)
    {
        if (seenContexts.Contains(context)) return true;
        else return false;
    }


    /// <summary>
    /// Tell the manager we've gone through a certain tutorial
    /// </summary>
    /// <param name="context">The context of the tutorial, e.g. "Crafting" or "Planting"</param>
    public void AddSeenContext(string context)
    {
        seenContexts.Add(context);
    }

    /// <summary>
    /// Checks to see if a context has been defined with a tutorial. TODO: Make this check for a valid Yarn scene, too.
    /// </summary>
    /// <param name="context">The context to check</param>
    /// <returns>True if the context is defined, false if not.</returns>
    bool IsValidContext(string context, out TutorialContextPairing pairFound)
    {
        
        foreach(TutorialContextPairing pair in tutorialContextPairings)
        {
            if(pair.ContextName.Equals(context) && !pair.YarnScene.Trim().Equals(""))
            {
                pairFound = pair;
                return true;
            }
        }

        pairFound = new TutorialContextPairing();
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Keyboard.current.iKey.wasPressedThisFrame)
        {
            SpawnTipWithContext("Crafting");
        }
    }

    [System.Serializable]
    public struct TutorialContextPairing
    {
        public string ContextName;
        public string YarnScene;
    }


}
