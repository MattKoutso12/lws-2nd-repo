using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// The script attached to a TipPanel. Clicking on this will start the relevant tutorial.
/// </summary>
public class TipPanel : MonoBehaviour
{

    public string contextName = "None";
    public string tutorialDialogue = "";

    TextMeshProUGUI title;

    private void Start()
    {
        title = GetComponentInChildren<TextMeshProUGUI>();
        title.text = "Tutorial: " + contextName;
    }




    public void TriggerTutorial()
    {
        // make sure we have a valid tutorial linked
        if (!tutorialDialogue.Equals(""))
        { 
            DialougeManager.Instance.StartDialogue(tutorialDialogue);
            RemoveTip(); // start the dialogue and remove the tip.
        }

    }


    /// <summary>
    /// Called when we hit the X button or after a tutorial has happened
    /// </summary>
    public void RemoveTip()
    {
        // put any extra relevant code for safely removing a tip here
        TipManager.Instance.AddSeenContext(contextName); // add ourself to the already-seen contexts
        Destroy(this.gameObject); // DIE
    }
}
