using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class handling the display and recieving of new orders.
/// </summary>
public class ROUI : MonoBehaviour
{
    private bool[] selectedOrder = new bool[3]; 
    [SerializeField] OrderCard[] availOrders;

    public static ROUI instance;

    public string name = "NONE";

    private void Awake() 
    {
        if (instance != null)
        {
            Debug.LogWarning("Warning: Multiple ROUI Instances Found! This could lead to errors");
        }
        instance = this;


        selectedOrder = new bool[availOrders.Length];
        foreach(OrderCard card in availOrders)
        {
            card.gameObject.SetActive(true);
        }

        
    }

    #region SetStatus comment
    /// <summary>
    /// Sets whether a designated order is selected.
    /// </summary>
    /// <param name="isSelected"> Whether it is selected. </param>
    /// <param name="index"> Order index. </param>
    #endregion
    public static void SetStatus(bool isSelected, int index)
    {
        instance.selectedOrder[index] = isSelected;
    }

    /// <summary>
    /// Takes all selected orders.
    /// </summary>
    public static void RecieveOrders()
    {
        for (var i = 0; i < instance.availOrders.Length; i++)
        {
            if(instance.selectedOrder[i])
            {
                instance.availOrders[i].TakeOrder();
            }
        }
        instance.gameObject.SetActive(false);
    }


}