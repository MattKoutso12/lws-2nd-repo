using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class UICursorManager : MonoBehaviour
{
    public static UICursorManager instance;
    RectTransform canvasRect, myrect;

    Image drag_display = null;

    public static bool grabbing = false;


    private void Awake()
    {
        canvasRect = (RectTransform)transform.parent;
        myrect = transform.GetChild(0).GetComponent<RectTransform>(); // get the transform of the child that holds the dragged image
        drag_display = GetComponentInChildren<Image>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
            instance = this;

        ResetDragSprite();
        StartCoroutine(ScaleOnGrab());

    }

    /// <summary>
    /// A coroutine which will continually make sure we're scaling toward the correct size when grabbing/not grabbing. Will fluidly work even if the user spams.
    /// </summary>
    /// <returns></returns>
    IEnumerator ScaleOnGrab()
    {
        float scaleTarget = 1f;
        Vector3 original_scale = myrect.localScale;
        float currentScalar = 1f;
        float speed = 3f;
        while (true)
        {
            if (grabbing)
            {
                scaleTarget = 1.35f;
            }
            else
            {
                scaleTarget = 1f;
            }
            currentScalar = Mathf.Lerp(currentScalar, scaleTarget, Time.unscaledDeltaTime * speed);
            transform.localScale = original_scale * currentScalar;
            yield return null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect, Mouse.current.position.ReadValue(), null, out Vector2 localPoint);
        transform.localPosition = localPoint;

        transform.SetAsLastSibling();

        
    }

    public static void SetDragSprite(Sprite sprite)
    {
        instance.drag_display.sprite = sprite;
        instance.drag_display.color = Color.white;
    }

    public static void ResetDragSprite()
    {
        instance.drag_display.color = Color.clear;
    }
   
   public bool GetGrabbing()
   {
    return grabbing;
   }

   
}
