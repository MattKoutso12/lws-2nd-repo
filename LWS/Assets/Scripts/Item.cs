using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using TMPro;

/// <summary>
/// Wrapper class for an inventory item.
/// </summary>
[System.Serializable]
public class Item : ScriptableObject, IToolTipable
{
    //item_name must be unique whereas displayed_name is just what's shown.
    //item_name is used as a key in the item dictionary.
    [SerializeField] protected string item_name, displayed_name;
    [SerializeField] protected string use_text;
    [SerializeField] protected int sellPrice;
    [SerializeReference] public Sprite sprite;
    [SerializeReference] public Color textColor = Color.white;
    [SerializeField] public bool stackable = true;
    [SerializeField] public int level = 1;


    public int icon_index = -1;

    static System.Random randomizer = new System.Random();



    // EXPERIMENTAL MARKET STUFF
    List<int> price_history;

    [SerializeField] public int price_min = 1;
    [SerializeField] public int price_max = 100;


    public int allTimeLow = -1;
    public int allTimeHigh = 50;

    [SerializeField] public int shop_level = 0; // Shop level this item's recipe is unlocked at.
    [SerializeField] public bool isSeed = false;
    [SerializeField] public int outputYield = 1;

    public static Dictionary<string, Item> _items = new Dictionary<string, Item>(); // dictionaries are ~O(1) for retrieval and adding, so we keep this shit QUICK AS HELL even if we access it a lot

    #region Item comment
    /// <summary>
    /// Constructor method for Item.
    /// </summary>
    /// <param name="item_name"> Name of item. </param>
    /// <param name="sprite"> Sprite of item.</param>
    /// <param name="stackable"> Whether the item is stackable. </param>
    /// <param name="sellPrice"> Selling price of item. </param>
    /// <param name="displayed_name"></param>
    /// <param name="use_text"></param>
    #endregion
    public Item(string item_name, Sprite sprite, bool stackable=true, int sellPrice=5, string displayed_name = "", string use_text = "", int level = 1)
    {
        this.item_name = item_name;
        this.sellPrice = sellPrice;
        this.sprite = sprite;
        this.stackable = stackable;
        if(displayed_name.Equals(""))
        {
            this.displayed_name = item_name;
        }
        else
        {
            this.displayed_name = displayed_name;
        }
        this.use_text = use_text;
        this.level = level;
        
    }

    public string GetRawName()
    {
        return item_name;
    }

    /// <summary>
    /// Sets item in item list to this item.
    /// </summary>
    public void Init()
    {
        _items[item_name] = this;
        this.price_history = new List<int>();
        if (price_min == 0) price_min = 1;
        if (price_max == 0) price_max = 100;
        allTimeLow = sellPrice;
        allTimeHigh = sellPrice;
        price_history.Add(sellPrice);
        GenerateWeightedPrice();
        PersistantManager.OnDayChanged += UpdatePriceOnNewDay;
    }

    public virtual void GenerateWeightedPrice()
    {
        double rando = WeightedRandom(Mathf.Ceil(GetCurrentMarketPrice() * 0.1f), GetCurrentMarketPrice()*0.01);
        if (Random.Range(0f, 1f) >= 0.5f) rando *= -1;
        //rando *= (Random.Range(0, 1) >= 0.5 ? 1 : -1);

        int pending_price = GetCurrentMarketPrice() + (int)rando;
        //Debug.Log(GetCurrentMarketPrice() + rando + " generated for " + item_name);
        Debug.Log(item_name + ":" + pending_price);
        if (pending_price < price_min) pending_price = price_min;
        if (pending_price > price_max) pending_price = price_max;
        price_history.Add(pending_price);
        if(allTimeHigh == -1 || pending_price > allTimeHigh)
        {
            allTimeHigh = pending_price;
        }

        if(pending_price < allTimeLow)
        {
            allTimeLow = pending_price;
        }

    }

    /// <summary>
    /// Generates a gaussian random. Uses doubles to avoid LOP
    /// </summary>
    public double WeightedRandom(double mean, double stdDev)
    {
        //System.Random rand = new System.Random((int) (item_name.ToIntArray()[0])*item_name.ToIntArray()[1]);
        double u1 = 1.0 - randomizer.NextDouble();
        double u2 = 1.0 - randomizer.NextDouble();
        u1 = 1.0d - Random.Range(0f, 1f);
        u2 = 1.0d - Random.Range(0f, 1f);
        
        double randStdNormal = System.Math.Sqrt(-2.0 * System.Math.Log(u1)) *
                     System.Math.Sin(2.0 * System.Math.PI * u2);
        double randNormal =
                     mean + stdDev * randStdNormal;
        return randNormal;
    }

    /// <summary>
    /// For now, prices fluxuate randomly by a little.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void UpdatePriceOnNewDay(object sender, System.EventArgs e)
    {
        GenerateWeightedPrice();
        
    }

    #region TryGetItem comment
    /// <summary>
    /// Attempts to return a given item.
    /// </summary>
    /// <param name="item_name"> Name of item we want to get. </param>
    /// <param name="item"> Item we want to return. </param>
    /// <returns> Whether the item was successfully retrieved. </returns>
    #endregion
    public static bool TryGetItem(string item_name, out Item item)
    {
        if(_items.Count == 0)
        {
            PersistantManager.InitializeItemData(); // make sure we init! Wait for this synchronously
        }
        if (_items.ContainsKey(item_name))
        {
            item = _items[item_name];
            return true;
        }
        item = null;
        return false;
        
    }

    public List<int> GetPriceHistory()
    {
        return price_history;
    }

    public virtual int GetCurrentMarketPrice()
    {
        if (price_history == null || price_history.Count == 0)
            return sellPrice;
        return price_history[price_history.Count - 1];
    }

    #region GetItem comment
    /// <summary>
    /// Returns an item of the given name.
    /// </summary>
    /// <param name="item_name"> Name of item we want to get. </param>
    /// <returns> Item with the given name. </returns>
    #endregion
    public static Item GetItem(string item_name)
    {
        if(_items.ContainsKey(item_name))
        {
            return _items[item_name];
        }

        return null;
    }

    public int SellPrice { get { return sellPrice; } }

    public string getDisplayName() { return displayed_name; }

    public string getIDName() { return item_name; }

    public string getUseText() { return use_text; }

    #region ColouredName comment
    /// <summary>
    /// Returns hexadecimal string representing RGB color
    /// </summary>
    /// <value> Hexadecimal string representing color. </value>
    #endregion
    public string ColouredName
    {
        get
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(textColor);
            return $"<color=#{hexColour}>{displayed_name}</color>";
        }
    }

    #region  GetTooltipInfo comment
    /// <summary>
    /// Returns tooltip info for this item.
    /// </summary>
    /// <returns> String representation of tooltip info. </returns>
    #endregion
    public virtual string GetTooltipInfo()
    {
        //creates a stringbuilder object
        StringBuilder builder = new StringBuilder();

        //adds the item's recipe list, and sell price to the tooltip info
        builder.Append("Retail Price: ").Append(SellPrice).Append(" Gold").AppendLine();
        builder.Append("<i>Usage: " + use_text + "</i>").AppendLine();
        builder.AppendLine().Append("\t\t<sprite=50> to <b>SPLIT STACK</b>").AppendLine();
        //returns the built up string
        return builder.ToString();
    }


    public override int GetHashCode()
    {
        return base.GetHashCode();
    }


    // Because this class stores Item DATA, there should never be two instances of an Item with the same name.
    // Thus, all comparisons simply compare the names of two items.
    public override bool Equals(object other)
    {
        if (other == null || other.GetType() != this.GetType()) return false;

        return item_name.Equals(((Item)other).getIDName());
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this); // give the .json version, so it's standardized
    }
}
