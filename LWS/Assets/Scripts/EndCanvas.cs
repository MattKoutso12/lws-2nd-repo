using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using Coffee.UIParticleExtensions;

public class EndCanvas : MonoBehaviour
{

    [SerializeField]
    public TextMeshProUGUI score_text, order_score, cash_score, familiar_score, upgrade_score;

    [SerializeField]
    //public ParticleSystem particleSystem;



    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
        PersistantManager.OnDayChanged += Instance_OnDayChanged;
    }

    private void OnDestroy()
    {
        PersistantManager.OnDayChanged -= Instance_OnDayChanged;
    }

    private void Instance_OnDayChanged(object sender, System.EventArgs e)
    {
        //return; // NOTE: DEMO BLOCKER HERE
        if (PersistantManager.Instance.dayCount == 8)
            DemoOver();
    }

    private void OnEnable()
    {
        //particleSystem.Play();
    }

    private void OnDisable()
    {
        //particleSystem.Pause();
    }

    public void DemoOver()
    {
        this.gameObject.SetActive(true);
        float demoCalc = CouncilReputation.Instance.EndCalculation();
        float[] scores = CouncilReputation.Instance.GetScoreCalc();
        score_text.SetText($"Your Total Score: \t{Mathf.CeilToInt(demoCalc)}");
        order_score.SetText("" + Mathf.CeilToInt(scores[0]));
        cash_score.SetText("" + Mathf.CeilToInt(scores[1]));
        familiar_score.SetText("" + Mathf.CeilToInt(scores[2]));
        upgrade_score.SetText("" + Mathf.CeilToInt(scores[3]));
    }

    public void RestartDemo()
    {
        Application.Quit(1);
    }


    // Update is called once per frame
    void Update()
    {
        if (Keyboard.current.f5Key.wasPressedThisFrame)
            RestartDemo();
    }
}
