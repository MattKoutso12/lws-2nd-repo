﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Handles UI for the crew inventory.
/// </summary>
public class UI_CrewInventory : MonoBehaviour
{

    [SerializeField] private Transform pfUI_Crew;
    public CrewInventory inventory;
    private Transform crewSlotContainer;
    private Transform crewSlotTemplate;
    private Player player;
    [SerializeField] private GameObject staging;

    GridLayoutGroup grid;


    private void Awake()
    {
        //crewSlotContainer = transform.Find("CrewSlotContainer");

        //crewSlotTemplate.gameObject.SetActive(false);
        //staging.SetActive(false);
        transform.root.gameObject.SetActive(false);
    }

    private void Start()
    {
        grid = GetComponentInChildren<GridLayoutGroup>();
        
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    #region SetInventory comment
    /// <summary>
    /// Sets the inventory and refreshes all the crew members.
    /// </summary>
    /// <param name="inventory"></param>
    #endregion
    public void SetInventory(CrewInventory inventory)
    {
        this.inventory = inventory;

        inventory.OnItemListChanged += Inventory_OnItemListChanged;  

        RefreshInventoryItems();
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }

    private void RefreshInventoryItems()
    {
        return;

    }
}