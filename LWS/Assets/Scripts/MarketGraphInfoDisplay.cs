using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class MarketGraphInfoDisplay : MonoBehaviour
{
    [SerializeField]
    public MarketGraph connected;


    [SerializeField]
    public TextMeshProUGUI ownershipDisplay, sellDisplay, itemNameDisplay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    Item selected = null;
    private void OnGUI()
    {
        if(connected.selected_item != selected)
        {
            selected = connected.selected_item;
            if(itemNameDisplay != null)
                itemNameDisplay.text = connected.selected_item.getDisplayName();
            if(ownershipDisplay != null && selected != null)
            {
                int count = GetItemCountInInventory(selected);
                ownershipDisplay.text = $"In Inventory:\t {count}x";
                Debug.Log("COUNT OF " + selected.getDisplayName() + ": " + count);
                if(sellDisplay != null)
                {
                    int value = connected.selected_item.GetCurrentMarketPrice();
                    sellDisplay.text = $"Value:\t {value} G ({value * count} G Owned)";
                }
            }
        }
    }

    int GetItemCountInInventory(Item item)
    {
        Inventory inventory = Player.Instance.GetInventory();
        InventoryItem [] items = inventory.GetItemList();

        if (items.Length == 0) return 0;

        int count = 0;
        foreach(InventoryItem i in items)
        {
            if(i.itemType.Equals(item))
            {
                count += i.amount;
            }
        }
        return count;
    }
}
