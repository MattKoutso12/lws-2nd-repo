using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.IO;
using UnityEngine.UI;

/// <summary>
/// Keeps track of persistent information.
/// </summary>
public class PersistantManager : MonoBehaviour, Savable
{
    public static PersistantManager Instance { get; private set; }

    [SerializeField] private TooltipPopup tooltipPopup;
    [SerializeField] private List<Item> items;
    [SerializeField] private List<Task> orders;
    [SerializeField] public List<Crew> allCrews;
    public Dictionary<int, List<Crew>> crews;
    public Dictionary<int, List<Crew>> hiredCrews;
    [SerializeField] public List<Crew> starter_crew;

    public int shopLevel = 0;

    public List<OrderWrapper> orderWrappers;
    public int hireScene;
    public int goldAmount;
    [SerializeReference] private TextMeshProUGUI goldText;
    [SerializeField] public int dayCount;
    [SerializeField] private TextMeshProUGUI dayText;

    [SerializeField] private GameObject letterRaycastBlocker;

    public string gameFilePath ;

    private bool plantingCheck = false;

    public bool goldTouch;

    //all visibility toggles
    [SerializeField] public bool inventoryVisible;
    [SerializeField] public bool crewVisible;
    [SerializeField] public bool craftingVisible;
    [SerializeField] public bool plantingVisible;
    [SerializeField] public bool plantingVisible2;
    [SerializeField] public bool plantingVisible3;
    [SerializeField] public bool recipesVisible;
    [SerializeField] public bool seedScreenVisible;
    [SerializeField] public bool upgradeScreenVisible;
    [SerializeField] public bool transactionScreenVisible;
    [SerializeField] public bool fHouseVisible;
    [SerializeField] public bool tabTailVisi;
    [SerializeField] public bool hideVisi;
    [SerializeField] public bool kIIVisi;
    [SerializeField] public bool blockerVisible;
    [SerializeField] public bool arrowVisi;

    public static bool isNewGame;

    public bool trader;

    private bool isFirstUpdate = true;


    //pause menu toggles
    [SerializeField] public bool helpGuideVisible;
    [SerializeField] public bool controlsVisible;

    public event EventHandler OnGoldChanged;
    public static event EventHandler OnDayChanged;
    public event EventHandler OnPreDayChanged;
    public event EventHandler OnShopLevelUp;
    public static event EventHandler OnPostShopLevelUp;

    private HashSet<CraftedItem> foundRecipes;

    private void Awake()
    {
        Instance = this;

        InitializeItemData();

        foundRecipes = new HashSet<CraftedItem>();


        orderWrappers = new List<OrderWrapper>();

        ChangeDayAnim.OnAnimEnded += _OnAnimEnded;
        OnGoldChanged += _OnGoldChanged;
        OnDayChanged += _OnDayChanged;

        hireScene = 1;


        crews = new Dictionary<int, List<Crew>>();
        hiredCrews = new Dictionary<int, List<Crew>>();

        for (int i = 1; i < 6; i++) //Creates empty lists for each potential familiar level
        {
            crews.Add(i, new List<Crew>());
            hiredCrews.Add(i, new List<Crew>());
        }

        foreach (Crew tempCrew in allCrews) //Assigns all crew members to their respective level list
        {
           // crews[tempCrew.Level].Add(tempCrew);
        }




        
    }

    private void _OnAnimEnded(object sender, EventArgs e)
    {
        if (ProductionSystem.checkOutputMoonleaf && !plantingCheck)
        {
            plantingCheck = true;
            TutorialRunner.Instance.PlantingCheck("producePlant");
        }
        if (ProductionSystem.checkOutputPotion) TutorialRunner.Instance.CraftingCheck("produceCraft");
        //if(!DCanvController.Instance.IsCanvasOn()) letterRaycastBlocker.SetActive(true);
    }

    private void OnDestroy()
    {
        OnGoldChanged -= _OnGoldChanged;
        OnDayChanged -= _OnDayChanged;
        ChangeDayAnim.OnAnimEnded -= _OnAnimEnded;
        SaveManager.OnPreSave -= AddToSavables;
    }

    private void Start()
    {


        SaveManager.OnPreSave += AddToSavables;

        OnGoldChanged?.Invoke(this, EventArgs.Empty);
        UpdateDayText();

        if (isNewGame)
        {
            FROUI.Instance.FetchDailyAvailableOrders(); // Fetch orders for the first day
            FamiliarWrapper.allfamiliars = new List<FamiliarWrapper>();
            shopLevel = 0;
            for (int i = 0; i < starter_crew.Count; i++)
            {
                //CrewSlot.linked_contents[i] = new FamiliarWrapper(starter_crew[i]);
                Player.Instance.GetCrewInventory().AddCrew(new FamiliarWrapper(starter_crew[i]));
            }
        }
    }

    public void Mute(bool active)
    {
        ButtonManager.Instance.Mute(active);
    }

    /*
    private void Start()
    {
        OnGoldChanged += _OnGoldChanged;
        OnDayChanged += _OnDayChanged;

        hireScene = 1;

        OnGoldChanged?.Invoke(this, EventArgs.Empty);

        crews = new Dictionary<int, List<Crew>>();
        hiredCrews = new Dictionary<int, List<Crew>>();

        for(int i = 1; i < 6; i++) //Creates empty lists for each potential familiar level
        {
            crews.Add(i, new List<Crew>());
            hiredCrews.Add(i, new List<Crew>());
        }

        foreach(Crew tempCrew in allCrews) //Assigns all crew members to their respective level list
        {
            crews[tempCrew.Level].Add(tempCrew);
        }

        FROUI.instance.FetchDailyAvailableOrders(); // Fetch orders for the first day
        for(int i = 0; i < starter_crew.Count; i++)
        {
            CrewSlot.linked_contents[i] = new FamiliarWrapper(starter_crew[i]);
        }
    }
    */
    /// <summary>
    /// Loads and initializes all items.
    /// </summary>
    public static void InitializeItemData()
    {
        Debug.Log("Loading Items...");

        Item[] loadedItems = Resources.LoadAll<Item>("Items");
        Debug.Log("Loaded " + loadedItems.Length + " items");
        foreach (Item item in loadedItems)
        {
            if(item.GetType() == typeof(CraftedItem))
            {
                ((CraftedItem)item).Init();
            }
            else item.Init();
        }

       

        foreach (string s in Item._items.Keys)
            Debug.Log("Init: " + s);

    }

    void InitializeItemIcons()
    {
        Debug.Log("Updating Icons...");
        Item[] loadedItems = new Item[Item._items.Values.Count];
        Item._items.Values.CopyTo(loadedItems, 0);
        Vector2[] sizes = new Vector2[loadedItems.Length];
        Texture2D[] textures = new Texture2D[loadedItems.Length];

        int ICON_SIZE = 128;

        for (int i = 0; i < loadedItems.Length; i++)
        {
            sizes[i] = new Vector2(loadedItems[i].sprite.texture.width,
                loadedItems[i].sprite.texture.height);
            //textures[i] = Texture2D.Instantiate(loadedItems[i].sprite.texture);
            Texture2D sourceTex = loadedItems[i].sprite.texture;
         
            RenderTexture renderTexture = RenderTexture.GetTemporary(
                sourceTex.width,
                sourceTex.height,
                0,
                RenderTextureFormat.Default,
                RenderTextureReadWrite.Linear);
            Graphics.Blit(sourceTex, renderTexture);

            // make sure we don't interrupt any RT use down the line (even though we don't use any now).
            RenderTexture inUse = RenderTexture.active;
            RenderTexture.active = renderTexture;

            Texture2D readableClone = new Texture2D(sourceTex.width, sourceTex.height);
            readableClone.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            readableClone.Apply();


            RenderTexture.active = inUse; // reset the RT
            RenderTexture.ReleaseTemporary(renderTexture); // close our render texture

            //readableClone.Resize(128, 128);
            
            textures[i] = readableClone; // add a modifiable version of the icon to our texture array.

            loadedItems[i].icon_index = i;
            
        }


        int size = 128 * loadedItems.Length;
        Texture2D atlas = new Texture2D(size,size);
        
        Rect[] rects = atlas.PackTextures(textures, 1, size);
        for (int i = 0; i < loadedItems.Length; i++)
        {

        }

        atlas.Apply();
        
        
      
        string path = "Assets/Resources/Items/icon_atlas.png";
        File.WriteAllBytes(path, atlas.EncodeToPNG());

        
        TMP_Settings.LoadDefaultSettings();

        //Sprite sprite_atlas = Sprite.Create(atlas, new Rect(0, 0, atlas.width, atlas.height), new Vector2(atlas.width / 2, atlas.height / 2));
        
    }

    void UpdateDayText()
    {
        dayText.text = (int)(dayCount / 10) + "" + (int)(dayCount % 10);
    }

    private void _OnDayChanged(object sender, EventArgs e)
    {
        UpdateDayText();
        foreach (OrderWrapper listed in orderWrappers)
        {
            listed.daysRemaining--;
        }
    }

    private void _OnGoldChanged(object sender, EventArgs e)
    {
        goldText.text = "" + goldAmount;
    }

    /// <summary>
    /// Displays tooltip.
    /// </summary>
    /// <param name="tool"> ToolTippable interface. </param>
    public void ShowTooltip(IToolTipable tool)
    {
        tooltipPopup.DisplayInfo(tool);
    }

    /// <summary>
    /// Hides tooltip.
    /// </summary>
    public void HideTooltip()
    {
        tooltipPopup.HideInfo();
    }

    #region RemoveGold comment
    /// <summary>
    /// Removes designated amount of gold.
    /// </summary>
    /// <param name="x"> Amount of gold to be removed. </param>
    #endregion
    public void RemoveGold(int x)
    {
        goldAmount -= x;
        OnGoldChanged?.Invoke(this, EventArgs.Empty);
        Debug.Log("Used Gold");
        CouncilReputation.Instance.KojiMiscTest(x);
    }

    public bool TryPurchase(int x)
    {
        if (goldAmount - x >= 0)
        {
            RemoveGold(x);
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/LWS Buying");
            return true;
        }
        return false;
    }

    #region AddGold comment
    /// <summary>
    /// Adds designated amount of gold. 
    /// </summary>
    /// <param name="x"> Amound of gold to be added. </param>
    #endregion
    public void AddGold(int x)
    {
        goldAmount += x;
        OnGoldChanged?.Invoke(this, EventArgs.Empty);
        CouncilReputation.Instance.GoldScore(x);
    }

    /// <summary>
    /// Increments the number of days that have passed.
    /// </summary>
    public void IncreaseDay()
    {
        dayCount++;
        OnPreDayChanged?.Invoke(this, EventArgs.Empty);
        OnDayChanged?.Invoke(this, EventArgs.Empty);
        //CheckForRecipes();
        if(dayCount == 7)
        {
            DialougeManager.Instance.StartDialogue("LWSL1");
        }
    }

    private void CheckForRecipes()
    {
        bool hasNewRecipe = false;
        foreach(CraftedItem item in CraftedItem.AllRecipes.Keys)
        {
            if (item.crafting_type == CraftedItem.CraftingType.PLANTING) continue;
            if ((item.shop_level <= shopLevel || item.availableOnDay == dayCount) && !foundRecipes.Contains(item))
            {
                hasNewRecipe = true;
                foundRecipes.Add(item);
            }
        }
        if (isFirstUpdate)
        {
            isFirstUpdate = false;
            return;
        }
        if (hasNewRecipe)
        {
            LetterStack.Instance.letterStack.Add("New Recipes");
        }
    }

    /// <summary>
    /// Saves PersistentManager data
    /// </summary>
    /// <param name="data"></param>
    public void Save(PlayerData data)
    {
        data.goldCount = goldAmount;
        data.dayCount = dayCount;
        data.shopLevel = shopLevel;
        data.familiarCap = FamiliarHouse.instance.famCap;

        // SAVING ITEM SLOTS !!!
        foreach(uint id in ItemSlot.id_to_slot.Keys)
        {
            if(ItemSlot.id_to_slot[id] is ShopSlot)
            {
                continue;
            }
            ItemSlot slot = ItemSlot.id_to_slot[id];
            data.itemSlotData.Add(slot.GetSerializedVersion());
        }

        data.serializableFamiliars = new List<FamiliarWrapper.SerializableFamiliarWrapper>(); // clear it every time
        string allFams = "ALL FAMILIARS: ";
        
        foreach(FamiliarWrapper familiar in FamiliarWrapper.allfamiliars)
        {
            data.serializableFamiliars.Add(familiar.GetSerializedVersion());
            allFams += $"{familiar}";
        }

        allFams += " " + this.GetInstanceID();

        Debug.Log(allFams, this);

        foreach (ExpansionTile tile in ExpansionTile.purchasedTiles)
        {
            data.tiles.Add(tile.GetSerializedVersion());
        }

        foreach (TableBuyPreview table in TableBuyPreview.purchased_slots)
        {
            Debug.Log("Table Saved: " + table.GetSerializableVersion().x+ "," + table.GetSerializableVersion().y, table);
            data.tables.Add(table.GetSerializableVersion());
        }
    }


    public void AddToSavables(SaveManager manager)
    {
        manager.saveables.Add(this);
    }

    public void RemoveFromSavables()
    {
        SaveManager.Instance.saveables.Remove(this);
    }

    public void IncreaseShopLevel()
    {
        shopLevel++;
        OnShopLevelUp?.Invoke(sender: this, EventArgs.Empty);
        switch(shopLevel)
        {
            case 1:
                LetterStack.Instance.letterStack.Add("Forest Edge");
                break;
            case 2:
                break;
            case 3:
                LetterStack.Instance.letterStack.Add("Forest Shrine");
                break;
            case 4:
                LetterStack.Instance.letterStack.Add("Intermediate Recipe Book");
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                LetterStack.Instance.letterStack.Add("Lightning Lake");
                break;
            case 8:
                LetterStack.Instance.letterStack.Add("Advanced Recipe Book");
                break;
            case 9:
                break;
            case 10:
                LetterStack.Instance.letterStack.Add("Swamp");
                break;
            case 11:
                LetterStack.Instance.letterStack.Add("Deep Woods");
                break;
            case 12:
                LetterStack.Instance.letterStack.Add("Mountain Caves");
                break;
            case 13:
                break;
            case 14:
                break;
            case 15:
                break;
            case 16:
                break;
        }
        OnPostShopLevelUp?.Invoke(sender: this, EventArgs.Empty);
    }
    
}
