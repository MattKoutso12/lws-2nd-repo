using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldObjectIndicator : MonoBehaviour
{
    [SerializeField]
    public float max_opacity = 1;

    [SerializeField]
    public AnimationCurve opacityByDistance;

    [SerializeField]
    public Color colorWhenVisible;

    Image image;
    Camera cam;
    ParticleSystem ps;
    ParticleSystem.Particle[] particles;
    public float minDist = 5;


    float dist = -1;
    float alpha = -1;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponentInChildren<Image>();
        ps = GetComponentInChildren<ParticleSystem>();
        cam = Camera.main;
        image.color = Color.clear; // start transparent, so it doesn't flash up on the first frame
        particles = new ParticleSystem.Particle[ps.particleCount];

        for(int i = 0; i < particles.Length; i++)
        {
            particles[i].startColor = Color.clear;
        }

        
    }

    // Update is called once per frame
    void OnGUI()
    {
        if (Player.Instance.IsMoveLocked())
        {
            image.color = Color.clear;

            return;
        }

        
        Event currentEvent = Event.current;
        Vector2 mousePosition = new Vector2(currentEvent.mousePosition.x,
            cam.pixelHeight - currentEvent.mousePosition.y);
        mousePosition = cam.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 0));
        float distance = Mathf.Abs(((Vector2)transform.position - mousePosition).magnitude);
        dist = distance;
        float opacity = opacityByDistance.Evaluate(distance);
        alpha = opacity;
        image.color = colorWhenVisible * new Color(1, 1, 1, opacity*max_opacity);

        // using deprecated code is cool, kids! B)
        ps.startColor = image.color;

    }

    private void Update()
    {

    }
}
