﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sets baseline info in the LWS scene.
/// </summary>
public class Testing : MonoBehaviour
{

    [SerializeField] private Player player;
    [SerializeField] private UI_Invetory uiInventory;
    [SerializeField] private UI_CrewInventory crewInvent;
    [SerializeField] private Crew witch;
    [SerializeField] private UIStaging uiStaging;

    [SerializeField] private UI_CraftingSystem uiCraftingSystem;
    [SerializeField] private UI_PlantingSystem uiPlantingSystem;
    [SerializeField] private UI_PlantingSystem uiPlantingSystem2;
    [SerializeField] private UI_PlantingSystem uiPlantingSystem3;

    private void Start()
    {
        Staging staging = new Staging(uiInventory.gameObject);
        uiStaging.SetStaging(staging);
        uiInventory.SetPlayer(player);
        uiInventory.SetInventory(player.GetInventory());
        crewInvent.SetPlayer(player);
        crewInvent.SetInventory(player.GetCrewInventory());

            
    }
}
