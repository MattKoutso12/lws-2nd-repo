using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingSlot : ItemSlot
{

    [SerializeField]
    public int system_id = -1;

    [SerializeField]
    public int slot_index = 0;
    
    CraftingSystem mySystem;
    
    private void Start()
    {
        mySystem = CraftingSystem.GetCraftingSystem(system_id);
        mySystem.SetSlot(this, slot_index);
    }

    protected override void OnContentsChanged(IDraggableObject old_holding, IDraggableObject new_holding)
    {
        base.OnContentsChanged(old_holding, new_holding);
        mySystem.CreateOutput();
        /*
        if(new_holding != null)
        {
            mySystem.SetItem((InventoryItem)new_holding, slot_index);
            
        }
        else
        {
            mySystem.SetItem(null, slot_index);
        }*/
    }

    public override bool TryTransfer(UISlot other)
    {
        return base.TryTransfer(other);
    }

    protected override void OnObjectAdded(IDraggableObject o)
    {
        base.OnObjectAdded(o);
    }

    protected override bool CanHoldThis(IDraggableObject objectToHold)
    {
        return base.CanHoldThis(objectToHold);
    }
}
