using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOptions : MonoBehaviour
{
    public void ChangeResolution(int width)
    {
        switch(width)
        {
            case 0:
            Screen.SetResolution(1920, 1080, true);
            break;
            case 1:
            Screen.SetResolution(1280, 720, true);
            break;
            case 2:
            Screen.SetResolution(960, 540, true);
            break;
            default:
            break;
        }
    }
}
