using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
    private GameObject childScreen;

    void Start()
    {
        childScreen = this.transform.GetChild(0).gameObject;
        childScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Keyboard.current.escapeKey.wasPressedThisFrame && !DialougeManager.Instance.dRunn.IsDialogueRunning) // don't let them open pause during a dialogue. If they hit main menu during a dialogue, it will screw EVERYTHING up
        {
            Toggle();
        }
    }

    public void Toggle()
    {
        if(childScreen.activeInHierarchy)
        {
            childScreen.transform.GetChild(0).gameObject.SetActive(true);
            childScreen.transform.GetChild(1).gameObject.SetActive(false);
            childScreen.SetActive(false);
            Player.Instance.SetMoveLock(false);
            Time.timeScale = 1;
        }
        else
        {
            childScreen.SetActive(true);
            Player.Instance.SetMoveLock(true);
            Time.timeScale = 0;
        }
    }

    public void ExitGame()
    {
        Time.timeScale = 1;
        Player.Instance.SetMoveLock(false);
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadScene(sceneName: "Main Menu");
    }
}
