using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketItemManager : MonoBehaviour
{
    [SerializeField]
    public GameObject market_item;

    [SerializeField]
    public MarketGraph graph;

    // Start is called before the first frame update
    void OnEnable()
    {
        string[] names = new string[Item._items.Count];
        Item._items.Keys.CopyTo(names, 0);

        List<string> sortednames = new List<string>(names);
        sortednames.Sort();
        sortednames.Reverse();

        foreach(string itemname in sortednames)
        {
            //if (itemname.Equals(market_item.GetComponent<UIMarketItem>().item_id)) continue;
            GameObject newitem = Instantiate(market_item);
            UIMarketItem entry = newitem.GetComponent<UIMarketItem>();
            entry.item_id = itemname;
            entry.ItemInit();
            newitem.transform.SetParent(this.transform);
            entry.graph = graph;
            //newitem.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 400);
        }
        market_item.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
