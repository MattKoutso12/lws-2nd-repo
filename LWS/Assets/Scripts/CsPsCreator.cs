using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// Handles creation of new CraftingSystems and PlantingSystems.
/// </summary>
public class CsPsCreator : MonoBehaviour
{
    public event EventHandler<OnSystemSpawnedEventArgs> OnSystemSpawn;

    public class OnSystemSpawnedEventArgs : EventArgs
    {
        public UI_CraftingSystem x;
        public UI_PlantingSystem y;
    }

    [SerializeField] GameObject system;
    [SerializeField] UIStaging staging;
    [SerializeField] Canvas parentff;
    protected GameObject spawnedSystem;
    protected UI_CraftingSystem systemCs;
    protected UI_PlantingSystem systemPs;

    // Start is called before the first frame update
    void Start()
    {
        spawnedSystem = Instantiate(system, parentff.transform);
        if(spawnedSystem.TryGetComponent<UI_CraftingSystem>(out UI_CraftingSystem compo))
        {
            compo.uiStaging = staging;
            systemCs = compo;
            compo.unique_id = 0;
        }
        if(spawnedSystem.TryGetComponent<UI_PlantingSystem>(out UI_PlantingSystem compon))
        {
            compon.uiStaging = staging;
            systemPs = compon;
            compon.unique_id = 1;
        }
        OnSystemSpawn?.Invoke(this, new OnSystemSpawnedEventArgs { x = systemCs, y = systemPs });
        UI_rect = spawnedSystem.GetComponent<RectTransform>();    
    }

    RectTransform UI_rect;


    // Update is called once per frame
    void Update()
    {

        //UI_rect.position = Camera.main.WorldToScreenPoint(transform.position);
        spawnedSystem.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(10, 10, 0));

    }
}
