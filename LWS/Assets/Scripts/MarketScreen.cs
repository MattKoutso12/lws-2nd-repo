using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketScreen : MonoBehaviour
{
    [SerializeField]
    public bool toggled_on = false;


   public void Toggle()
    {
        toggled_on = !toggled_on;
        this.transform.GetChild(0).gameObject.SetActive(toggled_on);

        if(toggled_on)
        {

            Player.Instance.SetMoveLock(true);
        }
        else
        {
            Player.Instance.SetMoveLock(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
