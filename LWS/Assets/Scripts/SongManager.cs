    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;
using FMODUnity;

public class SongManager : MonoBehaviour
{
    FMOD.Studio.EventInstance gameMusic;
    FMODUnity.StudioEventEmitter chords, main;




    FMOD.Studio.EventInstance fader;

    public static SongManager Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        fader = FMODUnity.RuntimeManager.CreateInstance("snapshot:/UI Volume");
        if (Instance == null) Instance = this;
        

    }



    public void MusicDown()
    {
        fader.start();
    }

    public void MusicUp()
    {
        fader.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void MusicUp(float delay)
    {
        StartCoroutine(MusicUpWithDelay(delay));
    }

    IEnumerator MusicUpWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        MusicUp();
    }
}
