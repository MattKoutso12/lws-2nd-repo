using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotifSpawner : MonoBehaviour
{
    public GameObject notif;
    private void Start()
    {
        Ability.Resolved += Ability_Resolved;
    }

    private void Ability_Resolved(Ability sender)
    {
        Debug.Log("der: " + sender.Name);
        GameObject newNotif = Instantiate(notif, gameObject.transform);
        newNotif.GetComponent<AbilityNotif>().Init(sender.Name);
    }
}
