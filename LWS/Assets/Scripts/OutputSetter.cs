using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

/// <summary>
/// Sets the output of a crafter or planter.
/// </summary>
public class OutputSetter : MonoBehaviour
{
    // these 3 are never used (?)
    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private Image image;
    private InventoryItem item;
    private TextMeshProUGUI amountText;

    [SerializeField] private List<Item> items;

    public void SetItemButton()
    {
        ItemButton itemButton = GetComponent<ItemButton>();
        itemButton.item = item.itemType;
    }
    
    public void SetSprite(Sprite newSprite)
    {
        if (image == null)
            image = transform.GetChild(1).GetComponent<Image>();
        if(image != null)
            image.sprite = newSprite;
    }

    public void SetAmountText(int amount)
    {
        amountText = transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        if (amount <= 1)
        {
            
            amountText.text = "";
        }
        else
        {
            // More than 1
            amountText.text = amount.ToString();
        }
    }

    public void SetItem(InventoryItem item)
    {
        if (item != null)
        {
            rectTransform = GetComponent<RectTransform>();
            canvasGroup = GetComponent<CanvasGroup>();
            canvas = GetComponentInParent<Canvas>();
            image = transform.Find("Image").GetComponent<Image>();
            amountText = transform.Find("Amount Text").GetComponent<TextMeshProUGUI>();
            this.item = item;
            SetSprite(item.itemType.sprite);
            SetAmountText(item.amount);
            SetItemButton();
        }
    }

    }