using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated. Contained sprites for all items.
/// </summary>
public class ItemAssets : MonoBehaviour
{
    /*

    public static ItemAssets Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    //Herbs
    public Sprite s_Miskweed;
    public Sprite s_Wormroot;
    public Sprite s_Moonleaf;
    public Sprite s_Poppy;
    public Sprite s_MoonlightRoot;

    //Drops
    public Sprite s_ImpLeather;
    public Sprite s_Wool;
    public Sprite s_GoliathFinger;
    public Sprite s_SpiderEye;
    public Sprite s_QuartzGeode;

    //Rocks
    public Sprite s_RedAmber;
    public Sprite s_BlackOpal;
    public Sprite s_Malachite;

    //Crafted Items
    public Sprite s_MuscleRelaxant;
    public Sprite s_MoTP;
    public Sprite s_PoNV;
    public Sprite s_PoCD;
    public Sprite s_PoWB;

    //Water
    public Sprite s_Water;

    //Seeds
    public Sprite s_WormrootSeed;
    public Sprite s_MoonleafSeed;
    public Sprite s_MiskweedSeed;
    public Sprite s_PoppySeed;

    //Familiars
    public Sprite s_Fox;
    public Sprite s_Witch;
    public Sprite s_Mouse;
    public Sprite s_Crookshanks;

    */


}