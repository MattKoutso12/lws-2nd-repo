using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Crew", menuName = "Items/Crewmembers")]
#region Crew comment
/// <summary>
/// Contains a single familiar's information.
/// </summary>
#endregion
public class Crew : ScriptableObject
{
    [SerializeField] public string species;
    [SerializeField] private new string name; //the crew members name
    [SerializeField] private int hirePrice; //the crew members hire price
    [SerializeField] private string ability; //the crew members ability
    [SerializeField] private int level; //the crew members level
    [SerializeReference] public Sprite sprite, background, icon; //the crew members sprite
    [SerializeField] public string FMOD_EventName;
    [Range(0, 10)]
    [SerializeField] public int brains, brawn;
    private Color textColor = Color.yellow; //the crew members text color

    public static Dictionary<string, Crew> crewtypes = new Dictionary<string, Crew>();

    public delegate void CrewTrigger(Crew sender);
    public static event CrewTrigger OnCrewViewed;


    //this meant something but I don't think it does anymore
    public enum Reference
    {
        Imp,
        Witch,
        Faerie,
        Dwarf,
        BlueImp,
    }

    // put this familiar type in our dict
    private void Awake()
    {
        if(!crewtypes.ContainsKey(species))
        {
            crewtypes[species] = this;
        }
    }




    //returns the crew members name
    public string Name { get { return name; } }

    //returns the crew members ability
    public string Ability { get { return ability; } set { ability = value; } }

    //returns the crew members hire price
    public int HirePrice { get { return hirePrice; } }

    public Sprite Sprite { get { return sprite; } }

    public Color Color { get { return textColor; } }
    
    /// <summary>
    /// Sends the event which will trigger the familiar's sound
    /// </summary>
    public void TriggerViewEvent()
    {
        OnCrewViewed?.Invoke(this);
    }

    //returns the crew members level
    public int Level {get { return level; } }

    public static Crew GetCrewOfType(string familiartype)
    {
        if(crewtypes.ContainsKey(familiartype))
        {
            return (Crew) crewtypes[familiartype].MemberwiseClone();
        }
        return null;
    }
}
