using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundScroller : MonoBehaviour
{
    [SerializeField]
    public RawImage trees1;

    [SerializeField]
    public RawImage trees2;

    [SerializeField]
    public RawImage trees3;

    [SerializeField]
    public RawImage trees4;

    [SerializeField]
    public RawImage trees5;

    void Update()
    {
        trees1.uvRect = new Rect(trees1.uvRect.position + new Vector2(-.05f, 0f) * Time.deltaTime, trees1.uvRect.size);
        trees2.uvRect = new Rect(trees2.uvRect.position + new Vector2(-.1f, 0f) * Time.deltaTime, trees2.uvRect.size);
        trees3.uvRect = new Rect(trees3.uvRect.position + new Vector2(-.15f, 0f) * Time.deltaTime, trees3.uvRect.size);
        trees4.uvRect = new Rect(trees4.uvRect.position + new Vector2(-.20f, 0f) * Time.deltaTime, trees4.uvRect.size);
        trees5.uvRect = new Rect(trees5.uvRect.position + new Vector2(-.25f, 0f) * Time.deltaTime, trees5.uvRect.size);
    }
}
