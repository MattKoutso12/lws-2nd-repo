﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Shows tooltips for an order. Destroys the order when necessary.
/// </summary>
public class OrderButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public OrderWrapper order;
    [SerializeField] UI_OrderLine orderLine;

    public void OnPointerEnter(PointerEventData eventData)
    {
        PersistantManager.Instance.ShowTooltip(order);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PersistantManager.Instance.HideTooltip();
    }

    private void Update()
    {
        if (order.daysRemaining == 0)
        {
            orderLine.orderLine.RemoveOrder(order);
            Destroy(this.gameObject);
        }
    }

    

}
