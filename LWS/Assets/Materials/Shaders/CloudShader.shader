Shader "Unlit/CloudShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Thresh("Threshold", float) = 0.3
        _WindSpeed("Wind Speed", float) = 1.0
        [Toggle(OPAQUE_MODE)] _OpaqueMode("Opaque Mode", int) = 0
        
        _NoiseOpacity("Noise Opacity", Range(0.0, 1.0)) = 1.0 
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            #pragma shader_feature OPAQUE_MODE
            #include "UnityCG.cginc"


            float hash(float n) { return frac(sin(n) * 1e4); }
            float hash(float2 p) { return frac(1e4 * sin(17.0 * p.x + p.y * 0.1) * (0.1 + abs(sin(p.y * 13.0 + p.x)))); }
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Thresh;
            float _WindSpeed;
            float _NoiseOpacity;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float noise(float x) {
                float i = floor(x);
                float f = frac(x);
                float u = f * f * (3.0 - 2.0 * f);
                return lerp(hash(i), hash(i + 1.0), u);
            }

            fixed4 darken(fixed4 a, fixed4 b)
            {
                fixed4 r = min(a, b);
                r.a = b.a;
                return r;
            }

            fixed4 overlay(fixed4 a, fixed4 b)
            {
                fixed4 r = a < .5 ? 2.0 * a * b : 1.0 - 2.0 * (1.0 - a) * (1.0 - b);
                r.a = b.a;
                return r;
            }

            float noise(float2 x) {
                float2 i = floor(x);
                float2 f = frac(x);

                // Four corners in 2D of a tile
                float a = hash(i);
                float b = hash(i + float2(1.0, 0.0));
                float c = hash(i + float2(0.0, 1.0));
                float d = hash(i + float2(1.0, 1.0));

                // Simple 2D lerp using smoothstep envelope between the values.
                // return float3(lerp(lerp(a, b, smoothstep(0.0, 1.0, f.x)),
                //			lerp(c, d, smoothstep(0.0, 1.0, f.x)),
                //			smoothstep(0.0, 1.0, f.y)));

                // Same code, with the clamps in smoothstep and common subexpressions
                // optimized away.
                float2 u = f * f * (3.0 - 2.0 * f);
                return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

                float2 pos = i.uv;
                pos.x += _Time * _WindSpeed;

                float n = noise(pos*4);
                n *= _NoiseOpacity;
#ifdef OPAQUE_MODE
                
                col *= n;
                col.a = 1.0;
#else
                col *= n;
#endif

                //CUTOUT
                //if (col.w < _Thresh)
                  //  clip(-1);
                
                return col;
            }
            ENDCG
        }
    }
}
